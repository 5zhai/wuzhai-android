package com.mrwujay.cascade.view;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import com.mrwujay.cascade.R;
import com.mrwujay.cascade.model.CityModel;
import com.mrwujay.cascade.model.DistrictModel;
import com.mrwujay.cascade.model.ProvinceModel;
import com.mrwujay.cascade.service.XmlParserHandler;

import kankan.wheel.widget.OnWheelChangedListener;
import kankan.wheel.widget.WheelView;
import kankan.wheel.widget.adapters.ArrayWheelAdapter;
import android.content.Context;
import android.content.res.AssetManager;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class AddressPicker extends LinearLayout implements OnClickListener, OnWheelChangedListener{

	protected String[] mProvinceDatas;
	protected Map<String, String[]> mCitisDatasMap = new HashMap<String, String[]>();
	protected Map<String, String[]> mDistrictDatasMap = new HashMap<String, String[]>();
	protected Map<String, String> mZipcodeDatasMap = new HashMap<String, String>(); 
	protected String mCurrentProviceName;
	protected String mCurrentCityName;
	protected String mCurrentDistrictName ="";
	protected String mCurrentZipCode ="";
	
	private LinearLayout wheelLayout;
	private WheelView wheelProvince;
	private WheelView wheelCitis;
	private WheelView wheelDistrict;
	private Button doneBtn;
	private TextView title;
	private Context context;

	public interface AddressListener{
		public void onAddressSelected(String proviceName,String cityName,String districtName);
	}
    private AddressListener addressListener;

	public AddressPicker(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;
		initView();
		initData();
		setUpListener();
	}

	public AddressPicker(Context context) {
		super(context);
		this.context = context;
		initView();
		initData();
		setUpListener();
	}

	private void initView(){
		this.setOrientation(LinearLayout.VERTICAL);

		wheelLayout = new LinearLayout(context);
		wheelLayout.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, 0,1));
		wheelLayout.setOrientation(LinearLayout.HORIZONTAL);

		LayoutParams wheelParams = new LayoutParams(0, LayoutParams.WRAP_CONTENT, 1);
		wheelProvince = new WheelView(context);
		wheelCitis = new WheelView(context);
		wheelDistrict = new WheelView(context);
		wheelProvince.setLayoutParams(wheelParams);
		wheelCitis.setLayoutParams(wheelParams);
		wheelDistrict.setLayoutParams(wheelParams);

		wheelLayout.addView(wheelProvince);
		wheelLayout.addView(wheelCitis);
		wheelLayout.addView(wheelDistrict);

		LayoutParams btnParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		btnParams.setMargins(8, 8, 8, 8);
		doneBtn = new Button(context);
		doneBtn.setText("确定");
		doneBtn.setTextColor(0xffffffff);
		doneBtn.setTextSize(18);
		doneBtn.setGravity(Gravity.CENTER);
		doneBtn.setLayoutParams(btnParams);
		doneBtn.setBackgroundResource(R.drawable.rounded_button);

		LayoutParams titleParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		titleParams.bottomMargin = 12;
		titleParams.gravity=Gravity.CENTER;
		title = new TextView(context);
		title.setText("请选择省市区");
		title.setTextSize(18);
		title.setGravity(Gravity.CENTER);
		title.setLayoutParams(titleParams);

		this.addView(title);
		this.addView(wheelLayout);
		this.addView(doneBtn);
	}
	private void initData(){
		initProvinceDatas();
		wheelProvince.setViewAdapter(new ArrayWheelAdapter<String>(context, mProvinceDatas));
		wheelProvince.setVisibleItems(5);
		wheelCitis.setVisibleItems(5);
		wheelDistrict.setVisibleItems(5);
		updateCities();
		updateAreas();
	}

    protected void initProvinceDatas()
	{
		List<ProvinceModel> provinceList = null;
    	AssetManager asset = context.getAssets();
        try {
            InputStream input = asset.open("province_data.xml");
			SAXParserFactory spf = SAXParserFactory.newInstance();
			SAXParser parser = spf.newSAXParser();
			XmlParserHandler handler = new XmlParserHandler();
			parser.parse(input, handler);
			input.close();
			provinceList = handler.getDataList();
			if (provinceList!= null && !provinceList.isEmpty()) {
				mCurrentProviceName = provinceList.get(0).getName();
				List<CityModel> cityList = provinceList.get(0).getCityList();
				if (cityList!= null && !cityList.isEmpty()) {
					mCurrentCityName = cityList.get(0).getName();
					List<DistrictModel> districtList = cityList.get(0).getDistrictList();
					mCurrentDistrictName = districtList.get(0).getName();
					mCurrentZipCode = districtList.get(0).getZipcode();
				}
			}

			mProvinceDatas = new String[provinceList.size()];
        	for (int i=0; i< provinceList.size(); i++) {
        		mProvinceDatas[i] = provinceList.get(i).getName();
        		List<CityModel> cityList = provinceList.get(i).getCityList();
        		String[] cityNames = new String[cityList.size()];
        		for (int j=0; j< cityList.size(); j++) {
        			cityNames[j] = cityList.get(j).getName();
        			List<DistrictModel> districtList = cityList.get(j).getDistrictList();
        			String[] distrinctNameArray = new String[districtList.size()];
        			DistrictModel[] distrinctArray = new DistrictModel[districtList.size()];
        			for (int k=0; k<districtList.size(); k++) {
        				DistrictModel districtModel = new DistrictModel(districtList.get(k).getName(), districtList.get(k).getZipcode());
        				mZipcodeDatasMap.put(districtList.get(k).getName(), districtList.get(k).getZipcode());
        				distrinctArray[k] = districtModel;
        				distrinctNameArray[k] = districtModel.getName();
        			}
        			mDistrictDatasMap.put(cityNames[j], distrinctNameArray);
        		}
        		mCitisDatasMap.put(provinceList.get(i).getName(), cityNames);
        	}
        } catch (Throwable e) {  
            e.printStackTrace();  
        } finally {

        } 
	}
	private void setUpListener() {
		wheelProvince.addChangingListener(this);
		wheelCitis.addChangingListener(this);
		wheelDistrict.addChangingListener(this);
    	doneBtn.setOnClickListener(this);
    }
	private void updateAreas() {
		int pCurrent = wheelCitis.getCurrentItem();
		mCurrentCityName = mCitisDatasMap.get(mCurrentProviceName)[pCurrent];
		String[] areas = mDistrictDatasMap.get(mCurrentCityName);

		if (areas == null) {
			areas = new String[] { "" };
		}
		wheelDistrict.setViewAdapter(new ArrayWheelAdapter<String>(context, areas));
		wheelDistrict.setCurrentItem(0);
		mCurrentDistrictName = areas[0];
	}

	private void updateCities() {
		int pCurrent = wheelProvince.getCurrentItem();
		mCurrentProviceName = mProvinceDatas[pCurrent];
		String[] cities = mCitisDatasMap.get(mCurrentProviceName);
		if (cities == null) {
			cities = new String[] { "" };
		}
		wheelCitis.setViewAdapter(new ArrayWheelAdapter<String>(context, cities));
		wheelCitis.setCurrentItem(0);
		updateAreas();
	}

	public void setAddressListener(AddressListener addressListener){
		this.addressListener = addressListener;
	}

	@Override
	public void onChanged(WheelView wheel, int oldValue, int newValue) {
		if (wheel == wheelProvince) {
			updateCities();
		} else if (wheel == wheelCitis) {
			updateAreas();
		} else if (wheel == wheelDistrict) {
			mCurrentDistrictName = mDistrictDatasMap.get(mCurrentCityName)[newValue];
			mCurrentZipCode = mZipcodeDatasMap.get(mCurrentDistrictName);
		}
	}

	@Override
	public void onClick(View v) {
		addressListener.onAddressSelected(mCurrentProviceName,mCurrentCityName,mCurrentDistrictName);
	}
}
