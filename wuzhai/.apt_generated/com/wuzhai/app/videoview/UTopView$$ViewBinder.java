// Generated code from Butter Knife. Do not modify!
package com.wuzhai.app.videoview;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class UTopView$$ViewBinder<T extends com.wuzhai.app.videoview.UTopView> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131690015, "field 'mLeftImgBtn'");
    target.mLeftImgBtn = finder.castView(view, 2131690015, "field 'mLeftImgBtn'");
    view = finder.findRequiredView(source, 2131690016, "field 'mTitleTxtv'");
    target.mTitleTxtv = finder.castView(view, 2131690016, "field 'mTitleTxtv'");
  }

  @Override public void unbind(T target) {
    target.mLeftImgBtn = null;
    target.mTitleTxtv = null;
  }
}
