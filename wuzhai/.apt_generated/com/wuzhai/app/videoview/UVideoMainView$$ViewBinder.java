// Generated code from Butter Knife. Do not modify!
package com.wuzhai.app.videoview;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class UVideoMainView$$ViewBinder<T extends com.wuzhai.app.videoview.UVideoMainView> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131689992, "field 'mLoadingContainer'");
    target.mLoadingContainer = view;
    view = finder.findRequiredView(source, 2131689985, "field 'mBrightnessView'");
    target.mBrightnessView = finder.castView(view, 2131689985, "field 'mBrightnessView'");
    view = finder.findRequiredView(source, 2131690018, "field 'mRotateVideoView'");
    target.mRotateVideoView = finder.castView(view, 2131690018, "field 'mRotateVideoView'");
    view = finder.findRequiredView(source, 2131690020, "field 'mVolumeView'");
    target.mVolumeView = finder.castView(view, 2131690020, "field 'mVolumeView'");
    view = finder.findRequiredView(source, 2131689993, "field 'mLoadingView'");
    target.mLoadingView = view;
    view = finder.findRequiredView(source, 2131690013, "field 'mPlayStatusView'");
    target.mPlayStatusView = view;
    view = finder.findRequiredView(source, 2131689972, "field 'mBottomView'");
    target.mBottomView = finder.castView(view, 2131689972, "field 'mBottomView'");
    view = finder.findRequiredView(source, 2131690001, "field 'mSettingMenuView'");
    target.mSettingMenuView = finder.castView(view, 2131690001, "field 'mSettingMenuView'");
    view = finder.findRequiredView(source, 2131690014, "field 'mTopView'");
    target.mTopView = finder.castView(view, 2131690014, "field 'mTopView'");
  }

  @Override public void unbind(T target) {
    target.mLoadingContainer = null;
    target.mBrightnessView = null;
    target.mRotateVideoView = null;
    target.mVolumeView = null;
    target.mLoadingView = null;
    target.mPlayStatusView = null;
    target.mBottomView = null;
    target.mSettingMenuView = null;
    target.mTopView = null;
  }
}
