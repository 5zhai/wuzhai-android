// Generated code from Butter Knife. Do not modify!
package com.wuzhai.app.videoview;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class UVerticalProgressView$$ViewBinder<T extends com.wuzhai.app.videoview.UVerticalProgressView> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131689986, "field 'mVolumeIcon'");
    target.mVolumeIcon = finder.castView(view, 2131689986, "field 'mVolumeIcon'");
    view = finder.findRequiredView(source, 2131689987, "field 'mVerticalProgressBar'");
    target.mVerticalProgressBar = finder.castView(view, 2131689987, "field 'mVerticalProgressBar'");
  }

  @Override public void unbind(T target) {
    target.mVolumeIcon = null;
    target.mVerticalProgressBar = null;
  }
}
