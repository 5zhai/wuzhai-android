// Generated code from Butter Knife. Do not modify!
package com.wuzhai.app.videoview;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class UBottomView$$ViewBinder<T extends com.wuzhai.app.videoview.UBottomView> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131689984, "field 'mSeekingIndexTxtv'");
    target.mSeekingIndexTxtv = finder.castView(view, 2131689984, "field 'mSeekingIndexTxtv'");
    view = finder.findRequiredView(source, 2131689981, "field 'mSeekBar'");
    target.mSeekBar = finder.castView(view, 2131689981, "field 'mSeekBar'");
    view = finder.findRequiredView(source, 2131689980, "field 'mFastSeekBar'");
    target.mFastSeekBar = finder.castView(view, 2131689980, "field 'mFastSeekBar'");
    view = finder.findRequiredView(source, 2131689975, "field 'mBrightnessImgBtn'");
    target.mBrightnessImgBtn = finder.castView(view, 2131689975, "field 'mBrightnessImgBtn'");
    view = finder.findRequiredView(source, 2131689982, "field 'mSeekIndexView'");
    target.mSeekIndexView = finder.castView(view, 2131689982, "field 'mSeekIndexView'");
    view = finder.findRequiredView(source, 2131689979, "field 'mDurationTxtv'");
    target.mDurationTxtv = finder.castView(view, 2131689979, "field 'mDurationTxtv'");
    view = finder.findRequiredView(source, 2131689977, "field 'mPlayPauseButton'");
    target.mPlayPauseButton = finder.castView(view, 2131689977, "field 'mPlayPauseButton'");
    view = finder.findRequiredView(source, 2131689978, "field 'mCurrentPositionTxtv'");
    target.mCurrentPositionTxtv = finder.castView(view, 2131689978, "field 'mCurrentPositionTxtv'");
  }

  @Override public void unbind(T target) {
    target.mSeekingIndexTxtv = null;
    target.mSeekBar = null;
    target.mFastSeekBar = null;
    target.mBrightnessImgBtn = null;
    target.mSeekIndexView = null;
    target.mDurationTxtv = null;
    target.mPlayPauseButton = null;
    target.mCurrentPositionTxtv = null;
  }
}
