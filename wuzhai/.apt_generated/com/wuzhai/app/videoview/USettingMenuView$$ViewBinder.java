// Generated code from Butter Knife. Do not modify!
package com.wuzhai.app.videoview;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class USettingMenuView$$ViewBinder<T extends com.wuzhai.app.videoview.USettingMenuView> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131690005, "field 'mMainMenuTitleTxtv'");
    target.mMainMenuTitleTxtv = finder.castView(view, 2131690005, "field 'mMainMenuTitleTxtv'");
    view = finder.findRequiredView(source, 2131690004, "field 'mSettingContentItemLv'");
    target.mSettingContentItemLv = finder.castView(view, 2131690004, "field 'mSettingContentItemLv'");
    view = finder.findRequiredView(source, 2131690002, "field 'mMenuContentTitleTxtv'");
    target.mMenuContentTitleTxtv = finder.castView(view, 2131690002, "field 'mMenuContentTitleTxtv'");
    view = finder.findRequiredView(source, 2131689910, "field 'mSettingItemLv'");
    target.mSettingItemLv = finder.castView(view, 2131689910, "field 'mSettingItemLv'");
  }

  @Override public void unbind(T target) {
    target.mMainMenuTitleTxtv = null;
    target.mSettingContentItemLv = null;
    target.mMenuContentTitleTxtv = null;
    target.mSettingItemLv = null;
  }
}
