package com.wuzhai.app.objects;

import com.wuzhai.app.main.market.widget.Goods;

public class Order {

	public static String PEND_BUYER_PAY = "pend_buyer_pay";
	public static String PEND_SALER_SEND = "pend_saler_send";
	public static String PEND_BUYER_RECEIVE = "pend_buyer_receive";
	public static String COMPLETTED = "completed";
	public static String CANCELED = "canceled";

	private int id;
	private int amount;
	private String code;
	private String payment;
	private int score;
	private String comment;
	private String state;
	private String shipment;
	private String receiver_name;
	private String receiver_address;
	private String receiver_tel;
	private long created_at;
	private Saler saler;
	private int commodity_id;
	private String commodity_title;
	private String commodity_image;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getPayment() {
		return payment;
	}
	public void setPayment(String payment) {
		this.payment = payment;
	}
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getShipment() {
		return shipment;
	}
	public void setShipment(String shipment) {
		this.shipment = shipment;
	}
	public String getReceiverName() {
		return receiver_name;
	}
	public void setReceiverName(String receiver_name) {
		this.receiver_name = receiver_name;
	}
	public String getReceiverAddress() {
		return receiver_address;
	}
	public void setReceiverAddress(String receiver_address) {
		this.receiver_address = receiver_address;
	}
	public String getReceiverTel() {
		return receiver_tel;
	}
	public void setReceiverTel(String receiver_tel) {
		this.receiver_tel = receiver_tel;
	}
	public long getCreatedAt() {
		return created_at;
	}
	public void setCreatedAt(long created_at) {
		this.created_at = created_at;
	}
	public Saler getSaler() {
		return saler;
	}
	public void setSaler(Saler saler) {
		this.saler = saler;
	}
	public int getCommodityId() {
		return commodity_id;
	}
	public void setCommodityId(int commodity_id) {
		this.commodity_id = commodity_id;
	}
	public String getCommodityTitle() {
		return commodity_title;
	}
	public void setCommodityTitle(String commodity_title) {
		this.commodity_title = commodity_title;
	}
	public String getCommodityImage() {
		return commodity_image;
	}
	public void setCommodityImage(String commodity_image) {
		this.commodity_image = commodity_image;
	}
}
