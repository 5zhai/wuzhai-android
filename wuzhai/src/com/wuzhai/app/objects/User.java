package com.wuzhai.app.objects;

import java.util.ArrayList;

public class User {

	private int id;
	private String name;
	private String phoneNumber;
	private String avatarUrl;
	private int sexId;
	private String sexString;
	private String signature;
	private String accessKey;
	private String imToken;
	private String birthday;
	private int followersCount;
	private int followedsCount;
	private String balance;
	private int level;
	private int currentExperience;
	private int needExperience;
	private ArrayList<OauthUser> oauthUserList = new ArrayList<User.OauthUser>();
	private LiveRoom liveRoom = new LiveRoom();

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getAvatarUrl() {
		return avatarUrl;
	}
	public void setAvatarUrl(String avatarUrl) {
		this.avatarUrl = avatarUrl;
	}
	public String getImToken() {
		return imToken;
	}
	public void setImToken(String token) {
		this.imToken = token;
	}
	public String getAccessKey() {
		return accessKey;
	}
	public void setAccessKey(String accessKey) {
		this.accessKey = accessKey;
	}
	public String getSignature() {
		return signature;
	}
	public void setSignature(String signature) {
		this.signature = signature;
	}
	public String getBirthday() {
		return birthday;
	}
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	public int getFollowersCount() {
		return followersCount;
	}
	public void setFollowersCount(int followersCount) {
		this.followersCount = followersCount;
	}
	public int getFollowedsCount() {
		return followedsCount;
	}
	public void setFollowedsCount(int followedsCount) {
		this.followedsCount = followedsCount;
	}
	public int getSexId() {
		return sexId;
	}
	public void setSexId(int sexId) {
		this.sexId = sexId;
		switch (sexId) {
		case 0:
			sexString = "你猜";
			break;
		case 1:
			sexString = "男";
			break;
		case 2:
			sexString = "女";
			break;
		case -1:
			sexString = "未设置";
			break;
		}
	}
	public String getSexString() {
		return sexString;
	}
	public void setSexString(String sexString) {
		this.sexString = sexString;
		if(sexString.equals("男")){
			this.sexId = 1;
		}else if (sexString.equals("女")){
			this.sexId = 2;
		}else if(sexString.equals("你猜")){
			this.sexId = 0;
		}else {
			this.sexId = -1;
		}
	}
	public String getBalance() {
		return balance;
	}
	public void setBalance(String balance) {
		this.balance = balance;
	}
	public int getLevel() {
		return level;
	}
	public void setLevel(int level) {
		this.level = level;
	}
	public int getCurrentExperience() {
		return currentExperience;
	}
	public void setCurrentExperience(int currentExperience) {
		this.currentExperience = currentExperience;
	}
	public int getNeedExperience() {
		return needExperience;
	}
	public void setNeedExperience(int needExperience) {
		this.needExperience = needExperience;
	}
	public ArrayList<OauthUser> getOauthUsers() {
		return oauthUserList;
	}
//	public void setOauthUsers(ArrayList<OauthUser> oauthUsers) {
//		this.oauthUsers = oauthUsers;
//	}
	public void addOauthUser(int id,String uid,String platform){
		OauthUser oauthUser = new OauthUser();
		oauthUser.setId(id);
		oauthUser.setPlatform(platform);
		oauthUser.setUid(uid);
		oauthUserList.add(oauthUser);
	}

	public LiveRoom getLiveRoom(){
		return liveRoom;
	}

	public class OauthUser{

		private int id;
		private String uid;
		private String platform;
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public String getUid() {
			return uid;
		}
		public void setUid(String uid) {
			this.uid = uid;
		}
		public String getPlatform() {
			return platform;
		}
		public void setPlatform(String platform) {
			this.platform = platform;
		}
	}

	public class LiveRoom{
		private int id;
		private String title;
		private String desc;
		private String roomKey;
		private String state;
		private int onlineCount;
		private String brief;
		private String streamJson;
		private LiveRoom(){}
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public String getTitle() {
			return title;
		}
		public void setTitle(String title) {
			this.title = title;
		}
		public String getDesc() {
			return desc;
		}
		public void setDesc(String desc) {
			this.desc = desc;
		}
		public String getRoomKey() {
			return roomKey;
		}
		public void setRoomKey(String roomKey) {
			this.roomKey = roomKey;
		}
		public String getState() {
			return state;
		}
		public void setState(String state) {
			this.state = state;
		}
		public int getOnlineCount() {
			return onlineCount;
		}
		public void setOnlineCount(int onlineCount) {
			this.onlineCount = onlineCount;
		}
		public String getBrief() {
			return brief;
		}
		public void setBrief(String brief) {
			this.brief = brief;
		}
		public String getStreamJson() {
			return streamJson;
		}
		public void setStreamJson(String streamJson) {
			this.streamJson = streamJson;
		}
	}
}
