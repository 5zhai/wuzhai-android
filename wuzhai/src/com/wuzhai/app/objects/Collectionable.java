package com.wuzhai.app.objects;

public class Collectionable {

	private String collectionableType;
	private int collectionableId;

	public String getCollectionableType() {
		return collectionableType;
	}
	public void setCollectionableType(String collectionableType) {
		this.collectionableType = collectionableType;
	}
	public int getCollectionableId() {
		return collectionableId;
	}
	public void setCollectionableId(int collectionableId) {
		this.collectionableId = collectionableId;
	}
}
