package com.wuzhai.app.objects;

public class Likable {
	private String likableType;
	private int likableId;
	public String getLikableType() {
		return likableType;
	}
	public void setLikableType(String likableType) {
		this.likableType = likableType;
	}
	public int getLikableId() {
		return likableId;
	}
	public void setLikableId(int likableId) {
		this.likableId = likableId;
	}
}
