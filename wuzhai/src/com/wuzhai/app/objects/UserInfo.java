package com.wuzhai.app.objects;

public class UserInfo {

	private int id;
	private int followers_count;
	private int followeds_count;
	private String avatar;
	private String username;

	public UserInfo(int id,int followers_count,int followeds_count,String avatar,String username){
		this.id = id;
		this.followeds_count = followeds_count;
		this.followers_count = followers_count;
		this.avatar = avatar;
		this.username = username;
	}

	public int getId() {
		return id;
	}

	public int getFollowers_count() {
		return followers_count;
	}

	public int getFolloweds_count() {
		return followeds_count;
	}

	public String getAvatar() {
		return avatar;
	}

	public String getUsername() {
		return username;
	}

}
