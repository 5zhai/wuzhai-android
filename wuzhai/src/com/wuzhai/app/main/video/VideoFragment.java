package com.wuzhai.app.main.video;

import java.util.ArrayList;

import com.squareup.picasso.Picasso;
import com.viewpagerindicator.CirclePageIndicator;
import com.wuzhai.app.R;
import com.wuzhai.app.application.WuZhaiApplication;
import com.wuzhai.app.main.MainActivity;
import com.wuzhai.app.main.WuzhaiService;
import com.wuzhai.app.main.WuzhaiServiceCallbackAdapter;
import com.wuzhai.app.main.market.widget.ComicConNews;
import com.wuzhai.app.main.market.widget.Goods;
import com.wuzhai.app.main.video.activity.DanceDetailActivity;
import com.wuzhai.app.main.video.activity.DanceListActivity;
import com.wuzhai.app.main.video.activity.LiveListActivity;
import com.wuzhai.app.main.video.widget.VideoEntity;
import com.wuzhai.app.objects.Carousel;
import com.wuzhai.app.tools.Utils;
import com.wuzhai.app.widget.AutoScrollViewPager;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class VideoFragment extends Fragment implements OnClickListener{
	private WuzhaiService service;
	private TextView liveMore;
	private TextView danceMore;
	private RelativeLayout danceOne;
	private RelativeLayout danceTow;
	private RelativeLayout danceThree;
	private RelativeLayout danceFour;
	private AutoScrollViewPager viewPager;
	private ArrayList<Carousel> carouselDataList;
	private CirclePageIndicator indicator;

	@Override
	@Nullable
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_video, null);
		return view;
	}
	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		service = ((WuZhaiApplication)getActivity().getApplication()).getService();
		service.setCallBack(callbackAdapter);
		service.getSelectedVideo(1);
		service.getCarousel(WuzhaiService.CAROUSEL_TYPE_VIDEO,3);
		liveMore = (TextView)view.findViewById(R.id.live_more);
		liveMore.setOnClickListener(this);
		danceMore = (TextView)view.findViewById(R.id.dance_more);
		danceMore.setOnClickListener(this);
		danceOne = (RelativeLayout)view.findViewById(R.id.dance_one);
		danceTow = (RelativeLayout)view.findViewById(R.id.dance_tow);
		danceThree = (RelativeLayout)view.findViewById(R.id.dance_three);
		danceFour = (RelativeLayout)view.findViewById(R.id.dance_four);
		//viewpager 初始化
		viewPager = (AutoScrollViewPager)view.findViewById(R.id.viewpager);
		indicator = (CirclePageIndicator)view.findViewById(R.id.indicator);
		viewPager.getLayoutParams().height = (int)(Utils.getScreenSize(getContext())[0]/2.0);
		viewPager.startAutoScroll(1000);
		viewPager.setItemClickListener(carouselItemClickListener);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.live_more:
			startActivity(new Intent(getActivity(), LiveListActivity.class));
			break;

		case R.id.dance_more:
			startActivity(new Intent(getActivity(), DanceListActivity.class));
			break;
		}
	}

	private void initDanceVideo(RelativeLayout videoLayout,final VideoEntity videoEntity){
		ImageView videoPic = (ImageView)videoLayout.findViewById(R.id.dance_pic);
		TextView title = (TextView)videoLayout.findViewById(R.id.dance_anchor_title);
		ImageView avatar = (ImageView)videoLayout.findViewById(R.id.dance_anchor_avatar);
		TextView username = (TextView)videoLayout.findViewById(R.id.dance_anchor_name);
		TextView watcherCount = (TextView)videoLayout.findViewById(R.id.dance_watcher_count);

		Picasso.with(getActivity()).load(videoEntity.getPicturePath()).resize(240, 160).centerInside().into(videoPic);
		Picasso.with(getContext()).load(videoEntity.getPublisherAvatar()).resize(50, 50).centerInside().into(avatar);
		title.setText(videoEntity.getTitle());
		username.setText(videoEntity.getPublisherName());
		watcherCount.setText(videoEntity.getPlayCount()+"");
		videoLayout.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getContext(),DanceDetailActivity.class);
				intent.putExtra("video", videoEntity);
				startActivity(intent);
			}
		});
	}

	WuzhaiServiceCallbackAdapter callbackAdapter = new WuzhaiServiceCallbackAdapter(){

		@Override
		public void onGetVideoListCompleted(ArrayList<VideoEntity> videoList,
				int next_page) {
			if(videoList!=null && videoList.size()!=0){
				switch (videoList.size()) {
				case 3:
					initDanceVideo(danceThree, videoList.get(2));
				case 2:
					initDanceVideo(danceTow, videoList.get(1));
				case 1:
					initDanceVideo(danceOne, videoList.get(0));
					break;
				default:
					initDanceVideo(danceOne, videoList.get(0));
					initDanceVideo(danceTow, videoList.get(1));
					initDanceVideo(danceThree, videoList.get(2));
					initDanceVideo(danceFour, videoList.get(3));
					break;
				}
			}
		}

		@Override
		public void onGetCarouselListDone(ArrayList<Carousel> carouselList) {
			if(carouselList!=null){
				carouselDataList = carouselList;
				ArrayList<View> pages = new ArrayList<View>();
				for(Carousel carousel : carouselList){
					pages.add(createViewPagerPage(getActivity(), carousel.getTitle(), carousel.getImageUrl()));
				}
				viewPager.addPageList(pages);
		        indicator.setViewPager(viewPager);
		        indicator.setRadius(Utils.dpToPx(3));
			}
		}
	};

	private View createViewPagerPage(Context context,String title,String picUrl){
		View view = LayoutInflater.from(context).inflate(R.layout.layout_homecarouse_layout, null);
		ImageView imageView = (ImageView)view.findViewById(R.id.pic);
		TextView textView = (TextView)view.findViewById(R.id.title);
		Picasso.with(context).load(picUrl).resize(800, 400).centerInside().into(imageView);
		textView.setText(title);
		return view;
	}

    private AutoScrollViewPager.CarouselItemClickListener carouselItemClickListener = new AutoScrollViewPager.CarouselItemClickListener() {
        @Override
		public void onItemClick(View v, int position) {
			Log.d("yue.huang", "colick position:" + position);
		}
    };
}
