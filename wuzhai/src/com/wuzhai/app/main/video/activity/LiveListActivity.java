package com.wuzhai.app.main.video.activity;

import java.util.ArrayList;

import android.app.SearchManager.OnCancelListener;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;

import com.wuzhai.app.R;
import com.wuzhai.app.application.WuZhaiApplication;
import com.wuzhai.app.main.WuzhaiService;
import com.wuzhai.app.main.WuzhaiServiceCallbackAdapter;
import com.wuzhai.app.main.video.widget.LiveRoomEntity;
import com.wuzhai.app.main.video.widget.LiveRoomListAdapter;
import com.wuzhai.app.main.video.widget.VideoAdapter;
import com.wuzhai.app.main.video.widget.VideoEntity;
import com.wuzhai.app.main.widget.OnRecyclerViewItemClickListener;
import com.wuzhai.app.widget.TitleToolbarActivity;

public class LiveListActivity extends TitleToolbarActivity {

	private WuzhaiService service;
	private RecyclerView recyclerView;
	private ArrayList<LiveRoomEntity> roomList = new ArrayList<>();
	private int cuurtenPage = 1;
	private LiveRoomListAdapter liveAdapter;
	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_live);
		setTitle("直播");
		setBackText("视频");
		setTextMenuString("申请直播");
		setTextMenuClickListener(clickListener);
		initView();
	}

	private void initView(){
		service = (WuzhaiService)((WuZhaiApplication)getApplication()).getService();
		service.setCallBack(callbackAdapter);
		service.getLiveList(cuurtenPage);
		recyclerView = (RecyclerView)findViewById(R.id.rvFeed_live);

		liveAdapter = new LiveRoomListAdapter(this, roomList);
		liveAdapter.setOnRecyclerViewItemClickListener(new OnRecyclerViewItemClickListener() {
			@Override
			public void onItemClick(View view, int position) {
				Intent intent = new Intent(LiveListActivity.this, NewLiveDetialActivity.class);
				intent.putExtra("anchor_id", roomList.get(position).getUserId());
				intent.putExtra("videoPath", roomList.get(position).getRtmpLiveUrls());
				intent.putExtra("room_id", roomList.get(position).getId());
				intent.putExtra("watch_count", roomList.get(position).getOnlineCount());
				startActivity(intent);
			}
		});
		GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 2);
		recyclerView.setLayoutManager(gridLayoutManager);
		recyclerView.setAdapter(liveAdapter);

		recyclerView.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
//				if(event.getAction() == MotionEvent.ACTION_DOWN){
//					startActivity(new Intent(LiveListActivity.this, NewLiveDetialActivity.class));
//				}
				return false;
			}
		});
	}

	private OnClickListener clickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			startActivity(new Intent(LiveListActivity.this,LiveActivity.class));
		}
	};

	private WuzhaiServiceCallbackAdapter callbackAdapter = new WuzhaiServiceCallbackAdapter(){

		@Override
		public void onGetLiveRoomListDone(ArrayList<LiveRoomEntity> liveRoomList) {
			Log.d("yue.huang", "onGetLiveRoomListDone:"+liveRoomList);
			if(liveRoomList!=null){
				roomList.addAll(liveRoomList);
				liveAdapter.notifyDataSetChanged();
			}
		}
	};
}
