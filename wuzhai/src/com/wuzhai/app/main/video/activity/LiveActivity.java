package com.wuzhai.app.main.video.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import com.wuzhai.app.R;
import com.wuzhai.app.application.WuZhaiApplication;
import com.wuzhai.app.main.WuzhaiService;
import com.wuzhai.app.main.WuzhaiService.CallBack;
import com.wuzhai.app.main.WuzhaiServiceCallbackAdapter;
import com.wuzhai.app.tools.Utils;

public class LiveActivity extends Activity{
	private WuzhaiService service;
	private Button startBtn;
	private ImageView bgImg;
	private ImageView closeBtn;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_liveactivity);
		service = ((WuZhaiApplication) getApplication()).getService();
		service.setCallBack(callbackAdapter);
		bgImg = (ImageView)findViewById(R.id.bg_pic);
		startBtn = (Button)findViewById(R.id.start_live);
		closeBtn = (ImageView)findViewById(R.id.close_pic);

		Bitmap bitmap = Utils.getScaleCompressedBitmap(getResources(),R.drawable.setpassword_background, 200);
		bgImg.setImageBitmap(Utils.blur(LiveActivity.this, bitmap, 15));
		startBtn.setOnClickListener(clickListener);
		closeBtn.setOnClickListener(clickListener);
	}

	private OnClickListener clickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.start_live:
				String streamJson = ((WuZhaiApplication) getApplication()).getUser().getLiveRoom().getStreamJson();
				if(TextUtils.isEmpty(streamJson)){
					//没输入的UI
//					service.createLiveRoom(title, brief, desc, tags);
				}else{
					Intent intent = new Intent(LiveActivity.this,SWCameraStreamingActivity.class);
					intent.putExtra("stream_json_str", streamJson);
					startActivity(intent);
				}
				break;

			case R.id.close_pic:
				onBackPressed();
				break;
			}
		}
	};

	private WuzhaiServiceCallbackAdapter callbackAdapter = new WuzhaiServiceCallbackAdapter(){

		@Override
		public void onCompleted(int result) {
			if(result == WuzhaiService.CREATE_LIVE_ROOM_DONE){
				startBtn.performClick();
			}
		}
	};
}
