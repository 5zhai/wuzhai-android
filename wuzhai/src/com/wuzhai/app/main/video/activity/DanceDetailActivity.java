package com.wuzhai.app.main.video.activity;

import java.util.ArrayList;

import android.R.bool;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.LinearLayout.LayoutParams;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.ucloud.player.widget.v2.UVideoView;
import com.wuzhai.app.R;
import com.wuzhai.app.application.WuZhaiApplication;
import com.wuzhai.app.main.WuzhaiService;
import com.wuzhai.app.main.WuzhaiServiceCallbackAdapter;
import com.wuzhai.app.main.video.widget.VideoEntity;
import com.wuzhai.app.main.video.widget.VideoInfo;
import com.wuzhai.app.main.widget.CommentListAdapter;
import com.wuzhai.app.objects.Comment;
import com.wuzhai.app.objects.User;
import com.wuzhai.app.person.activity.GiftGiveActivity;
import com.wuzhai.app.tools.Utils;
import com.wuzhai.app.videoview.UPlayer;
import com.wuzhai.app.videoview.UVideoMainView;
import com.wuzhai.app.widget.TitleToolbarActivity;

public class DanceDetailActivity extends TitleToolbarActivity {

	private WuzhaiService service;
	private LinearLayout commentsContainer;
	private ImageView giftBT;
	private UPlayer videoView;
	private TextView titleTV;
	private TextView timeTV;
	private TextView commentCountTV;
	private ImageView shareBT;
	private ImageView collectBT;
	private ImageView likeBT;
	private ImageView publisherAvatar;
	private TextView publishernameTV;
	private TextView publishTimeTV;
	private Button followBT;
	private VideoEntity video;
	private boolean isCollected = false;
	private boolean isLiked = false;
	private ArrayList<Comment> commentList;
	private ListView commentLV;
	private EditText commentET;
	private CommentListAdapter adapter;
	private ImageView bigerBtn;
	private ImageView modelSwitchBtn;
	private RelativeLayout videoLayout;
	private boolean isFullScreen = false;
	private ImageButton topLeftBtn;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		service = ((WuZhaiApplication)getApplication()).getService();
		service.setCallBack(callbackAdapter);
		setBackText("视频");
		setContentView(R.layout.activity_dance_detail);
		initView();
	}

	private void initView(){
		video = (VideoEntity)getIntent().getSerializableExtra("video");
		setTitle(video.getTitle());
		service.IsLike(WuzhaiService.LIKE_VIDEO, video.getId());
		service.Iscollected(WuzhaiService.TYPE_VIDEO, video.getId());
		service.getCommentList(WuzhaiService.TYPE_VIDEO, video.getId());
		initVideoView(video);
		titleTV = (TextView)findViewById(R.id.title);
		timeTV = (TextView)findViewById(R.id.time);
		commentCountTV = (TextView)findViewById(R.id.comment_count);
		shareBT = (ImageView)findViewById(R.id.share);
		shareBT.setOnClickListener(clickListener);
		collectBT  = (ImageView)findViewById(R.id.collect);
		collectBT.setOnClickListener(clickListener);
		likeBT = (ImageView)findViewById(R.id.like);
		likeBT.setOnClickListener(clickListener);
		giftBT = (ImageView)findViewById(R.id.gift);
		giftBT.setOnClickListener(clickListener);
		publisherAvatar = (ImageView)findViewById(R.id.publisherAvatar);
		publishernameTV = (TextView)findViewById(R.id.publishername);
		publishTimeTV = (TextView)findViewById(R.id.publishTime);
		followBT = (Button)findViewById(R.id.followbtn);
		followBT.setOnClickListener(clickListener);
		commentLV = (ListView)findViewById(R.id.commentList);
		commentET = (EditText)findViewById(R.id.comments_edit);
		bigerBtn = (ImageView)findViewById(R.id.bigerBtn);
		bigerBtn.setOnClickListener(clickListener);
		videoLayout = (RelativeLayout)findViewById(R.id.video_layout);
		modelSwitchBtn = (ImageView)findViewById(R.id.img_btn_switch);
		modelSwitchBtn.setOnClickListener(clickListener);
		topLeftBtn = (ImageButton)findViewById(R.id.topview_left_button);
		topLeftBtn.setOnClickListener(clickListener);

		titleTV.setText(video.getTitle());
		timeTV.setText(Utils.getDateFromMillisecond(video.getCreatedAt()));
		commentCountTV.setText(video.getCommentsCount()+"");
		Picasso.with(this).load(video.getPublisherAvatar()).resize(80, 80).centerInside().into(publisherAvatar);
		publishernameTV.setText(video.getPublisherName());
		publishTimeTV.setText(Utils.getDifferenceWithCurrentTime(video.getCreatedAt()));
		commentET.setOnKeyListener(new OnKeyListener() {
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if(event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER && !commentET.getText().toString().equals("")){
					service.addComment(WuzhaiService.TYPE_VIDEO, video.getId(),commentET.getText().toString());
					Comment comment = new Comment();
					User user = ((WuZhaiApplication)getApplication()).getUser();
					comment.setAvatar(user.getAvatarUrl());
					comment.setUsername(user.getName());
					comment.setDesc(commentET.getText().toString());
					comment.setCreated_at(System.currentTimeMillis()/1000);
					commentList.add(comment);
					adapter.setCommentList(commentList);
					commentET.getText().clear();
					return true;
				}
				return false;
			}
		});
//		commentsContainer = (LinearLayout)findViewById(R.id.comments_container);
//		for(int n = 0;n<5;n++){
//			View view = LayoutInflater.from(this).inflate(R.layout.dance_detail_comment, null);
//			LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.WRAP_CONTENT);
//			params.bottomMargin = Utils.dpToPx(1);
//			view.setPadding(8, 8, 8, 8);
//			view.setLayoutParams(params);
//			commentsContainer.addView(view);
//		}

	}

	private void initVideoView(VideoEntity video){
		videoView = (UPlayer)findViewById(R.id.video_main_view);
		videoView.setVideoPath(video.getUrl());
//		videoView.setScreenOriention(UPlayer.SCREEN_ORIENTATION_SENSOR);
		videoView.setRatio(UVideoView.VIDEO_RATIO_MATCH_PARENT);
		videoView.setTitle(video.getTitle());
	}

	private OnClickListener clickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.gift:
				startActivity(new Intent(DanceDetailActivity.this, GiftGiveActivity.class));
				break;
			case R.id.share:
				Toast.makeText(DanceDetailActivity.this, "分享行为还没定义 ^_^", Toast.LENGTH_SHORT).show();
				break;
			case R.id.collect:
				if(!isCollected){
					service.collection(WuzhaiService.TYPE_VIDEO, video.getId());
				}else {
					service.cancleCollection(WuzhaiService.TYPE_VIDEO, video.getId());
				}
				break;
			case R.id.like:
				if(!isLiked){
					service.like(WuzhaiService.LIKE_VIDEO, video.getId());
				}else {
					service.cancleLike(WuzhaiService.LIKE_VIDEO, video.getId());
				}
				break;
			case R.id.followbtn:
				break;
			case R.id.bigerBtn:
				break;
			case R.id.img_btn_switch:
				switchScreenModel();
				break;
			case R.id.topview_left_button:
				switchScreenModel();
				break;
			}
		}
	};

	private WuzhaiServiceCallbackAdapter callbackAdapter = new WuzhaiServiceCallbackAdapter(){

		@Override
		public void onCompleted(int result) {
			switch (result) {
			case WuzhaiService.COLLECTE_SUCC:
				Toast.makeText(DanceDetailActivity.this, "收藏成功", Toast.LENGTH_SHORT).show();
				collectBT.setImageResource(R.drawable.video_icon_collect);
				isCollected = true;
				break;
			case WuzhaiService.COLLECTE_FAL:
				Toast.makeText(DanceDetailActivity.this, "收藏失败", Toast.LENGTH_SHORT).show();
				break;
			case WuzhaiService.CANCLE_COLLECTE_SUCC:
				Toast.makeText(DanceDetailActivity.this, "取消收藏", Toast.LENGTH_SHORT).show();
				collectBT.setImageResource(R.drawable.video_icon_not_collect);
				isCollected = false;
				break;
			case WuzhaiService.CANCLE_COLLECTE_FAL:
				Toast.makeText(DanceDetailActivity.this, "取消收藏失败", Toast.LENGTH_SHORT).show();
				break;
			case WuzhaiService.LIKE_SUCC:
				Toast.makeText(DanceDetailActivity.this, "喜欢成功", Toast.LENGTH_SHORT).show();
				likeBT.setImageResource(R.drawable.video_icon_liked);
				isLiked = true;
				break;
			case WuzhaiService.LIKE_FAL:
				Toast.makeText(DanceDetailActivity.this, "喜欢失败", Toast.LENGTH_SHORT).show();
				break;
			case WuzhaiService.CANCLE_LIKABLE_SUCC:
				Toast.makeText(DanceDetailActivity.this, "取消喜欢", Toast.LENGTH_SHORT).show();
				likeBT.setImageResource(R.drawable.video_icon_like);
				isLiked = false;
				break;
			case WuzhaiService.CANCLE_LIKABLE_FAL:
				Toast.makeText(DanceDetailActivity.this, "取消喜欢失败", Toast.LENGTH_SHORT).show();
				break;
			case WuzhaiService.ADD_COMMENT_FAL:
				Toast.makeText(DanceDetailActivity.this, "评论失败", Toast.LENGTH_SHORT).show();
				break;
			case WuzhaiService.ADD_COMMENT_SUCC:
				Toast.makeText(DanceDetailActivity.this, "评论成功", Toast.LENGTH_SHORT).show();
				break;
			}
		}
		@Override
		public void onCheckIsLikeDone(boolean isLike) {
			Log.d("yue.huang", "islike:"+isLike);
			isLiked = isLike;
			if(isLike){
				likeBT.setBackgroundColor(0xffFF6A6A);
			}
		}
		@Override
		public void onCheckIsCollectedDone(boolean is_collected) {
			isCollected = is_collected;
			if(is_collected){
				collectBT.setImageResource(R.drawable.video_icon_collect);
			}
		}
		@Override
		public void onGetCommentListDone(ArrayList<Comment> comments) {
			Log.d("yue.huang", "onGetCommentListDone:"+comments);
			if(comments!=null){
				commentList = comments;
				adapter = new CommentListAdapter(DanceDetailActivity.this);
				adapter.setCommentList(commentList);
				commentLV.setAdapter(adapter);
			}
		}
	};

	@Override
	protected void onStop() {
		super.onStop();
		videoView.stop(true);
	}

	private void switchScreenModel(){
		if(!isFullScreen){
			switchToFullScreen();
			videoLayout.getLayoutParams().height = LayoutParams.MATCH_PARENT;
			setToolbarVisibility(View.GONE);
			isFullScreen = true;
		}else {
			cancleFullScreen();
			videoLayout.getLayoutParams().height = Utils.dpToPx(200);
			setToolbarVisibility(View.VISIBLE);
			isFullScreen = false;
		}
		videoView.toggleScreenStyle();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if(keyCode == KeyEvent.KEYCODE_BACK ){
			if(isFullScreen){
				switchScreenModel();
			}else {
				finish();
			}
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	//动态切换到全屏
	private void switchToFullScreen(){
		 WindowManager.LayoutParams params = getWindow().getAttributes();
		 params.flags |= WindowManager.LayoutParams.FLAG_FULLSCREEN;
		 getWindow().setAttributes(params);
//		 getWindow().addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
	}
	//动态取消全屏
	private void cancleFullScreen(){
		 WindowManager.LayoutParams params = getWindow().getAttributes();
		 params.flags &= (~WindowManager.LayoutParams.FLAG_FULLSCREEN);
		 getWindow().setAttributes(params);
//		 getWindow().clearFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
	}
}
