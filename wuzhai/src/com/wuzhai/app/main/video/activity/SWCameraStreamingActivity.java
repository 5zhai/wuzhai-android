package com.wuzhai.app.main.video.activity;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import com.pili.pldroid.streaming.CameraStreamingManager;
import com.pili.pldroid.streaming.CameraStreamingSetting;
import com.pili.pldroid.streaming.StreamingEnv;
import com.pili.pldroid.streaming.StreamingProfile;
import com.pili.pldroid.streaming.widget.AspectFrameLayout;
import com.wuzhai.app.R;

import android.app.Activity;
import android.hardware.Camera;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

public class SWCameraStreamingActivity extends Activity implements
		CameraStreamingManager.StreamingStateListener {
	private JSONObject mJSONObject;
	private CameraStreamingManager mCameraStreamingManager;
	private StreamingProfile mProfile;
	private ImageView closeBtn;
	private ImageView cameraBtn;
	private ImageView micBtn;
	private ImageView wordsBtn;
	private ImageView chatBtn;
	private ImageView menuBackBtn;
	private ListView wordsList;
	private boolean isMute = false;
	private boolean hasWords = true;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		StreamingEnv.init(getApplicationContext());
		setContentView(R.layout.activity_swcamera_streaming);
		initView();

		AspectFrameLayout afl = (AspectFrameLayout) findViewById(R.id.cameraPreview_afl);

		// Decide FULL screen or real size
		afl.setShowMode(AspectFrameLayout.SHOW_MODE.REAL);
		GLSurfaceView glSurfaceView = (GLSurfaceView) findViewById(R.id.cameraPreview_surfaceView);

		String streamJsonStrFromServer = getIntent().getStringExtra(
				"stream_json_str");
		try {
			mJSONObject = new JSONObject(streamJsonStrFromServer);
			StreamingProfile.Stream stream = new StreamingProfile.Stream(
					mJSONObject);
			mProfile = new StreamingProfile();
			mProfile.setVideoQuality(StreamingProfile.VIDEO_QUALITY_HIGH1)
					.setAudioQuality(StreamingProfile.AUDIO_QUALITY_MEDIUM2)
					.setEncodingSizeLevel(
							StreamingProfile.VIDEO_ENCODING_HEIGHT_480)
					.setEncoderRCMode(
							StreamingProfile.EncoderRCModes.QUALITY_PRIORITY)
					.setStream(stream); // You can invoke this before
										// startStreaming, but not in
										// initialization phase.

			CameraStreamingSetting setting = new CameraStreamingSetting();
			setting.setCameraId(Camera.CameraInfo.CAMERA_FACING_BACK)
					.setContinuousFocusModeEnabled(true)
					.setCameraPrvSizeLevel(
							CameraStreamingSetting.PREVIEW_SIZE_LEVEL.MEDIUM)
					.setCameraPrvSizeRatio(
							CameraStreamingSetting.PREVIEW_SIZE_RATIO.RATIO_16_9);

			mCameraStreamingManager = new CameraStreamingManager(
					this,
					afl,
					glSurfaceView,
					CameraStreamingManager.EncodingType.SW_VIDEO_WITH_SW_AUDIO_CODEC); // soft
																						// codec

			mCameraStreamingManager.prepare(setting, mProfile);
			mCameraStreamingManager.setStreamingStateListener(this);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	private void initView() {
		closeBtn = (ImageView) findViewById(R.id.close_pic);
		cameraBtn = (ImageView) findViewById(R.id.camera_pic);
		micBtn = (ImageView) findViewById(R.id.mic_pic);
		wordsBtn = (ImageView) findViewById(R.id.words_pic);
		chatBtn = (ImageView) findViewById(R.id.chat_pic);
		menuBackBtn = (ImageView) findViewById(R.id.menu_back_pic);
		wordsList = (ListView) findViewById(R.id.words_list);

		ArrayList<String> wordsDataList = new ArrayList<>();
		wordsDataList.add("好喜欢这个主播~~~~请收下我的礼物");
		wordsDataList.add("好喜欢这个主播~~~~请收下我的礼物");
		wordsDataList.add("好喜欢这个主播~~~~请收下我的礼物");
		wordsDataList.add("好喜欢这个主播~~~~请收下我的礼物,好喜欢这个主播~~~~请收下我的礼物");
		wordsDataList.add("好喜欢这个主播~~~~请收下我的礼物,好喜欢这个主播~~~~请收下我的礼物");
		ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
				R.layout.layout_live_wordslist_item, wordsDataList);
		wordsList.setAdapter(adapter);

		closeBtn.setOnClickListener(clickListener);
		cameraBtn.setOnClickListener(clickListener);
		micBtn.setOnClickListener(clickListener);
		wordsBtn.setOnClickListener(clickListener);
		chatBtn.setOnClickListener(clickListener);
		menuBackBtn.setOnClickListener(clickListener);
	}

	@Override
	protected void onResume() {
		super.onResume();
		mCameraStreamingManager.resume();
	}

	@Override
	protected void onPause() {
		super.onPause();
		// You must invoke pause here.
		mCameraStreamingManager.pause();
	}

	@Override
	public void onStateChanged(int state, Object info) {
		switch (state) {
		case CameraStreamingManager.STATE.PREPARING:
			break;
		case CameraStreamingManager.STATE.READY:
			// start streaming when READY
			new Thread(new Runnable() {
				@Override
				public void run() {
					if (mCameraStreamingManager != null) {
						 mCameraStreamingManager.startStreaming();
					}
				}
			}).start();
			break;
		case CameraStreamingManager.STATE.CONNECTING:
			break;
		case CameraStreamingManager.STATE.STREAMING:
			// The av packet had been sent.
			break;
		case CameraStreamingManager.STATE.SHUTDOWN:
			// The streaming had been finished.
			break;
		case CameraStreamingManager.STATE.IOERROR:
			// Network connect error.
			break;
		case CameraStreamingManager.STATE.SENDING_BUFFER_EMPTY:
			break;
		case CameraStreamingManager.STATE.SENDING_BUFFER_FULL:
			break;
		case CameraStreamingManager.STATE.AUDIO_RECORDING_FAIL:
			// Failed to record audio.
			break;
		case CameraStreamingManager.STATE.OPEN_CAMERA_FAIL:
			// Failed to open camera.
			break;
		case CameraStreamingManager.STATE.DISCONNECTED:
			// The socket is broken while streaming
			break;
		}
	}

	@Override
	public boolean onStateHandled(int i, Object o) {
		return false;
	}

	private OnClickListener clickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.close_pic:
				onBackPressed();
				break;

			case R.id.camera_pic:
				Toast.makeText(SWCameraStreamingActivity.this,
						"这个btn不知道干嘛用的，开启camera？有什么意义。。。", Toast.LENGTH_SHORT)
						.show();
				break;

			case R.id.mic_pic:
				if (isMute) {
					mCameraStreamingManager.mute(false);
					micBtn.setImageResource(R.drawable.icon_mic);
				} else {
					mCameraStreamingManager.mute(true);
					micBtn.setImageResource(R.drawable.icon_no_mic);
				}
				isMute = !isMute;
				break;
			case R.id.words_pic:
				if (hasWords) {
					wordsList.setVisibility(View.GONE);
					wordsBtn.setImageResource(R.drawable.icon_no_words);
				} else {
					wordsList.setVisibility(View.VISIBLE);
					wordsBtn.setImageResource(R.drawable.icon_words);
				}
				hasWords = !hasWords;
				break;
			case R.id.chat_pic:
				break;
			}
		}
	};
}
