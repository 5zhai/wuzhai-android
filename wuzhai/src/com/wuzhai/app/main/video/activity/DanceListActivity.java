package com.wuzhai.app.main.video.activity;

import java.util.ArrayList;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;

import com.viewpagerindicator.TabPageIndicator;
import com.wuzhai.app.R;
import com.wuzhai.app.application.WuZhaiApplication;
import com.wuzhai.app.main.WuzhaiService;
import com.wuzhai.app.main.video.widget.DanceListFragment;
import com.wuzhai.app.main.widget.ViewPagerAdapter;
import com.wuzhai.app.widget.TitleToolbarActivity;

public class DanceListActivity extends TitleToolbarActivity {
	private WuzhaiService service;
	private ViewPager viewPager;
	private TabPageIndicator indicator;
	private ArrayList<Fragment> pageList = new ArrayList<Fragment>();
	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTitle("宅舞");
		setBackText("视频");
		setContentView(R.layout.activity_dance);
		service = ((WuZhaiApplication)getApplication()).getService();
		initView();
	}
	
	private void initView(){
		viewPager = (ViewPager)findViewById(R.id.dance_viewpager);
		indicator = (TabPageIndicator)findViewById(R.id.dance_indicator);
		initPageList();
		ViewPagerAdapter vpAdapter= new ViewPagerAdapter(getSupportFragmentManager(), pageList);
		vpAdapter.setPageTitle(new String[]{"最多播放","点赞最多","最新发布"});
		viewPager.setAdapter(vpAdapter);
		indicator.setViewPager(viewPager);
		viewPager.setCurrentItem(0);
	}
	private void initPageList(){
		//因为dance页的三个page布局都一样，只是内容不一样，所以在activity中使用了三个
		//DanceListFragment对象，用flag区分向服务器请求的数据分类
		DanceListFragment playMostFragment = new DanceListFragment(service);
		playMostFragment.setFlag(WuzhaiService.VIDEO_PLAY_MOST);
		DanceListFragment likeMostFragment = new DanceListFragment(service);
		likeMostFragment.setFlag(WuzhaiService.VIDEO_LIKE_MOST);
		DanceListFragment newReleaseFragment = new DanceListFragment(service);
		newReleaseFragment.setFlag(WuzhaiService.VIDEO_LATEST);
		pageList.add(playMostFragment);
		pageList.add(likeMostFragment);
		pageList.add(newReleaseFragment);
	}
}
