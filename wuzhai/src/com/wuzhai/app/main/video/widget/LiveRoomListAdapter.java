package com.wuzhai.app.main.video.widget;

import java.util.ArrayList;

import android.content.Context;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.wuzhai.app.R;
import com.wuzhai.app.main.widget.HyRecyclerViewAdapter;

public class LiveRoomListAdapter extends HyRecyclerViewAdapter implements OnClickListener{

	private Context context;
	private ArrayList<LiveRoomEntity> roomList;

	public LiveRoomListAdapter(Context context,ArrayList<LiveRoomEntity> videoList){
		this.context = context;
		this.roomList = videoList;
	}

	@Override
	public int getItemCount() {
		// TODO Auto-generated method stub
		return roomList.size();
	}

	@Override
	public void onBindViewHolder(ViewHolder arg0, int arg1) {
		LiveRoomEntity room = roomList.get(arg1);
		roomViewHolder viewHolder = (roomViewHolder)arg0;
//		Picasso.with(context).load(room.getSnapshot()).resize(320, 240).centerInside().into(viewHolder.pic);
		Picasso.with(context).load(R.drawable.featured_pic4).resize(320, 240).centerInside().into(viewHolder.pic);
		Picasso.with(context).load(room.getAvatar()).resize(50, 50).into(viewHolder.publisherAvatar);
		viewHolder.title.setText(room.getTitle());
		viewHolder.publisherName.setText(room.getUserName());
		viewHolder.watcherCount.setText(room.getOnlineCount()+"");
		viewHolder.itemView.setTag(arg1);
	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup arg0, int arg1) {
		View view = LayoutInflater.from(context).inflate(R.layout.video_item, arg0,false);
		view.setOnClickListener(this);
		return new roomViewHolder(view);
	}

	private static class roomViewHolder extends ViewHolder{
		ImageView pic;
		TextView title;
		ImageView publisherAvatar;
		TextView publisherName;
		TextView watcherCount;
//		TextView likeCount;
//		TextView messageCount;

		public roomViewHolder(View view) {
			super(view);
			pic = (ImageView) view.findViewById(R.id.video_pic);
			publisherAvatar = (ImageView) view.findViewById(R.id.publisher_avatar);
			title = (TextView) view.findViewById(R.id.video_title);
			publisherName = (TextView) view.findViewById(R.id.publisher_name);
			watcherCount = (TextView) view.findViewById(R.id.watcher_count);
//			likeCount = (TextView) view.findViewById(R.id.like_count);
//			messageCount = (TextView) view.findViewById(R.id.message_count);
			watcherCount.setVisibility(View.VISIBLE);	
		}
	}

	@Override
	public void onClick(View v) {
		if(recyclerViewItemClickListener!=null){
			recyclerViewItemClickListener.onItemClick(v, (Integer)v.getTag());
		}
	}
}
