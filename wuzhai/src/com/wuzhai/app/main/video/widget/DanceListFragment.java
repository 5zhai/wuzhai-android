package com.wuzhai.app.main.video.widget;

import java.util.ArrayList;

import com.wuzhai.app.R;
import com.wuzhai.app.main.WuzhaiService;
import com.wuzhai.app.main.WuzhaiServiceCallbackAdapter;
import com.wuzhai.app.main.video.activity.DanceDetailActivity;
import com.wuzhai.app.main.widget.LoadMoreRecyclerView;
import com.wuzhai.app.main.widget.LoadMoreRecyclerView.LoadMoreCallBack;
import com.wuzhai.app.main.widget.OnRecyclerViewItemClickListener;
import com.wuzhai.app.tools.Utils;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

public class DanceListFragment extends Fragment {
	private WuzhaiService service;
	private LoadMoreRecyclerView recyclerView;
	private ArrayList<VideoEntity> videoList = new ArrayList<VideoEntity>();
	private VideoAdapter videoAdapter;
	private int page = 1;
	private String pageFlag;//因为dance页的三个page布局都一样，只是内容不一样，所以在activity中使用了三个
	//DanceListFragment对象，用此flag区分向服务器请求的数据分类
	private boolean needLoad = true;

	public DanceListFragment(){}
	public DanceListFragment(WuzhaiService service){
		this.service = service;
	}

	@Override
	@Nullable
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//		service = ((WuZhaiApplication)getActivity().getApplication()).getService();
		View view = inflater.inflate(R.layout.fragment_dancelist, null);;
		return view;
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		recyclerView = (LoadMoreRecyclerView) view.findViewById(R.id.rvFeed_dance);
		videoAdapter = new VideoAdapter(getActivity(), videoList,true);
		videoAdapter.setOnRecyclerViewItemClickListener(new OnRecyclerViewItemClickListener() {
			@Override
			public void onItemClick(View view, int position) {
				Intent intent = new Intent(getContext(),DanceDetailActivity.class);
				intent.putExtra("video", videoList.get(position));
				startActivity(intent);
			}
		});
		GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 2);
		recyclerView.setLayoutManager(gridLayoutManager);
		recyclerView.setAdapter(videoAdapter);
		recyclerView.setLoadMoreCallBack(loadMoreCallBack);

//		if(needLoad){
//			service.getVideoList(pageFlag, page);
//		}
	}

	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
		//防止viewpager加载第一个fragment时马上又加载第二个导致callback错乱
		if(isVisibleToUser && needLoad){
			Log.d("yue.huang", "pageflag:"+pageFlag);
			service.setCallBack(callbackAdapter);
			service.getVideoList(pageFlag, page);
		}
		super.setUserVisibleHint(isVisibleToUser);
	}

	public void setFlag(String flag){
		pageFlag = flag;
	}

	private WuzhaiServiceCallbackAdapter callbackAdapter = new WuzhaiServiceCallbackAdapter(){

		@Override
		public void onGetVideoListCompleted(ArrayList<VideoEntity> video_list,int next_page) {
			if(video_list!=null && video_list.size()!=0){
				videoList.clear();
				page = next_page;
				videoList.addAll(video_list);
				//WrapRecyclerView对RecyclerView进行了继承，当有header或footerview时内部对adapter进行了包装替换，
				//所以要不能直接使用adapter的notifyDataSetChanged()
				recyclerView.getAdapter().notifyDataSetChanged();
				needLoad = false;
			}else {
				recyclerView.setFooterNoMoreToLoad();
			}
		}
	};

	private LoadMoreCallBack loadMoreCallBack = new LoadMoreCallBack() {
		@Override
		public void loadMore() {
			if(page!=0){
				if(Utils.isNetworkAvailable(getContext())){
					service.getVideoList(pageFlag, page);
				}else {
					Toast.makeText(getContext(), "无网络连接", Toast.LENGTH_SHORT).show();
				}
			}else {
				recyclerView.setFooterNoMoreToLoad();
			}
		}
	};
}
