package com.wuzhai.app.main.home.widget;

import java.util.ArrayList;

import com.squareup.picasso.Picasso;
import com.wuzhai.app.R;
import com.wuzhai.app.objects.MediaObject;

import android.content.Context;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class PictureVideoAdapter extends BaseAdapter {
private ArrayList<? extends MediaObject> mediaList;
private Context context;
	public PictureVideoAdapter(Context context,ArrayList<MediaObject> mediaList){
		this.context = context;
		this.mediaList = mediaList;
	}
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mediaList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return mediaList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if(convertView == null){
			holder = new ViewHolder();
			convertView = LayoutInflater.from(context).inflate(R.layout.gridview_picture_video, null);
			holder.picture = (ImageView)convertView.findViewById(R.id.picture);
			holder.picTitle = (TextView)convertView.findViewById(R.id.picTitle);
			convertView.setTag(holder);
		}
		holder = (ViewHolder)convertView.getTag();
		Picasso.with(context).load(mediaList.get(position).getPicturePath()).into(holder.picture);
		holder.picTitle.setText(mediaList.get(position).getTitle());
		return convertView;
	}

	private static class ViewHolder{
		ImageView picture;
		TextView picTitle;
	}

	public void setItemList(ArrayList<? extends MediaObject> mediaList){
		this.mediaList = mediaList;
		notifyDataSetChanged();
	}
}
