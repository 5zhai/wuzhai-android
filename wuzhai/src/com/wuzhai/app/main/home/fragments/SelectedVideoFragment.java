package com.wuzhai.app.main.home.fragments;

import java.util.ArrayList;

import com.squareup.picasso.Picasso;
import com.wuzhai.app.R;
import com.wuzhai.app.application.WuZhaiApplication;
import com.wuzhai.app.main.WuzhaiService;
import com.wuzhai.app.main.WuzhaiServiceCallbackAdapter;
import com.wuzhai.app.main.home.widget.HeaderGridView;
import com.wuzhai.app.main.home.widget.News;
import com.wuzhai.app.main.home.widget.PictureVideoAdapter;
import com.wuzhai.app.main.video.activity.DanceDetailActivity;
import com.wuzhai.app.main.video.widget.VideoEntity;
import com.wuzhai.app.main.video.widget.VideoInfo;
import com.wuzhai.app.widget.RefreshableView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.TextView;

public class SelectedVideoFragment extends Fragment {
	private WuzhaiService service;
	private View gridHeaderView;
	private HeaderGridView gridView;
	private ImageView headerPic;
	private TextView headerTitle;
	private PictureVideoAdapter adapter;
	private ArrayList<VideoEntity> videoList;
	private int nextPage = 0;
	private RefreshableView pullRefreshLayout;

	public SelectedVideoFragment(){}
    public SelectedVideoFragment(WuzhaiService service) {
	    this.service = service;
    }

	@Override
	@Nullable
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_home_pictures_video, null);
		gridHeaderView = inflater.inflate(R.layout.header_gridview_video, null);
		gridView = (HeaderGridView)view.findViewById(R.id.gridview);
		gridView.addHeaderView(gridHeaderView);
		gridView.setOnItemClickListener(itemClickListener);
		return view;
	}
	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		init();
		super.onViewCreated(view, savedInstanceState);
	}

	@Override
	public void onResume() {
		super.onResume();
	}

	private void init(){
		service.getSelectedVideo(1);
		headerPic = (ImageView)gridHeaderView.findViewById(R.id.video_header_pic);
		headerTitle = (TextView)gridHeaderView.findViewById(R.id.video_header_title);
        Picasso.with(getContext()).load(R.drawable.home_video1).into(headerPic);
        headerTitle.setText("这么可爱肯定是个男孩");

        videoList = new ArrayList<VideoEntity>();
		adapter = new PictureVideoAdapter(getContext(), null);
	}

	public void updateVideoList(ArrayList<VideoEntity> videoList,int next_page){
		this.videoList.addAll(videoList);
		adapter.setItemList(videoList);
		gridView.setAdapter(adapter);
	}

	private OnItemClickListener itemClickListener = new OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,long id) {
			if(position == 0){return;}
			VideoEntity videoEntity = videoList.get(position-2);
			Intent intent = new Intent(getContext(),DanceDetailActivity.class);
			intent.putExtra("video", videoEntity);
			startActivity(intent);
		}
	};
}
