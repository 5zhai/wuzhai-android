package com.wuzhai.app.main.home.fragments;

import java.util.ArrayList;

import com.squareup.picasso.Picasso;
import com.wuzhai.app.R;
import com.wuzhai.app.main.home.widget.HeaderGridView;
import com.wuzhai.app.main.home.widget.News;
import com.wuzhai.app.main.home.widget.PictureVideoAdapter;
import com.wuzhai.app.main.home.widget.RecommendAdapter;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

public class ExquisitePicturesFragment extends Fragment {
	private HeaderGridView gridView;
	private ImageView headPicVertical;
	private ImageView headPicHorizontalOne;
	private ImageView headPicHorizontalTwo;
	private View gridHeaderView;
	@Override
	@Nullable
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_home_pictures_video, null);
		gridHeaderView = inflater.inflate(R.layout.header_gridview_picture, null);
		gridView = (HeaderGridView)view.findViewById(R.id.gridview);
		gridView.addHeaderView(gridHeaderView);
		return view;
	}
	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		initGridview();
		super.onViewCreated(view, savedInstanceState);
	}
	@Override
	public void onResume() {
		Log.d("yue.huang", "ExquisitePicturesFragment");
		super.onResume();
	}
	private void initGridview(){
		headPicVertical = (ImageView)gridHeaderView.findViewById(R.id.pic_vertical);
		headPicHorizontalOne = (ImageView)gridHeaderView.findViewById(R.id.pic_horizontal_one);
		headPicHorizontalTwo = (ImageView)gridHeaderView.findViewById(R.id.pic_horizontal_two);
        Picasso.with(getContext()).load(R.drawable.featured_pic1).into(headPicVertical);
        Picasso.with(getContext()).load(R.drawable.featured_pic2).into(headPicHorizontalOne);
        Picasso.with(getContext()).load(R.drawable.featured_pic3).into(headPicHorizontalTwo);

		ArrayList<News> newsList = new ArrayList<News>();
		for(int n=0;n<20;n++){
			newsList.add(new News(0,""+R.drawable.featured_pic4, "剑侠情缘3同人绘本", null,null));
		}
//		PictureVideoAdapter adapter = new PictureVideoAdapter(getContext(), newsList);
//		gridView.setAdapter(adapter);
	}
}
