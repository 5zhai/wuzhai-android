package com.wuzhai.app.main.home;

import java.util.ArrayList;

import com.squareup.picasso.Picasso;
import com.wuzhai.app.R;
import com.wuzhai.app.application.WuZhaiApplication;
import com.wuzhai.app.main.MainActivity;
import com.wuzhai.app.main.WuzhaiService;
import com.wuzhai.app.main.WuzhaiServiceCallbackAdapter;
import com.wuzhai.app.main.home.fragments.ExquisitePicturesFragment;
import com.wuzhai.app.main.home.fragments.SelectedVideoFragment;
import com.wuzhai.app.main.home.fragments.WonderfulRecommendationFragment;
import com.wuzhai.app.main.home.widget.News;
import com.wuzhai.app.main.market.widget.ComicConNews;
import com.wuzhai.app.main.video.widget.VideoEntity;
import com.wuzhai.app.main.widget.ViewPagerFragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.TextView;

public class HomeFragment extends ViewPagerFragment implements OnPageChangeListener{

	private WuzhaiService service;
	private WonderfulRecommendationFragment wonderfulRecommendationFragment;
	private ExquisitePicturesFragment exquisitePicturesFragment;
	private SelectedVideoFragment selectedVideoFragment;
	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		updateToolBar(getString(R.string.wonderful_recommendation));//解决从其它tab切回首页第一个page时title不变的问题
		setOnPageChangeListener(this);
	}

	@Override
	protected void initFragments(ArrayList<Fragment> pageList) {
		service = ((WuZhaiApplication)getActivity().getApplication()).getService();
		service.setCallBack(callbackAdapter);
		wonderfulRecommendationFragment = new WonderfulRecommendationFragment(service);
		exquisitePicturesFragment = new ExquisitePicturesFragment();
		selectedVideoFragment = new SelectedVideoFragment(service);
		pageList.add(wonderfulRecommendationFragment);
		pageList.add(exquisitePicturesFragment);
		pageList.add(selectedVideoFragment);
	}

	@Override
	public void onPageScrollStateChanged(int arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onPageScrolled(int arg0, float arg1, int arg2) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onPageSelected(int arg0) {
		switch (arg0) {
		case 0:
			updateToolBar(getString(R.string.wonderful_recommendation));
			break;
		case 1:
			updateToolBar(getString(R.string.exquisite_pictures));
			break;
		case 2:
			updateToolBar(getString(R.string.selected_video));
			break;
		}
	}

	private void updateToolBar(String title){
		((MainActivity)getActivity()).updateTextToolBar(title);
	}

	private WuzhaiServiceCallbackAdapter callbackAdapter = new WuzhaiServiceCallbackAdapter(){
		@Override
		public void onGetLatestComicConNewsCompleted(
				ArrayList<ComicConNews> comicConNewsList) {
			wonderfulRecommendationFragment.updateLatestComicCon(comicConNewsList);
			
		}

		@Override
		public void onGetRecommendationsCompleted(
				ArrayList<News> recommendationsList,int next_page) {
			wonderfulRecommendationFragment.updateRecommendations(recommendationsList, next_page);
		}

		@Override
		public void onGetVideoListCompleted(ArrayList<VideoEntity> videoList,int next_page) {
			selectedVideoFragment.updateVideoList(videoList, next_page);
		}
	};

}
