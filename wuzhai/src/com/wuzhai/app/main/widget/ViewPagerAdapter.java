package com.wuzhai.app.main.widget;

import java.util.ArrayList;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;
import android.view.ViewGroup;

public class ViewPagerAdapter extends FragmentPagerAdapter {

	private ArrayList<Fragment> pageList;
	private String[] pageTitles;

	public ViewPagerAdapter(FragmentManager fm,ArrayList<Fragment> pageList) {
		super(fm);
		this.pageList = pageList;
		// TODO Auto-generated constructor stub
	}

	@Override
	public Fragment getItem(int arg0) {
		// TODO Auto-generated method stub
		return pageList.get(arg0);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return pageList.size();
	}

	@Override
	public CharSequence getPageTitle(int position) {
		if(pageTitles==null){return null;}
		return pageTitles[position];
	}

	public void setPageTitle(String[] titles){
		pageTitles = titles;
	}
}
