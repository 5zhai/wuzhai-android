package com.wuzhai.app.main.widget;

import com.wuzhai.app.R;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

public class LoadMoreRecyclerView extends WrapRecyclerView {

	public interface LoadMoreCallBack{
		public void loadMore();
	}

	private LoadMoreCallBack callBack;
	private ProgressBar footerProgressBar;
	private TextView footerTextView;
	private View footerView;

	public LoadMoreRecyclerView(Context context, AttributeSet attrs) {
		super(context, attrs);
		setOnScrollListener(scrollListener);
		initFooterView(context);
	}

	public LoadMoreRecyclerView(Context context) {
		super(context);
		setOnScrollListener(scrollListener);
		initFooterView(context);
	}

	private void initFooterView(Context context){
		footerView = LayoutInflater.from(context).inflate(R.layout.listview_footer_more_layout, null);
		footerProgressBar = (ProgressBar)footerView.findViewById(R.id.loadmore_progress);
		footerTextView = (TextView)footerView.findViewById(R.id.loadmore_text);
		footerView.setVisibility(GONE);
		addFooterView(footerView);
	}

	public void setFooterViewLoaderMore(){
		footerView.setVisibility(VISIBLE);
		footerProgressBar.setVisibility(View.VISIBLE);
		footerTextView.setText("加载中");
	}

	public void setFooterNoMoreToLoad(){
		footerView.setVisibility(VISIBLE);
		footerProgressBar.setVisibility(View.GONE);
		footerTextView.setText("无更多数据");
	}

	public void removeFooterView(){
		footerView.setVisibility(View.GONE);
	}

	public void setLoadMoreCallBack(LoadMoreCallBack callBack){
		this.callBack = callBack;
	}

	private OnScrollListener scrollListener = new OnScrollListener() {

		@Override
		public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
			super.onScrollStateChanged(recyclerView, newState);
			if(recyclerView.getAdapter() == null){return;}//无数据时adapter为空，防止数据为空时滑动造成空指针
            int endIndex = ((LinearLayoutManager)recyclerView.getLayoutManager()).findLastVisibleItemPosition();
            int itemsLastIndex = recyclerView.getAdapter().getItemCount() - 1;
            if (newState == RecyclerView.SCROLL_STATE_IDLE && itemsLastIndex == endIndex) {
				if (callBack != null) {
					callBack.loadMore();
				}
            }
		}
	};
}
