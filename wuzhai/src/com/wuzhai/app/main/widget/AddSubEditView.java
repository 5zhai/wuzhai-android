package com.wuzhai.app.main.widget;

import com.wuzhai.app.R;
import com.wuzhai.app.tools.Utils;

import android.content.Context;
import android.content.res.ColorStateList;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.view.View.OnClickListener;

public class AddSubEditView extends LinearLayout implements OnClickListener{

	private Button addBtn;
	private EditText editText;
	private Button subBtn;
	private int count = 1;
	public AddSubEditView(Context context, AttributeSet attrs) {
		super(context, attrs);
		setupView(context);
	}

	public AddSubEditView(Context context) {
		super(context);
		setupView(context);
	}

	private void setupView(Context context){
		this.setOrientation(LinearLayout.HORIZONTAL);
		setBackgroundResource(R.drawable.addsubview_bg);
		setPadding(2, 2, 2, 2);
		setFocusable(true);
		setFocusableInTouchMode(true);
		initSubView(context);
		addSubView();
	}

	private void initSubView(Context context){
		LayoutParams params = new LayoutParams(0, LayoutParams.MATCH_PARENT);
		params.weight = 1;
		ColorStateList csl = (ColorStateList)getResources().getColorStateList(R.drawable.addsubview_btn_textcolor);
		addBtn = new Button(context);
		addBtn.setText("+");
		addBtn.setTextColor(csl);
		addBtn.setBackgroundColor(0x00ffffff);
		addBtn.setLayoutParams(params);
		addBtn.setPadding(1, 1, 1, 1);
		addBtn.setGravity(Gravity.CENTER);
		addBtn.setTag(1);
		addBtn.setOnClickListener(this);
		editText = new EditText(context);
		editText.setHint("1");
		editText.setHintTextColor(0xffafb5c1);
		editText.setTextColor(0xff626f85);
		editText.setPadding(1, 1, 1, 1);
		editText.setGravity(Gravity.CENTER);
		editText.setBackgroundResource(R.drawable.addsubview_edit_bg);
		editText.setLayoutParams(params);
		editText.setCursorVisible(false);
		editText.setEnabled(false);
		subBtn = new Button(context);
		subBtn.setText("—");
		subBtn.setTextColor(csl);
		subBtn.setBackgroundColor(0x00ffffff);
		subBtn.setLayoutParams(params);
		subBtn.setPadding(1, 1, 1, 1);
		subBtn.setGravity(Gravity.CENTER);
		subBtn.setTag(-1);
		subBtn.setOnClickListener(this);
	}
	private void addSubView(){
		this.addView(subBtn);
		this.addView(editText);
		this.addView(addBtn);
	}

	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b) {
		addBtn.setTextSize(getHeight()/5);
		subBtn.setTextSize(getHeight()/6);
		super.onLayout(changed, l, t, r, b);
	}

	@Override
	public void onClick(View v) {
		switch ((Integer)v.getTag()){
		case 1:
			editText.setText(++count+"");
			break;
		case -1:
			if(count!=1)
			editText.setText(--count+"");
			break;
		}
	}

	public int getCount(){
		return count;
	}

	public void setOnCountChanged(TextWatcher textWatcher){
		editText.addTextChangedListener(textWatcher);
	}
}
