package com.wuzhai.app.main.widget;

import com.wuzhai.app.R;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class LoadMoreListView extends ListView{

	public interface LoadMoreCallBack{
		public void loadMore();
	}

	private LoadMoreCallBack callBack;
	private ProgressBar footerProgressBar;
	private TextView footerTextView;

	public LoadMoreListView(Context context, AttributeSet attrs) {
		super(context, attrs);
		setOnScrollListener(scrollListener);
		initFooterView(context);
	}

	public LoadMoreListView(Context context) {
		super(context);
		setOnScrollListener(scrollListener);
		initFooterView(context);
	}

	private void initFooterView(Context context){
		View footerView = LayoutInflater.from(context).inflate(R.layout.listview_footer_more_layout, null);
		footerProgressBar = (ProgressBar)footerView.findViewById(R.id.loadmore_progress);
		footerTextView = (TextView)footerView.findViewById(R.id.loadmore_text);
		addFooterView(footerView);
	}

	public void setLoadMoreCallBack(LoadMoreCallBack callBack){
		this.callBack = callBack;
	}

	public void setFooterNoMoreToLoad(){
		footerProgressBar.setVisibility(View.GONE);
		footerTextView.setText("无更多数据");
	}

	public void setFooterMoreToLoad(){
		footerProgressBar.setVisibility(View.VISIBLE);
		footerTextView.setText("加载中");
	}

	private OnScrollListener scrollListener = new OnScrollListener() {
		@Override
		public void onScroll(AbsListView view, int firstVisibleItem,
				int visibleItemCount, int totalItemCount) {
			
		}
		@Override
		public void onScrollStateChanged(AbsListView view, int scrollState) {
            int endIndex = view.getLastVisiblePosition();
            int itemsLastIndex = view.getAdapter().getCount() - 1;
            if (scrollState == OnScrollListener.SCROLL_STATE_IDLE && itemsLastIndex == endIndex) {
            	if(callBack!=null)
            	callBack.loadMore();
            }
		}

	};
}
