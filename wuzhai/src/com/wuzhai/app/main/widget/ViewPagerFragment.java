package com.wuzhai.app.main.widget;

import java.util.ArrayList;

import com.viewpagerindicator.CirclePageIndicator;
import com.wuzhai.app.R;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

public abstract class ViewPagerFragment extends Fragment {

	private ViewPager viewPager;
	private ArrayList<Fragment> pageList;
	private CirclePageIndicator pageIndicator;
	FrameLayout frameLayout;
	boolean isReselect = false;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		if(frameLayout==null){
			isReselect = false;
			frameLayout = (FrameLayout) inflater.inflate(
					R.layout.viewpagerfragment_layout, null);
		}else {
			isReselect = true;
		}

		return frameLayout;
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		if(!isReselect){
			viewPager = (ViewPager) getActivity().findViewById(R.id.viewpager);
			viewPager.setOffscreenPageLimit(3);
			pageIndicator = (CirclePageIndicator) getActivity().findViewById(
					R.id.indicator);
			pageList = new ArrayList<Fragment>();
			initFragments(pageList);
			ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager(), pageList);
			viewPager.setAdapter(adapter);
			pageIndicator.setViewPager(viewPager);
		}
		super.onViewCreated(view, savedInstanceState);
	}

	public void setOnPageChangeListener(OnPageChangeListener listener) {
		viewPager.addOnPageChangeListener(listener);
	}

	protected abstract void initFragments(ArrayList<Fragment> pageList);
}
