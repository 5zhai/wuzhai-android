package com.wuzhai.app.main.widget;

import android.view.View;

public interface OnRecyclerViewItemClickListener {
	void onItemClick(View view,int position);
}
