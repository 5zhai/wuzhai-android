package com.wuzhai.app.main.widget;

import java.util.ArrayList;

import com.squareup.picasso.Picasso;
import com.wuzhai.app.R;
import com.wuzhai.app.objects.Comment;
import com.wuzhai.app.tools.Utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class CommentListAdapter extends BaseAdapter {

	private ArrayList<Comment> commentList;
	private Context context;
	public CommentListAdapter(Context context){
		this.context = context;
	}

	public void setCommentList(ArrayList<Comment> commentList){
		this.commentList = commentList;
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return commentList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return commentList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if(convertView==null){
			convertView = LayoutInflater.from(context).inflate(R.layout.dance_detail_comment, null);
			holder = new ViewHolder();
			holder.commentatorsAvatar = (ImageView)convertView.findViewById(R.id.commentators_avatar);
			holder.commentatorsName = (TextView)convertView.findViewById(R.id.commentators_name);
			holder.time = (TextView)convertView.findViewById(R.id.time);
			holder.comment = (TextView)convertView.findViewById(R.id.comment);
			convertView.setTag(holder);
		}
		holder = (ViewHolder)convertView.getTag();
		Comment comment = commentList.get(position);
		Picasso.with(context).load(comment.getAvatar()).resize(80, 80).centerInside().into(holder.commentatorsAvatar);
		holder.commentatorsName.setText(comment.getUsername());
		holder.comment.setText(comment.getDesc());
		holder.time.setText(Utils.getDateFromMillisecond((int)comment.getCreated_at()));
		return convertView;
	}

	private class ViewHolder{
		public ImageView commentatorsAvatar;
		public TextView commentatorsName;
		public TextView time;
		public TextView comment;
		
	}
}
