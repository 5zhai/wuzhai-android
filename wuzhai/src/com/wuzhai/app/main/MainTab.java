package com.wuzhai.app.main;

import com.wuzhai.app.R;
import com.wuzhai.app.main.home.HomeFragment;
import com.wuzhai.app.main.home.HomeFragmentNew;
import com.wuzhai.app.main.market.MarketFragment;
import com.wuzhai.app.main.video.VideoFragment;
import com.wuzhai.app.main.welfare.WelfareFragment;

public enum MainTab {

	HOME(0,R.string.main_tab_name_home,R.drawable.tab_icon_home,HomeFragmentNew.class),
	MARKET(1,R.string.main_tab_name_market,R.drawable.tab_icon_market,MarketFragment.class),
	RELEASE(2,R.string.main_tab_name_release,R.drawable.icon_checked,null),
	WELFARE(3,R.string.main_tab_name_welfare,R.drawable.tab_icon_welfare,WelfareFragment.class),
	VIDEO(4,R.string.main_tab_name_video,R.drawable.tab_icon_video,VideoFragment.class);
	
	private int id;
	private int tabName;
	private int tabIcon;
	private Class<?> tabCls;
	
	private MainTab(int id,int tabName,int tabIcon,Class<?>cls){
		this.id = id;
		this.tabName = tabName;
		this.tabIcon = tabIcon;
		this.tabCls = cls;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getTabName() {
		return tabName;
	}

	public void setTabName(int tabName) {
		this.tabName = tabName;
	}

	public int getTabIcon() {
		return tabIcon;
	}

	public void setTabIcon(int tabIcon) {
		this.tabIcon = tabIcon;
	}

	public Class<?> getTabCls() {
		return tabCls;
	}

	public void setTabCls(Class<?> cls) {
		this.tabCls = cls;
	}

}
