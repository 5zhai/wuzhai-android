package com.wuzhai.app.main;

import io.rong.imkit.RongIM;
import io.rong.imkit.RongIM.UserInfoProvider;
import io.rong.imlib.RongIMClient;
import io.rong.imlib.model.UserInfo;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.crypto.MacSpi;
import javax.net.ssl.HttpsURLConnection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cn.ucloud.ufilesdk.Callback;

import com.mob.commons.logcollector.c;
import com.qiniu.android.http.ResponseInfo;
import com.qiniu.android.storage.UpCompletionHandler;
import com.qiniu.android.storage.UploadManager;
import com.tencent.mm.sdk.modelpay.PayReq;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.wuzhai.app.application.WuZhaiApplication;
import com.wuzhai.app.main.home.widget.News;
import com.wuzhai.app.main.market.widget.Aftercases;
import com.wuzhai.app.main.market.widget.ComicConNews;
import com.wuzhai.app.main.market.widget.DeliveryAddress;
import com.wuzhai.app.main.market.widget.Goods;
import com.wuzhai.app.main.video.widget.LiveRoomEntity;
import com.wuzhai.app.main.video.widget.VideoEntity;
import com.wuzhai.app.objects.Carousel;
import com.wuzhai.app.objects.Collectionable;
import com.wuzhai.app.objects.Follower;
import com.wuzhai.app.objects.Gift;
import com.wuzhai.app.objects.GiftCategory;
import com.wuzhai.app.objects.Likable;
import com.wuzhai.app.objects.MediaObject;
import com.wuzhai.app.objects.Order;
import com.wuzhai.app.objects.Photographer;
import com.wuzhai.app.objects.Saler;
import com.wuzhai.app.objects.Tag;
import com.wuzhai.app.objects.User;
import com.wuzhai.app.objects.Comment;
import com.wuzhai.app.objects.UserCenterInfo;
import com.wuzhai.app.objects.UserCenterInfo.Video;
import com.wuzhai.app.person.widget.FollowerFans;
import com.wuzhai.app.tools.UcloudFileToUploadTool;
import com.wuzhai.app.tools.Utils;
import com.wuzhai.app.objects.Photo;

import android.R.bool;
import android.R.integer;
import android.app.Application;
import android.app.ProgressDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.net.Uri;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

public class WuzhaiService extends Service {

	private CallBack callBack;
	private Handler threadHandler;
	private Handler mainHandler;
	//publish file
	private final int PUBLISH_FILE = 0;
	public static final int PUBLISH_FILE_DONE = 2;
	private final int UPLOAD_FILE_TO_QN_SUCC = 5;
	private final int UPLOAD_FILE_TO_QN_FAL = 6;
	private final int PUBLISH_VIDEO = 3;
	private boolean isUploadPicSucc = true;
	//get delivery address
	private final int GET_DELIVERY_ADDRESS = 7;
	private final int GET_DELIVERY_ADDRESS_DONE = 8;
	//add delivery address
	private final int ADD_DELIVERY_ADDRESS = 9;
	public static final int ADD_DELIVERY_ADDRESS_SUCC = 10;
	public static final int ADD_DELIVERY_ADDRESS_FAL = 11;
	//get ComicCon
	public static final int GET_COMICCON = 12;
	public static final int GET_COMICCON_DONE = 13;
	public static final int GET_LATEST_COMICCON = 21;
	public static final int GET_LATEST_COMICCON_DONE = 22;
	//get user info
	private static final int GET_USERINFO = 14;
	public static final int GET_USERINFO_SUCC = 15;
	public static final int GET_USERINFO_FAIL = 33;
	//follow user
	private static final int FOLLOW_USER = 16;
	public static final int FOLLOW_USER_SUCC = 17;
	public static final int FOLLOW_USER_FAL = 18;
	//get followers
	private static final int GET_FOLLOWERS = 19;
	private static final int GET_FOLLOWERS_DONE = 20;
	//get recommendations
	private static final int GET_RECOMMENDATIONS = 23;
	private static final int GET_RECOMMENDATIONS_DONE = 24;
	//save User Info
	private static final int SAVE_USER_INFO = 25;
	public static final int SAVE_USER_INFO_SUCC = 26;
	public static final int SAVE_USER_INFO_FAL = 27;
	//publish pic
//	public static final int PUBLISH_PIC_SUC = 2;
//	public static final int PUBLISH_PIC_FAL = 3;

	//goods
	private static final int GET_GOODS = 28;
	private static final int GET_GOODS_DONE = 29;
	private static final int PUBLISH_GOODS = 34;
	public static final int PUBLISH_GOODS_SUCC = 35;
	public static final int PUBLISH_GOODS_FAL = 36;
	//get Photographer service
	private static final int GET_PHOTOGRAPHER = 37;
	private static final int GET_PHOTOGRAPHER_DONE = 38;
	//collection
	private static final int COLLECTE = 39;
	public static final int COLLECTE_SUCC = 40;
	public static final int COLLECTE_FAL = 41;
	public static final String TYPE_PHOTOGRAPHY = "PhotographyService";
	public static final String TYPE_PROCESSING = "ProcessingService";
	public static final String TYPE_COMMODITY = "Commodity";
	public static final String TYPE_VIDEO = "Video";
	public static final String TYPE_PHOTO = "Photo";
	private static final int GET_COLLECTIONABLES = 42;
	private static final int GET_COLLECTIONABLES_DONE = 43;
	private static final int CANCLE_COLLECTE = 44;
	public static final int CANCLE_COLLECTE_SUCC = 45;
	public static final int CANCLE_COLLECTE_FAL = 46;
	//publish Photographer
	private static final int PUBLISH_PHOTOGRAPHER = 47;
	public static final int PUBLISH_PHOTOGRAPHER_SUCC = 48;
	public static final int PUBLISH_PHOTOGRAPHER_FAL = 49;
	//get Aftercase
	private static final int GET_AFTERCASE = 50;
	private static final int GET_AFTERCASE_DONE = 51;
	private static final int PUBLISH_AFTERCASE = 52;
	public static final int PUBLISH_AFTERCASE_SUCC = 54;
	public static final int PUBLISH_AFTERCASE_FAL = 53;
	//get goods detial
	private static final int GET_GOODS_DETIAL = 55;
	private static final int GET_GOODS_DETIAL_DONE = 56;
	//get default address
	private static final int GET_DEFAULT_DELIVERY_ADDRESS = 57;
	private static final int GET_DEFAULT_DELIVERY_ADDRESS_DONE = 58;
	//get selected video
	private static final int GET_SELECTED_VIDOE = 59;
	private static final int GET_SELECTED_VIDOE_DONE = 60;
	//-------connect to rong--------
	private final int CONNECT_TO_RONG_CLOUD = 61;
	private final int CONNECT_TO_RONG_CLOUD_SUCC = 62;
	private final int CONNECT_TO_RONG_CLOUD_FAL = 63;
	//-----order------------------
	private final int CREATE_ORDER = 64;
	private final int CREATE_ORDER_DONE = 65;
	private final int GET_ORDER = 66;
	private final int GET_ORDER_DONE = 67;
	private final int CANCLE_ORDER = 68;
	public static final int CANCLE_ORDER_SUCC = 69;
	public static final int CANCLE_ORDER_FAL = 70;
	private final int RECEIVE_ORDER = 73;
	public static final int RECEIVE_ORDER_SUCC = 75;
	public static final int RECEIVE_ORDER_FAL = 74;
	private final int COMMIT_ORDER = 76;
	public static final int COMMIT_ORDER_SUCC = 78;
	public static final int COMMIT_ORDER_FAL = 77;
	//-----like---------
	private static final int LIKE = 78;
	public static final int LIKE_SUCC = 79;
	public static final int LIKE_FAL = 83;
	public static final String LIKE_VIDEO = "Video";
	public static final String LIKE_PHOTO = "Photo";
	private static final int GET_LIKABLE = 85;
	private static final int GET_LIKABLE_DONE = 81;
	private static final int CANCLE_LIKABLE = 82;
	public static final int CANCLE_LIKABLE_SUCC = 86;
	public static final int CANCLE_LIKABLE_FAL = 80;
	private static final int CHECK_IS_LIKE = 87;
	private static final int CHECK_IS_LIKE_DONE = 88;
	private static final int CHECK_IS_COLLECTED = 89;
	private static final int CHECK_IS_COLLECTED_DONE = 90;
	//-------comments-------
	private static final int GET_COMMENT_LIST = 91;
	private static final int GET_COMMENT_LIST_DONE = 92;
	private static final int ADD_COMMENT = 95;
	public static final int ADD_COMMENT_SUCC = 94;
	public static final int ADD_COMMENT_FAL = 93;
	//------gift-------
	private static final int GET_GIFT = 96;
	private static final int GET_GIFT_DONE = 97;
	//------find password code-----------
	private static final int GET_FIND_PASSWORD_CODE = 98;
	public static final int GET_FIND_PASSWORD_SUCC = 99;
	public static final int GET_FIND_PASSWORD_FAIL = -11;
	//------change password----------
	private static final int CHANGE_PASSWORD = 100;
	public static final int CHANGE_PASSWORD_SUCC = 101;
	public static final int CHANGE_PASSWORD_FAIL = -12;
	//--------get about us----------
	private static final int GET_ABOUT_US = 102;
	private static final int GET_ABOUT_US_DONE = 103;
	//--------cancle follow--------------
	private static final int CANCLE_FOLLOW = 104;
	public static final int CANCLE_FOLLOW_SUCC = 105;
	public static final int CANCLE_FOLLOW_FAIL = 106;
	//-----gift categories--------
	private static final int GET_GIFT_CATEGORIES = 107;
	private static final int GET_GIFT_CATEGORIES_DONE = 108;
	//----------get user center info-----
	private static final int GET_USER_CENTER_INFO = 109;
	private static final int GET_USER_CENTER_INFO_DONE = 110;
	//----------suggestions feedback-----------
	private static final int SUGGESTIONS_FEEDBACK = 111;
	public static final int SUGGESTIONS_FEEDBACK_SUCC = 112;
	public static final int SUGGESTIONS_FEEDBACK_FAL = -13;
	//---------------------location-----------
	private static final int LOCATION_TO_CITY = 113;
	//----------------hot video-----------
	private static final int GET_HOT_VIDEO = 114;
	private static final int GET_HOT_VIDEO_DONE = 115;
	//---------------home recommendations----------
	private static final int GET_HOME_RECOMMENDATION = 116;
	private static final int GET_HOME_RECOMMENDATION_DONE = 117;
	//---------get photo------------
	private static final int GET_PHOTO_LIST = 118;
	private static final int GET_PHOTO_LIST_DONE = 119;
	//get video list
	private static final int GET_VIDOE_LIST = 120;
	private static final int GET_VIDOE_LIST_DONE = 121;
	//------pay-------
	private final int PAY_WITH_BALANCE = 71;
	public static final int PAY_WITH_BALANCE_DONE = 72;
	//get type
	private static final int GET_TYPE = 32;
	private static final int GET_TYPE_DONE = 33;
	public static final String GOODS_TYPE = "commodity_categories";
	public static final String PHOTO_TYPE = "photo_categories";

	public static final int NETWORK_ERR = 4;
    //get Tags
	private static final int GET_TAGS = 30;
	private static final int GET_TAGS_DONE = 31;
	//video upload
	private static final int GET_UPLOAD_AUTH = 122;
	private static final int GET_UPLOAD_AUTH_DONE = 123;
	public static final int UPLOAD_VIDEO_FAIL = -14;
	private Bundle videoUploadParamter = new Bundle();
	private static final int UPLOAD_VIDEO_TO_UCLOUD_DONE = 124;
	public static final int UPLOAD_VIDEO_INFO_SUCC = 125;
	public static final int UPLOAD_VIDEO_INFO_FAIL = -15;
//------------微信支付------------
	private static final int GET_WX_INFO = 128;
	private static final int GET_WX_PERPAYID = 126;
	public static final int START_WX_PAY_FAiL = -16;
	public static final int START_WX_PAY = 127;
	//--------轮播---------
	private static final int GET_CAROUSEL = 129;
	private static final int GET_CAROUSEL_DONE = 130;
	public static final int CAROUSEL_TYPE_HOME = 131;
	public static final int CAROUSEL_TYPE_VIDEO = 132;
	//------直播间---------
	private static final int CREATE_LIVE_ROOM = 133;
	public static final int CREATE_LIVE_ROOM_DONE = 134;
	private static final int GET_LIVEROOM_LIST = 135;
	private static final int GET_LIVEROOM_LIST_DONE = 136;
	private static final int JOIN_LEAVE_LIVEROOM = 137;
	//-------赠送礼物-----
	private static final int SENT_GIFT = 138;
	private static final int SENT_GIFT_DONE = 139;
	//------删除图片------
	public static final int DEL_MEDIA = 140;
	public static final int DEL_MEDIA_SUCC = 141;
	public static final int DEL_MEDIA_FAL = -17;
	//------获取礼物种类列表----
	private static final int GET_GIFT_TYPE_LIST = 142;
	private static final int GET_GIFT_TYPE_LIST_DONE = 143;

	public static final String TAG_GOODS = "commodity";
	public static final String TAG_NORMAL = "normal";
	public static final String TAG_PHOTO = "photo";
	public static final String TAG_PHOTOGRAPHY_SERVICE = "photography_service";
	public static final String TAG_PROCESSING_SERVICE = "processing_service";
	public static final String TAG_VIDEO = "video";
	//video sort type
	public static final String 	VIDEO_LATEST = "last_create";
	public static final String 	VIDEO_LIKE_MOST = "like_desc";
	public static final String 	VIDEO_PLAY_MOST = "play_desc";
	private int fileCountNeedToPublish = 0;

	private int recommendationsNextPage = 0;
	private int comicConNextPage = 0;
	private int goodsNextPage = 0;
	private int photographerNextPage = 0;
	private int aftercaseNextPage = 0;
	private int collectionableNextPage = 0;
	private int selectedVideoNextPage = 0;
	private int giftNextPage = 0;
	private int photos_NextPage = 0;
	private int videos_NextPage = 0;
	private int liveRoom_NextPage = 0;
	private UcloudFileToUploadTool uploadTool;

	private ArrayList<String> publishFileUtlList = new ArrayList<String>();
	@Override
	public IBinder onBind(Intent arg0) {
		return new WuzhaiBind();
	}

	public class WuzhaiBind extends Binder{
		public WuzhaiService getService(){
			return WuzhaiService.this;
		}
	}
	
	public interface CallBack{
		public void onCompleted(int result);
		public void onGetDeliveryAddressCompleted(ArrayList<DeliveryAddress> addressList);
		public void onGetComicConNewsCompleted(ArrayList<ComicConNews> comicConNewsList,int nextPage);
		public void onGetLatestComicConNewsCompleted(ArrayList<ComicConNews> comicConNewsList);
		public void onGetFollowersCompleted(ArrayList<FollowerFans> followersList);
		public void onGetRecommendationsCompleted(ArrayList<News> recommendationsList,int nextPage);
		public void onUploadFileCompleted(ArrayList<String> picPathList);
		public void onGetGoodsCompleted(ArrayList<Goods> goodsList,int nextPage);
		public void onGetTypeCompleted(HashMap<Integer, String> typeMap);
		public void onGetTagsCompleted(ArrayList<Tag> tagsList);
		public void onGetPhotographerCompleted(ArrayList<Photographer> photographerList,int next_page);
		public void onGetCollectionableListCompleted(ArrayList<Collectionable> collectionable_List,int next_page);
		public void onGetAftercasesCompleted(ArrayList<Aftercases> aftercasesList,int next_page);
		public void onGetGoodsDetialCompleted(Goods goods);
		public void onGetDefaultDeliveryAddressCompleted(DeliveryAddress address);
		public void onGetVideoListCompleted(ArrayList<VideoEntity> videoList,int next_page);
		public void onCreateOrderCompleted(int order_id);
		public void onPayWithBalanceCompleted(String msg);
		public void onGetOrderListCompleted(ArrayList<Order> order_list);
		public void onGetLikableListCompleted(ArrayList<Likable> likable_List,int next_page);
		public void onCheckIsLikeDone(boolean isLike);
		public void onCheckIsCollectedDone(boolean isCollected);
		public void onGetCommentListDone(ArrayList<Comment> comments);
		public void onGetAboutUsDone(String result);
		public void onGetGiftListDone(ArrayList<Gift> gifts,int nextPage);
		public void onGetGiftCategoryListDone(ArrayList<GiftCategory> giftCategorys);
		public void onGetUserCenterInfoDone(UserCenterInfo userCenterInfo);
		public void onGetHotVideosDone(ArrayList<VideoEntity> videoList);
		public void onGetHomeRecommendationsDone(ArrayList<News> recommendationsList);
		public void onGetPhotoListDone(ArrayList<Photo> photoList,int nextPage);
		public void onGetCarouselListDone(ArrayList<Carousel> carouselList);
		public void onGetLiveRoomListDone(ArrayList<LiveRoomEntity> liveRoomList);
		public void onSendGiftDone(boolean result);
	}

	public void setCallBack(CallBack callBack){
		this.callBack = callBack;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		mainHandler = new Handler(){
			@Override
			public void handleMessage(Message msg) {
				switch (msg.what) {
				case GET_DELIVERY_ADDRESS_DONE:
					callBack.onGetDeliveryAddressCompleted((ArrayList<DeliveryAddress>)msg.obj);
					break;
				case GET_COMICCON_DONE:
					callBack.onGetComicConNewsCompleted((ArrayList<ComicConNews>)msg.obj,comicConNextPage);
					break;
				case GET_LATEST_COMICCON_DONE:
					callBack.onGetLatestComicConNewsCompleted((ArrayList<ComicConNews>)msg.obj);
					break;
				case GET_FOLLOWERS_DONE:
					callBack.onGetFollowersCompleted((ArrayList<FollowerFans>)msg.obj);
					break;
				case GET_RECOMMENDATIONS_DONE:
					callBack.onGetRecommendationsCompleted((ArrayList<News>)msg.obj,recommendationsNextPage);
					break;
				case PUBLISH_FILE_DONE:
					callBack.onUploadFileCompleted(publishFileUtlList);
					break;
				case GET_GOODS_DONE:
					callBack.onGetGoodsCompleted((ArrayList<Goods>)msg.obj, goodsNextPage);
					break;
				case GET_TAGS_DONE:
					callBack.onGetTagsCompleted((ArrayList<Tag>)msg.obj);
					break;
				case GET_TYPE_DONE:
					callBack.onGetTypeCompleted((HashMap<Integer, String>)msg.obj);
					break;
				case GET_PHOTOGRAPHER_DONE:
					callBack.onGetPhotographerCompleted((ArrayList<Photographer>)msg.obj, photographerNextPage);
					break;
				case GET_COLLECTIONABLES_DONE:
					callBack.onGetCollectionableListCompleted((ArrayList<Collectionable>)msg.obj, collectionableNextPage);
					break;
				case GET_AFTERCASE_DONE:
					callBack.onGetAftercasesCompleted((ArrayList<Aftercases>)msg.obj, aftercaseNextPage);
					break;
				case GET_GOODS_DETIAL_DONE:
					callBack.onGetGoodsDetialCompleted((Goods)msg.obj);
					break;
				case GET_DEFAULT_DELIVERY_ADDRESS_DONE:
					callBack.onGetDefaultDeliveryAddressCompleted((DeliveryAddress)msg.obj);
					break;
				case GET_SELECTED_VIDOE_DONE:
					callBack.onGetVideoListCompleted((ArrayList<VideoEntity>)msg.obj, selectedVideoNextPage);
					break;
				case CREATE_ORDER_DONE:
					callBack.onCreateOrderCompleted(msg.arg1);
					break;
				case PAY_WITH_BALANCE_DONE:
					callBack.onPayWithBalanceCompleted(msg.obj.toString());
					break;
				case GET_ORDER_DONE:
					callBack.onGetOrderListCompleted((ArrayList<Order>)msg.obj);
					break;
				case GET_LIKABLE_DONE:
					callBack.onGetLikableListCompleted((ArrayList<Likable>)msg.obj, 1);
					break;
				case CHECK_IS_LIKE_DONE:
					callBack.onCheckIsLikeDone((Boolean)msg.obj);
					break;
				case CHECK_IS_COLLECTED_DONE:
					callBack.onCheckIsCollectedDone((Boolean)msg.obj);
					break;
				case GET_COMMENT_LIST_DONE:
					callBack.onGetCommentListDone((ArrayList<Comment>)msg.obj);
					break;
				case GET_ABOUT_US_DONE:
					callBack.onGetAboutUsDone(msg.obj.toString());
					break;
				case GET_GIFT_DONE:
					callBack.onGetGiftListDone((ArrayList<Gift>)msg.obj,giftNextPage);
					break;
				case GET_GIFT_CATEGORIES_DONE:
					callBack.onGetGiftCategoryListDone((ArrayList<GiftCategory>)msg.obj);
					break;
				case GET_USER_CENTER_INFO_DONE:
					callBack.onGetUserCenterInfoDone((UserCenterInfo)msg.obj);
					break;
				case GET_HOT_VIDEO_DONE:
					callBack.onGetHotVideosDone((ArrayList<VideoEntity>)msg.obj);
					break;
				case GET_HOME_RECOMMENDATION_DONE:
					callBack.onGetHomeRecommendationsDone((ArrayList<News>)msg.obj);
					break;
				case GET_PHOTO_LIST_DONE:
					callBack.onGetPhotoListDone((ArrayList<Photo>)msg.obj,photos_NextPage);
					break;
				case GET_VIDOE_LIST_DONE:
					callBack.onGetVideoListCompleted((ArrayList<VideoEntity>)msg.obj, videos_NextPage);
					break;
					//视频上传相关
				case GET_UPLOAD_AUTH_DONE:
					String authorization = msg.getData().getString("authorization");
					String path = msg.getData().getString("path");
					Log.d("yue.huang", "path:"+path);
					Log.d("yue.huang", "authorization:"+authorization);
					uploadTool.putFile(path,authorization,threadHandler,UPLOAD_VIDEO_TO_UCLOUD_DONE);
					break;
				case START_WX_PAY:
					Log.d("yue.huang", "START_WX_PAY");
					WuZhaiApplication application = (WuZhaiApplication)getApplication();
					String appid = application.getWxAppId();
					String partnerid = application.getWxMchId();
					String prepayid = msg.obj.toString();
					String pkg = "Sign=WXPay";
					String noncestr = Math.random()+"";
					String timestamp = System.currentTimeMillis()/1000+"";
					String key = application.getWxKey();
					String sign = Utils.getWxPaySignedString(appid, partnerid, prepayid, pkg, noncestr, timestamp, key);
					Log.d("yue.huang", "signsign:"+sign);
					startWxPay(appid, partnerid, prepayid, pkg, noncestr, timestamp, sign);
					break;
				case GET_CAROUSEL_DONE:
					callBack.onGetCarouselListDone((ArrayList<Carousel>)msg.obj);
					break;
				case GET_LIVEROOM_LIST_DONE:
					callBack.onGetLiveRoomListDone((ArrayList<LiveRoomEntity>)msg.obj);
					break;
				case SENT_GIFT_DONE:
					callBack.onSendGiftDone(((Boolean)msg.obj).booleanValue());
					break;
				case GET_GIFT_TYPE_LIST_DONE:
					callBack.onGetGiftListDone((ArrayList<Gift>)msg.obj,giftNextPage);
					break;
				default:
					callBack.onCompleted(msg.what);
					break;
				}
			}
		};
		WuzhaiThread thread = new WuzhaiThread();
		thread.start();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		threadHandler.getLooper().quit();
	}

	private class WuzhaiThread extends Thread{
		@Override
		public void run() {
			Looper.prepare();
			threadHandler = new Handler(){
				@Override
				public void handleMessage(Message msg) {
					switch (msg.what) {
					case PUBLISH_FILE:
						Bundle bundle = msg.getData();
						String fileType = bundle.getString("fileType");
						HashMap<String, String> parametersMap = (HashMap<String, String>)bundle.get("parametersMap");
						ArrayList<String> filePathList = (ArrayList<String>)bundle.get("files");
						if(filePathList.size()!=0){
							uploadFileToQINIU(parametersMap,filePathList,fileType);
						}else {
							if(fileCountNeedToPublish == publishFileUtlList.size()){
								Message uploadFileSuccMessage = Message.obtain();
								uploadFileSuccMessage.what = UPLOAD_FILE_TO_QN_SUCC;
								uploadFileSuccMessage.obj = parametersMap;
								Bundle bundle1 = new Bundle();
								bundle1.putString("fileType", fileType);
								uploadFileSuccMessage.setData(bundle1);
								threadHandler.sendMessage(uploadFileSuccMessage);
							}else {
								threadHandler.sendEmptyMessage(UPLOAD_FILE_TO_QN_FAL);
							}
						}
						break;

					case UPLOAD_FILE_TO_QN_SUCC:
						String type = msg.getData().getString("fileType");
						if("image".equals(type)){
							String picUrls = "";
							for(String picUrl:publishFileUtlList){
								picUrls += picUrl + ",";
							}
							HashMap<String, String> map = new HashMap<String, String>((HashMap<String, String>)msg.obj);
							map.put("images", picUrls);
							publishPicToServer(map);
						}else {
							HashMap<String, String> map = new HashMap<String, String>((HashMap<String, String>)msg.obj);
							map.put("url", publishFileUtlList.get(0));
							map.put("screenshot", "");
							uploadVideoInfoToServer(map);
						}

						break;
					case UPLOAD_FILE_TO_QN_FAL:
						//如果上传多张图片的话则上传都上传，否则都不上传，所以删除刚刚上传成功的图片
						//删除七牛上图片
						publishFileUtlList.clear();
						Log.d("yue.huang", "1");
						mainHandler.sendEmptyMessage(PUBLISH_FILE_DONE);
						break;
					case GET_DELIVERY_ADDRESS:
						ArrayList<DeliveryAddress> addressList = getDeliveryAddressFromServer();
//						if(addressList!=null){
							Message message = Message.obtain();
							message.what = GET_DELIVERY_ADDRESS_DONE;
							message.obj = addressList;
							mainHandler.sendMessage(message);
//						}else {
//							mainHandler.sendEmptyMessage(GET_DELIVERY_ADDRESS_FAL);
//						}
						break;
					case ADD_DELIVERY_ADDRESS:
						addDeliveryAddressToServer((HashMap<String, String>)msg.obj);
						break;
					case GET_COMICCON:
						ArrayList<ComicConNews> comicConNewsList = getComicConFromServer(msg.arg1);
						Message messageComicCon = Message.obtain();
						messageComicCon.what = GET_COMICCON_DONE;
						messageComicCon.obj = comicConNewsList;
						mainHandler.sendMessage(messageComicCon);
						break;
					case GET_LATEST_COMICCON:
						ArrayList<ComicConNews> latestComicConNewsList = getLatestComicConFromServer();
						Message messageLatestComicCon = Message.obtain();
						messageLatestComicCon.what = GET_LATEST_COMICCON_DONE;
						messageLatestComicCon.obj = latestComicConNewsList;
						mainHandler.sendMessage(messageLatestComicCon);
						break;
					case GET_USERINFO:
						if(getUserInfoFromServer()){
							mainHandler.sendEmptyMessage(GET_USERINFO_SUCC);
						}else {
							mainHandler.sendEmptyMessage(GET_USERINFO_FAIL);
						}
						break;
					case FOLLOW_USER:
						HashMap<String, String> fuUarametersMap = new HashMap<String, String>();
						fuUarametersMap.put("user_id", ""+msg.arg1);
						sendFollowUserToServer(fuUarametersMap);
						break;
					case GET_FOLLOWERS:
						ArrayList<FollowerFans> followerList = getFollowerListFromServer(msg.arg1);
						Message messageFollower = Message.obtain();
						messageFollower.what = GET_FOLLOWERS_DONE;
						messageFollower.obj = followerList;
						mainHandler.sendMessage(messageFollower);
						break;
					case GET_RECOMMENDATIONS:
						ArrayList<News> newsList = getRecommendationsListFromServer(msg.arg1);
						Message messageNews = Message.obtain();
						messageNews.what = GET_RECOMMENDATIONS_DONE;
						messageNews.obj = newsList;
						mainHandler.sendMessage(messageNews);
						break;
					case SAVE_USER_INFO:
						uploadUserInfoToServer((HashMap<String, String>)msg.obj);
						break;
					case GET_GOODS:
						ArrayList<Goods> goodsList = getGoodsListFromServer((HashMap<String, String>)msg.obj, msg.arg1);
						Message messageGoods = Message.obtain();
						messageGoods.what = GET_GOODS_DONE;
						messageGoods.obj = goodsList;
						mainHandler.sendMessage(messageGoods);
						break;
					case GET_TAGS:
						ArrayList<Tag> tagList = getTagsFromServer(msg.obj.toString());
						Message messageTag = Message.obtain();
						messageTag.what = GET_TAGS_DONE;
						messageTag.obj = tagList;
						mainHandler.sendMessage(messageTag);
						break;
					case GET_TYPE:
						HashMap<Integer, String> goodsTypeMap = getTypeFromServer(msg.obj.toString());
						Message messageType = Message.obtain();
						messageType.what = GET_TYPE_DONE;
						messageType.obj = goodsTypeMap;
						mainHandler.sendMessage(messageType);
						break;
					case PUBLISH_GOODS:
						publishGoodsToServer((HashMap<String, String>)msg.obj);
						break;
					case GET_PHOTOGRAPHER:
						ArrayList<Photographer> photographerList = getPhotographerServiceFromServer(msg.arg1);
						Message messagePhotographer = Message.obtain();
						messagePhotographer.what = GET_PHOTOGRAPHER_DONE;
						messagePhotographer.obj = photographerList;
						mainHandler.sendMessage(messagePhotographer);
						break;
				    case COLLECTE:
						HashMap<String, String> collecteParametersMap = new HashMap<String, String>();
						collecteParametersMap.put("collectionable_id", ""+msg.arg1);
						collecteParametersMap.put("collectionable_type", msg.obj.toString());
						sendCollectionToServer(collecteParametersMap);
						break;
				    case GET_COLLECTIONABLES:
						ArrayList<Collectionable> collectionables = getCollectionableListFromServer(msg.arg1);
						Message messagecollectionables = Message.obtain();
						messagecollectionables.what = GET_COLLECTIONABLES_DONE;
						messagecollectionables.obj = collectionables;
						mainHandler.sendMessage(messagecollectionables);
						break;
				    case CANCLE_COLLECTE:
						HashMap<String, String> cancleCollecteParametersMap = new HashMap<String, String>();
						cancleCollecteParametersMap.put("collectionable_id", ""+msg.arg1);
						cancleCollecteParametersMap.put("collectionable_type", msg.obj.toString());
						sendCancleCollectionToServer(cancleCollecteParametersMap);
						break;
				    case LIKE:
						HashMap<String, String> likeParametersMap = new HashMap<String, String>();
						likeParametersMap.put("likable_id", ""+msg.arg1);
						likeParametersMap.put("likable_type", msg.obj.toString());
						sendLikeToServer(likeParametersMap);
						break;
				    case GET_LIKABLE:
						ArrayList<Likable> likables = getLikableListFromServer(msg.arg1);
						Message messagelikables = Message.obtain();
						messagelikables.what = GET_LIKABLE_DONE;
						messagelikables.obj = likables;
						mainHandler.sendMessage(messagelikables);
						break;
				    case CANCLE_LIKABLE:
						HashMap<String, String> cancleLikeParametersMap = new HashMap<String, String>();
						cancleLikeParametersMap.put("likable_id", ""+msg.arg1);
						cancleLikeParametersMap.put("likable_type", msg.obj.toString());
						sendCancleLikeToServer(cancleLikeParametersMap);
						break;
					case PUBLISH_PHOTOGRAPHER:
						publishPhotographerToServer((HashMap<String, String>) msg.obj);
						break;
					case GET_AFTERCASE:
						ArrayList<Aftercases> aftercasesList = getAftercaseServiceFromServer(msg.arg1);
						Message messageAftercases = Message.obtain();
						messageAftercases.what = GET_AFTERCASE_DONE;
						messageAftercases.obj = aftercasesList;
						mainHandler.sendMessage(messageAftercases);
						break;
					case PUBLISH_AFTERCASE:
						publishAftercasesToServer((HashMap<String, String>) msg.obj);
						break;
					case GET_GOODS_DETIAL:
						Goods goods = getGoodsDetialFromServer(msg.arg1);
						Message messageGoodsDetial = Message.obtain();
						messageGoodsDetial.what = GET_GOODS_DETIAL_DONE;
						messageGoodsDetial.obj = goods;
						mainHandler.sendMessage(messageGoodsDetial);
						break;
					case GET_DEFAULT_DELIVERY_ADDRESS:
						DeliveryAddress defaultAddress= getDefaultDeliveryAddressFromServer();
						Message messageDefaultAddress = Message.obtain();
						messageDefaultAddress.what = GET_DEFAULT_DELIVERY_ADDRESS_DONE;
						messageDefaultAddress.obj = defaultAddress;
						mainHandler.sendMessage(messageDefaultAddress);
						break;
					case GET_SELECTED_VIDOE:
						ArrayList<VideoEntity> videoList = getSelectedVideoFromServer(msg.arg1);
						Message messageSelectedVideo = Message.obtain();
						messageSelectedVideo.what = GET_SELECTED_VIDOE_DONE;
						messageSelectedVideo.obj = videoList;
						mainHandler.sendMessage(messageSelectedVideo);
						break;
					case CONNECT_TO_RONG_CLOUD:
						connectToRongCloudServer();
						break;
					case CREATE_ORDER:
						int orderId = createOrderToServer((HashMap<String, String>)msg.obj);
						Message messageOrder = Message.obtain();
						messageOrder.what = CREATE_ORDER_DONE;
						messageOrder.arg1 = orderId;
						mainHandler.sendMessage(messageOrder);
						break;
					case PAY_WITH_BALANCE:
						payWithBalanceToServer((HashMap<String, String>)msg.obj);
						break;
					case GET_ORDER:
						ArrayList<Order> orderList = getOrdersFromServer(msg.obj.toString());
						Message messageOrderList = Message.obtain();
						messageOrderList.what = GET_ORDER_DONE;
						messageOrderList.obj = orderList;
						mainHandler.sendMessage(messageOrderList);
						break;
					case CANCLE_ORDER:
						HashMap<String, String> cancleOrderParametersMap = new HashMap<String, String>();
						cancleOrderParametersMap.put("order_id", ""+msg.arg1);
						cancleOrderParametersMap.put("access_key", ((WuZhaiApplication)getApplication()).getUser().getAccessKey());
						sendCancleOrderToServer(cancleOrderParametersMap);
						break;
					case RECEIVE_ORDER:
						HashMap<String, String> receiveOrderParametersMap = new HashMap<String, String>();
						receiveOrderParametersMap.put("order_id", ""+msg.arg1);
						receiveOrderParametersMap.put("access_key", ((WuZhaiApplication)getApplication()).getUser().getAccessKey());
						sendReceiveOrderToServer(receiveOrderParametersMap);
						break;
					case COMMIT_ORDER:
						HashMap<String, String> commitOrderParametersMap = new HashMap<String, String>();
						Bundle data = msg.getData();
						commitOrderParametersMap.put("order_id", ""+data.getInt("order_id"));
						commitOrderParametersMap.put("score", ""+data.getFloat("score"));
						commitOrderParametersMap.put("comment", ""+data.getString("comment"));
						commitOrderParametersMap.put("access_key", ((WuZhaiApplication)getApplication()).getUser().getAccessKey());
						sendCommentOrderToServer(commitOrderParametersMap);
						break;
					case CHECK_IS_LIKE:
						HashMap<String, String> checkIsLikeParametersMap = new HashMap<String, String>();
						checkIsLikeParametersMap.put("likable_id", ""+msg.arg1);
						checkIsLikeParametersMap.put("likable_type", msg.obj.toString());
						boolean isLike = checkIsLikeFromServer(checkIsLikeParametersMap);
						Message checkIsLikeMsg = Message.obtain();
						checkIsLikeMsg.what = CHECK_IS_LIKE_DONE;
						checkIsLikeMsg.obj = isLike;
						mainHandler.sendMessage(checkIsLikeMsg);
						break;
					case CHECK_IS_COLLECTED:
						HashMap<String, String> checkIsCollectedParametersMap = new HashMap<String, String>();
						checkIsCollectedParametersMap.put("collectionable_id", ""+msg.arg1);
						checkIsCollectedParametersMap.put("collectionable_type", msg.obj.toString());
						boolean isCollected = checkIsCollectedFromServer(checkIsCollectedParametersMap);
						Message checkIsCollectedMsg = Message.obtain();
						checkIsCollectedMsg.what = CHECK_IS_COLLECTED_DONE;
						checkIsCollectedMsg.obj = isCollected;
						mainHandler.sendMessage(checkIsCollectedMsg);
						break;
					case GET_COMMENT_LIST:
						ArrayList<Comment> commList = getCommentListFromServer(msg.obj.toString(), msg.arg1);
						Message commListMsg = Message.obtain();
						commListMsg.what = GET_COMMENT_LIST_DONE;
						commListMsg.obj = commList;
						mainHandler.sendMessage(commListMsg);
						break;
					case ADD_COMMENT:
						HashMap<String, String> commentParametersMap = new HashMap<String, String>();
						Bundle commentData = msg.getData();
						commentParametersMap.put("commentable_id", ""+commentData.getInt("commentable_id"));
						commentParametersMap.put("commentable_type", ""+commentData.getString("commentable_type"));
						commentParametersMap.put("comment", ""+commentData.getString("comment"));
						commentParametersMap.put("desc", commentData.getString("desc"));
						addCommentToServer(commentParametersMap);
					case GET_GIFT:
						ArrayList<Gift> gifts = getGiftFromServer(msg.arg1,msg.arg2);
						Message giftMsg = Message.obtain();
						giftMsg.what = GET_GIFT_DONE;
						giftMsg.obj = gifts;
						mainHandler.sendMessage(giftMsg);
						break;
					case GET_FIND_PASSWORD_CODE:
						HashMap<String, String> findPsdParametersMap = new HashMap<String, String>();
						findPsdParametersMap.put("phone_number", msg.obj.toString());
						getFindPasswordCode(findPsdParametersMap);
						break;
					case CHANGE_PASSWORD:
						changePassword((HashMap<String, String>)msg.obj);
						break;
					case GET_ABOUT_US:
						String result = getAboutUsFromServer();
						Message aboutMsg = Message.obtain();
						aboutMsg.what = GET_ABOUT_US_DONE;
						aboutMsg.obj = result;
						mainHandler.sendMessage(aboutMsg);
						break;
					case CANCLE_FOLLOW:
						HashMap<String, String> cancleFollowParametersMap = new HashMap<String, String>();
						cancleFollowParametersMap.put("user_id", ""+msg.arg1);
						sendCancleFollowUserToServer(cancleFollowParametersMap);
						break;
					case GET_GIFT_CATEGORIES:
						ArrayList<GiftCategory> giftCategories = getGiftCategoryFromServer();
						Message giftCategoriesMsg = Message.obtain();
						giftCategoriesMsg.what = GET_GIFT_CATEGORIES_DONE;
						giftCategoriesMsg.obj = giftCategories;
						mainHandler.sendMessage(giftCategoriesMsg);
						break;
					case GET_USER_CENTER_INFO:
						HashMap<String, String> userCenterInfoParametersMap = new HashMap<String, String>();
						userCenterInfoParametersMap.put("user_id", ""+msg.arg1);
						UserCenterInfo userCenterInfo = getUserCenterInfoFromServer(userCenterInfoParametersMap);
						Message uciMsg = Message.obtain();
						uciMsg.what = GET_USER_CENTER_INFO_DONE;
						uciMsg.obj = userCenterInfo;
						mainHandler.sendMessage(uciMsg);
						break;
					case SUGGESTIONS_FEEDBACK:
						HashMap<String, String> suggestionParametersMap = new HashMap<String, String>();
						suggestionParametersMap.put("desc", msg.obj.toString());
						submitSuggestionToServer(suggestionParametersMap);
						break;
					case LOCATION_TO_CITY:
						String city = locationToCity(msg.getData().getDouble("latitude"),msg.getData().getDouble("longitude"));
						Log.d("yue.huang", "city:"+city);
						((WuZhaiApplication)getApplication()).setCurrentCity(city);
						break;
					case GET_HOT_VIDEO:
						ArrayList<VideoEntity> hotVideos = getHotVideosFromServer();
						Message messageHotVideo = Message.obtain();
						messageHotVideo.what = GET_HOT_VIDEO_DONE;
						messageHotVideo.obj = hotVideos;
						mainHandler.sendMessage(messageHotVideo);
						break;
					case GET_HOME_RECOMMENDATION:
						ArrayList<News> homeNews = getHomeRecommendationsFromServer(msg.arg1);
						Message homeNewsMsg = Message.obtain();
						homeNewsMsg.what = GET_HOME_RECOMMENDATION_DONE;
						homeNewsMsg.obj = homeNews;
						mainHandler.sendMessage(homeNewsMsg);
						break;
					case GET_PHOTO_LIST:
						ArrayList<Photo> photoList = null;
						if(msg.obj!=null){
							 photoList = getPhotoListFromServer(msg.arg1, msg.obj.toString(), msg.arg2);
						}else {
							photoList = getPhotoListFromServer(msg.arg1, null, msg.arg2);
						}
						Message photoListMsg = Message.obtain();
						photoListMsg.what = GET_PHOTO_LIST_DONE;
						photoListMsg.obj = photoList;
						mainHandler.sendMessage(photoListMsg);
						break;
					case GET_VIDOE_LIST:
						String sortType = msg.getData().getString("sorttype");
						int page = msg.getData().getInt("page");
						ArrayList<VideoEntity> videos = getVideoListFromServer(sortType, page);
						Message videoListMsg = Message.obtain();
						videoListMsg.what = GET_VIDOE_LIST_DONE;
						videoListMsg.obj = videos;
						mainHandler.sendMessage(videoListMsg);
						break;
					case GET_UPLOAD_AUTH:
						String path = msg.obj.toString();
						String strToSign = uploadTool.getStrToSign(path);
//						getUploadAuthorization(path, strToSign);
						break;
					case UPLOAD_VIDEO_TO_UCLOUD_DONE:
						Log.d("yue.huang", "上传到ucloud成功");
						String videoUrl = msg.getData().getString("videoUrl");
						String screenShot = msg.getData().getString("screenShot");
						String title = videoUploadParamter.getString("title");
						String desc = videoUploadParamter.getString("desc");
						String tags = videoUploadParamter.getString("tags");
						String coordinate = videoUploadParamter.getString("coordinate");

//						uploadVideoInfoToServer(videoUrl,title,desc,tags,coordinate,screenShot);
					case GET_WX_PERPAYID:
						String amount = msg.getData().getString("amount");
						String clientIP = msg.getData().getString("clientIP");
						getprepayIdFromServer(amount,clientIP);
						break;
					case GET_WX_INFO:
						getWXInfoFromServer();
						break;
					case GET_CAROUSEL:
						int carouselCount = msg.arg1;
						int carouselType = msg.arg2;
						ArrayList<Carousel> carouselList = getCarouselFromServer(carouselType,carouselCount);
						Message carouselMessage = Message.obtain();
						carouselMessage.what = GET_CAROUSEL_DONE;
						carouselMessage.obj = carouselList;
						mainHandler.sendMessage(carouselMessage);
						break;
					case CREATE_LIVE_ROOM:
						boolean res = createLiveRoomAtServer((HashMap<String, String>)msg.obj);
						if(res){
							Message createLiveMsg = Message.obtain();
							createLiveMsg.what = CREATE_LIVE_ROOM_DONE;
							mainHandler.sendMessage(createLiveMsg);
						}else {
							Log.d("yue.huang", "创建直播间出错");
						}
						break;
					case GET_LIVEROOM_LIST:
						ArrayList<LiveRoomEntity> roomList = getLiveRoomListFromServer(msg.arg1);
						Message liveRoomMsg = Message.obtain();
						liveRoomMsg.what = GET_LIVEROOM_LIST_DONE;
						liveRoomMsg.obj = roomList;
						mainHandler.sendMessage(liveRoomMsg);
						break;
					case JOIN_LEAVE_LIVEROOM:
						joinOrLeaveLiveRoomOnServer(msg.arg1,msg.arg2);
						break;
					case SENT_GIFT:
						boolean bool = sendGiftfromServer(msg.arg1,(HashMap<String, String>)msg.obj);
						Message sendGiftMsg = Message.obtain();
						sendGiftMsg.what = SENT_GIFT_DONE;
						sendGiftMsg.obj = bool;
						mainHandler.sendMessage(sendGiftMsg);
						break;
					case DEL_MEDIA:
						boolean delResult = deleteMediaFromServer(msg.arg2,msg.arg1);
						if(delResult){
							mainHandler.sendEmptyMessage(DEL_MEDIA_SUCC);
						}else {
							mainHandler.sendEmptyMessage(DEL_MEDIA_FAL);
						}
						break;
					case GET_GIFT_TYPE_LIST:
						ArrayList<Gift> giftTypeList=getGiftTypeListFromServer(msg.arg1,msg.arg2);
						Message giftTypeListMsg = Message.obtain();
						giftTypeListMsg.what = GET_GIFT_TYPE_LIST_DONE;
						giftTypeListMsg.obj = giftTypeList;
						mainHandler.sendMessage(giftTypeListMsg);
						break;
					}
				}
			};
			Looper.loop();
		}
	}

	//------------------publish pic-------------------
	public void publishPic(int photo_category_id,String title,String desc,ArrayList<String> filePathList,String tags,String coordinate){
		String access_key = ((WuZhaiApplication)getApplication()).getUser().getAccessKey();
		fileCountNeedToPublish = filePathList.size();
		Message msg = Message.obtain();
		msg.what = PUBLISH_FILE;
		HashMap<String, String> parametersMap = new HashMap<String, String>();
		parametersMap.put("access_key", access_key);
		parametersMap.put("photo_category_id", photo_category_id+"");
		parametersMap.put("title", title);
		parametersMap.put("desc", desc);
		parametersMap.put("tags", tags);
		parametersMap.put("coordinate", coordinate);
		Bundle bundle = new Bundle();
		bundle.putString("fileType", "image");
		bundle.putSerializable("parametersMap", parametersMap);
		//new ArrayList<Bitmap>(bitmapList)重新创建一个list是因为在发布商品中如果发布失败，bitmapList会被清空，导致界面还有图片但list中没有，
		//用户再次点击发布时会报无图片
		bundle.putSerializable("files", new ArrayList<String>(filePathList));
		msg.setData(bundle);
		threadHandler.sendMessage(msg);
	}

	private void uploadFileToQINIU(final HashMap<String, String> parametersMap,final ArrayList<String> filePathList,final String type){
		String filePath = filePathList.remove(0);
		String access_key = parametersMap.get("access_key");
		String uploadToken = getUploadToken(access_key,type);
		if(null!=uploadToken){
			UploadManager uploadManager = new UploadManager();
//            ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
//            bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteOut);
//			byte[] data = byteOut.toByteArray();
			final String key = getFileName(access_key,filePath);
				uploadManager.put(filePath, key, uploadToken, new UpCompletionHandler() {
					@Override
					public void complete(String arg0, ResponseInfo arg1,
							JSONObject arg2) {
						if(arg1.isOK()){
							if("image".equals(type)){
								publishFileUtlList.add("http://ob23p88ai.bkt.clouddn.com/"+key);
							}else {
								publishFileUtlList.add("http://ob23p0kvz.bkt.clouddn.com/"+key);
							}
							Message msg = Message.obtain();
							msg.what = PUBLISH_FILE;
							Bundle bundle = new Bundle();
							bundle.putString("fileType", type);
							bundle.putSerializable("parametersMap", parametersMap);
							bundle.putSerializable("files", filePathList);
							msg.setData(bundle);
							threadHandler.sendMessage(msg);
						}else {
//							threadHandler.sendEmptyMessage(UPLOAD_PIC_TO_QN_FAL);
							Message msg = Message.obtain();
							msg.what = PUBLISH_FILE;
							Bundle bundle = new Bundle();
							bundle.putString("fileType", type);
							bundle.putSerializable("parametersMap", parametersMap);
							bundle.putSerializable("files", filePathList);
							msg.setData(bundle);
							threadHandler.sendMessage(msg);
						}
					}
				},null);
		}
	}

	private String getUploadToken(String access_key,String type){
		HttpsURLConnection connection =null;
		String token = null;
		try {
			URL url = new URL("https://www.5yuzhai.com:443/api/v1/config/qiniu?access_key="+access_key+"&type="+type);
//			connection = (HttpURLConnection)url.openConnection();
			connection = Utils.getHttpsConnection(url);
			connection.setConnectTimeout(5000);
			connection.setRequestMethod("GET");
            if(connection.getResponseCode() == 200){
                InputStream inputStream = connection.getInputStream();
                byte[] bytes = Utils.readStream(inputStream);
                String jsonString = new String(bytes);
                JSONObject resultJson = new JSONObject(jsonString);
                JSONObject data = resultJson.getJSONObject("data");
                token = data.getString("token");
            }
		} catch (Exception e) {
			e.printStackTrace();
			Log.d("yue.huang", "getUploadToken:"+e.toString());
		}finally{
			if(null!=connection){
				connection.disconnect();
			}
		}
		return token;
	}

	private String getFileName(String access_key,String path){
		  SimpleDateFormat sdFormatter = new SimpleDateFormat("yyyyMMddHHmmss");
		  Log.d("yue.huang", "pathpathpathpath:"+path);
		  String suffix = null;
		  if(path.contains(".")){
			  suffix = path.substring(path.lastIndexOf("."));
		  }
		  return access_key+sdFormatter.format(new Date())+suffix;
	}

	private void publishPicToServer(HashMap<String, String> parametersMap){
		String access_key = parametersMap.get("access_key");
		String url = "https://www.5yuzhai.com:443/api/v1/photos?access_key="+access_key;
		HttpsURLConnection connection = null;
		try {
			connection = Utils.getHttpsConnectionPost(url, parametersMap);
			if(connection.getResponseCode() == 200){
				mainHandler.sendEmptyMessage(PUBLISH_FILE_DONE);
			}else {
				publishFileUtlList.clear();
				Log.d("yue.huang", "2:"+connection.getResponseCode());
				mainHandler.sendEmptyMessage(PUBLISH_FILE_DONE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			mainHandler.sendEmptyMessage(NETWORK_ERR);
		}finally{
			if(null!=connection){
				connection.disconnect();
			}
		}
	}
	// ----------------删除图片获取视频---------------------
	public void deleteMedia(int type,int id){
		Message msg = Message.obtain();
		msg.what = DEL_MEDIA;
		msg.arg1 = id;
		msg.arg2 = type;
		threadHandler.sendMessage(msg);
	}

	private boolean deleteMediaFromServer(int type,int id){
		String access_key = ((WuZhaiApplication)getApplication()).getUser().getAccessKey();
		HashMap<String, String> parametersMap = new HashMap<>();
		parametersMap.put("access_key", access_key);
		String url = null;
		if(type == 0){
			parametersMap.put("photo_id", id+"");
			url = "https://www.5yuzhai.com:443/api/v1/photos/delete";
		}else {
			parametersMap.put("video_id", id+"");
			url = "https://www.5yuzhai.com:443/api/v1/videos/delete";
		}
		HttpsURLConnection connection = null;
		try {
			connection = Utils.getHttpsConnectionPost(url, parametersMap);
			if(connection.getResponseCode() == 200){
				return true;
			}else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}finally{
			if(null!=connection){
				connection.disconnect();
			}
		}
	}
//----------------获取收货地址---------------
	public void getDeliveryAddress(){
		threadHandler.sendEmptyMessage(GET_DELIVERY_ADDRESS);
	}

	private ArrayList<DeliveryAddress> getDeliveryAddressFromServer(){
		HttpsURLConnection connection =null;
		String access_key = ((WuZhaiApplication)getApplication()).getUser().getAccessKey();
		try {
			URL url = new URL("https://www.5yuzhai.com:443/api/v1/contacts?access_key="+access_key);
//			connection = (HttpURLConnection)url.openConnection();
			connection = Utils.getHttpsConnection(url);
			connection.setConnectTimeout(5000);
			connection.setRequestMethod("GET");
            if(connection.getResponseCode() == 200){
                InputStream inputStream = connection.getInputStream();
                byte[] bytes = Utils.readStream(inputStream);
                String jsonString = new String(bytes);
                JSONObject resultJson = new JSONObject(jsonString);
                JSONArray addressList = resultJson.getJSONArray("data");
                return deliveryAddressListParse(addressList);
            }
		} catch (Exception e) {
			e.printStackTrace();
			Log.d("yue.huang", "getDeliveryAddressFromServer:"+e.toString());
			return null;
		}finally{
			if(null!=connection){
				connection.disconnect();
			}
		}
		return null;
	}

	private ArrayList<DeliveryAddress> deliveryAddressListParse(JSONArray jsonArray) throws JSONException{
		ArrayList<DeliveryAddress> addresseList = new ArrayList<DeliveryAddress>();
		for(int n=0;n<jsonArray.length();n++){
			addresseList.add(parseDeliveryAddress(jsonArray.getJSONObject(n)));
		}
		return addresseList;
	}

	private DeliveryAddress parseDeliveryAddress(JSONObject jsonObject) throws JSONException{
		String id = jsonObject.getString("id");
		String userId = jsonObject.getString("user_id");
		String receiverName = jsonObject.getString("receive_name");
		String area = jsonObject.getString("area");
		String street = jsonObject.getString("street");
		String detial = jsonObject.optString("detail");
		String tel = jsonObject.getString("tel_no");
		String zipCode = jsonObject.getString("zip_code");

		return new DeliveryAddress(id, userId, receiverName, area, street, detial,tel, zipCode);
	}
	//----------------获取默认收货地址---------------
	public void getDefaultDeliveryAddress(){
		threadHandler.sendEmptyMessage(GET_DEFAULT_DELIVERY_ADDRESS);
	}

	private DeliveryAddress getDefaultDeliveryAddressFromServer(){
		HttpsURLConnection connection =null;
		String access_key = ((WuZhaiApplication)getApplication()).getUser().getAccessKey();
		try {
			URL url = new URL("https://www.5yuzhai.com:443/api/v1/contacts/default_contact?access_key="+access_key);
			connection = Utils.getHttpsConnection(url);
			connection.setConnectTimeout(5000);
			connection.setRequestMethod("GET");
            if(connection.getResponseCode() == 200){
                InputStream inputStream = connection.getInputStream();
                byte[] bytes = Utils.readStream(inputStream);
                String jsonString = new String(bytes);
                JSONObject resultJson = new JSONObject(jsonString);
                JSONObject address = resultJson.getJSONObject("data");
                return parseDeliveryAddress(address);
            }
		} catch (Exception e) {
			e.printStackTrace();
			Log.d("yue.huang", "getDefaultDeliveryAddressFromServer:"+e.toString());
			return null;
		}finally{
			if(null!=connection){
				connection.disconnect();
			}
		}
		return null;
	}

	//----------------添加收货地址---------------
	public void addDeliveryAddress(DeliveryAddress address){
		Message msg = Message.obtain();
		msg.what = ADD_DELIVERY_ADDRESS;
		HashMap<String, String> parametersMap = new HashMap<String, String>();
		parametersMap.put("access_key", ((WuZhaiApplication)getApplication()).getUser().getAccessKey());
		parametersMap.put("receive_name", address.getReceiverName());
		parametersMap.put("area", address.getArea());
		parametersMap.put("street", address.getStreet());
		parametersMap.put("detail", address.getDetial());
		parametersMap.put("tel_no", address.getTel());
		parametersMap.put("zip_code", address.getZipCode());
		msg.obj = parametersMap;
		threadHandler.sendMessage(msg);
	}

	private void addDeliveryAddressToServer(HashMap<String, String> parametersMap){
		String access_key = ((WuZhaiApplication)getApplication()).getUser().getAccessKey();
		String url = "https://www.5yuzhai.com:443/api/v1/contacts?access_key="+access_key;
		HttpsURLConnection connection = null;
		try {
			connection = Utils.getHttpsConnectionPost(url, parametersMap);
			if(connection.getResponseCode() == 200){
				mainHandler.sendEmptyMessage(ADD_DELIVERY_ADDRESS_SUCC);
			}else {
				mainHandler.sendEmptyMessage(ADD_DELIVERY_ADDRESS_FAL);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.d("yue.huang", "addDeliveryAddressToServer:"+e.toString());
			mainHandler.sendEmptyMessage(NETWORK_ERR);
		}finally{
			if(null!=connection){
				connection.disconnect();
			}
		}
	}

	//----------------获取漫展信息---------------
	public void getComicCon(int page){
		Message msg = Message.obtain();
		msg.what = GET_COMICCON;
		msg.arg1 = page;
		threadHandler.sendMessage(msg);
	}
	public void getLatestComicCon(){
		threadHandler.sendEmptyMessage(GET_LATEST_COMICCON);
	}
	private ArrayList<ComicConNews> getComicConFromServer(int page){

		HttpsURLConnection connection = null;
		try {
			URL url = new URL("https://www.5yuzhai.com:443/api/v1/activities?page="+page+"&per=10");
//			connection = (HttpsURLConnection)url.openConnection();
			connection = Utils.getHttpsConnection(url);
			connection.setConnectTimeout(5000);
			connection.setRequestMethod("GET");
			if(connection.getResponseCode() == 200){
                InputStream inputStream = connection.getInputStream();
                byte[] bytes = Utils.readStream(inputStream);
                String jsonString = new String(bytes);
                JSONObject resultJson = new JSONObject(jsonString);
                comicConNextPage = resultJson.optInt("next_page");
                JSONArray newsList = resultJson.getJSONArray("list");
                return comicConNewsListParse(newsList);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.d("yue.huang", "getComicConFromServer:"+e.toString());
			return null;
		}finally{
			if(null!=connection){
				connection.disconnect();
			}
		}
		return null;
	}

	private ArrayList<ComicConNews> getLatestComicConFromServer(){

		HttpsURLConnection connection = null;
		try {
			URL url = new URL("https://www.5yuzhai.com:443/api/v1/activities/latest?count=3");
//			connection = (HttpsURLConnection)url.openConnection();
			connection = Utils.getHttpsConnection(url);
			connection.setConnectTimeout(5000);
			connection.setRequestMethod("GET");
			if(connection.getResponseCode() == 200){
                InputStream inputStream = connection.getInputStream();
                byte[] bytes = Utils.readStream(inputStream);
                String jsonString = new String(bytes);
                JSONObject resultJson = new JSONObject(jsonString);
                JSONArray newsList = resultJson.getJSONArray("data");
                return comicConNewsListParse(newsList);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.d("yue.huang", "getLatestComicConFromServer:"+e.toString());
			return null;
		}finally{
			if(null!=connection){
				connection.disconnect();
			}
		}
		return null;
	}

	private ArrayList<ComicConNews> comicConNewsListParse(JSONArray jsonArray) throws JSONException{
		ArrayList<ComicConNews> comicConNewsList = new ArrayList<ComicConNews>();
		for(int n=0;n<jsonArray.length();n++){
			comicConNewsList.add(parseComicConNews(jsonArray.getJSONObject(n)));
		}
		return comicConNewsList;
	}

	private ComicConNews parseComicConNews(JSONObject jsonObject) throws JSONException{
		int id = jsonObject.getInt("id");
		int begin_time = jsonObject.getInt("begin_time");
		int end_time = jsonObject.getInt("end_time");
		String desc = jsonObject.getString("desc");
		String title = jsonObject.getString("title");
		String image_url = jsonObject.getString("image_url");
		String city = jsonObject.getString("city");
		String summery = jsonObject.getString("summary");
		String category = jsonObject.getString("category");
		JSONObject coordinate = jsonObject.getJSONObject("coordinate");
		double longitude = coordinate.getDouble("longitude");
		double latitude = coordinate.getDouble("latitude");
		ComicConNews comicConNews = new ComicConNews(id,image_url, title, city, begin_time, 
				end_time, desc, longitude, latitude,summery,category);
		return comicConNews;
	}

	//----------------获取用户基本信息---------------
	public void getUserInfo(){
		threadHandler.sendEmptyMessage(GET_USERINFO);
	}

	private boolean getUserInfoFromServer(){
		HttpsURLConnection connection =null;
		String access_key = ((WuZhaiApplication)getApplication()).getUser().getAccessKey();
		try {
			URL url = new URL("https://www.5yuzhai.com:443/api/v1/users/base_info?access_key="+access_key);
//			connection = (HttpURLConnection)url.openConnection();
			connection = Utils.getHttpsConnection(url);
			connection.setConnectTimeout(5000);
			connection.setRequestMethod("GET");
            if(connection.getResponseCode() == 200){
                InputStream inputStream = connection.getInputStream();
                byte[] bytes = Utils.readStream(inputStream);
                String jsonString = new String(bytes);
                JSONObject resultJson = new JSONObject(jsonString);
                return pauseUserInfo(resultJson);
            }
		} catch (Exception e) {
			e.printStackTrace();
			Log.d("yue.huang", "getUserInfoFromServer:"+e.toString());
			return false;
		}finally{
			if(null!=connection){
				connection.disconnect();
			}
		}
		return false;
	}

	private boolean pauseUserInfo(JSONObject userJsonObject) throws JSONException{
		User user = ((WuZhaiApplication) getApplication()).getUser();
		user.setId(userJsonObject.getInt("id"));
		user.setName(userJsonObject.getString("username"));
		user.setPhoneNumber(userJsonObject.getString("phone_number"));
		user.setAvatarUrl(userJsonObject.getString("avatar"));
		user.setAccessKey(userJsonObject.getString("access_key"));
		user.setImToken(userJsonObject.getString("im_token"));
		user.setBirthday(userJsonObject.getString("birthday"));
		user.setSexId(userJsonObject.optInt("sex", -1));
		user.setSignature(userJsonObject.getString("signature"));
		user.setFollowedsCount(userJsonObject.getInt("followeds_count"));
		user.setFollowersCount(userJsonObject.getInt("followers_count"));
		user.setLevel(userJsonObject.getInt("level"));
		user.setBalance(userJsonObject.getString("balance"));
		user.setNeedExperience(userJsonObject.getInt("need_experience"));
		user.setCurrentExperience(userJsonObject.getInt("current_experience"));
		JSONArray oauthUsersJSONArray = userJsonObject.getJSONArray("oauth_users");
		for (int i = 0; i < oauthUsersJSONArray.length(); i++) {
			JSONObject oauthUserJsonObject = oauthUsersJSONArray.getJSONObject(i);
			user.addOauthUser(oauthUserJsonObject.getInt("id"), oauthUserJsonObject.getString("uid"), oauthUserJsonObject.getString("platform"));
		}
//		user.setOauthUsers(userJsonObject.get)
		return true;
	}

	//----------------关注用户---------------
	public void followUser(int userId){
		Message msg = Message.obtain();
		msg.what = FOLLOW_USER;
		msg.arg1 = userId;
		threadHandler.sendMessage(msg);
	}

	private void sendFollowUserToServer(HashMap<String,String> parametersMap){
		String access_key = ((WuZhaiApplication)getApplication()).getUser().getAccessKey();
		String url = "https://www.5yuzhai.com:443/api/v1/following_relationships?access_key="+access_key;
		HttpsURLConnection connection = null;
		try {
			connection = Utils.getHttpsConnectionPost(url, parametersMap);
			if (connection.getResponseCode() == 200) {
				InputStream inputStream = connection.getInputStream();
				byte[] bytes = Utils.readStream(inputStream);
				String jsonString = new String(bytes);
				JSONObject resultJson = new JSONObject(jsonString);
				if (resultJson.getBoolean("success")) {
					mainHandler.sendEmptyMessage(FOLLOW_USER_SUCC);
					getUserInfoFromServer();
				} else {
					mainHandler.sendEmptyMessage(FOLLOW_USER_FAL);
				}
			} else {
				mainHandler.sendEmptyMessage(NETWORK_ERR);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.d("yue.huang", "sendFollowUserToServer:"+e.toString());
			mainHandler.sendEmptyMessage(NETWORK_ERR);
		}finally{
			if(null!=connection){
				connection.disconnect();
			}
		}
	}
	//----------取消关注--------------
	public void cancleFollowUser(int userId){
		Message msg = Message.obtain();
		msg.what = CANCLE_FOLLOW;
		msg.arg1 = userId;
		threadHandler.sendMessage(msg);
	}

	private void sendCancleFollowUserToServer(HashMap<String,String> parametersMap){
		String access_key = ((WuZhaiApplication)getApplication()).getUser().getAccessKey();
		String url = "https://www.5yuzhai.com:443/api/v1/following_relationships/cancel?access_key="+access_key;
		HttpsURLConnection connection = null;
		try {
			connection = Utils.getHttpsConnectionPost(url, parametersMap);
			if (connection.getResponseCode() == 200) {
				InputStream inputStream = connection.getInputStream();
				byte[] bytes = Utils.readStream(inputStream);
				String jsonString = new String(bytes);
				JSONObject resultJson = new JSONObject(jsonString);
				if (resultJson.getBoolean("success")) {
					mainHandler.sendEmptyMessage(CANCLE_FOLLOW_SUCC);
					getUserInfoFromServer();
				} else {
					mainHandler.sendEmptyMessage(CANCLE_FOLLOW_FAIL);
				}
			} else {
				mainHandler.sendEmptyMessage(NETWORK_ERR);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.d("yue.huang", "sendCancleFollowUserToServer:"+e.toString());
			mainHandler.sendEmptyMessage(NETWORK_ERR);
		}finally{
			if(null!=connection){
				connection.disconnect();
			}
		}
	}

	//----------------获取关注用户列表---------------
	public void getFollowerFansList(int type){
		Message msg = Message.obtain();
		msg.what = GET_FOLLOWERS;
		msg.arg1 = type;
		threadHandler.sendMessage(msg);
	}

	private ArrayList<FollowerFans> getFollowerListFromServer(int type){
		HttpsURLConnection connection =null;
		String access_key = ((WuZhaiApplication)getApplication()).getUser().getAccessKey();
		URL url = null;
		try {
			if(type == 0){
				url = new URL("https://www.5yuzhai.com:443/api/v1/following_relationships/followers?access_key="+access_key);
			}else {
				url = new URL("https://www.5yuzhai.com:443/api/v1/following_relationships/followeds?access_key="+access_key);
			}
//			connection = (HttpURLConnection)url.openConnection();
			connection = Utils.getHttpsConnection(url);
			connection.setConnectTimeout(5000);
			connection.setRequestMethod("GET");
            if(connection.getResponseCode() == 200){
                InputStream inputStream = connection.getInputStream();
                byte[] bytes = Utils.readStream(inputStream);
                String jsonString = new String(bytes);
                JSONArray resultJson = new JSONArray(jsonString);
                return followerListParse(resultJson);
            }
		} catch (Exception e) {
			e.printStackTrace();
			Log.d("yue.huang", "getFollowerListFromServer:"+e.toString());
			return null;
		}finally{
			if(null!=connection){
				connection.disconnect();
			}
		}
		return null;
	}

	private ArrayList<FollowerFans> followerListParse(JSONArray jsonArray) throws JSONException{
		ArrayList<FollowerFans> followerList = new ArrayList<FollowerFans>();
		for(int n=0;n<jsonArray.length();n++){
			followerList.add(parseFollower(jsonArray.getJSONObject(n)));
		}
		return followerList;
	}

	private FollowerFans parseFollower(JSONObject jsonObject) throws JSONException{
		FollowerFans follower = new FollowerFans();
		follower.setId(jsonObject.getInt("id"));
		follower.setUsername(jsonObject.getString("username"));
		follower.setAvatarPath(jsonObject.getString("avatar"));
		follower.setSex(jsonObject.optInt("sex",-1));
		follower.setLevel(jsonObject.getInt("level"));
		follower.setSignature(jsonObject.getString("signature"));
		follower.setImToken(jsonObject.getString("im_token"));
		follower.setFollowed(jsonObject.getBoolean("followed"));
		return follower;
	}
	//----------------获取精彩推荐---------------
	public void getRecommendationsList(int page){
		Log.d("yue.huang", "page:"+page);
		Message msg = Message.obtain();
		msg.what = GET_RECOMMENDATIONS;
		msg.arg1 = page;
		threadHandler.sendMessage(msg);
	}

	private ArrayList<News> getRecommendationsListFromServer(int page){
		HttpsURLConnection connection =null;
		try {
			URL url = new URL("https://www.5yuzhai.com:443/api/v1/recommendations?page="+page+"&per=15");
//			connection = (HttpURLConnection)url.openConnection();
			connection = Utils.getHttpsConnection(url);
			connection.setConnectTimeout(5000);
			connection.setRequestMethod("GET");
            if(connection.getResponseCode() == 200){
                InputStream inputStream = connection.getInputStream();
                byte[] bytes = Utils.readStream(inputStream);
                String jsonString = new String(bytes);
                JSONObject resultJson = new JSONObject(jsonString);
//                int current_page = resultJson.optInt("current_page");
                recommendationsNextPage = resultJson.optInt("next_page");
                JSONArray newsList = resultJson.getJSONArray("list");
                return newsListParse(newsList);
            }
		} catch (Exception e) {
			e.printStackTrace();
			Log.d("yue.huang", "getRecommendationsListFromServer:"+e.toString());
			return null;
		}finally{
			if(null!=connection){
				connection.disconnect();
			}
		}
		return null;
	}

	private ArrayList<News> newsListParse(JSONArray jsonArray) throws JSONException{
		ArrayList<News> newsList = new ArrayList<News>();
		for(int n=0;n<jsonArray.length();n++){
			newsList.add(parseNews(jsonArray.getJSONObject(n)));
		}
		return newsList;
	}

	private News parseNews(JSONObject jsonObject) throws JSONException{
		int id = jsonObject.getInt("id");
		String desc = jsonObject.getString("desc");
		String bref = jsonObject.getString("bref");
		String title = jsonObject.getString("title");
		String image_url = jsonObject.getString("image_url");
		News news = new News(id, image_url, title, bref, desc);
		return news;
	}
	//----------------获取首页推荐---------------
	public void getHomeRecommendations(int count){
		Message msg = Message.obtain();
		msg.what = GET_HOME_RECOMMENDATION;
		msg.arg1 = count;
		threadHandler.sendMessage(msg);
	}
	private ArrayList<News> getHomeRecommendationsFromServer(int count){
		HttpsURLConnection connection =null;
		try {
			URL url = new URL("https://www.5yuzhai.com:443/api/v1/recommendations/latest?count="+count);
			connection = Utils.getHttpsConnection(url);
			connection.setConnectTimeout(5000);
			connection.setRequestMethod("GET");
            if(connection.getResponseCode() == 200){
                InputStream inputStream = connection.getInputStream();
                byte[] bytes = Utils.readStream(inputStream);
                String jsonString = new String(bytes);
                JSONArray resultJson = new JSONArray(jsonString);
                return newsListParse(resultJson);
            }
		} catch (Exception e) {
			e.printStackTrace();
			Log.d("yue.huang", "getHomeRecommendationsFromServer:"+e.toString());
			return null;
		}finally{
			if(null!=connection){
				connection.disconnect();
			}
		}
		return null;
	}
	//----------------上传用户资料---------------
	public void saveUserInfo(String avatar,String username,String birthday,String signature,String sex){
		Message msg = Message.obtain();
		msg.what = SAVE_USER_INFO;
		HashMap<String, String> parametersMap = new HashMap<String, String>();
		parametersMap.put("access_key", ((WuZhaiApplication)getApplication()).getUser().getAccessKey());
		if(avatar!=null){
			parametersMap.put("avatar", avatar);
		}
		parametersMap.put("username", username);
		parametersMap.put("birthday", birthday);
		parametersMap.put("signature", signature);
		if(sex.equals("男")){
			parametersMap.put("sex", "1");
		}else if(sex.equals("女")){
			parametersMap.put("sex", "2");
		}else {
			parametersMap.put("sex", "0");
		}
		msg.obj = parametersMap;
		threadHandler.sendMessage(msg);
	}

	private void uploadUserInfoToServer(HashMap<String,String> parametersMap){
		String access_key = ((WuZhaiApplication)getApplication()).getUser().getAccessKey();
		String url = "https://www.5yuzhai.com:443/api/v1/users/setting?access_key="+access_key;
		HttpsURLConnection connection = null;
		try {
			connection = Utils.getHttpsConnectionPost(url, parametersMap);
			if (connection.getResponseCode() == 200) {
				InputStream inputStream = connection.getInputStream();
				byte[] bytes = Utils.readStream(inputStream);
				String jsonString = new String(bytes);
				JSONObject resultJson = new JSONObject(jsonString);
				if (resultJson.getBoolean("success")) {
					mainHandler.sendEmptyMessage(SAVE_USER_INFO_SUCC);
					getUserInfo();//update user info
				} else {
					mainHandler.sendEmptyMessage(SAVE_USER_INFO_FAL);
				}
			} else {
				mainHandler.sendEmptyMessage(NETWORK_ERR);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.d("yue.huang", "uploadUserInfoToServer:"+e.toString());
			mainHandler.sendEmptyMessage(NETWORK_ERR);
		}finally{
			if(null!=connection){
				connection.disconnect();
			}
		}
	}
	//----------------获取商品列表---------------
	public void getGoods(String city,String commodityCategoryId,String commoditySortType,int page){
		Message msg = Message.obtain();
		HashMap<String, String> parametersMap = new HashMap<String, String>();
		parametersMap.put("city", city);
		parametersMap.put("commodity_category_id", city);
		parametersMap.put("commodity_sort_type", commoditySortType);
	    parametersMap.put("commodity_category_id", commodityCategoryId);

		msg.obj = parametersMap;
		msg.arg1 = page;
		msg.what = GET_GOODS;
		threadHandler.sendMessage(msg);
	}

	private ArrayList<Goods> getGoodsListFromServer(HashMap<String, String> parametersMap,int page){
		HttpsURLConnection connection =null;
		StringBuilder path = new StringBuilder("https://www.5yuzhai.com:443/api/v1/commodities?page="+page+"&per=20");
		String city = parametersMap.get("city");
		String commodity_category_id = parametersMap.get("commodity_category_id");
		String commodity_sort_type = parametersMap.get("commodity_sort_type");

		try {
			if(city!=null){
				String cityUTF = URLEncoder.encode(city, "UTF-8");
				path.append("&city="+cityUTF);
			}if(commodity_category_id!=null){
				path.append("&commodity_category_id="+commodity_category_id);
			}if(commodity_sort_type!=null){
				path.append("&commodity_sort_type="+commodity_sort_type);
			}
			URL url = new URL(path.toString());
			connection = Utils.getHttpsConnection(url);
			connection.setConnectTimeout(5000);
			connection.setRequestMethod("GET");
            if(connection.getResponseCode() == 200){
                InputStream inputStream = connection.getInputStream();
                byte[] bytes = Utils.readStream(inputStream);
                String jsonString = new String(bytes);
                JSONObject resultJson = new JSONObject(jsonString);
                goodsNextPage = resultJson.optInt("next_page");
                JSONArray goodsList = resultJson.getJSONArray("list");
                return goodsListParse(goodsList);
            }
		} catch (Exception e) {
			e.printStackTrace();
			Log.d("yue.huang", "getGoodsListFromServer:"+e.toString());
			return null;
		}finally{
			if(null!=connection){
				connection.disconnect();
			}
		}
		return null;
	}

	private ArrayList<Goods> goodsListParse(JSONArray jsonArray) throws JSONException{
		ArrayList<Goods> goodsList = new ArrayList<Goods>();
		for(int n=0;n<jsonArray.length();n++){
			goodsList.add(parseGoods(jsonArray.getJSONObject(n)));
		}
		return goodsList;
	}

	private Goods parseGoods(JSONObject jsonObject) throws JSONException{
		Goods goods = new Goods();
		goods.setId(jsonObject.getInt("id"));
		goods.setTitle(jsonObject.getString("title"));
		goods.setDesc(jsonObject.getString("desc"));
		goods.setCommodityCategoryId(jsonObject.getInt("commodity_category_id"));
		goods.setCommodityCategory(jsonObject.getString("commodity_category"));
		goods.setPublisherName(jsonObject.getString("username"));
		goods.setPublisherAvatar(jsonObject.getString("avatar"));
		goods.setPrice(jsonObject.getInt("price"));
		goods.setCity(jsonObject.getString("city"));
		goods.setDetialImages(jsonObject.getString("images"));
		goods.setMainImage(jsonObject.getString("image"));
		goods.setTags(jsonObject.getString("tags"));
		goods.setViewCount(jsonObject.optInt("view_count"));
		goods.setStock(jsonObject.optInt("stock"));
		goods.setSaleCount(jsonObject.optInt("sale_count",0));
		goods.setOrderCount(jsonObject.optInt("order_count",0));
		goods.setCommentsCount(jsonObject.optInt("comments_count"));
		JSONObject coordinate = jsonObject.getJSONObject("coordinate");
		goods.setLongitude(coordinate.optDouble("longitude",-1));
		goods.setLatitude(coordinate.optDouble("latitude",-1));
		goods.setCollected(jsonObject.getBoolean("collected"));
		return goods;
	}
	//----------------发布商品---------------
	public void publishGoods(int commodity_category_id,String title,String city,String desc,String mainImage,
			String detialImage,float price,int stock,String tags,String coordinate){
		Message msg = Message.obtain();
		msg.what = PUBLISH_GOODS;
		HashMap<String, String> parametersMap = new HashMap<String, String>();
		parametersMap.put("access_key", ((WuZhaiApplication)getApplication()).getUser().getAccessKey());
		parametersMap.put("commodity_category_id", commodity_category_id+"");
		parametersMap.put("title", title);
		parametersMap.put("city", city);
		parametersMap.put("desc", desc);
		parametersMap.put("image", mainImage);
		parametersMap.put("images", detialImage);
		parametersMap.put("price", price+"");
		parametersMap.put("stock", stock+"");
		parametersMap.put("tags", tags);
		parametersMap.put("coordinate", coordinate);

		msg.obj = parametersMap;
		threadHandler.sendMessage(msg);
	}

	private void publishGoodsToServer(HashMap<String,String> parametersMap){
		String access_key = ((WuZhaiApplication)getApplication()).getUser().getAccessKey();
		String url = "https://www.5yuzhai.com:443/api/v1/commodities?access_key="+access_key;
		HttpsURLConnection connection = null;
		try {
			connection = Utils.getHttpsConnectionPost(url, parametersMap);
			if (connection.getResponseCode() == 200) {
				InputStream inputStream = connection.getInputStream();
				byte[] bytes = Utils.readStream(inputStream);
				String jsonString = new String(bytes);
				JSONObject resultJson = new JSONObject(jsonString);
				if (resultJson.getBoolean("success")) {
					mainHandler.sendEmptyMessage(PUBLISH_GOODS_SUCC);
				} else {
					mainHandler.sendEmptyMessage(PUBLISH_GOODS_FAL);
				}
			} else {
				mainHandler.sendEmptyMessage(NETWORK_ERR);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.d("yue.huang", "publishGoodsToServer:"+e.toString());
			mainHandler.sendEmptyMessage(NETWORK_ERR);
		}finally{
			if(null!=connection){
				connection.disconnect();
			}
		}
	}
	//----------------获取tags---------------
	public void getTags(String type){
		Message msg = Message.obtain();
		msg.what=GET_TAGS;
		msg.obj = type;
		threadHandler.sendMessage(msg);
	}

	private ArrayList<Tag> getTagsFromServer(String type){
		HttpsURLConnection connection =null;
		try {
			URL url = new URL("https://www.5yuzhai.com:443/api/v1/tags?category="+type);
			connection = Utils.getHttpsConnection(url);
			connection.setConnectTimeout(5000);
			connection.setRequestMethod("GET");
            if(connection.getResponseCode() == 200){
                InputStream inputStream = connection.getInputStream();
                byte[] bytes = Utils.readStream(inputStream);
                String jsonString = new String(bytes);
                JSONObject resultJson = new JSONObject(jsonString);
                JSONArray tagList = resultJson.getJSONArray("data");
                return tagListParse(tagList);
            }
		} catch (Exception e) {
			e.printStackTrace();
			Log.d("yue.huang", "getTagsFromServer:"+e.toString());
			return null;
		}finally{
			if(null!=connection){
				connection.disconnect();
			}
		}
		return null;
	}

	private ArrayList<Tag> tagListParse(JSONArray jsonArray) throws JSONException{
		ArrayList<Tag> tagList = new ArrayList<Tag>();
		for(int n=0;n<jsonArray.length();n++){
			tagList.add(parseTag(jsonArray.getJSONObject(n)));
		}
		return tagList;
	}

	private Tag parseTag(JSONObject jsonObject) throws JSONException{
		Tag tag = new Tag();
		tag.setId(jsonObject.getInt("id"));
		tag.setDesc(jsonObject.getString("desc"));
		return tag;
	}
	//----------------获取类型---------------
	public void getType(String type){
		Message msg = Message.obtain();
		msg.what=GET_TYPE;
		msg.obj = type;
		threadHandler.sendMessage(msg);
	}

	private HashMap<Integer, String> getTypeFromServer(String type){
		HttpsURLConnection connection =null;
		try {
			URL url = new URL("https://www.5yuzhai.com:443/api/v1/selects/"+type);
			connection = Utils.getHttpsConnection(url);
			connection.setConnectTimeout(5000);
			connection.setRequestMethod("GET");
            if(connection.getResponseCode() == 200){
                InputStream inputStream = connection.getInputStream();
                byte[] bytes = Utils.readStream(inputStream);
                String jsonString = new String(bytes);
                JSONObject resultJson = new JSONObject(jsonString);
                JSONArray goodsTypeList = resultJson.getJSONArray("data");
                return typeParse(goodsTypeList);
//                JSONArray resultJson = new JSONArray(jsonString);
//                return typeParse(resultJson);
            }
		} catch (Exception e) {
			e.printStackTrace();
			Log.d("yue.huang", "getTypeFromServer:"+e.toString());
			return null;
		}finally{
			if(null!=connection){
				connection.disconnect();
			}
		}
		return null;
	}

	private HashMap<Integer, String> typeParse(JSONArray jsonArray) throws JSONException{
		HashMap<Integer, String> typeMap = new HashMap<Integer, String>();
		for(int n=0;n<jsonArray.length();n++){
			JSONObject typeJson= jsonArray.getJSONObject(n);
			typeMap.put(typeJson.getInt("key"), typeJson.getString("value"));
		}
		return typeMap;
	}
	//----------------获取后期服务---------------
	public void getAftercaseService(int page){
		Message msg = Message.obtain();
		msg.what = GET_AFTERCASE;
		msg.arg1 = page;
		threadHandler.sendMessage(msg);
	}

	private ArrayList<Aftercases> getAftercaseServiceFromServer(int page){
		String access_key = ((WuZhaiApplication)getApplication()).getUser().getAccessKey();
		HttpsURLConnection connection =null;
		try {
			URL url = new URL("https://www.5yuzhai.com:443/api/v1/processing_services?access_key="+access_key+"&page="+page+"&per=20");
			connection = Utils.getHttpsConnection(url);
			connection.setConnectTimeout(5000);
			connection.setRequestMethod("GET");
            if(connection.getResponseCode() == 200){
                InputStream inputStream = connection.getInputStream();
                byte[] bytes = Utils.readStream(inputStream);
                String jsonString = new String(bytes);
                JSONObject resultJson = new JSONObject(jsonString);
                aftercaseNextPage = resultJson.optInt("next_page");
                JSONArray aftercaseList = resultJson.getJSONArray("list");
                return aftercaseListParse(aftercaseList);
            }
		} catch (Exception e) {
			e.printStackTrace();
			Log.d("yue.huang", "getAftercaseServiceFromServer:"+e.toString());
			return null;
		}finally{
			if(null!=connection){
				connection.disconnect();
			}
		}
		return null;
	}

	private ArrayList<Aftercases> aftercaseListParse(JSONArray jsonArray) throws JSONException{
		ArrayList<Aftercases> aftercasesList = new ArrayList<Aftercases>();
		for(int n=0;n<jsonArray.length();n++){
			aftercasesList.add(parseAftercases(jsonArray.getJSONObject(n)));
		}
		return aftercasesList;
	}

	private Aftercases parseAftercases(JSONObject jsonObject) throws JSONException{
		Aftercases aftercases = new Aftercases();
		aftercases.setId(jsonObject.getInt("id"));
		aftercases.setTitle(jsonObject.getString("title"));
		aftercases.setDesc(jsonObject.getString("desc"));
		aftercases.setPublisherId(jsonObject.getInt("user_id"));
		aftercases.setPublisherName(jsonObject.getString("username"));
		aftercases.setPublisherAvatar(jsonObject.getString("avatar"));
		aftercases.setCity(jsonObject.optString("city","成都"));
		aftercases.setPrice((float)jsonObject.getDouble("price"));
		aftercases.setDetialImages(jsonObject.getString("images"));
		aftercases.setMainImage(jsonObject.getString("image"));
		aftercases.setTags(jsonObject.getString("tags"));
		JSONObject coordinate = jsonObject.getJSONObject("coordinate");
		aftercases.setLongitude(coordinate.optDouble("longitude",-1));
		aftercases.setLatitude(coordinate.optDouble("latitude",-1));
		aftercases.setBrief(jsonObject.getString("brief"));
		aftercases.setCommentsCount(jsonObject.getInt("comments_count"));
		aftercases.setFollowed(jsonObject.getBoolean("followed"));
		aftercases.setCollected(jsonObject.getBoolean("collected"));
		return aftercases;
	}
	//----------------发布后期服务---------------
	public void publishAftercases(String title,String brief,String city,String desc,String mainImage,
			String detialImage,float price,String tags,String coordinate){
		Message msg = Message.obtain();
		msg.what = PUBLISH_AFTERCASE;
		HashMap<String, String> parametersMap = new HashMap<String, String>();
		parametersMap.put("access_key", ((WuZhaiApplication)getApplication()).getUser().getAccessKey());
		parametersMap.put("title", title);
		parametersMap.put("city", city);
		parametersMap.put("brief", title);
		parametersMap.put("desc", desc);
		parametersMap.put("image", mainImage);
		parametersMap.put("images", detialImage);
		parametersMap.put("price", price+"");
		parametersMap.put("tags", tags);
		parametersMap.put("coordinate", coordinate);

		msg.obj = parametersMap;
		threadHandler.sendMessage(msg);
	}

	private void publishAftercasesToServer(HashMap<String,String> parametersMap){
		String access_key = ((WuZhaiApplication)getApplication()).getUser().getAccessKey();
		String url = "https://www.5yuzhai.com:443/api/v1/processing_services?access_key="+access_key;
		HttpsURLConnection connection = null;
		try {
			connection = Utils.getHttpsConnectionPost(url, parametersMap);
			if (connection.getResponseCode() == 200) {
				InputStream inputStream = connection.getInputStream();
				byte[] bytes = Utils.readStream(inputStream);
				String jsonString = new String(bytes);
				JSONObject resultJson = new JSONObject(jsonString);
				if (resultJson.getBoolean("success")) {
					mainHandler.sendEmptyMessage(PUBLISH_AFTERCASE_SUCC);
				} else {
					mainHandler.sendEmptyMessage(PUBLISH_AFTERCASE_FAL);
				}
			} else {
				mainHandler.sendEmptyMessage(NETWORK_ERR);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.d("yue.huang", "publishAftercasesToServer:"+e.toString());
			mainHandler.sendEmptyMessage(NETWORK_ERR);
		}finally{
			if(null!=connection){
				connection.disconnect();
			}
		}
	}

	//----------------获取摄影师服务---------------
	public void getPhotographerService(int page){
		Message msg = Message.obtain();
		msg.what = GET_PHOTOGRAPHER;
		msg.arg1 = page;
		threadHandler.sendMessage(msg);
	}

	private ArrayList<Photographer> getPhotographerServiceFromServer(int page){
		String access_key = ((WuZhaiApplication)getApplication()).getUser().getAccessKey();
		HttpsURLConnection connection =null;
		try {
			URL url = new URL("https://www.5yuzhai.com:443/api/v1/photography_services?access_key="+access_key+"&page="+page+"&per=20");
			connection = Utils.getHttpsConnection(url);
			connection.setConnectTimeout(5000);
			connection.setRequestMethod("GET");
            if(connection.getResponseCode() == 200){
                InputStream inputStream = connection.getInputStream();
                byte[] bytes = Utils.readStream(inputStream);
                String jsonString = new String(bytes);
                JSONObject resultJson = new JSONObject(jsonString);
                photographerNextPage = resultJson.optInt("next_page");
                JSONArray photographerList = resultJson.getJSONArray("list");
                return photographerListParse(photographerList);
            }
		} catch (Exception e) {
			e.printStackTrace();
			Log.d("yue.huang", "getPhotographerServiceFromServer:"+e.toString());
			return null;
		}finally{
			if(null!=connection){
				connection.disconnect();
			}
		}
		return null;
	}

	private ArrayList<Photographer> photographerListParse(JSONArray jsonArray) throws JSONException{
		ArrayList<Photographer> photographerList = new ArrayList<Photographer>();
		for(int n=0;n<jsonArray.length();n++){
			photographerList.add(parsePhotographer(jsonArray.getJSONObject(n)));
		}
		return photographerList;
	}

	private Photographer parsePhotographer(JSONObject jsonObject) throws JSONException{
		Photographer photographer = new Photographer();
		photographer.setId(jsonObject.getInt("id"));
		photographer.setTitle(jsonObject.getString("title"));
		photographer.setDesc(jsonObject.getString("desc"));
		photographer.setPublisherId(jsonObject.getInt("user_id"));
		photographer.setPublisherName(jsonObject.getString("username"));
		photographer.setPublisherAvatar(jsonObject.getString("avatar"));
		photographer.setCity(jsonObject.optString("city","成都"));
		photographer.setPrice((float)jsonObject.getDouble("price"));
		photographer.setDetialImages(jsonObject.getString("images"));
		photographer.setMainImage(jsonObject.getString("image"));
		photographer.setTags(jsonObject.getString("tags"));
		JSONObject coordinate = jsonObject.getJSONObject("coordinate");
		photographer.setLongitude(coordinate.optDouble("longitude",-1));
		photographer.setLatitude(coordinate.optDouble("latitude",-1));
		photographer.setBrief(jsonObject.getString("brief"));
		photographer.setCommentsCount(jsonObject.getInt("comments_count"));
		photographer.setFollowed(jsonObject.getBoolean("followed"));
		photographer.setCollected(jsonObject.getBoolean("collected"));
		return photographer;
	}
	//----------------发布摄影师服务---------------
	public void publishPhotographer(String title,String brief,String city,String desc,String mainImage,
			String detialImage,float price,String tags,String coordinate){
		Message msg = Message.obtain();
		msg.what = PUBLISH_PHOTOGRAPHER;
		HashMap<String, String> parametersMap = new HashMap<String, String>();
		parametersMap.put("access_key", ((WuZhaiApplication)getApplication()).getUser().getAccessKey());
		parametersMap.put("title", title);
		parametersMap.put("city", city);
		parametersMap.put("brief", title);
		parametersMap.put("desc", desc);
		parametersMap.put("image", mainImage);
		parametersMap.put("images", detialImage);
		parametersMap.put("price", price+"");
		parametersMap.put("tags", tags);
		parametersMap.put("coordinate", coordinate);

		msg.obj = parametersMap;
		threadHandler.sendMessage(msg);
	}

	private void publishPhotographerToServer(HashMap<String,String> parametersMap){
		String access_key = ((WuZhaiApplication)getApplication()).getUser().getAccessKey();
		String url = "https://www.5yuzhai.com:443/api/v1/photography_services?access_key="+access_key;
		HttpsURLConnection connection = null;
		try {
			connection = Utils.getHttpsConnectionPost(url, parametersMap);
			if (connection.getResponseCode() == 200) {
				InputStream inputStream = connection.getInputStream();
				byte[] bytes = Utils.readStream(inputStream);
				String jsonString = new String(bytes);
				JSONObject resultJson = new JSONObject(jsonString);
				if (resultJson.getBoolean("success")) {
					mainHandler.sendEmptyMessage(PUBLISH_PHOTOGRAPHER_SUCC);
				} else {
					mainHandler.sendEmptyMessage(PUBLISH_PHOTOGRAPHER_FAL);
				}
			} else {
				mainHandler.sendEmptyMessage(NETWORK_ERR);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.d("yue.huang", "publishPhotographerToServer:"+e.toString());
			mainHandler.sendEmptyMessage(NETWORK_ERR);
		}finally{
			if(null!=connection){
				connection.disconnect();
			}
		}
	}
	//----------------收藏---------------
	public void collection(String collectionable_type,int collectionable_id){
		Message msg = Message.obtain();
		msg.what = COLLECTE;
		msg.arg1 = collectionable_id;
		msg.obj = collectionable_type;
		threadHandler.sendMessage(msg);
	}

	private void sendCollectionToServer(HashMap<String,String> parametersMap){
		String access_key = ((WuZhaiApplication)getApplication()).getUser().getAccessKey();
		String url = "https://www.5yuzhai.com:443/api/v1/collections?access_key="+access_key;
		HttpsURLConnection connection = null;
		try {
			connection = Utils.getHttpsConnectionPost(url, parametersMap);
			if (connection.getResponseCode() == 200) {
				InputStream inputStream = connection.getInputStream();
				byte[] bytes = Utils.readStream(inputStream);
				String jsonString = new String(bytes);
				JSONObject resultJson = new JSONObject(jsonString);
				if (resultJson.getBoolean("success")) {
					mainHandler.sendEmptyMessage(COLLECTE_SUCC);
				} else {
					mainHandler.sendEmptyMessage(COLLECTE_FAL);
				}
			} else {
				mainHandler.sendEmptyMessage(NETWORK_ERR);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.d("yue.huang", "sendCollectionToServer:"+e.toString());
			mainHandler.sendEmptyMessage(NETWORK_ERR);
		}finally{
			if(null!=connection){
				connection.disconnect();
			}
		}
	}
	//----------------获取收藏列表---------------
	public void getCollectionableList(int page){
		Message msg = Message.obtain();
		msg.what = GET_COLLECTIONABLES;
		msg.arg1 = page;
		threadHandler.sendMessage(msg);
	}

	private ArrayList<Collectionable> getCollectionableListFromServer(int page){
		HttpsURLConnection connection =null;
		String access_key = ((WuZhaiApplication)getApplication()).getUser().getAccessKey();
		try {
			URL url = new URL("https://www.5yuzhai.com:443/api/v1/collections?access_key="+access_key+"&page="+page+"&per=20");
			connection = Utils.getHttpsConnection(url);
			connection.setConnectTimeout(5000);
			connection.setRequestMethod("GET");
            if(connection.getResponseCode() == 200){
                InputStream inputStream = connection.getInputStream();
                byte[] bytes = Utils.readStream(inputStream);
                String jsonString = new String(bytes);
                JSONObject resultJson = new JSONObject(jsonString);
                collectionableNextPage = resultJson.optInt("next_page");
                JSONArray collectionableList = resultJson.getJSONArray("list");
                return collectionableListParse(collectionableList);
            }
		} catch (Exception e) {
			e.printStackTrace();
			Log.d("yue.huang", "getCollectionableListFromServer:"+e.toString());
			return null;
		}finally{
			if(null!=connection){
				connection.disconnect();
			}
		}
		return null;
	}

	private ArrayList<Collectionable> collectionableListParse(JSONArray jsonArray) throws JSONException{
		ArrayList<Collectionable> followerList = new ArrayList<Collectionable>();
		for(int n=0;n<jsonArray.length();n++){
			followerList.add(parseCollectionable(jsonArray.getJSONObject(n)));
		}
		return followerList;
	}

	private Collectionable parseCollectionable(JSONObject jsonObject) throws JSONException{
		Collectionable collectionable = new Collectionable();
		collectionable.setCollectionableId(jsonObject.getInt("collectionable_id"));
		collectionable.setCollectionableType(jsonObject.getString("collectionable_type"));
		return collectionable;
	}
    //---------取消收藏----------------------
	public void cancleCollection(String collectionable_type,int collectionable_id){
		Message msg = Message.obtain();
		msg.what = CANCLE_COLLECTE;
		msg.arg1 = collectionable_id;
		msg.obj = collectionable_type;
		threadHandler.sendMessage(msg);
	}

	private void sendCancleCollectionToServer(HashMap<String,String> parametersMap){
		String access_key = ((WuZhaiApplication)getApplication()).getUser().getAccessKey();
		String url = "https://www.5yuzhai.com:443/api/v1/collections/cancel?access_key="+access_key;
		HttpsURLConnection connection = null;
		try {
			connection = Utils.getHttpsConnectionPost(url, parametersMap);
			if (connection.getResponseCode() == 200) {
				InputStream inputStream = connection.getInputStream();
				byte[] bytes = Utils.readStream(inputStream);
				String jsonString = new String(bytes);
				JSONObject resultJson = new JSONObject(jsonString);
				if (resultJson.getBoolean("success")) {
					mainHandler.sendEmptyMessage(CANCLE_COLLECTE_SUCC);
				} else {
					mainHandler.sendEmptyMessage(CANCLE_COLLECTE_FAL);
				}
			} else {
				mainHandler.sendEmptyMessage(NETWORK_ERR);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.d("yue.huang", "sendCancleCollectionToServer:"+e.toString());
			mainHandler.sendEmptyMessage(NETWORK_ERR);
		}finally{
			if(null!=connection){
				connection.disconnect();
			}
		}
	}

	//---------商品详情----------------------
	public void getGoodsDetial(int goodsId){
		Message msg = Message.obtain();
		msg.what = GET_GOODS_DETIAL;
		msg.arg1 = goodsId;
		threadHandler.sendMessage(msg);
	}

	private Goods getGoodsDetialFromServer(int goodId){
		HttpsURLConnection connection =null;
		String access_key = ((WuZhaiApplication)getApplication()).getUser().getAccessKey();
		try {
			URL url = new URL("https://www.5yuzhai.com:443/api/v1/commodities/do_view?access_key="+access_key+"&id="+goodId);
			connection = Utils.getHttpsConnection(url);
			connection.setConnectTimeout(5000);
			connection.setRequestMethod("GET");
            if(connection.getResponseCode() == 200){
                InputStream inputStream = connection.getInputStream();
                byte[] bytes = Utils.readStream(inputStream);
                String jsonString = new String(bytes);
                JSONObject resultJson = new JSONObject(jsonString);
                JSONObject goods = resultJson.getJSONObject("data");
                return parseGoods(goods);
            }
		} catch (Exception e) {
			e.printStackTrace();
			Log.d("yue.huang", "getGoodsDetialFromServer:"+e.toString());
			return null;
		}finally{
			if(null!=connection){
				connection.disconnect();
			}
		}
		return null;
	}
	//------------获取精选视频--------------
	public void getSelectedVideo(int page){
		Message msg = Message.obtain();
		msg.what = GET_SELECTED_VIDOE;
		msg.arg1 = page;
		threadHandler.sendMessage(msg);
	}

	private ArrayList<VideoEntity> getSelectedVideoFromServer(int page){
		HttpsURLConnection connection =null;
		String access_key = ((WuZhaiApplication)getApplication()).getUser().getAccessKey();
		try {
			URL url = new URL("https://www.5yuzhai.com:443/api/v1/videos/best?access_key="+access_key+"&page="+page+"&per=20");
			connection = Utils.getHttpsConnection(url);
			connection.setConnectTimeout(5000);
			connection.setRequestMethod("GET");
            if(connection.getResponseCode() == 200){
                InputStream inputStream = connection.getInputStream();
                byte[] bytes = Utils.readStream(inputStream);
                String jsonString = new String(bytes);
                JSONObject resultJson = new JSONObject(jsonString);
                selectedVideoNextPage = resultJson.optInt("next_page");
                JSONArray videoList = resultJson.getJSONArray("list");
                return videoListParse(videoList);
            }
		} catch (Exception e) {
			e.printStackTrace();
			Log.d("yue.huang", "getCollectionableListFromServer:"+e.toString());
			return null;
		}finally{
			if(null!=connection){
				connection.disconnect();
			}
		}
		return null;
	}

	private ArrayList<VideoEntity> videoListParse(JSONArray jsonArray) throws JSONException{
		ArrayList<VideoEntity> videoList = new ArrayList<VideoEntity>();
		for(int n=0;n<jsonArray.length();n++){
			videoList.add(parseVideo(jsonArray.getJSONObject(n)));
		}
		return videoList;
	}

	private VideoEntity parseVideo(JSONObject jsonObject) throws JSONException{
		VideoEntity video = new VideoEntity();
		video.setId(jsonObject.getInt("id"));
		video.setTitle(jsonObject.getString("title"));
		video.setUrl(jsonObject.getString("url"));
		video.setPicturePath(jsonObject.getString("screenshot"));
		video.setDesc(jsonObject.getString("desc"));
		video.setPublisherId(jsonObject.getInt("user_id"));
		video.setPublisherName(jsonObject.getString("username"));
		video.setPublisherAvatar(jsonObject.getString("avatar"));
		video.setCreatedAt(jsonObject.getInt("created_at"));
		video.setLikesCount(jsonObject.getInt("likes_count"));
		video.setCommentsCount(jsonObject.getInt("comments_count"));
		video.setCollectionsCount(jsonObject.getInt("collections_count"));
		video.setLiked(jsonObject.getBoolean("liked"));
		video.setCollected(jsonObject.getBoolean("collected"));
		video.setFollowed(jsonObject.getBoolean("followed"));
		video.setPlayCount(jsonObject.getInt("play_count"));
		JSONObject coordinate = jsonObject.getJSONObject("coordinate");
		video.setLongitude(coordinate.optDouble("longitude",-1));
		video.setLatitude(coordinate.optDouble("latitude",-1));
		return video;
	}
	//------------获取最热视频--------------
	public void getHotVideos(){
		threadHandler.sendEmptyMessage(GET_HOT_VIDEO);
	}
	private ArrayList<VideoEntity> getHotVideosFromServer(){
		HttpsURLConnection connection =null;
		String access_key = ((WuZhaiApplication)getApplication()).getUser().getAccessKey();
		try {
			URL url = new URL("https://www.5yuzhai.com:443/api/v1/videos/hot?access_key="+access_key);
			connection = Utils.getHttpsConnection(url);
			connection.setConnectTimeout(5000);
			connection.setRequestMethod("GET");
            if(connection.getResponseCode() == 200){
                InputStream inputStream = connection.getInputStream();
                byte[] bytes = Utils.readStream(inputStream);
                String jsonString = new String(bytes);
                JSONArray resultJson = new JSONArray(jsonString);
                return videoListParse(resultJson);
            }
		} catch (Exception e) {
			e.printStackTrace();
			Log.d("yue.huang", "getHotVideosFromServer:"+e.toString());
			return null;
		}finally{
			if(null!=connection){
				connection.disconnect();
			}
		}
		return null;
	}
	//----------获取视频列表-------------
	public void getVideoList(String sortType,int page){
		Message msg = Message.obtain();
		msg.what = GET_VIDOE_LIST;
		Bundle bundle = new Bundle();
		bundle.putString("sorttype", sortType);
		bundle.putInt("page", page);
		msg.setData(bundle);
		threadHandler.sendMessage(msg);
	}

	private ArrayList<VideoEntity> getVideoListFromServer(String sortType,int page){
		HttpsURLConnection connection =null;
		String access_key = ((WuZhaiApplication)getApplication()).getUser().getAccessKey();
		try {
			URL url = new URL("https://www.5yuzhai.com:443/api/v1/videos?access_key="+access_key+"&page="+page+"&per=20&video_sort_type="+sortType);
			connection = Utils.getHttpsConnection(url);
			connection.setConnectTimeout(5000);
			connection.setRequestMethod("GET");
            if(connection.getResponseCode() == 200){
                InputStream inputStream = connection.getInputStream();
                byte[] bytes = Utils.readStream(inputStream);
                String jsonString = new String(bytes);
                JSONObject resultJson = new JSONObject(jsonString);
                videos_NextPage = resultJson.optInt("next_page");
                JSONArray videoList = resultJson.getJSONArray("list");
                return videoListParse(videoList);
            }
		} catch (Exception e) {
			e.printStackTrace();
			Log.d("yue.huang", "getCollectionableListFromServer:"+e.toString());
			return null;
		}finally{
			if(null!=connection){
				connection.disconnect();
			}
		}
		return null;
	}
//-----------连接融云------------------------
	public void connectToRongCloud(){
		Message msg = Message.obtain();
		msg.what = CONNECT_TO_RONG_CLOUD;
		threadHandler.sendMessage(msg);
	}

	private void connectToRongCloudServer(){
		if (getApplicationInfo().packageName.equals(WuZhaiApplication.getCurProcessName(getApplicationContext()))) {
			String token = ((WuZhaiApplication)getApplication()).getUser().getImToken();
			Log.d("yue.huang", "token:"+token);
	        /**
	         * IMKit SDK调用第二步,建立与服务器的连接
	         */
	        RongIM.connect(token, new RongIMClient.ConnectCallback() {

	            /**
	             * Token 错误，在线上环境下主要是因为 Token 已经过期，您需要向 App Server 重新请求一个新的 Token
	             */
	            @Override
	            public void onTokenIncorrect() {

	                Log.d("yue.huang", "--onTokenIncorrect");
	            }

	            /**
	             * 连接融云成功
	             * @param userid 当前 token
	             */
	            @Override
	            public void onSuccess(String userid) {
	            	Log.d("yue.huang", "连接融云成功");
//	            	handler.sendEmptyMessage(CONNECT_TO_RONG_CLOUD_SUCC);
					RongIM.setUserInfoProvider(new UserInfoProvider() {
						User user = ((WuZhaiApplication) getApplication()).getUser();
						@Override
						public UserInfo getUserInfo(String arg0) {
							Log.d("yue.huang", "------userId:"+arg0);
							UserInfo userInfo = new UserInfo(arg0, user
									.getName(), Uri.parse(user.getAvatarUrl()));
							return userInfo;
						}
					}, true);
	            }

	            /**
	             * 连接融云失败
	             * @param errorCode 错误码，可到官网 查看错误码对应的注释
	             */
	            @Override
	            public void onError(RongIMClient.ErrorCode errorCode) {
	            	Log.d("yue.huang", "连接融云失败");
//	            	handler.sendEmptyMessage(CONNECT_TO_RONG_CLOUD_FAL);
	            }
	        });
	    }
	}
	//-----------创建订单----------------------
	public void createOrder(int commodityId,int amount,int contactId){
		Message msg = Message.obtain();
		msg.what = CREATE_ORDER;
		HashMap<String, String> parametersMap = new HashMap<String, String>();
		parametersMap.put("access_key", ((WuZhaiApplication)getApplication()).getUser().getAccessKey());
		parametersMap.put("commodity_id", commodityId+"");
		parametersMap.put("amount", amount+"");
		parametersMap.put("contact_id", contactId+"");
		msg.obj = parametersMap;
		threadHandler.sendMessage(msg);
	}

	private int createOrderToServer(HashMap<String, String> parametersMap){
		String access_key = ((WuZhaiApplication)getApplication()).getUser().getAccessKey();
		String url = "https://www.5yuzhai.com:443/api/v1/orders?access_key="+access_key;
		HttpsURLConnection connection = null;
		try {
			connection = Utils.getHttpsConnectionPost(url, parametersMap);
			if (connection.getResponseCode() == 200) {
				InputStream inputStream = connection.getInputStream();
				byte[] bytes = Utils.readStream(inputStream);
				String jsonString = new String(bytes);
				JSONObject resultJson = new JSONObject(jsonString);
				if (resultJson.getBoolean("success")) {
					return paseOrderId(resultJson.getJSONObject("data"));
				} else {
					Toast.makeText(this, resultJson.getString("msg"), Toast.LENGTH_SHORT).show();
					return -1;
				}
			} else {
				return -1;
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.d("yue.huang", "createOrderToServer:"+e.toString());
			return -1;
		}finally{
			if(null!=connection){
				connection.disconnect();
			}
		}
	}

	private int paseOrderId(JSONObject jsonObject) throws JSONException{
		return jsonObject.getInt("id");
	}
	//-----------账户支付----------------------
	public void payWithBalance(int order_id){
		Message msg = Message.obtain();
		msg.what = PAY_WITH_BALANCE;
		HashMap<String, String> parametersMap = new HashMap<String, String>();
		parametersMap.put("access_key", ((WuZhaiApplication)getApplication()).getUser().getAccessKey());
		parametersMap.put("order_id", order_id+"");
		msg.obj = parametersMap;
		threadHandler.sendMessage(msg);
	}

	private void payWithBalanceToServer(HashMap<String, String> parametersMap){
		String access_key = ((WuZhaiApplication)getApplication()).getUser().getAccessKey();
		String url = "https://www.5yuzhai.com:443/api/v1/orders/buyer_pay?access_key="+access_key;
		HttpsURLConnection connection = null;
		try {
			connection = Utils.getHttpsConnectionPost(url, parametersMap);
			if (connection.getResponseCode() == 200) {
				InputStream inputStream = connection.getInputStream();
				byte[] bytes = Utils.readStream(inputStream);
				String jsonString = new String(bytes);
				JSONObject resultJson = new JSONObject(jsonString);
				Message msg = Message.obtain();
				msg.what = PAY_WITH_BALANCE_DONE;
				msg.obj = resultJson.getString("msg");
				mainHandler.sendMessage(msg);
			} else {
				Log.d("yue.huang", "payWithBalanceToServer:"+connection.getResponseCode());
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.d("yue.huang", "payWithBalanceToServer:"+e.toString());
		}finally{
			if(null!=connection){
				connection.disconnect();
			}
		}
	}
	//-----------获取订单----------------------
	public void getOrders(String state){
		Message msg = Message.obtain();
		msg.what = GET_ORDER;
		msg.obj = state;
		threadHandler.sendMessage(msg);
	}

	private ArrayList<Order> getOrdersFromServer(String state){
		HttpsURLConnection connection =null;
		String access_key = ((WuZhaiApplication)getApplication()).getUser().getAccessKey();
		try {
			URL url = new URL("https://www.5yuzhai.com:443/api/v1/orders?access_key="+access_key+"&state="+state);
			connection = Utils.getHttpsConnection(url);
			connection.setConnectTimeout(5000);
			connection.setRequestMethod("GET");
            if(connection.getResponseCode() == 200){
                InputStream inputStream = connection.getInputStream();
                byte[] bytes = Utils.readStream(inputStream);
                String jsonString = new String(bytes);
                JSONObject resultJson = new JSONObject(jsonString);
                JSONArray orderList = resultJson.getJSONArray("list");
                return orderListParse(orderList);
            }
		} catch (Exception e) {
			e.printStackTrace();
			Log.d("yue.huang", "getOrdersFromServer:"+e.toString());
			return null;
		}finally{
			if(null!=connection){
				connection.disconnect();
			}
		}
		return null;
	}

	private ArrayList<Order> orderListParse(JSONArray jsonArray) throws JSONException{
		ArrayList<Order> orderList = new ArrayList<Order>();
		for(int n=0;n<jsonArray.length();n++){
			orderList.add(parseOrder(jsonArray.getJSONObject(n)));
		}
		return orderList;
	}

	private Order parseOrder(JSONObject jsonObject) throws JSONException{
		Order order = new Order();
		order.setId(jsonObject.getInt("id"));
		order.setAmount(jsonObject.getInt("amount"));
		order.setCode(jsonObject.getString("code"));
		order.setPayment(jsonObject.getString("payment"));
		order.setScore(jsonObject.getInt("score"));
		order.setComment(jsonObject.getString("comment"));
		order.setState(jsonObject.getString("state"));
		order.setShipment(jsonObject.getString("shipment"));
		order.setReceiverName(jsonObject.getString("receiver_name"));
		order.setReceiverAddress(jsonObject.getString("receiver_address"));
		order.setReceiverTel(jsonObject.getString("receiver_tel"));
		order.setCreatedAt(jsonObject.getLong("created_at"));
		JSONObject commodity = jsonObject.getJSONObject("commodity");
		order.setCommodityId(commodity.getInt("id"));
		order.setCommodityTitle(commodity.getString("title"));
		order.setCommodityImage(commodity.getString("image"));
		order.setSaler(parseSaler(jsonObject.getJSONObject("saler")));
		return order;
	}

	private Saler parseSaler(JSONObject jsonObject) throws JSONException{
		Saler saler = new Saler();
		saler.setId(jsonObject.getInt("id"));
		saler.setUsername(jsonObject.getString("username"));
		saler.setAvatar(jsonObject.getString("avatar"));
		saler.setSex(jsonObject.getString("sex"));
		saler.setSignature(jsonObject.getString("signature"));
		saler.setIm_token(jsonObject.getString("im_token"));
		saler.setFollowed(jsonObject.getBoolean("followed"));
		return saler;
	}
	//-----------取消订单----------------------
	public void cancleOrder(int order_id){
		Message msg = Message.obtain();
		msg.what = RECEIVE_ORDER;
		msg.arg1 = order_id;
		threadHandler.sendMessage(msg);
	}

	private void sendCancleOrderToServer(HashMap<String,String> parametersMap){
		String access_key = ((WuZhaiApplication)getApplication()).getUser().getAccessKey();
		String url = "https://www.5yuzhai.com:443/api/v1/orders/cancel_order?access_key="+access_key;
		HttpsURLConnection connection = null;
		try {
			connection = Utils.getHttpsConnectionPost(url, parametersMap);
			if (connection.getResponseCode() == 200) {
				InputStream inputStream = connection.getInputStream();
				byte[] bytes = Utils.readStream(inputStream);
				String jsonString = new String(bytes);
				JSONObject resultJson = new JSONObject(jsonString);
				if (resultJson.getBoolean("success")) {
					mainHandler.sendEmptyMessage(CANCLE_ORDER_SUCC);
				} else {
					mainHandler.sendEmptyMessage(CANCLE_ORDER_FAL);
				}
			} else {
				mainHandler.sendEmptyMessage(NETWORK_ERR);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.d("yue.huang", "sendCancleOrderToServer:"+e.toString());
			mainHandler.sendEmptyMessage(NETWORK_ERR);
		}finally{
			if(null!=connection){
				connection.disconnect();
			}
		}
	}

	//-----------确认收货----------------------
	public void receiveOrder(int order_id){
		Message msg = Message.obtain();
		msg.what = RECEIVE_ORDER;
		msg.arg1 = order_id;
		threadHandler.sendMessage(msg);
	}

	private void sendReceiveOrderToServer(HashMap<String,String> parametersMap){
		String access_key = ((WuZhaiApplication)getApplication()).getUser().getAccessKey();
		String url = "https://www.5yuzhai.com:443/api/v1/orders/buyer_receive?access_key="+access_key;
		HttpsURLConnection connection = null;
		try {
			connection = Utils.getHttpsConnectionPost(url, parametersMap);
			if (connection.getResponseCode() == 200) {
				InputStream inputStream = connection.getInputStream();
				byte[] bytes = Utils.readStream(inputStream);
				String jsonString = new String(bytes);
				JSONObject resultJson = new JSONObject(jsonString);
				if (resultJson.getBoolean("success")) {
					mainHandler.sendEmptyMessage(RECEIVE_ORDER_SUCC);
				} else {
					Log.d("yue.huang", "-------------:"+resultJson.getString("msg"));
					mainHandler.sendEmptyMessage(RECEIVE_ORDER_FAL);
				}
			} else {
				mainHandler.sendEmptyMessage(NETWORK_ERR);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.d("yue.huang", "sendReceiveOrderToServer:"+e.toString());
			mainHandler.sendEmptyMessage(NETWORK_ERR);
		}finally{
			if(null!=connection){
				connection.disconnect();
			}
		}
	}

	//-----------评论订单----------------------
	public void commentOrder(int order_id,float score,String comment){
		Message msg = Message.obtain();
		msg.what = COMMIT_ORDER;
		Bundle bundle = new Bundle();
		bundle.putInt("order_id", order_id);
		bundle.putFloat("score", score);
		bundle.putString("comment", comment);
		msg.setData(bundle);
		threadHandler.sendMessage(msg);
	}

	private void sendCommentOrderToServer(HashMap<String,String> parametersMap){
		String access_key = ((WuZhaiApplication)getApplication()).getUser().getAccessKey();
		String url = "https://www.5yuzhai.com:443/api/v1/orders/score_comment?access_key="+access_key;
		HttpsURLConnection connection = null;
		try {
			connection = Utils.getHttpsConnectionPost(url, parametersMap);
			if (connection.getResponseCode() == 200) {
				InputStream inputStream = connection.getInputStream();
				byte[] bytes = Utils.readStream(inputStream);
				String jsonString = new String(bytes);
				JSONObject resultJson = new JSONObject(jsonString);
				if (resultJson.getBoolean("success")) {
					mainHandler.sendEmptyMessage(COMMIT_ORDER_SUCC);
				} else {
					Log.d("yue.huang", "-------------:"+resultJson.getString("msg"));
					mainHandler.sendEmptyMessage(COMMIT_ORDER_FAL);
				}
			} else {
				mainHandler.sendEmptyMessage(NETWORK_ERR);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.d("yue.huang", "sendCommentOrderToServer:"+e.toString());
			mainHandler.sendEmptyMessage(NETWORK_ERR);
		}finally{
			if(null!=connection){
				connection.disconnect();
			}
		}
	}

	//----------------喜欢---------------
	public void like(String likable_type,int likable_id){
		Message msg = Message.obtain();
		msg.what = LIKE;
		msg.arg1 = likable_id;
		msg.obj = likable_type;
		threadHandler.sendMessage(msg);
	}

	private void sendLikeToServer(HashMap<String,String> parametersMap){
		String access_key = ((WuZhaiApplication)getApplication()).getUser().getAccessKey();
		String url = "https://www.5yuzhai.com:443/api/v1/likes?access_key="+access_key;
		HttpsURLConnection connection = null;
		try {
			connection = Utils.getHttpsConnectionPost(url, parametersMap);
			if (connection.getResponseCode() == 200) {
				InputStream inputStream = connection.getInputStream();
				byte[] bytes = Utils.readStream(inputStream);
				String jsonString = new String(bytes);
				JSONObject resultJson = new JSONObject(jsonString);
				if (resultJson.getBoolean("success")) {
					mainHandler.sendEmptyMessage(LIKE_SUCC);
				} else {
					Log.d("yue.huang", "message:"+resultJson.getString("msg"));
					mainHandler.sendEmptyMessage(LIKE_FAL);
				}
			} else {
				mainHandler.sendEmptyMessage(NETWORK_ERR);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.d("yue.huang", "sendLikeToServer:"+e.toString());
			mainHandler.sendEmptyMessage(NETWORK_ERR);
		}finally{
			if(null!=connection){
				connection.disconnect();
			}
		}
	}
	//----------------获取喜欢列表---------------
	public void getLikableList(int page){
		Message msg = Message.obtain();
		msg.what = GET_LIKABLE;
		msg.arg1 = page;
		threadHandler.sendMessage(msg);
	}

	private ArrayList<Likable> getLikableListFromServer(int page){
		HttpsURLConnection connection =null;
		String access_key = ((WuZhaiApplication)getApplication()).getUser().getAccessKey();
		try {
			URL url = new URL("https://www.5yuzhai.com:443/api/v1/likes?access_key="+access_key);
			connection = Utils.getHttpsConnection(url);
			connection.setConnectTimeout(5000);
			connection.setRequestMethod("GET");
            if(connection.getResponseCode() == 200){
                InputStream inputStream = connection.getInputStream();
                byte[] bytes = Utils.readStream(inputStream);
                String jsonString = new String(bytes);
                JSONObject resultJson = new JSONObject(jsonString);
//                collectionableNextPage = resultJson.optInt("next_page");
                JSONArray likableList = resultJson.getJSONArray("list");
                return likableListParse(likableList);
            }
		} catch (Exception e) {
			e.printStackTrace();
			Log.d("yue.huang", "getLikableListFromServer:"+e.toString());
			return null;
		}finally{
			if(null!=connection){
				connection.disconnect();
			}
		}
		return null;
	}

	private ArrayList<Likable> likableListParse(JSONArray jsonArray) throws JSONException{
		ArrayList<Likable> likableList = new ArrayList<Likable>();
		for(int n=0;n<jsonArray.length();n++){
			likableList.add(parseLikable(jsonArray.getJSONObject(n)));
		}
		return likableList;
	}

	private Likable parseLikable(JSONObject jsonObject) throws JSONException{
		Likable likable = new Likable();
		likable.setLikableId(jsonObject.getInt("likable_id"));
		likable.setLikableType(jsonObject.getString("likable_type"));
		return likable;
	}
    //---------取消喜欢-----------------
	public void cancleLike(String likable_type,int likable_id){
		Message msg = Message.obtain();
		msg.what = CANCLE_LIKABLE;
		msg.arg1 = likable_id;
		msg.obj = likable_type;
		threadHandler.sendMessage(msg);
	}

	private void sendCancleLikeToServer(HashMap<String,String> parametersMap){
		String access_key = ((WuZhaiApplication)getApplication()).getUser().getAccessKey();
		String url = "https://www.5yuzhai.com:443/api/v1/likes/cancel?access_key="+access_key;
		HttpsURLConnection connection = null;
		try {
			connection = Utils.getHttpsConnectionPost(url, parametersMap);
			if (connection.getResponseCode() == 200) {
				InputStream inputStream = connection.getInputStream();
				byte[] bytes = Utils.readStream(inputStream);
				String jsonString = new String(bytes);
				JSONObject resultJson = new JSONObject(jsonString);
				if (resultJson.getBoolean("success")) {
					mainHandler.sendEmptyMessage(CANCLE_LIKABLE_SUCC);
				} else {
					mainHandler.sendEmptyMessage(CANCLE_LIKABLE_FAL);
				}
			} else {
				mainHandler.sendEmptyMessage(NETWORK_ERR);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.d("yue.huang", "sendCancleLikeToServer:"+e.toString());
			mainHandler.sendEmptyMessage(NETWORK_ERR);
		}finally{
			if(null!=connection){
				connection.disconnect();
			}
		}
	}
	//---------检测是否喜欢-----------
	public void IsLike(String likable_type,int likable_id){
		Message msg = Message.obtain();
		msg.what = CHECK_IS_LIKE;
		msg.arg1 = likable_id;
		msg.obj = likable_type;
		threadHandler.sendMessage(msg);
	}

	private boolean checkIsLikeFromServer(HashMap<String,String> parametersMap){
		String access_key = ((WuZhaiApplication)getApplication()).getUser().getAccessKey();
		String url = "https://www.5yuzhai.com:443/api/v1/likes/check?access_key="+access_key;
		HttpsURLConnection connection = null;
		try {
			connection = Utils.getHttpsConnectionPost(url, parametersMap);
			if (connection.getResponseCode() == 200) {
				InputStream inputStream = connection.getInputStream();
				byte[] bytes = Utils.readStream(inputStream);
				String jsonString = new String(bytes);
				JSONObject resultJson = new JSONObject(jsonString);
				if (resultJson.getJSONObject("data").getBoolean("liked")) {
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.d("yue.huang", "checkIsLikeFromServer:"+e.toString());
			return false;
		}finally{
			if(null!=connection){
				connection.disconnect();
			}
		}
	}

	//---------检测是否收藏-----------
	public void Iscollected(String collectionable_type,int collectionable_id){
		Message msg = Message.obtain();
		msg.what = CHECK_IS_COLLECTED;
		msg.arg1 = collectionable_id;
		msg.obj = collectionable_type;
		threadHandler.sendMessage(msg);
	}

	private boolean checkIsCollectedFromServer(HashMap<String,String> parametersMap){
		String access_key = ((WuZhaiApplication)getApplication()).getUser().getAccessKey();
		String url = "https://www.5yuzhai.com:443/api/v1/collections/check?access_key="+access_key;
		HttpsURLConnection connection = null;
		try {
			connection = Utils.getHttpsConnectionPost(url, parametersMap);
			if (connection.getResponseCode() == 200) {
				InputStream inputStream = connection.getInputStream();
				byte[] bytes = Utils.readStream(inputStream);
				String jsonString = new String(bytes);
				JSONObject resultJson = new JSONObject(jsonString);
				if (resultJson.getJSONObject("data").getBoolean("collected")) {
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.d("yue.huang", "checkIscollectedFromServer:"+e.toString());
			return false;
		}finally{
			if(null!=connection){
				connection.disconnect();
			}
		}
	}
	//-------获取评论列表--------
	public void getCommentList(String commentable_type,int commentable_id){
		Message msg = Message.obtain();
		msg.what = GET_COMMENT_LIST;
		msg.arg1 = commentable_id;
		msg.obj = commentable_type;
		threadHandler.sendMessage(msg);
	}

	private ArrayList<Comment> getCommentListFromServer(String commentable_type,int commentable_id){
		HttpsURLConnection connection =null;
		try {
			URL url = new URL("https://www.5yuzhai.com:443/api/v1/comments?commentable_type="+commentable_type+"&commentable_id="+commentable_id);
			connection = Utils.getHttpsConnection(url);
			connection.setConnectTimeout(5000);
			connection.setRequestMethod("GET");
            if(connection.getResponseCode() == 200){
                InputStream inputStream = connection.getInputStream();
                byte[] bytes = Utils.readStream(inputStream);
                String jsonString = new String(bytes);
                JSONObject resultJson = new JSONObject(jsonString);
//                collectionableNextPage = resultJson.optInt("next_page");
                JSONArray commentList = resultJson.getJSONArray("list");
                return commentListParse(commentList);
            }
		} catch (Exception e) {
			e.printStackTrace();
			Log.d("yue.huang", "getCommentListFromServer:"+e.toString());
			return null;
		}finally{
			if(null!=connection){
				connection.disconnect();
			}
		}
		return null;
	}

	private ArrayList<Comment> commentListParse(JSONArray jsonArray) throws JSONException{
		ArrayList<Comment> commentList = new ArrayList<Comment>();
		for(int n=0;n<jsonArray.length();n++){
			commentList.add(parseCommentList(jsonArray.getJSONObject(n)));
		}
		return commentList;
	}

	private Comment parseCommentList(JSONObject jsonObject) throws JSONException{
		Comment comment = new Comment();
		comment.setId(jsonObject.getInt("id"));
		comment.setAvatar(jsonObject.getString("avatar"));
		comment.setUsername(jsonObject.getString("username"));
		comment.setDesc(jsonObject.getString("desc"));
		comment.setCreated_at(jsonObject.getLong("created_at"));
		return comment;
	}

	//--------------添加评论-------------
	public void addComment(String commentable_type,int commentable_id,String desc){
		Message msg = Message.obtain();
		msg.what = ADD_COMMENT;
		Bundle commentBundle = new Bundle();
	    commentBundle.putString("commentable_type", commentable_type);
	    commentBundle.putString("desc", desc);
	    commentBundle.putInt("commentable_id", commentable_id);
	    msg.setData(commentBundle);
	    threadHandler.sendMessage(msg);
	}

	private void addCommentToServer(HashMap<String,String> parametersMap){
		String access_key = ((WuZhaiApplication)getApplication()).getUser().getAccessKey();
		String url = "https://www.5yuzhai.com:443/api/v1/comments?access_key="+access_key;
		HttpsURLConnection connection = null;
		try {
			connection = Utils.getHttpsConnectionPost(url, parametersMap);
			if (connection.getResponseCode() == 200) {
				InputStream inputStream = connection.getInputStream();
				byte[] bytes = Utils.readStream(inputStream);
				String jsonString = new String(bytes);
				JSONObject resultJson = new JSONObject(jsonString);
				if (resultJson.getBoolean("success")) {
					mainHandler.sendEmptyMessage(ADD_COMMENT_SUCC);
				} else {
					mainHandler.sendEmptyMessage(ADD_COMMENT_FAL);
				}
			} else {
				mainHandler.sendEmptyMessage(NETWORK_ERR);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.d("yue.huang", "addCommentToServer:"+e.toString());
			mainHandler.sendEmptyMessage(NETWORK_ERR);
		}finally{
			if(null!=connection){
				connection.disconnect();
			}
		}
	}
	//---------获取礼物列表---------------
	//type 对应 
	//1:用户礼物列表,2:用户礼物列表(按种类分组)等
	public void getGift(int type,int page){
		Message msg = Message.obtain();
		msg.what = GET_GIFT;
		msg.arg1 = type;
		msg.arg2 = page;
		threadHandler.sendMessage(msg);
	}

	private ArrayList<Gift> getGiftFromServer(int type,int page){
		String access_key = ((WuZhaiApplication)getApplication()).getUser().getAccessKey();
		HttpsURLConnection connection =null;
		URL url = null;
		try {
			if(type == 1){
				url = new URL("https://www.5yuzhai.com:443/api/v1/gifts?access_key="+access_key+"&page="+page+"&per=20");
			}else if(type == 2){
				url = new URL("https://www.5yuzhai.com:443/api/v1/gifts/group_by_category?access_key="+access_key);
			}
			connection = Utils.getHttpsConnection(url);
			connection.setConnectTimeout(5000);
			connection.setRequestMethod("GET");
            if(connection.getResponseCode() == 200){
                InputStream inputStream = connection.getInputStream();
                byte[] bytes = Utils.readStream(inputStream);
                String jsonString = new String(bytes);
                JSONObject resultJson = new JSONObject(jsonString);
                giftNextPage = resultJson.optInt("next_page");
                JSONArray giftList = resultJson.getJSONArray("list");
                return giftListParse(giftList);
            }
		} catch (Exception e) {
			e.printStackTrace();
			Log.d("yue.huang", "getGiftFromServer:"+e.toString());
			return null;
		}finally{
			if(null!=connection){
				connection.disconnect();
			}
		}
		return null;
	}

	private ArrayList<Gift> giftListParse(JSONArray jsonArray) throws JSONException{
		ArrayList<Gift> giftList = new ArrayList<Gift>();
		for(int n=0;n<jsonArray.length();n++){
			giftList.add(parseGift(jsonArray.getJSONObject(n)));
		}
		return giftList;
	}

	private Gift parseGift(JSONObject jsonObject) throws JSONException{
		Gift gift = new Gift();
		gift.setId(jsonObject.getInt("id"));
		gift.setName(jsonObject.getString("name"));
		gift.setImagePath(jsonObject.getString("image"));
		gift.setPrice(jsonObject.getString("price"));
		gift.setCostCoin(jsonObject.getInt("cost_coin"));
		gift.setSerialNumber(jsonObject.optString("serial_number"));
		gift.setAmount(jsonObject.optInt("amount"));
		return gift;
	}
//-------------获取礼物种类(全部，恋爱，萌宠。。。)下对应的item列表------------------
	public void getGiftTypeList(int giftTypeId,int page){
		Message msg = Message.obtain();
		msg.what = GET_GIFT_TYPE_LIST;
		msg.arg1 = giftTypeId;
		msg.arg2 = page;
		threadHandler.sendMessage(msg);
	}
	private ArrayList<Gift> getGiftTypeListFromServer(int type,int page){
		String access_key = ((WuZhaiApplication)getApplication()).getUser().getAccessKey();
		HttpsURLConnection connection =null;
		URL url = null;
		try {
			url = new URL("https://www.5yuzhai.com:443/api/v1/gift_categories?access_key="+access_key+"&gift_type_id="+type+"&page="+page+"&per=20");
			connection = Utils.getHttpsConnection(url);
			connection.setConnectTimeout(5000);
			connection.setRequestMethod("GET");
            if(connection.getResponseCode() == 200){
                InputStream inputStream = connection.getInputStream();
                byte[] bytes = Utils.readStream(inputStream);
                String jsonString = new String(bytes);
                JSONObject resultJson = new JSONObject(jsonString);
                giftNextPage = resultJson.optInt("next_page");
                JSONArray giftList = resultJson.getJSONArray("list");
                return giftListParse(giftList);
            }
		} catch (Exception e) {
			e.printStackTrace();
			Log.d("yue.huang", "getGiftTypeListFromServer:"+e.toString());
			return null;
		}finally{
			if(null!=connection){
				connection.disconnect();
			}
		}
		return null;
	}
//-------------获取找回密码验证码-----------------
	public void getFindPasswordCode(String phoneNumber){
		Message msg = Message.obtain();
		msg.what = GET_FIND_PASSWORD_CODE;
		msg.obj = phoneNumber;
		threadHandler.sendMessage(msg);
	}

	private void getFindPasswordCode(HashMap<String, String> parametersMap){
		String url = "https://www.5yuzhai.com:443/api/v1/find_password_code";
		HttpsURLConnection connection = null;
		try {
			connection = Utils.getHttpsConnectionPost(url, parametersMap);
			if (connection.getResponseCode() == 200) {
				InputStream inputStream = connection.getInputStream();
				byte[] bytes = Utils.readStream(inputStream);
				String jsonString = new String(bytes);
				JSONObject resultJson = new JSONObject(jsonString);
				if (resultJson.getBoolean("success")) {
					mainHandler.sendEmptyMessage(GET_FIND_PASSWORD_SUCC);
				} else {
					mainHandler.sendEmptyMessage(GET_FIND_PASSWORD_FAIL);
				}
			} else {
				mainHandler.sendEmptyMessage(NETWORK_ERR);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.d("yue.huang", "getFindPasswordCode:"+e.toString());
			mainHandler.sendEmptyMessage(NETWORK_ERR);
		}finally{
			if(null!=connection){
				connection.disconnect();
			}
		}
	}
	//-------------修改密码-----------------

	public void changePassword(String phoneNumber,String code,String password,String passwordConfirmation){
		Message msg = Message.obtain();
		msg.what = CHANGE_PASSWORD;
		HashMap<String, String> parametersMap = new HashMap<String, String>();
		parametersMap.put("phone_number", phoneNumber);
		parametersMap.put("code", code);
		parametersMap.put("password", password);
		parametersMap.put("password_confirmation", passwordConfirmation);
		msg.obj = parametersMap;
		threadHandler.sendMessage(msg);
	}

	private void changePassword(HashMap<String, String> parametersMap){
		String url = "https://www.5yuzhai.com:443/api/v1/reset_password";
		HttpsURLConnection connection = null;
		try {
			connection = Utils.getHttpsConnectionPost(url, parametersMap);
			if (connection.getResponseCode() == 200) {
				InputStream inputStream = connection.getInputStream();
				byte[] bytes = Utils.readStream(inputStream);
				String jsonString = new String(bytes);
				JSONObject resultJson = new JSONObject(jsonString);
				if (resultJson.getBoolean("success")) {
					mainHandler.sendEmptyMessage(CHANGE_PASSWORD_SUCC);
				} else {
					mainHandler.sendEmptyMessage(CHANGE_PASSWORD_FAIL);
				}
			} else {
				mainHandler.sendEmptyMessage(NETWORK_ERR);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.d("yue.huang", "changePassword:"+e.toString());
			mainHandler.sendEmptyMessage(NETWORK_ERR);
		}finally{
			if(null!=connection){
				connection.disconnect();
			}
		}
	}

	//-------------获取关于我们-------------
	public void getAboutUs(){
		threadHandler.sendEmptyMessage(GET_ABOUT_US);
	}

	private String getAboutUsFromServer(){
		HttpsURLConnection connection =null;
		try {
			URL url = new URL("https://www.5yuzhai.com:443/api/v1/platforms/about_us");
			connection = Utils.getHttpsConnection(url);
			connection.setConnectTimeout(5000);
			connection.setRequestMethod("GET");
            if(connection.getResponseCode() == 200){
                InputStream inputStream = connection.getInputStream();
                byte[] bytes = Utils.readStream(inputStream);
                String jsonString = new String(bytes);
                JSONObject resultJson = new JSONObject(jsonString);
                JSONObject data = resultJson.getJSONObject("data");
                String result = data.getString("desc");
                return result;
            }
		} catch (Exception e) {
			e.printStackTrace();
			Log.d("yue.huang", "getAboutUsFromServer:"+e.toString());
			return null;
		}finally{
			if(null!=connection){
				connection.disconnect();
			}
		}
		return null;
	}

	//-----------获取礼物的所有种类(小黄人，别墅)---------
	public void getGiftCategory(){
		threadHandler.sendEmptyMessage(GET_GIFT_CATEGORIES);
	}

	private ArrayList<GiftCategory> getGiftCategoryFromServer(){
		String access_key = ((WuZhaiApplication)getApplication()).getUser().getAccessKey();
		HttpsURLConnection connection =null;
		try {
			URL url = new URL("https://www.5yuzhai.com:443/api/v1/gift_categories?access_key="+access_key);
			connection = Utils.getHttpsConnection(url);
			connection.setConnectTimeout(5000);
			connection.setRequestMethod("GET");
            if(connection.getResponseCode() == 200){
                InputStream inputStream = connection.getInputStream();
                byte[] bytes = Utils.readStream(inputStream);
                String jsonString = new String(bytes);
                JSONObject resultJson = new JSONObject(jsonString);
//                giftNextPage = resultJson.optInt("next_page");
                JSONArray giftCategoryList = resultJson.getJSONArray("list");
                return giftCategoryListParse(giftCategoryList);
            }
		} catch (Exception e) {
			e.printStackTrace();
			Log.d("yue.huang", "getGiftCategoryFromServer:"+e.toString());
			return null;
		}finally{
			if(null!=connection){
				connection.disconnect();
			}
		}
		return null;
	}

	private ArrayList<GiftCategory> giftCategoryListParse(JSONArray jsonArray) throws JSONException{
		ArrayList<GiftCategory> giftCategoryList = new ArrayList<GiftCategory>();
		for(int n=0;n<jsonArray.length();n++){
			giftCategoryList.add(parseGiftCategory(jsonArray.getJSONObject(n)));
		}
		return giftCategoryList;
	}

	private GiftCategory parseGiftCategory(JSONObject jsonObject) throws JSONException{
		GiftCategory giftCategory = new GiftCategory();
		giftCategory.setId(jsonObject.getInt("id"));
		giftCategory.setName(jsonObject.getString("name"));
		giftCategory.setImagePath(jsonObject.getString("image"));
		giftCategory.setPrice(jsonObject.getString("price"));
		giftCategory.setCostCoin(jsonObject.getInt("cost_coin"));
		return giftCategory;
	}

	//---------------赠送礼物-------------------
	public void sendGift(int receiverId,int giftId,String source,int sourceId){
		String access_key = ((WuZhaiApplication)getApplication()).getUser().getAccessKey();
		HashMap<String, String> paramters = new HashMap<>();
		paramters.put("access_key", access_key);
		paramters.put("gift_id", giftId+"");
		paramters.put("receiver_id", receiverId+"");
		paramters.put("source", source);
		paramters.put("source_id", sourceId+"");

		Message msg = Message.obtain();
		msg.what = SENT_GIFT;
		//赠送方式,普通赠送
		msg.arg1 = 0;
		msg.obj = paramters;

		threadHandler.sendMessage(msg);
	}

	public void sendGiftWithType(int receiverId,int giftId,String source,int sourceId,int amount){
		String access_key = ((WuZhaiApplication)getApplication()).getUser().getAccessKey();
		HashMap<String, String> paramters = new HashMap<>();
		paramters.put("access_key", access_key);
		paramters.put("gift_category_id", giftId+"");
		paramters.put("receiver_id", receiverId+"");
		paramters.put("source", source);
		paramters.put("source_id", sourceId+"");
		paramters.put("amount", amount+"");

		Message msg = Message.obtain();
		msg.what = SENT_GIFT;
		//赠送方式,根据类型赠送
		msg.arg1 = 1;
		msg.obj = paramters;

		threadHandler.sendMessage(msg);
	}

	private boolean sendGiftfromServer(int type,HashMap<String, String> parametersMap){
		String access_key = ((WuZhaiApplication)getApplication()).getUser().getAccessKey();
		String url;
		if(type == 0){
			url = "https://www.5yuzhai.com:443/api/v1/gifts/transfer?access_key="+access_key;
		}else {
			url = "https://www.5yuzhai.com:443/api/v1/gifts/transfer_by_category?access_key="+access_key;
		}
		HttpsURLConnection connection = null;
		try {
			connection = Utils.getHttpsConnectionPost(url, parametersMap);
			if (connection.getResponseCode() == 200) {
				InputStream inputStream = connection.getInputStream();
				byte[] bytes = Utils.readStream(inputStream);
				String jsonString = new String(bytes);
				JSONObject resultJson = new JSONObject(jsonString);
				if (resultJson.getBoolean("success")) {
					return true;
				} else {
					Log.d("yue.huang","------:"+resultJson.getString("msg"));
					return false;
				}
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.d("yue.huang", "sendGiftfromServer:"+e.toString());
			return false;
		}finally{
			if(null!=connection){
				connection.disconnect();
			}
		}
	}


	//---------------获取用户中心信息-------------
	public void getUserCenterInfo(int userId){
		Log.d("yue.huang", "getUserCenterInfo:");
		Message msg = Message.obtain();
		msg.what = GET_USER_CENTER_INFO;
		msg.arg1 = userId;
		threadHandler.sendMessage(msg);
	}

	private UserCenterInfo getUserCenterInfoFromServer(HashMap<String, String> parametersMap){
		String access_key = ((WuZhaiApplication)getApplication()).getUser().getAccessKey();
		String url = "https://www.5yuzhai.com:443/api/v1/users/person_center?access_key="+access_key;
		HttpsURLConnection connection = null;
		try {
			connection = Utils.getHttpsConnectionPost(url, parametersMap);
			if (connection.getResponseCode() == 200) {
				InputStream inputStream = connection.getInputStream();
				byte[] bytes = Utils.readStream(inputStream);
				String jsonString = new String(bytes);
				JSONObject resultJson = new JSONObject(jsonString);
				if (resultJson.getBoolean("success")) {
					return parseUserCenterInfo(resultJson.getJSONObject("data"));
				} else {
					return null;
				}
			} else {
				return null;
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.d("yue.huang", "getUserCenterInfoFromServer:"+e.toString());
			return null;
		}finally{
			if(null!=connection){
				connection.disconnect();
			}
		}
	}

	private UserCenterInfo parseUserCenterInfo(JSONObject dataJsonObject) throws JSONException{
		Log.d("yue.huang", "adfadfasfassfsadf:"+dataJsonObject.toString());
		UserCenterInfo userCenterInfo = new UserCenterInfo();
		JSONObject userJsonObject = dataJsonObject.getJSONObject("base");
		User user = new User();
		user.setName(userJsonObject.getString("username"));
		user.setAvatarUrl(userJsonObject.getString("avatar"));
		user.setAccessKey(userJsonObject.getString("access_key"));
		user.setImToken(userJsonObject.getString("im_token"));
		user.setSexId(userJsonObject.optInt("sex", -1));
		user.setFollowedsCount(userJsonObject.getInt("followeds_count"));
		user.setFollowersCount(userJsonObject.getInt("followers_count"));
		user.setLevel(userJsonObject.getInt("level"));
		user.setNeedExperience(userJsonObject.getInt("need_experience"));
		user.setCurrentExperience(userJsonObject.getInt("current_experience"));
		userCenterInfo.setUser(user);
		userCenterInfo.setCollectionsCount(dataJsonObject.optInt("collections_count"));
		userCenterInfo.setCommoditiesCount(dataJsonObject.optInt("commodities_count"));
		userCenterInfo.setPhotosCount(dataJsonObject.getInt("photos_count"));
		userCenterInfo.setVideosCount(dataJsonObject.getInt("videos_count"));
		//解析图片列表
		JSONArray photoList = dataJsonObject.getJSONArray("photos");
		ArrayList<UserCenterInfo.Photo> photos = new ArrayList<UserCenterInfo.Photo>();
		for (int i = 0; i < photoList.length(); i++) {
			UserCenterInfo.Photo photo = userCenterInfo.new Photo();
			JSONObject photoJsonObject = photoList.getJSONObject(i);
			photo.setId(photoJsonObject.getInt("id"));
			photo.setImage(photoJsonObject.getString("images"));
			photos.add(photo);
		}
		userCenterInfo.setPhotoList(photos);
		//解析视频列表
		JSONArray videoList = dataJsonObject.getJSONArray("videos");
		ArrayList<UserCenterInfo.Video> videos = new ArrayList<UserCenterInfo.Video>();
		for (int i = 0; i < videoList.length(); i++) {
			UserCenterInfo.Video video = userCenterInfo.new Video();
			JSONObject videoJsonObject = videoList.getJSONObject(i);
			video.setId(videoJsonObject.getInt("id"));
			video.setImage(videoJsonObject.getString("screenshot"));
			videos.add(video);
		}
		userCenterInfo.setVideoList(videos);
		return userCenterInfo;
	}

	//--------------意见反馈-------------------
	public void suggestionsFeedback(String suggestions){
		Message msg = Message.obtain();
		msg.what = SUGGESTIONS_FEEDBACK;
		msg.obj = suggestions;
		threadHandler.sendMessage(msg);
	}

	private void submitSuggestionToServer(HashMap<String, String> parametersMap){
		String access_key = ((WuZhaiApplication)getApplication()).getUser().getAccessKey();
		String url = "https://www.5yuzhai.com:443/api/v1/suggestions?access_key="+access_key;
		HttpsURLConnection connection = null;
		try {
			connection = Utils.getHttpsConnectionPost(url, parametersMap);
			if (connection.getResponseCode() == 200) {
				InputStream inputStream = connection.getInputStream();
				byte[] bytes = Utils.readStream(inputStream);
				String jsonString = new String(bytes);
				JSONObject resultJson = new JSONObject(jsonString);
				if (resultJson.getBoolean("success")) {
					mainHandler.sendEmptyMessage(SUGGESTIONS_FEEDBACK_SUCC);
				} else {
					mainHandler.sendEmptyMessage(SUGGESTIONS_FEEDBACK_FAL);
				}
			} else {
				mainHandler.sendEmptyMessage(NETWORK_ERR);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.d("yue.huang", "submitSuggestionToServer:"+e.toString());
			mainHandler.sendEmptyMessage(NETWORK_ERR);
		}finally{
			if(null!=connection){
				connection.disconnect();
			}
		}
	}
//--------------定位相关--------------
    private LocationManager locationManager;
	public void fixedPosition() {
		Log.d("yue.huang", "fixedPosition");
		locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
//		  Criteria c = new Criteria();
//		  c.setAccuracy(Criteria.ACCURACY_FINE); //精度高
//		  c.setPowerRequirement(Criteria.POWER_LOW); //电量消耗低
//		  c.setAltitudeRequired(false); //不需要海拔
//		  c.setSpeedRequired(false); //不需要速度
//		  c.setCostAllowed(false); //不需要费用
//		  String provider = locationManager.getBestProvider(c,false);
		LocationProvider provider = locationManager.getProvider(LocationManager.NETWORK_PROVIDER);
		if (provider != null) {
			locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
		}else {
			Toast.makeText(this, "无法定位", Toast.LENGTH_SHORT).show();
		}
	}

	LocationListener locationListener = new LocationListener() {

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
		}

		@Override
		public void onProviderEnabled(String provider) {
		}

		@Override
		public void onProviderDisabled(String provider) {
		}

		@Override
		public void onLocationChanged(Location location) {
			locationManager.removeUpdates(this);
			Log.d("yue.huang", "location:"+location.toString());

//			Message msg = Message.obtain();
//			msg.what = LOCATION_TO_CITY;
//			Bundle bundle = new Bundle();
//			bundle.putDouble("latitude", location.getLatitude());
//			bundle.putDouble("longitude", location.getLongitude());
//			msg.setData(bundle);
//			threadHandler.sendMessage(msg);
//			locationManager.removeUpdates(locationListener);
		}
	};

	private String locationToCity(double latitude,double longitude){
		String path = "http://maps.google.com/maps/api/geocode/json?latlng="+latitude+","+longitude+"&language=zh-CN&sensor=true";
		Log.d("yue.huang", "location path:"+path);
		HttpURLConnection connection = null;
		try {
			URL url = new URL(path);
			connection = (HttpURLConnection)url.openConnection();
			connection.setConnectTimeout(5000);
			connection.setRequestMethod("GET");
			if(connection.getResponseCode() == 200){
                InputStream inputStream = connection.getInputStream();
                byte[] bytes = Utils.readStream(inputStream);
                String jsonString = new String(bytes);
                JSONObject resultJson = new JSONObject(jsonString);
                String status = resultJson.getString("status");
                if(status.equals("OK")){
                	return parseCity(resultJson);
                }
			}
		} catch (Exception e) {
			Log.d("yue.huang", "locationToCity:"+e.toString());
			e.printStackTrace();
		}finally{
			if(null!=connection){
				connection.disconnect();
			}
		}
		return null;
	}

	private String parseCity(JSONObject resultJson) throws JSONException{
		JSONArray addressArray = resultJson.getJSONArray("results");
		JSONObject cityJsonObject = addressArray.getJSONObject(0).getJSONArray("address_components").getJSONObject(3);
		String city = cityJsonObject.optString("short_name",null);
		return city;
	}
	//----------------获取照片------------
	public void getPhotoList(int photoCategoryId,String tags,int page){
		Message msg = Message.obtain();
		msg.what = GET_PHOTO_LIST;
		msg.arg1 = photoCategoryId;
		msg.arg2 = page;
		msg.obj = tags;
		threadHandler.sendMessage(msg);
	}

	private ArrayList<Photo> getPhotoListFromServer(int photoCategoryId,String tags,int page){
		String access_key = ((WuZhaiApplication)getApplication()).getUser().getAccessKey();
		HttpsURLConnection connection =null;
		StringBuilder path = new StringBuilder("https://www.5yuzhai.com:443/api/v1/photos?access_key="+access_key+"&page="+page+"&per=15");
		try {
			if(photoCategoryId != -1){
				path.append("&photo_category_id="+photoCategoryId);
			}
			if(tags!=null){
				path.append("&tags="+tags);
			}
			Log.d("yue.huang", "path:"+path.toString());
			URL url = new URL(path.toString());
			connection = Utils.getHttpsConnection(url);
			connection.setConnectTimeout(5000);
			connection.setRequestMethod("GET");
            if(connection.getResponseCode() == 200){
                InputStream inputStream = connection.getInputStream();
                byte[] bytes = Utils.readStream(inputStream);
                String jsonString = new String(bytes);
                JSONObject resultJson = new JSONObject(jsonString);
                photos_NextPage = resultJson.optInt("next_page");
                JSONArray photoList = resultJson.getJSONArray("list");
                return photoListParse(photoList);
            }
		} catch (Exception e) {
			e.printStackTrace();
			Log.d("yue.huang", "getPhotoListFromServer:"+e.toString());
			return null;
		}finally{
			if(null!=connection){
				connection.disconnect();
			}
		}
		return null;
	}

	private ArrayList<Photo> photoListParse(JSONArray photoListArray) throws JSONException{
		ArrayList<Photo> photoList = new ArrayList<Photo>();
		for(int n=0;n<photoListArray.length();n++){
			photoList.add(parsePhoto(photoListArray.getJSONObject(n)));
		}
		return photoList;
	}

	private Photo parsePhoto(JSONObject jsonObject) throws JSONException{
		Photo photoObject = new Photo();
		photoObject.setId(jsonObject.getInt("id"));
		photoObject.setTitle(jsonObject.getString("title"));
		photoObject.setPicturePath(jsonObject.getString("images"));
		photoObject.setPhotoCategory(jsonObject.getString("photo_category"));
		photoObject.setDesc(jsonObject.getString("desc"));
		photoObject.setPublisherId(jsonObject.getInt("user_id"));
		photoObject.setPublisherName(jsonObject.getString("username"));
		photoObject.setPublisherAvatar(jsonObject.getString("avatar"));
		photoObject.setTags(jsonObject.getString("tags"));
		JSONObject coordinateObject = jsonObject.getJSONObject("coordinate");
		photoObject.setLongitude(coordinateObject.optDouble("longitude",-1));
		photoObject.setLatitude(coordinateObject.optDouble("latitude",-1));
		photoObject.setCreatedAt(jsonObject.getInt("created_at"));
		photoObject.setLikesCount(jsonObject.getInt("likes_count"));
		photoObject.setCommentsCount(jsonObject.getInt("comments_count"));
		photoObject.setLiked(jsonObject.getBoolean("liked"));
		photoObject.setCollected(jsonObject.getBoolean("collected"));
		photoObject.setFollowed(jsonObject.getBoolean("followed"));
		return photoObject;
	}

	//-----------------发布视频------------
	public void publishVideo(Context context,String path,String title,String desc,String tags,String coordinate){
		ArrayList<String> filePathList = new ArrayList<>();
		filePathList.add(path);
		String access_key = ((WuZhaiApplication)getApplication()).getUser().getAccessKey();
		fileCountNeedToPublish = filePathList.size();
		Message msg = Message.obtain();
		msg.what = PUBLISH_FILE;
		HashMap<String, String> parametersMap = new HashMap<String, String>();
		parametersMap.put("access_key", access_key);
		parametersMap.put("title", title);
		parametersMap.put("desc", desc);
		parametersMap.put("tags", tags);
		parametersMap.put("coordinate", coordinate);
		Bundle bundle = new Bundle();
		bundle.putString("fileType", "video");
		bundle.putSerializable("parametersMap", parametersMap);
		//new ArrayList<String>(filePathList)重新创建一个list是因为在发布商品中如果发布失败，filePathList会被清空，导致界面还有图片但list中没有，
		//用户再次点击发布时会报无图片
		bundle.putSerializable("files", new ArrayList<String>(filePathList));
		msg.setData(bundle);
		threadHandler.sendMessage(msg);
	}

//	private void getUploadAuthorization(String path,String strToSign){
//		String access_key = ((WuZhaiApplication)getApplication()).getUser().getAccessKey();
//		HashMap<String, String> parametersMap = new HashMap<String, String>();
//		parametersMap.put("access_key", access_key);
//		parametersMap.put("string_to_sign", strToSign);
//		String url = "https://www.5yuzhai.com:443/api/v1/config/ucloud_authorization?access_key="+access_key;
//		HttpsURLConnection connection = null;
//		try {
//			connection = Utils.getHttpsConnectionPost(url, parametersMap);
//			if (connection.getResponseCode() == 200) {
//				InputStream inputStream = connection.getInputStream();
//				byte[] bytes = Utils.readStream(inputStream);
//				String jsonString = new String(bytes);
//				JSONObject resultJson = new JSONObject(jsonString);
//				if (resultJson.getBoolean("success")) {
//					JSONObject dataJsonObject = resultJson.getJSONObject("data");
//					String authorization = dataJsonObject.getString("authorization");
//					Message msg = Message.obtain();
//					Bundle data = new Bundle();
//					data.putString("path", path);
//					data.putString("authorization", authorization);
//					msg.setData(data);
//					msg.what = GET_UPLOAD_AUTH_DONE;
//					mainHandler.sendMessage(msg);
//				} else {
//					mainHandler.sendEmptyMessage(UPLOAD_VIDEO_FAIL);
//				}
//			} else {
//				mainHandler.sendEmptyMessage(NETWORK_ERR);
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//			Log.d("yue.huang", "sendCancleLikeToServer:"+e.toString());
//			mainHandler.sendEmptyMessage(NETWORK_ERR);
//		}finally{
//			if(null!=connection){
//				connection.disconnect();
//			}
//		}
//	}

	private void uploadVideoInfoToServer(HashMap<String, String> parametersMap){
		String access_key = ((WuZhaiApplication)getApplication()).getUser().getAccessKey();
		String url = "https://www.5yuzhai.com:443/api/v1/videos?access_key="+access_key;
//		HashMap<String, String> parametersMap = new HashMap<String, String>();
//		parametersMap.put("access_key", access_key);
//		parametersMap.put("title", title);
//		parametersMap.put("desc", desc);
//		parametersMap.put("url", videoUrl);
//		parametersMap.put("screenshot", screenshot);
//		parametersMap.put("tags", tags);
//		parametersMap.put("coordinate", coordinate);
//
//		Log.d("yue.huang", "screenshot:"+screenshot);

		HttpsURLConnection connection = null;
		try {
			connection = Utils.getHttpsConnectionPost(url, parametersMap);
			if (connection.getResponseCode() == 200) {
				InputStream inputStream = connection.getInputStream();
				byte[] bytes = Utils.readStream(inputStream);
				String jsonString = new String(bytes);
				JSONObject resultJson = new JSONObject(jsonString);
				if (resultJson.getBoolean("success")) {
					mainHandler.sendEmptyMessage(PUBLISH_FILE_DONE);
				} else {
					mainHandler.sendEmptyMessage(PUBLISH_FILE_DONE);
				}
			} else {
				mainHandler.sendEmptyMessage(NETWORK_ERR);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.d("yue.huang", "submitSuggestionToServer:"+e.toString());
			mainHandler.sendEmptyMessage(NETWORK_ERR);
		}finally{
			if(null!=connection){
				connection.disconnect();
			}
		}
	}

	//-----------获取微信账号信息----------------
	public void getWXInfo(){
		threadHandler.sendEmptyMessage(GET_WX_INFO);
	}
	private void getWXInfoFromServer(){
		String access_key = ((WuZhaiApplication)getApplication()).getUser().getAccessKey();
		HttpsURLConnection connection =null;
		String path = "https://www.5yuzhai.com:443/api/v1/config/wx_pay?access_key="+access_key;
		try {

			URL url = new URL(path.toString());
			connection = Utils.getHttpsConnection(url);
			connection.setConnectTimeout(5000);
			connection.setRequestMethod("GET");
            if(connection.getResponseCode() == 200){
                InputStream inputStream = connection.getInputStream();
                byte[] bytes = Utils.readStream(inputStream);
                String jsonString = new String(bytes);
                JSONObject resultJson = new JSONObject(jsonString);
                JSONObject data = resultJson.getJSONObject("data");
                String appid = data.getString("appid");
                String key = data.getString("key");
                String mch_id = data.getString("mch_id");
                ((WuZhaiApplication)getApplication()).setWxAppId(appid);
                ((WuZhaiApplication)getApplication()).setWxKey(key);
                ((WuZhaiApplication)getApplication()).setWxMchId(mch_id);
            }
		} catch (Exception e) {
			e.printStackTrace();
			Log.d("yue.huang", "getWXInfo:"+e.toString());
		}finally{
			if(null!=connection){
				connection.disconnect();
			}
		}
	}
	
	//发起微信充值请求，从服务器获取prepay_id
	private void getprepayIdFromServer(String amount,String clientIP){
		String access_key = ((WuZhaiApplication)getApplication()).getUser().getAccessKey();
		String url = "https://www.5yuzhai.com:443/api/v1/deposit_orders/deposit_by_wx?access_key="+access_key;
		HashMap<String, String> parametersMap = new HashMap<String, String>();
		parametersMap.put("access_key", access_key);
		parametersMap.put("amount", amount);
		parametersMap.put("spbill_create_ip", clientIP);

		HttpsURLConnection connection = null;
		try {
			connection = Utils.getHttpsConnectionPost(url, parametersMap);
			if (connection.getResponseCode() == 200) {
				InputStream inputStream = connection.getInputStream();
				byte[] bytes = Utils.readStream(inputStream);
				String jsonString = new String(bytes);
				JSONObject resultJson = new JSONObject(jsonString);
				if (resultJson.getBoolean("success")) {
					JSONObject data = resultJson.getJSONObject("data");
					String prepay_id = data.getString("prepay_id");
					Message msg = Message.obtain();
					msg.obj = prepay_id;
					msg.what = START_WX_PAY;
					mainHandler.sendMessage(msg);
					Log.d("yue.huang", "prepay_id:"+prepay_id);
				} else {
					//发起微信支付失败
					mainHandler.sendEmptyMessage(START_WX_PAY_FAiL);
				}
			} else {
				mainHandler.sendEmptyMessage(NETWORK_ERR);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.d("yue.huang", "submitSuggestionToServer:"+e.toString());
			mainHandler.sendEmptyMessage(NETWORK_ERR);
		}finally{
			if(null!=connection){
				connection.disconnect();
			}
		}
	}
	
	//调起微信支付
	private void startWxPay(String appid,String partnerid,String prepayid,String pkg,String noncestr,String timestamp,String sign){
		IWXAPI api = WXAPIFactory.createWXAPI(this, appid);
		PayReq request = new PayReq();
		request.appId = appid;
		request.partnerId = partnerid;
		request.prepayId= prepayid;
		request.packageValue = pkg;
		request.nonceStr= noncestr;
		request.timeStamp= timestamp;
		request.sign= sign;
		request.extData="app data";
		boolean bool = api.sendReq(request);
		Log.d("yue.huang", "start_result:"+bool);
	}
	//供外部调用的接口
	public void gotoWxRecharge(String amount,String clientIP){
		Message msg = Message.obtain();
		Bundle bundle = new Bundle();
		bundle.putString("amount", amount);
		bundle.putString("clientIP", clientIP);
		msg.setData(bundle);
		msg.what = GET_WX_PERPAYID;
		threadHandler.sendMessage(msg);
	}
	//--------------获取轮播------------
	public void getCarousel(int type,int size){
		Message msg = Message.obtain();
		msg.what = GET_CAROUSEL;
		msg.arg1 = size;
		msg.arg2 = type;
		threadHandler.sendMessage(msg);
	}

	private ArrayList<Carousel> getCarouselFromServer(int type,int size){
		HttpsURLConnection connection =null;
		String urlString = null;
		if(type == CAROUSEL_TYPE_HOME){
			urlString = "https://www.5yuzhai.com:443/api/v1/carousels/home";
		}else {
			urlString = "https://www.5yuzhai.com:443/api/v1/carousels/video";
		}

		try {
			URL url = new URL(urlString+"?limit="+size);
			connection = Utils.getHttpsConnection(url);
			connection.setConnectTimeout(5000);
			connection.setRequestMethod("GET");
            if(connection.getResponseCode() == 200){
                InputStream inputStream = connection.getInputStream();
                byte[] bytes = Utils.readStream(inputStream);
                String jsonString = new String(bytes);
                JSONObject resultJson = new JSONObject(jsonString);
				if (resultJson.getBoolean("success")) {
					return carouselListParse(resultJson.getJSONArray("data"));
				} else {
					return null;
				}

            }
		} catch (Exception e) {
			e.printStackTrace();
			Log.d("yue.huang", "getCarouselFromServer:"+e.toString());
			return null;
		}finally{
			if(null!=connection){
				connection.disconnect();
			}
		}
		return null;
	}

	private ArrayList<Carousel> carouselListParse(JSONArray jsonArray) throws Exception{
		if(jsonArray==null || jsonArray.length() == 0){return null;}
		ArrayList<Carousel> carouselsList = new ArrayList<>();
		for(int n=0;n<jsonArray.length();n++){
			JSONObject jsonObject = jsonArray.getJSONObject(n);
			Carousel carousel = new Carousel();
			carousel.setId(jsonObject.getInt("id"));
			carousel.setObjType(jsonObject.getString("obj_id"));
			carousel.setObjId(jsonObject.getInt("obj_id"));
			carousel.setCreatedAt(jsonObject.getInt("created_at"));
			carousel.setImageUrl(jsonObject.getString("image_url"));
			carousel.setTitle(jsonObject.getString("title"));
			carouselsList.add(carousel);
		}
		return carouselsList;
	}

	//创建直播间
	public void createLiveRoom(String title,String brief,String desc,String tags){
		String access_key = ((WuZhaiApplication)getApplication()).getUser().getAccessKey();
		HashMap<String, String> param = new HashMap<>();
		param.put("access_key", access_key);
		param.put("title", title);
		param.put("brief", brief);
		param.put("desc", desc);
		param.put("tags", tags);

		Message msg = Message.obtain();
		msg.what = CREATE_LIVE_ROOM;
		msg.obj = param;
	}

	private boolean createLiveRoomAtServer(HashMap<String, String> paramMap){
		HttpsURLConnection connection = null;
		try {
			connection = Utils.getHttpsConnectionPost("https://www.5yuzhai.com:443/api/v1/live_rooms", paramMap);
			if (connection.getResponseCode() == 200) {
				InputStream inputStream = connection.getInputStream();
				byte[] bytes = Utils.readStream(inputStream);
				String jsonString = new String(bytes);
				JSONObject resultJson = new JSONObject(jsonString);
				if (resultJson.getBoolean("success")) {
					JSONObject data = resultJson.getJSONObject("data");
					parseSelfLiveRoom(data);
					return true;
				} else {
					Log.d("yue.huang", resultJson.getString("msg"));
					return false;
				}
			} else {
				mainHandler.sendEmptyMessage(NETWORK_ERR);
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.d("yue.huang", "createLiveRoomAtServer:"+e.toString());
			mainHandler.sendEmptyMessage(NETWORK_ERR);
			return false;
		}finally{
			if(null!=connection){
				connection.disconnect();
			}
		}
	}
	//用户自己的直播间
	private void parseSelfLiveRoom(JSONObject liveRoomJson)throws Exception{
		JSONObject streamJson = liveRoomJson.getJSONObject("stream_json");
		User user = ((WuZhaiApplication)getApplication()).getUser();
		user.getLiveRoom().setStreamJson(streamJson.toString());
		user.getLiveRoom().setId(liveRoomJson.getInt("id"));
		user.getLiveRoom().setRoomKey(liveRoomJson.getString("room_key"));
		user.getLiveRoom().setTitle(liveRoomJson.getString("title"));
		user.getLiveRoom().setBrief(liveRoomJson.getString("brief"));
		user.getLiveRoom().setDesc(liveRoomJson.getString("desc"));
		user.getLiveRoom().setState(liveRoomJson.getString("live_in"));
		user.getLiveRoom().setOnlineCount(liveRoomJson.getInt("online_count"));
	}

	//----------获取直播间列表-------------
	public void getLiveList(int page){
		Message msg = Message.obtain();
		msg.what = GET_LIVEROOM_LIST;
		msg.arg1 = page;
		threadHandler.sendMessage(msg);
	}

	private ArrayList<LiveRoomEntity> getLiveRoomListFromServer(int page){
		HttpsURLConnection connection =null;
		String access_key = ((WuZhaiApplication)getApplication()).getUser().getAccessKey();
		try {
			URL url = new URL("https://www.5yuzhai.com:443/api/v1/live_rooms?access_key="+access_key+"&page="+page+"&per=20");
			connection = Utils.getHttpsConnection(url);
			connection.setConnectTimeout(5000);
			connection.setRequestMethod("GET");
            if(connection.getResponseCode() == 200){
                InputStream inputStream = connection.getInputStream();
                byte[] bytes = Utils.readStream(inputStream);
                String jsonString = new String(bytes);
                JSONObject resultJson = new JSONObject(jsonString);
                liveRoom_NextPage = resultJson.optInt("next_page");
                JSONArray liveRoomListJson = resultJson.getJSONArray("list");
                return liveRoomListParse(liveRoomListJson);
            }
		} catch (Exception e) {
			e.printStackTrace();
			Log.d("yue.huang", "getLiveRoomListFromServer:"+e.toString());
			return null;
		}finally{
			if(null!=connection){
				connection.disconnect();
			}
		}
		return null;
	}

	private ArrayList<LiveRoomEntity> liveRoomListParse(JSONArray jsonArray) throws Exception{
		ArrayList<LiveRoomEntity> liveRoomList = new ArrayList<LiveRoomEntity>();
		for(int n=0;n<jsonArray.length();n++){
			liveRoomList.add(parseLiveRoom(jsonArray.getJSONObject(n)));
		}
		return liveRoomList;
	}

	//直播间列表中的item
	private LiveRoomEntity parseLiveRoom(JSONObject liveRoomJson)throws Exception{
		LiveRoomEntity roomEntity = new LiveRoomEntity();
		roomEntity.setUserId(liveRoomJson.getInt("user_id"));
		roomEntity.setUserName(liveRoomJson.getString("username"));
		roomEntity.setAvatar(liveRoomJson.getString("avatar"));
		roomEntity.setSnapshot(liveRoomJson.getString("snapshot"));
		roomEntity.setRtmpLiveUrls(liveRoomJson.getString("rtmp_live_urls"));
		roomEntity.setHlsLiveUrls(liveRoomJson.getString("hls_live_urls"));
		roomEntity.setHttpFlvLiveUrls(liveRoomJson.getString("http_flv_live_urls"));
		roomEntity.setId(liveRoomJson.getInt("id"));
		roomEntity.setKey(liveRoomJson.getString("room_key"));
		roomEntity.setTitle(liveRoomJson.getString("title"));
		roomEntity.setBrief(liveRoomJson.getString("brief"));
		roomEntity.setDesc(liveRoomJson.getString("desc"));
		roomEntity.setState(liveRoomJson.getString("state"));
		roomEntity.setOnlineCount(liveRoomJson.getInt("online_count"));

		return roomEntity;
	}
    //----------------加入直播间---------------------
	public void joinLiveRoom(int roomId){
		Message msg = Message.obtain();
		msg.what = JOIN_LEAVE_LIVEROOM;
		msg.arg1 = roomId;
		msg.arg2 = 1;
		threadHandler.sendMessage(msg);
	}

	// ----------------离开直播间---------------------
	public void leaveLiveRoom(int roomId) {
		Message msg = Message.obtain();
		msg.what = JOIN_LEAVE_LIVEROOM;
		msg.arg1 = roomId;
		msg.arg2 = 0;
		threadHandler.sendMessage(msg);
	}

	private void joinOrLeaveLiveRoomOnServer(int roomId, int flag) {
		// 1加入，0离开
		String access_key = ((WuZhaiApplication) getApplication()).getUser()
				.getAccessKey();
		String url = null;
		if (flag == 1) {
			url = "https://www.5yuzhai.com:443/api/v1/live_rooms/join?access_key="
					+ access_key;
		} else {
			url = "https://www.5yuzhai.com:443/api/v1/live_rooms/leave?access_key="
					+ access_key;
		}

		HashMap<String, String> parametersMap = new HashMap<String, String>();
		parametersMap.put("access_key", access_key);
		parametersMap.put("live_room_id", roomId + "");

		HttpsURLConnection connection = null;
		try {
			connection = Utils.getHttpsConnectionPost(url, parametersMap);
			if (connection.getResponseCode() == 200) {
				InputStream inputStream = connection.getInputStream();
				byte[] bytes = Utils.readStream(inputStream);
				String jsonString = new String(bytes);
				JSONObject resultJson = new JSONObject(jsonString);
				if (resultJson.getBoolean("success")) {
					Log.d("yue.huang", "joinLiveRoomOnServer_加入离开房间成功");
				} else {
					Log.d("yue.huang", "joinLiveRoomOnServer_加入离开房间失败");
				}
			} else {
				mainHandler.sendEmptyMessage(NETWORK_ERR);
				Log.d("yue.huang", "joinLiveRoomOnServer_加入离开房间失败");
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.d("yue.huang", "submitSuggestionToServer:" + e.toString());
			mainHandler.sendEmptyMessage(NETWORK_ERR);
			Log.d("yue.huang", "joinLiveRoomOnServer_加入离开房间失败");
		} finally {
			if (null != connection) {
				connection.disconnect();
			}
		}
	}
}
