package com.wuzhai.app.main;

import com.wuzhai.app.R;
import com.wuzhai.app.login.activity.LoginActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;

public class StartActivity extends Activity{

	private Handler handler;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		initView();
		handler = new Handler(){

			@Override
			public void handleMessage(Message msg) {
				startActivity(new Intent(StartActivity.this,LoginActivity.class));
				StartActivity.this.finish();
			}
		};
		handler.sendEmptyMessageDelayed(100, 1800);
	}

	private void initView(){
		ImageView imageView = new ImageView(this);
		imageView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		imageView.setScaleType(ScaleType.FIT_XY);
		imageView.setImageResource(R.drawable.start_page);
		setContentView(imageView);
	}
}
