package com.wuzhai.app.main.welfare;

import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnKeyListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.wuzhai.app.R;
import com.wuzhai.app.application.WuZhaiApplication;
import com.wuzhai.app.main.WuzhaiService;
import com.wuzhai.app.main.WuzhaiServiceCallbackAdapter;
import com.wuzhai.app.main.widget.CommentListAdapter;
import com.wuzhai.app.objects.Comment;
import com.wuzhai.app.objects.User;
import com.wuzhai.app.widget.TitleToolbarActivity;

public class CommentActivity extends TitleToolbarActivity {
	private WuzhaiService service;
	private ArrayList<Comment> commentList;
	private ListView commentLV;
	private EditText commentET;
	private CommentListAdapter adapter;
	private String commentType;
	private int commentId = -1;
	private int commentCount = 0;
	private TextView countTipTV;
	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		service = ((WuZhaiApplication)getApplication()).getService();
		service.setCallBack(callbackAdapter);
		commentType = getIntent().getStringExtra("type");
		commentId = getIntent().getIntExtra("id", -1);
		service.getCommentList(commentType, commentId);
		setTitle("评论");
		setContentView(R.layout.activity_comment);
		initView();
	}

	private void initView(){
		countTipTV = (TextView)findViewById(R.id.count_tip);
		commentLV = (ListView)findViewById(R.id.commentList);
		commentET = (EditText)findViewById(R.id.comments_edit);
		commentET.setOnKeyListener(new OnKeyListener() {
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if(event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER && !commentET.getText().toString().equals("")){
					service.addComment(commentType, commentId,commentET.getText().toString());
					Comment comment = new Comment();
					User user = ((WuZhaiApplication)getApplication()).getUser();
					comment.setAvatar(user.getAvatarUrl());
					comment.setUsername(user.getName());
					comment.setDesc(commentET.getText().toString());
					comment.setCreated_at(System.currentTimeMillis()/1000);
					commentList.add(comment);
					adapter.setCommentList(commentList);
					commentET.setText("");
					commentCount += 1;
					countTipTV.setText("所有评论("+(Integer.parseInt(countTipTV.getText().toString())+1)+")");
					return true;
				}
				return false;
			}
		});
	}

	@Override
	public void onBackPressed() {
		Intent intent = new Intent();
		intent.putExtra("comment_count", commentCount);
		setResult(RESULT_OK, intent);
		Log.d("yue.huang", "commentCount:"+commentCount);
		super.onBackPressed();
	}

	private WuzhaiServiceCallbackAdapter callbackAdapter = new WuzhaiServiceCallbackAdapter(){

		@Override
		public void onGetCommentListDone(ArrayList<Comment> comments) {
			if(comments!=null){
				commentList = comments;
				adapter = new CommentListAdapter(CommentActivity.this);
				adapter.setCommentList(commentList);
				commentLV.setAdapter(adapter);
				countTipTV.setText("所有评论("+comments.size()+")");
			}
		}
	};
}
