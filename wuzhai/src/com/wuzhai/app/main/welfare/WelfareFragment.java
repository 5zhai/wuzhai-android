package com.wuzhai.app.main.welfare;

import java.util.ArrayList;
import java.util.HashMap;

import com.wuzhai.app.R;
import com.wuzhai.app.application.WuZhaiApplication;
import com.wuzhai.app.main.MainActivity;
import com.wuzhai.app.main.WuzhaiService;
import com.wuzhai.app.main.WuzhaiServiceCallbackAdapter;
import com.wuzhai.app.main.market.activity.AftercareActivity;
import com.wuzhai.app.main.widget.LoadMoreRecyclerView;
import com.wuzhai.app.main.widget.WelfareListAdapter;
import com.wuzhai.app.main.widget.LoadMoreRecyclerView.LoadMoreCallBack;
import com.wuzhai.app.main.widget.WelfareListAdapter.WelfareListItemOnClickListener;
import com.wuzhai.app.objects.MediaObject;
import com.wuzhai.app.objects.Photo;
import com.wuzhai.app.person.widget.FollowerFans;
import com.wuzhai.app.tools.Utils;
import com.wuzhai.app.widget.RefreshableView;
import com.wuzhai.app.widget.RefreshableView.PullToRefreshListener;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class WelfareFragment extends Fragment {
	private WuzhaiService service;
	private Button allTrends;
	private Button followedTrends;
	private RefreshableView pullRefreshLayout;
	private LoadMoreRecyclerView trendsList;
	private WelfareListAdapter adapter;
	private HashMap<Integer, String> photoTypeMap;
	private int currentPhotoTypeId = 1;
	private int nextPage;
	private ArrayList<Photo> photoList;

	private View clickedFollowBtn;
	private View clickedCommentView;
	private int clickedCommentIndex;
	private final int PUBLISH_COMMENT = 100;

	@Override
	@Nullable
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		service = ((WuZhaiApplication)getActivity().getApplication()).getService();
		service.setCallBack(callbackAdapter);
		service.getType(WuzhaiService.PHOTO_TYPE);
//		service.getPhotoList(-1, null, 1);
		View view = LayoutInflater.from(getContext()).inflate(R.layout.fragment_welfare, null);
		initView(view);
		return view;
	}

	private void initView(View rootView){
		allTrends = (Button)rootView.findViewById(R.id.all_trends);
		followedTrends = (Button)rootView.findViewById(R.id.followed_trends);
		pullRefreshLayout = (RefreshableView)rootView.findViewById(R.id.pullRefreshLayout);
		pullRefreshLayout.setOnRefreshListener(refreshListener, 2);
//		pullRefreshLayout.finishRefreshing();

		trendsList = (LoadMoreRecyclerView)rootView.findViewById(R.id.trends_list);
		LayoutManager layoutManager = new LinearLayoutManager(getContext());
		trendsList.setLayoutManager(layoutManager);
		trendsList.setLoadMoreCallBack(loadMoreCallBack);

		photoList = new ArrayList<Photo>();
		adapter = new WelfareListAdapter(getContext(),photoList);
		adapter.setItemOnClickListener(itemOnClickListener);

		trendsList.setAdapter(adapter);

		allTrends.setOnClickListener(onClickListener);
		followedTrends.setOnClickListener(onClickListener);
	}

	private OnClickListener onClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.all_trends:
				allTrends.setBackgroundResource(R.drawable.left_oval_solid_bg_pressed);
				allTrends.setTextColor(0xfff2f2f2);
				followedTrends.setBackgroundResource(R.drawable.right_oval_solid_bg_nomal);
				followedTrends.setTextColor(0xffffffff);
				break;

			case R.id.followed_trends:
				followedTrends.setBackgroundResource(R.drawable.right_oval_solid_bg_pressed);
				followedTrends.setTextColor(0xfff2f2f2);
				allTrends.setBackgroundResource(R.drawable.left_oval_solid_bg_nomal);
				allTrends.setTextColor(0xffffffff);
				break;
			}
		}
	};

	private WuzhaiServiceCallbackAdapter callbackAdapter = new WuzhaiServiceCallbackAdapter(){

		@Override
		public void onGetPhotoListDone(ArrayList<Photo> photo_list,int next_page) {
			if(photo_list!=null){
				nextPage = next_page;
				if(pullRefreshLayout.isRefreshing()){
					photoList.clear();
				}
				photoList.addAll(photo_list);
				trendsList.getAdapter().notifyDataSetChanged();
				pullRefreshLayout.finishRefreshing();
			}
		}

		@Override
		public void onGetTypeCompleted(HashMap<Integer, String> typeMap) {
			if(typeMap!=null){
				photoTypeMap = typeMap;
				((MainActivity)getActivity()).updateSpinnerToolBar(photoTypeMap.values().toArray(new String[0]),itemSelectedListener);
			}
		}

		@Override
		public void onCompleted(int result) {
			switch (result) {
			case WuzhaiService.FOLLOW_USER_SUCC:
				clickedFollowBtn.setEnabled(false);
				((Button)clickedFollowBtn).setText("已关注");
				clickedFollowBtn.setBackgroundColor(0xf2f2f2f2);
				Toast.makeText(getContext(), "关注成功", Toast.LENGTH_SHORT).show();
				break;
			case WuzhaiService.FOLLOW_USER_FAL:
				Toast.makeText(getContext(), "关注失败", Toast.LENGTH_SHORT).show();
				break;
			case WuzhaiService.LIKE_SUCC:
				Toast.makeText(getContext(), "喜欢成功", Toast.LENGTH_SHORT).show();
				break;
			case WuzhaiService.LIKE_FAL:
				Toast.makeText(getContext(), "喜欢失败", Toast.LENGTH_SHORT).show();
				break;
			case WuzhaiService.CANCLE_LIKABLE_SUCC:
				Toast.makeText(getContext(), "取消喜欢成功", Toast.LENGTH_SHORT).show();
				break;
			case WuzhaiService.CANCLE_LIKABLE_FAL:
				Toast.makeText(getContext(), "取消喜欢失败", Toast.LENGTH_SHORT).show();
				break;
			}
		}
	};

	private WelfareListItemOnClickListener itemOnClickListener = new WelfareListItemOnClickListener() {
		@Override
		public void onPicture(MediaObject mediaObject) {
			Intent intent = new Intent(getContext(), WelfareDetialActivity.class);
			intent.putExtra("photo", mediaObject);
			startActivity(intent);
//			Toast.makeText(getContext(), "详情", Toast.LENGTH_SHORT).show();
		}

		@Override
		public void onLikeClick(MediaObject mediaObject,boolean isLike) {
//			Toast.makeText(getContext(), "喜欢", Toast.LENGTH_SHORT).show();
			if(isLike){
				service.like("Photo", mediaObject.getId());
			}else {
				service.cancleLike("Photo", mediaObject.getId());
			}
		}

		@Override
		public void onFollowBtnClick(MediaObject mediaObject,View v) {
			service.followUser(mediaObject.getPublisherId());
			clickedFollowBtn = v;
		}

		@Override
		public void onCommentClick(MediaObject mediaObject,View v,int index) {
			clickedCommentView = v;
			clickedCommentIndex = index;
//			Toast.makeText(getContext(), "评论", Toast.LENGTH_SHORT).show();
			Intent intent = new Intent(getContext(), CommentActivity.class);
			intent.putExtra("type", "Photo");
			intent.putExtra("id", mediaObject.getId());
			startActivityForResult(intent, PUBLISH_COMMENT);
		}
	};

	private OnItemSelectedListener itemSelectedListener = new OnItemSelectedListener() {

		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			String value = (String)arg0.getAdapter().getItem(arg2);
			currentPhotoTypeId = Utils.getIntTypeFromString(photoTypeMap, value);
			photoList.clear();
			service.getPhotoList(currentPhotoTypeId, null, 1);
			
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub
		}
	};
	
	private PullToRefreshListener refreshListener = new PullToRefreshListener() {
		@Override
		public void onRefresh() {
			Log.d("yue.huang", "onRefresh");
			trendsList.removeFooterView();
			if (Utils.isNetworkAvailable(getContext())) {
				service.getPhotoList(currentPhotoTypeId, null, 1);
			} else {
				Toast.makeText(getContext(), "无网络连接",Toast.LENGTH_SHORT).show();
				pullRefreshLayout.finishRefreshing();
			}
		}
	};

	private LoadMoreCallBack loadMoreCallBack = new LoadMoreCallBack() {
		@Override
		public void loadMore() {
			if (nextPage != 0) {
				if (Utils.isNetworkAvailable(getContext())) {
					trendsList.setFooterViewLoaderMore();
					service.getPhotoList(currentPhotoTypeId, null,nextPage);
				} else {
					Toast.makeText(getContext(), "无网络连接",Toast.LENGTH_SHORT).show();
				}
			}else {
				trendsList.setFooterNoMoreToLoad();
			}
		}
	};

	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode) {
		case PUBLISH_COMMENT:
			Log.d("yue.huang", "PUBLISH_COMMENT:"+data.getIntExtra("comment_count", 0));
			int currentCommentCount = photoList.get(clickedCommentIndex).getCommentsCount();
			Log.d("yue.huang", "currentCommentCount:"+currentCommentCount);
			int commentCount = data.getIntExtra("comment_count", 0);
			photoList.get(clickedCommentIndex).setCommentsCount(currentCommentCount+commentCount);
			((TextView)clickedCommentView.findViewById(R.id.comment_count)).setText(currentCommentCount+commentCount+"");
			break;
		}
	};
}
