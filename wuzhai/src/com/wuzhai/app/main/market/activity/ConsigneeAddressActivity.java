package com.wuzhai.app.main.market.activity;

import java.util.ArrayList;

import com.wuzhai.app.R;
import com.wuzhai.app.application.WuZhaiApplication;
import com.wuzhai.app.main.WuzhaiService;
import com.wuzhai.app.main.WuzhaiServiceCallbackAdapter;
import com.wuzhai.app.main.market.widget.AddressListAdapter;
import com.wuzhai.app.main.market.widget.AddressListAdapter.ViewHolder;
import com.wuzhai.app.main.market.widget.DeliveryAddress;
import com.wuzhai.app.widget.TitleToolbarActivity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

public class ConsigneeAddressActivity extends TitleToolbarActivity implements OnItemClickListener{
	private Button addNewAddress;
	private ListView addressListView;
	private AddressListAdapter adapter;
	private WuzhaiService service;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_consignee_address);
		setTitle("收货地址");
		service = ((WuZhaiApplication)getApplication()).getService();
		addressListView = (ListView)findViewById(R.id.address_list);
		addNewAddress = (Button) findViewById(R.id.addNewAddress);
		addNewAddress.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				startActivity(new Intent(ConsigneeAddressActivity.this,
						AddNewAddressActivity.class));
			}
		});
		service.setCallBack(callbackAdapter);
		service.getDeliveryAddress();
	}

	private WuzhaiServiceCallbackAdapter callbackAdapter = new WuzhaiServiceCallbackAdapter(){

		@Override
		public void onGetDeliveryAddressCompleted(ArrayList<DeliveryAddress> addressList) {
			if(addressList != null){
				adapter = new AddressListAdapter(ConsigneeAddressActivity.this, addressList);
				addressListView.setAdapter(adapter);
				addressListView.setOnItemClickListener(ConsigneeAddressActivity.this);
			}else {
				Toast.makeText(ConsigneeAddressActivity.this, "获取数据失败", Toast.LENGTH_SHORT).show();
			}
		}
	};

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		arg1.setBackgroundColor(0xff8B3A3A);
		AddressListAdapter.ViewHolder holder = (AddressListAdapter.ViewHolder)arg1.getTag();
		holder.address.setTextColor(0xffffffff);
		holder.consignee.setTextColor(0xffffffff);
		holder.tel.setTextColor(0xffffffff);
		holder.checkFlag.setVisibility(View.VISIBLE);

		for(int n = 0;n<arg0.getChildCount();n++){
			if(n!=arg2){
				View view = arg0.getChildAt(n);
				view.setBackgroundColor(0xfffffff);
				holder = (AddressListAdapter.ViewHolder)view.getTag();
				holder.address.setTextColor(0xff626f85);
				holder.consignee.setTextColor(0xff626f85);
				holder.tel.setTextColor(0xff626f85);
				holder.checkFlag.setVisibility(View.INVISIBLE);
			}
		}
		Intent intent = new Intent();
		intent.putExtra("address", (DeliveryAddress)arg0.getAdapter().getItem(arg2));
		setResult(RESULT_OK, intent);
		finish();
	}

}
