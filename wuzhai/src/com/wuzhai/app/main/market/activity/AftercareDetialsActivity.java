package com.wuzhai.app.main.market.activity;

import java.util.ArrayList;

import com.squareup.picasso.Picasso;
import com.wuzhai.app.R;
import com.wuzhai.app.application.WuZhaiApplication;
import com.wuzhai.app.main.WuzhaiService;
import com.wuzhai.app.main.WuzhaiServiceCallbackAdapter;
import com.wuzhai.app.main.market.widget.Aftercases;
import com.wuzhai.app.objects.Collectionable;
import com.wuzhai.app.objects.Follower;
import com.wuzhai.app.objects.Photographer;
import com.wuzhai.app.person.widget.FollowerFans;
import com.wuzhai.app.tools.Utils;
import com.wuzhai.app.widget.TitleToolbarActivity;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

public class AftercareDetialsActivity extends TitleToolbarActivity {
	private ImageView userAvatar;
	private TextView username;
	private TextView userplace;
	private Button followBtn;
	private TextView serviceDetail;
	private LinearLayout lablesLayout;
	private ListView detailPicList;
	private WuzhaiService service;
	private Aftercases aftercases;
	private TextView collectBtn;
	private TextView shareBtn;
	private TextView commentBtn;
	private boolean isFollowed = false;
	private boolean isCollected = false;
	private String type = "ProcessingService";
	private Drawable collectDrawable;
	private Drawable unCollectDrawable;
	private Drawable connectionDrawable;
	private Drawable shareDrawable;
	private ImageView bigImg;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_aftercase_details);
		setTitle("详情");
		service = ((WuZhaiApplication)getApplication()).getService();
		service.setCallBack(callbackAdapter);
		service.getFollowerFansList(0);
		service.getCollectionableList(1);
		initView();
	}
	
	private void initView(){
		Intent intent = getIntent();
		collectDrawable = Utils.getDrawableFromResources(this,R.drawable.video_icon_not_collect,25,25);
		unCollectDrawable = Utils.getDrawableFromResources(this,R.drawable.icon_collect_footer,25,25);
		connectionDrawable = Utils.getDrawableFromResources(this, R.drawable.comment_list_big, 25, 25);
		shareDrawable = Utils.getDrawableFromResources(this, R.drawable.icon_share_footer, 25, 25);
		userAvatar = (ImageView)findViewById(R.id.userAvatar);
		username = (TextView)findViewById(R.id.username);
		userplace = (TextView)findViewById(R.id.userplace);
		followBtn = (Button)findViewById(R.id.followbtn);
		followBtn.setOnClickListener(clickListener);
		serviceDetail = (TextView)findViewById(R.id.service_detail);
		lablesLayout = (LinearLayout)findViewById(R.id.lables_layout);
		detailPicList = (ListView)findViewById(R.id.detail_pic_list);
		collectBtn = (TextView)findViewById(R.id.service_collect);
		collectBtn.setOnClickListener(clickListener);
		shareBtn = (TextView)findViewById(R.id.service_share);
		shareBtn.setOnClickListener(clickListener);
		shareBtn.setCompoundDrawables(null, shareDrawable, null, null);
		commentBtn = (TextView)findViewById(R.id.service_comment);
		commentBtn.setOnClickListener(clickListener);
		commentBtn.setCompoundDrawables(null, connectionDrawable, null, null);
		bigImg = (ImageView)findViewById(R.id.big_img);
		bigImg.setOnClickListener(clickListener);
		aftercases = (Aftercases)intent.getSerializableExtra("aftercase");
		if(aftercases instanceof Photographer){
			type = "PhotographyService";
		}
		Picasso.with(this).load(aftercases.getPublisherAvatar()).resize(80, 80).centerInside().into(userAvatar);
		username.setText(aftercases.getPublisherName());
		userplace.setText(aftercases.getCity());
		serviceDetail.setText(aftercases.getDesc());
		setTags(aftercases.getTags());
		if(aftercases.isFollowed()){
			followBtn.setText("已关注");
			followBtn.setEnabled(false);
		}
		if(aftercases.isCollected()){
			isCollected = true;
			collectBtn.setCompoundDrawables(null, collectDrawable, null, null);
		}else {
			isCollected = false;
			collectBtn.setCompoundDrawables(null, unCollectDrawable, null, null);
		}
		DetailPicAdapter adapter = new DetailPicAdapter(this,aftercases.getDetialImages());
		detailPicList.setAdapter(adapter);
		detailPicList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				String imageUrl = parent.getAdapter().getItem(position).toString();
				Picasso.with(AftercareDetialsActivity.this).load(imageUrl).into(bigImg);
				bigImg.setVisibility(View.VISIBLE);
				bigImg.setScaleX(0.5f);
				bigImg.setScaleY(0.5f);
				bigImg.animate().scaleX(1).scaleY(1).setDuration(200).start();
			}
		});
		boolean isShowLable = intent.getBooleanExtra("isShowLable", false);
		if(!isShowLable){
			lablesLayout.setVisibility(View.GONE);
		}
	}

	private void setTags(String[] tags){
		for(String tag: tags){
			TextView tagTextView = new TextView(this);
			android.widget.LinearLayout.LayoutParams layoutParams = new LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
			layoutParams.rightMargin = Utils.dpToPx(4);
			tagTextView.setLayoutParams(layoutParams);
			tagTextView.setPadding(Utils.dpToPx(4), Utils.dpToPx(4), Utils.dpToPx(4), Utils.dpToPx(4));
			tagTextView.setTextColor(0xffff7e9c);
			tagTextView.setTextSize(12);
			tagTextView.setBackgroundResource(R.drawable.photographer_lable_bg);
			tagTextView.setText(tag);
			lablesLayout.addView(tagTextView);
		}
	}

	private OnClickListener clickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			if(!Utils.isNetworkAvailable(AftercareDetialsActivity.this)){
				Toast.makeText(AftercareDetialsActivity.this, "无网络连接", Toast.LENGTH_SHORT).show();
				return;
			}
			switch (v.getId()) {
			case R.id.followbtn:
				if(!isFollowed){
					service.followUser(aftercases.getPublisherId());
				}
				break;

			case R.id.service_collect:
				if(!isCollected){
					if(type.equals("PhotographyService")){
						service.collection(WuzhaiService.TYPE_PHOTOGRAPHY, aftercases.getId());
					}else {
						service.collection(WuzhaiService.TYPE_PROCESSING, aftercases.getId());
					}
				}else {
					if(type.equals("PhotographyService")){
						service.cancleCollection(WuzhaiService.TYPE_PHOTOGRAPHY, aftercases.getId());
					}else {
						service.cancleCollection(WuzhaiService.TYPE_PROCESSING, aftercases.getId());
					}
				}
				break;
			case R.id.service_share:
				Utils.onKeyShareOperation(AftercareDetialsActivity.this, aftercases.getTitle(),
						"https://www.5yuzhai.com/",aftercases.getMainImage(),aftercases.getDesc(), null, "好厉害", "吾宅", null);
				break;
			case R.id.big_img:
				bigImg.setVisibility(View.GONE);
				break;
			}
		}
	};

//	private Drawable getDrawableFromResources(int id){
//		Drawable drawable = getResources().getDrawable(id);
//		drawable.setBounds(0,0,50,50);
//		return drawable;
//	}

	private class DetailPicAdapter extends BaseAdapter{

		private Context context;
		private String[] picPaths;
		public DetailPicAdapter(Context context,String[] picPaths){
			this.context = context;
			this.picPaths = picPaths;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return picPaths.length;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return picPaths[position];
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if(convertView == null){
				ImageView imageView = new ImageView(context);
				imageView.setLayoutParams(new AbsListView.LayoutParams(LayoutParams.MATCH_PARENT,Utils.dpToPx(200)));
				imageView.setScaleType(ScaleType.FIT_XY);
				convertView = imageView;
			}
			Picasso.with(context).load(picPaths[position]).resize(300, 150).centerCrop().into((ImageView)convertView);
			return convertView;
		}
	}

	private WuzhaiServiceCallbackAdapter callbackAdapter = new WuzhaiServiceCallbackAdapter(){

		@Override
		public void onCompleted(int result) {
			switch (result) {
			case WuzhaiService.FOLLOW_USER_SUCC:
				isFollowed = true;
				followBtn.setText("已关注");
				followBtn.setEnabled(false);
				Toast.makeText(AftercareDetialsActivity.this, "关注成功", Toast.LENGTH_SHORT).show();
				break;
			case WuzhaiService.FOLLOW_USER_FAL:
				Toast.makeText(AftercareDetialsActivity.this, "关注失败，请重试", Toast.LENGTH_SHORT).show();
				break;
			case WuzhaiService.COLLECTE_SUCC:
				isCollected = true;
				collectBtn.setCompoundDrawables(null, collectDrawable, null, null);
				Toast.makeText(AftercareDetialsActivity.this, "收藏成功", Toast.LENGTH_SHORT).show();
				break;
			case WuzhaiService.CANCLE_COLLECTE_SUCC:
				isCollected = false;
				collectBtn.setCompoundDrawables(null, unCollectDrawable, null, null);
				Toast.makeText(AftercareDetialsActivity.this, "取消收藏成功", Toast.LENGTH_SHORT).show();
				break;
			}
		}

		@Override
		public void onGetFollowersCompleted(ArrayList<FollowerFans> followersList) {
			if(followersList == null){
				return;
			}
			for(FollowerFans follower : followersList){
				if(follower.getId() == aftercases.getPublisherId()){
					isFollowed = true;
					followBtn.setText("已关注");
					followBtn.setEnabled(false);
				}
			}
		}

		@Override
		public void onGetCollectionableListCompleted(
				ArrayList<Collectionable> collectionable_List, int next_page) {
			for(Collectionable collectionable : collectionable_List){
				if(collectionable.getCollectionableId() == aftercases.getId() && collectionable.getCollectionableType().equals(type)){
					isCollected = true;
					collectBtn.setCompoundDrawables(null, collectDrawable, null, null);
					return;
				}
			}
			isCollected = false;
			collectBtn.setCompoundDrawables(null, unCollectDrawable, null, null);
		}
	};
}
