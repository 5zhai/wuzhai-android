package com.wuzhai.app.main.market.activity;

import java.util.ArrayList;
import java.util.HashMap;

import com.wuzhai.app.R;
import com.wuzhai.app.application.WuZhaiApplication;
import com.wuzhai.app.main.WuzhaiService;
import com.wuzhai.app.main.WuzhaiServiceCallbackAdapter;
import com.wuzhai.app.main.market.widget.ComicConNews;
import com.wuzhai.app.main.market.widget.Goods;
import com.wuzhai.app.main.market.widget.GoodsAdapter;
import com.wuzhai.app.main.market.widget.SpinnerAdapter;
import com.wuzhai.app.main.widget.LoadMoreRecyclerView;
import com.wuzhai.app.main.widget.OnRecyclerViewItemClickListener;
import com.wuzhai.app.main.widget.LoadMoreRecyclerView.LoadMoreCallBack;
import com.wuzhai.app.tools.Utils;
import com.wuzhai.app.widget.RefreshableView;
import com.wuzhai.app.widget.RefreshableView.PullToRefreshListener;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class TradingMarketActivity extends AppCompatActivity implements OnItemSelectedListener,OnClickListener,OnRecyclerViewItemClickListener{
	private WuzhaiService service;
	private LoadMoreRecyclerView recyclerView;
	private Spinner sortSpinner;
	private Spinner goodsClassifySpinner;
	private Spinner citySpinner;
	private Toolbar toolbar;
	private LinearLayout toolbarBack;
	private TextView toolbarPublish;
	private EditText toolbarSearch;
	private int nextPage = 0;
	private ArrayList<Goods> goodsList = new ArrayList<Goods>();
	private GridLayoutManager gridLayoutManager;
	private GoodsAdapter adapter;
	private RefreshableView pullRefreshLayout;
	private String filterSortType = null;
	private boolean isFiltering = false;
	private HashMap<Integer, String> goodsTypeMap;
	private String filterCategoryId = null;
	private String filterCity = null;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		service = ((WuZhaiApplication)getApplication()).getService();
		service.setCallBack(callbackAdapter);
		setContentView(R.layout.activity_market_search);
		setUpToobar();
		initSpinner();
		pullRefreshLayout = (RefreshableView)findViewById(R.id.pullRefreshLayout);
		pullRefreshLayout.setOnRefreshListener(refreshListener,0);
		gridLayoutManager = new GridLayoutManager(this, 2);
		recyclerView.setLayoutManager(gridLayoutManager);
		adapter = new GoodsAdapter(this);
		adapter.setOnRecyclerViewItemClickListener(this);
		adapter.setGoodsList(goodsList);
		recyclerView.setAdapter(adapter);
//		service.getGoods(null, null, null, 1);
		service.getType(WuzhaiService.GOODS_TYPE);
	}

	private void setUpToobar(){
		toolbar = (Toolbar)findViewById(R.id.tradingMarketToolbar);
		toolbar.setTitle("");
		toolbarBack = (LinearLayout)toolbar.findViewById(R.id.toolbar_back);
		toolbarPublish = (TextView)toolbar.findViewById(R.id.toolbar_publish);
		toolbarSearch = (EditText)toolbar.findViewById(R.id.toolbarSearch);
		toolbarBack.setOnClickListener(this);
		toolbarPublish.setOnClickListener(this);
		setSupportActionBar(toolbar);
	}
	
	@SuppressLint("NewApi")
	private void initSpinner(){
		int spinnerOffSet = (int)(getResources().getDimension(R.dimen.spinner_height)-Utils.dpToPx(7));
		sortSpinner = (Spinner)findViewById(R.id.sort);
		goodsClassifySpinner = (Spinner)findViewById(R.id.goodsClassify);
		citySpinner = (Spinner)findViewById(R.id.city);
		recyclerView = (LoadMoreRecyclerView) findViewById(R.id.rvFeed_marketTrading);
		recyclerView.setLoadMoreCallBack(loadMoreCallBack);
		int screenWidth = ((WuZhaiApplication)getApplication()).getScreenWidth();
		sortSpinner.setDropDownWidth(screenWidth);
		sortSpinner.setOnItemSelectedListener(this);
		sortSpinner.setDropDownVerticalOffset(spinnerOffSet);
		goodsClassifySpinner.setDropDownWidth(screenWidth);
		goodsClassifySpinner.setOnItemSelectedListener(this);
		goodsClassifySpinner.setDropDownVerticalOffset(spinnerOffSet);
		citySpinner.setOnItemSelectedListener(this);
		citySpinner.setDropDownWidth(screenWidth);
		citySpinner.setDropDownVerticalOffset(spinnerOffSet);
		SpinnerAdapter sortAdapter = new SpinnerAdapter(this, android.R.layout.simple_spinner_item,getResources().getStringArray(R.array.sortSpinner));
		sortAdapter.setSpinner(sortSpinner);
		sortSpinner.setAdapter(sortAdapter);
//		SpinnerAdapter goodsClassifyAdapter = new SpinnerAdapter(this, android.R.layout.simple_spinner_item,getResources().getStringArray(R.array.sortSpinner));
//		goodsClassifyAdapter.setSpinner(goodsClassifySpinner);
//		goodsClassifySpinner.setAdapter(goodsClassifyAdapter);
		SpinnerAdapter cityAdapter = new SpinnerAdapter(this, android.R.layout.simple_spinner_item,getResources().getStringArray(R.array.citySpinner));
		cityAdapter.setSpinner(citySpinner);
		citySpinner.setAdapter(cityAdapter);
	}
	@Override
	protected void onResume() {
		service.setCallBack(callbackAdapter);
		super.onResume();
	}
	@SuppressLint("NewApi")
	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
			long arg3) {

		switch (arg0.getId()) {
		case R.id.sort:
//			sortType = arg0.getAdapter().getItem(arg2).toString();
			setSortResult(arg2);
			break;
		case R.id.goodsClassify:
//			Toast.makeText(this, "商品类型", Toast.LENGTH_SHORT).show();
			String goodsType = arg0.getAdapter().getItem(arg2).toString();
			int goodsTypeId = Utils.getIntTypeFromString(goodsTypeMap, goodsType);
			if(goodsTypeId!=0){
				filterCategoryId = goodsTypeId+"";
			}else {
				filterCategoryId = null;
			}
			break;
		case R.id.city:
			filterCity = arg0.getAdapter().getItem(arg2).toString();
			break;
		}
		isFiltering = true;
		service.getGoods(filterCity, filterCategoryId, filterSortType, 1);
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub
	}

	private void setSortResult(int n){
		switch (n) {
		case 0:
			filterSortType = "recommend_type";
			break;
		case 1:
			filterSortType = "price_asc";
			break;
		case 2:
			filterSortType = "price_desc";
			break;
		case 3:
			filterSortType = "sale_count";
			break;
		}
	}
//
//	private void setCategoryId(int n){
//	}
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.toolbar_back:
			onBackPressed();
			break;
		case R.id.toolbar_publish:
			Intent intent = new Intent(this,PublishActivity.class);
			intent.putExtra("page_flag", "goods");
			startActivity(intent);
			break;
		}
	}

	@Override
	public void onItemClick(View view, int position) {
		Intent intent = new Intent(this, GoodsDetialActivity.class);
		intent.putExtra("goodsId", goodsList.get(position).getId());
		startActivity(intent);
	}

	private PullToRefreshListener refreshListener = new PullToRefreshListener() {
		@Override
		public void onRefresh() {
			recyclerView.removeFooterView();
			if (Utils.isNetworkAvailable(TradingMarketActivity.this)) {
				service.getGoods(filterCity, filterCategoryId, filterSortType, 1);
			} else {
				Toast.makeText(TradingMarketActivity.this, "无网络连接",
						Toast.LENGTH_SHORT).show();
				pullRefreshLayout.finishRefreshing();
			}
		}
	};

	private LoadMoreCallBack loadMoreCallBack = new LoadMoreCallBack() {
		@Override
		public void loadMore() {
			if (nextPage != 0) {
				if (Utils.isNetworkAvailable(TradingMarketActivity.this)) {
					recyclerView.setFooterViewLoaderMore();
					isFiltering = false;
					service.getGoods(filterCity, filterCategoryId, filterSortType, nextPage);
				} else {
					Toast.makeText(TradingMarketActivity.this, "无网络连接",
							Toast.LENGTH_SHORT).show();
				}
			} else {
				recyclerView.setFooterNoMoreToLoad();
			}
		}
	};

	WuzhaiServiceCallbackAdapter callbackAdapter = new WuzhaiServiceCallbackAdapter(){

		@Override
		public void onGetTypeCompleted(HashMap<Integer, String> typeMap) {
			if (typeMap != null) {
				typeMap.put(0, "全部商品");
				goodsTypeMap = typeMap;
				String[] types = typeMap.values().toArray(new String[typeMap.values().size()]);
				SpinnerAdapter goodsClassifyAdapter = new SpinnerAdapter(TradingMarketActivity.this, android.R.layout.simple_spinner_item,types);
				goodsClassifyAdapter.setSpinner(goodsClassifySpinner);
				goodsClassifySpinner.setAdapter(goodsClassifyAdapter);
			}
		}

		@Override
		public void onGetGoodsCompleted(ArrayList<Goods> goods_list, int next_page) {
			if(goods_list!=null){
				nextPage = next_page;
				if(pullRefreshLayout.isRefreshing() || isFiltering){
					goodsList.clear();
				}
				goodsList.addAll(goods_list);
				//WrapRecyclerView对RecyclerView进行了继承，当有header或footerview时内部对adapter进行了包装替换，
				//所以要不能直接使用adapter的notifyDataSetChanged()
				recyclerView.getAdapter().notifyDataSetChanged();
				pullRefreshLayout.finishRefreshing();
			}else {
				Toast.makeText(TradingMarketActivity.this, "获取数据失败", Toast.LENGTH_SHORT).show();
			}
		}
	};
}
