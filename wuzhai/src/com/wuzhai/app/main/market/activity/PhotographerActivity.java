package com.wuzhai.app.main.market.activity;

import java.util.ArrayList;

import com.wuzhai.app.R;
import com.wuzhai.app.application.WuZhaiApplication;
import com.wuzhai.app.main.WuzhaiService;
import com.wuzhai.app.main.WuzhaiServiceCallbackAdapter;
import com.wuzhai.app.main.market.widget.AftercaseAdapter;
import com.wuzhai.app.main.market.widget.Aftercases;
import com.wuzhai.app.main.market.widget.SpinnerAdapter;
import com.wuzhai.app.main.widget.LoadMoreRecyclerView;
import com.wuzhai.app.main.widget.OnRecyclerViewItemClickListener;
import com.wuzhai.app.main.widget.LoadMoreRecyclerView.LoadMoreCallBack;
import com.wuzhai.app.objects.Photographer;
import com.wuzhai.app.tools.Utils;
import com.wuzhai.app.widget.RefreshableView;
import com.wuzhai.app.widget.RefreshableView.PullToRefreshListener;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;

public class PhotographerActivity extends AppCompatActivity implements OnItemSelectedListener,OnClickListener{
	private WuzhaiService service;
	private LoadMoreRecyclerView recyclerView;
	private Spinner sortSpinner;
	private Spinner goodsClassifySpinner;
	private Spinner citySpinner;
	private Toolbar toolbar;
	private LinearLayout toolbarBack;
	private TextView toolbarPublish;
	private EditText toolbarSearch;
	private int nextPage = 0;
	private ArrayList<Photographer> photographerList = new ArrayList<Photographer>();
	private AftercaseAdapter adapter;
	private RefreshableView pullRefreshLayout;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_market_search);
		service = ((WuZhaiApplication)getApplication()).getService();
		service.getPhotographerService(1);
		setUpToobar();
		initSpinner();
		pullRefreshLayout = (RefreshableView)findViewById(R.id.pullRefreshLayout);
		pullRefreshLayout.setOnRefreshListener(refreshListener,2);
		LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
		recyclerView.setLayoutManager(linearLayoutManager);
		recyclerView.setPadding(0, 0, 0, 0);
		adapter = new AftercaseAdapter(this);
		adapter.setOnRecyclerViewItemClickListener(recyclerViewItemClickListener);
		adapter.setAftercaseList(photographerList);
		recyclerView.setAdapter(adapter);
	}

	private void setUpToobar(){
		toolbar = (Toolbar)findViewById(R.id.tradingMarketToolbar);
		toolbar.setTitle("");
		toolbarBack = (LinearLayout)toolbar.findViewById(R.id.toolbar_back);
		toolbarPublish = (TextView)toolbar.findViewById(R.id.toolbar_publish);
		toolbarSearch = (EditText)toolbar.findViewById(R.id.toolbarSearch);
		toolbarBack.setOnClickListener(this);
		toolbarPublish.setOnClickListener(this);
		setSupportActionBar(toolbar);
	}

	@SuppressLint("NewApi")
	private void initSpinner(){
		int spinnerOffSet = (int)(getResources().getDimension(R.dimen.spinner_height)-Utils.dpToPx(7));
		sortSpinner = (Spinner)findViewById(R.id.sort);
		goodsClassifySpinner = (Spinner)findViewById(R.id.goodsClassify);
		citySpinner = (Spinner)findViewById(R.id.city);
		recyclerView = (LoadMoreRecyclerView) findViewById(R.id.rvFeed_marketTrading);
		recyclerView.setLoadMoreCallBack(loadMoreCallBack);
		int screenWidth = ((WuZhaiApplication)getApplication()).getScreenWidth();
		sortSpinner.setDropDownWidth(screenWidth);
		sortSpinner.setOnItemSelectedListener(this);
		sortSpinner.setDropDownVerticalOffset(spinnerOffSet);
		goodsClassifySpinner.setDropDownWidth(screenWidth);
		goodsClassifySpinner.setOnItemSelectedListener(this);
		goodsClassifySpinner.setDropDownVerticalOffset(spinnerOffSet);
		citySpinner.setOnItemSelectedListener(this);
		citySpinner.setDropDownWidth(screenWidth);
		citySpinner.setDropDownVerticalOffset(spinnerOffSet);
		SpinnerAdapter sortAdapter = new SpinnerAdapter(this, android.R.layout.simple_spinner_item,getResources().getStringArray(R.array.sortSpinner));
		sortAdapter.setSpinner(sortSpinner);
		sortSpinner.setAdapter(sortAdapter);
		SpinnerAdapter goodsClassifyAdapter = new SpinnerAdapter(this, android.R.layout.simple_spinner_item,getResources().getStringArray(R.array.sortSpinner));
		goodsClassifyAdapter.setSpinner(goodsClassifySpinner);
		goodsClassifySpinner.setAdapter(goodsClassifyAdapter);
		SpinnerAdapter cityAdapter = new SpinnerAdapter(this, android.R.layout.simple_spinner_item,getResources().getStringArray(R.array.sortSpinner));
		cityAdapter.setSpinner(citySpinner);
		citySpinner.setAdapter(cityAdapter);
	}

	@Override
	protected void onResume() {
		service.setCallBack(callbackAdapter);
		super.onResume();
	}
	@SuppressLint("NewApi")
	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
			long arg3) {
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.toolbar_back:
			onBackPressed();
			break;
		case R.id.toolbar_publish:
			Intent intent = new Intent(this,PublishActivity.class);
			intent.putExtra("page_flag", "photographer");
			startActivity(intent);
			break;
		}
	}

	private PullToRefreshListener refreshListener = new PullToRefreshListener() {
		@Override
		public void onRefresh() {
			recyclerView.removeFooterView();
			if (Utils.isNetworkAvailable(PhotographerActivity.this)) {
				service.getPhotographerService(1);
			} else {
				Toast.makeText(PhotographerActivity.this, "无网络连接",Toast.LENGTH_SHORT).show();
				pullRefreshLayout.finishRefreshing();
			}
		}
	};

	private WuzhaiServiceCallbackAdapter callbackAdapter = new WuzhaiServiceCallbackAdapter(){

		@Override
		public void onGetPhotographerCompleted(
				ArrayList<Photographer> photographer_list, int next_page) {
			if(photographer_list!=null){
				nextPage = next_page;
				if(pullRefreshLayout.isRefreshing()){
					photographerList.clear();
				}
				photographerList.addAll(photographer_list);
				//WrapRecyclerView对RecyclerView进行了继承，当有header或footerview时内部对adapter进行了包装替换，
				//所以要不能直接使用adapter的notifyDataSetChanged()
				recyclerView.getAdapter().notifyDataSetChanged();
				pullRefreshLayout.finishRefreshing();
			}else {
				Toast.makeText(PhotographerActivity.this, "获取数据失败", Toast.LENGTH_SHORT).show();
			}
		}
	};

	private LoadMoreCallBack loadMoreCallBack = new LoadMoreCallBack() {
		@Override
		public void loadMore() {
			if (nextPage != 0) {
				if (Utils.isNetworkAvailable(PhotographerActivity.this)) {
					recyclerView.setFooterViewLoaderMore();
					service.getPhotographerService(nextPage);
				} else {
					Toast.makeText(PhotographerActivity.this, "无网络连接",Toast.LENGTH_SHORT).show();
				}
			} else {
				recyclerView.setFooterNoMoreToLoad();
			}
		}
	};

	private OnRecyclerViewItemClickListener recyclerViewItemClickListener = new OnRecyclerViewItemClickListener() {

		@Override
		public void onItemClick(View view, int position) {
			Intent intent = new Intent(PhotographerActivity.this,AftercareDetialsActivity.class);
			intent.putExtra("aftercase", photographerList.get(position));
			intent.putExtra("isShowLable", true);
			startActivity(intent);
		}
	};
}
