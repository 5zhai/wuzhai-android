package com.wuzhai.app.main.market.activity;

import com.squareup.picasso.Picasso;
import com.wuzhai.app.R;
import com.wuzhai.app.application.WuZhaiApplication;
import com.wuzhai.app.main.WuzhaiService;
import com.wuzhai.app.main.WuzhaiServiceCallbackAdapter;
import com.wuzhai.app.main.market.widget.DeliveryAddress;
import com.wuzhai.app.main.market.widget.Goods;
import com.wuzhai.app.main.widget.AddSubEditView;
import com.wuzhai.app.tools.Utils;
import com.wuzhai.app.widget.TitleToolbarActivity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class GoodsPaymentActivity extends TitleToolbarActivity {
	private WuzhaiService service;
	private LinearLayout consigneeAddress;
	private TextView consignee;
	private TextView tel;
	private TextView address;
	private ImageView goodsPic;
	private TextView goodsInfo;
	private TextView goodsPrice;
	private TextView goodsPlace;
	private AddSubEditView addSubView;
	private TextView freight;
	private LinearLayout balancePayment;
	private ImageView paysFlagBalance;
	private LinearLayout wechatPayment;
	private ImageView paysFlagWechat;
	private LinearLayout alipayPayment;
	private ImageView paysFlagAlipay;
	private TextView paymentAmount;
	private Button paysBtn;
	private int totlePrice;
	private Goods goods;
	private String payType;
	private int contactId;
	private int buyAmount = 1;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_goods_payment);
		service = ((WuZhaiApplication)getApplication()).getService();
		service.setCallBack(callbackAdapter);
		service.getDefaultDeliveryAddress();
		setTitle("商品支付");
		initView();
		initData();
	}

	private void initView(){
		consigneeAddress = (LinearLayout) findViewById(R.id.consignee_address);
		consigneeAddress.setOnClickListener(clickListener);
		consignee = (TextView)findViewById(R.id.consignee);
		tel = (TextView)findViewById(R.id.tel);
		address = (TextView)findViewById(R.id.address);
		goodsPic = (ImageView)findViewById(R.id.goods_pic);
		goodsInfo = (TextView)findViewById(R.id.goods_info);
		goodsPrice = (TextView)findViewById(R.id.goods_price);
		goodsPlace = (TextView)findViewById(R.id.goods_place);
		addSubView = (AddSubEditView)findViewById(R.id.addSubView);
		addSubView.setOnCountChanged(countWatcher);
		freight = (TextView)findViewById(R.id.freight);
		balancePayment = (LinearLayout)findViewById(R.id.balance_payment);
		balancePayment.setOnClickListener(clickListener);
		paysFlagBalance = (ImageView)findViewById(R.id.pays_flag_balance);
		wechatPayment = (LinearLayout)findViewById(R.id.wechat_payment);
		wechatPayment.setOnClickListener(clickListener);
		paysFlagWechat = (ImageView)findViewById(R.id.pays_flag_wechat);
		alipayPayment = (LinearLayout)findViewById(R.id.alipay_payment);
		alipayPayment.setOnClickListener(clickListener);
		paysFlagAlipay = (ImageView)findViewById(R.id.pays_flag_alipay);
		paymentAmount = (TextView)findViewById(R.id.payment_amount);
		paysBtn = (Button)findViewById(R.id.pays_btn);
		paysBtn.setOnClickListener(clickListener);
	}

	private void initData(){
		service = ((WuZhaiApplication)getApplication()).getService();
		goods = (Goods)getIntent().getSerializableExtra("goods");
		initGoodsData(goods);
		totlePrice = goods.getPrice();
		updatePaymentAmount(totlePrice);
	}
	private void initGoodsData(Goods goods){
		if(goods!=null){
			Picasso.with(this).load(goods.getMainImage()).into(goodsPic);
			goodsInfo.setText(goods.getTitle());
			goodsPrice.setText(""+goods.getPrice());
			goodsPlace.setText(goods.getCity());
		}
	}
	private void initAddressData(DeliveryAddress deliveryAddress){
		if(address!=null){
			contactId = Integer.parseInt(deliveryAddress.getId());
			consignee.setText(deliveryAddress.getReceiverName());
			tel.setText(deliveryAddress.getTel());
			address.setText(deliveryAddress.getDetial());
		}

	}

	private void updatePaymentAmount(int payment){
		paymentAmount.setText(""+(payment+12));
	}

	private TextWatcher countWatcher = new TextWatcher() {
		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {
			buyAmount = Integer.parseInt(s.toString());
			totlePrice = goods.getPrice() * buyAmount;
			updatePaymentAmount(totlePrice);
		}
		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			// TODO Auto-generated method stub
		}
		@Override
		public void afterTextChanged(Editable s) {
			// TODO Auto-generated method stub
		}
	};

	private OnClickListener clickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.consignee_address:
				startActivityForResult(new Intent(GoodsPaymentActivity.this,ConsigneeAddressActivity.class), 100);
				break;
			case R.id.balance_payment:
				paysFlagBalance.setImageResource(R.drawable.icon_pays_select);
				paysFlagWechat.setImageResource(R.drawable.icon_pays_normal);
				paysFlagAlipay.setImageResource(R.drawable.icon_pays_normal);
				payType = "balance";
				break;
			case R.id.wechat_payment:
				paysFlagWechat.setImageResource(R.drawable.icon_pays_select);
				paysFlagBalance.setImageResource(R.drawable.icon_pays_normal);
				paysFlagAlipay.setImageResource(R.drawable.icon_pays_normal);
				payType = "wechat";
				break;
			case R.id.alipay_payment:
				paysFlagAlipay.setImageResource(R.drawable.icon_pays_select);
				paysFlagWechat.setImageResource(R.drawable.icon_pays_normal);
				paysFlagBalance.setImageResource(R.drawable.icon_pays_normal);
				payType = "alipay";
				break;
			case R.id.pays_btn:
				createOrders(goods.getId(), totlePrice, contactId);
				break;
			}
		}
	};

	private void createOrders(int commodityId,int amount,int contactId){
		if(payType!=null && payType.equals("balance")){
			service.createOrder(commodityId, amount, contactId);
		}else if(payType!=null && payType.equals("wechat")){
//			service.gotoWxRecharge(amount+"", Utils.getLocalHostIp());
			Toast.makeText(GoodsPaymentActivity.this, "支付类型暂不支持", Toast.LENGTH_SHORT).show();
		}else {
			Toast.makeText(GoodsPaymentActivity.this, "支付类型暂不支持", Toast.LENGTH_SHORT).show();
		}
	}

	private void payWithBalance(int order_id){
		service.payWithBalance(order_id);
	}

	protected void onActivityResult(int arg0, int arg1, Intent arg2) {
		if(arg1 == RESULT_OK){
			DeliveryAddress address = (DeliveryAddress)arg2.getSerializableExtra("address");
			initAddressData(address);
		}
	};

	private WuzhaiServiceCallbackAdapter callbackAdapter = new WuzhaiServiceCallbackAdapter(){

		@Override
		public void onGetDefaultDeliveryAddressCompleted(DeliveryAddress address) {
			initAddressData(address);
			//进度条消失
		}

		@Override
		public void onCreateOrderCompleted(int order_id) {
			Log.d("yue.huang", "orderId:"+order_id);
			if(order_id != -1){
				payWithBalance(order_id);
			}
		}

		@Override
		public void onPayWithBalanceCompleted(String msg) {
			Toast.makeText(GoodsPaymentActivity.this, msg, Toast.LENGTH_SHORT).show();
		}
	};
}
