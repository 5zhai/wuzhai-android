package com.wuzhai.app.main.market.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mrwujay.cascade.view.AddressPicker;
import com.mrwujay.cascade.view.AddressPicker.AddressListener;
import com.wuzhai.app.R;
import com.wuzhai.app.application.WuZhaiApplication;
import com.wuzhai.app.main.WuzhaiService;
import com.wuzhai.app.main.WuzhaiServiceCallbackAdapter;
import com.wuzhai.app.main.market.widget.DeliveryAddress;
import com.wuzhai.app.tools.Utils;
import com.wuzhai.app.widget.TitleToolbarActivity;

public class AddNewAddressActivity extends TitleToolbarActivity implements OnClickListener{
 
	private AddressPicker addressPicker;
	private TextView shengshiquTextView;
	private RelativeLayout shadowView;
	private EditText receiverNameEditText;
	private EditText receiverTelEditText;
	private EditText zipCodeEditText;
	private EditText streetEditText;
	private EditText detialEditText;
	private WuzhaiService service;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_addnewaddress);
		setTitle("新建收货地址");
		setTextMenuString("保存");
		setTextMenuClickListener(this);

		service = ((WuZhaiApplication)getApplication()).getService();
		service.setCallBack(callbackAdapter);
		receiverNameEditText = (EditText)findViewById(R.id.name);
		receiverTelEditText = (EditText)findViewById(R.id.tel);
		zipCodeEditText = (EditText)findViewById(R.id.postcode);
		streetEditText = (EditText)findViewById(R.id.street);
		detialEditText = (EditText)findViewById(R.id.detial_address);
		shadowView = (RelativeLayout)findViewById(R.id.shadow);
		shengshiquTextView = (TextView)findViewById(R.id.shengshiqu);
		shengshiquTextView.setOnClickListener(this);
		addressPicker = (AddressPicker)findViewById(R.id.addressPicker);
		addressPicker.setAddressListener(new AddressListener() {
			@Override
			public void onAddressSelected(String proviceName, String cityName,
					String districtName) {
				shengshiquTextView.setText(proviceName+" "+cityName+" "+districtName);
				addressPicker.setVisibility(View.GONE);
				shadowView.setVisibility(View.GONE);
			}
		});
	}

	private void saveNewAddress(){
		String name = receiverNameEditText.getText().toString();
		String tel = receiverTelEditText.getText().toString();
		String zipCode = zipCodeEditText.getText().toString();
		String shengshiqu = shengshiquTextView.getText().toString();
		String street = streetEditText.getText().toString();
		String detial = detialEditText.getText().toString();
		if(TextUtils.isEmpty(name) || TextUtils.isEmpty(tel) || TextUtils.isEmpty(zipCode) ||
				TextUtils.isEmpty(shengshiqu) || TextUtils.isEmpty(street) || TextUtils.isEmpty(detial)){
			Toast.makeText(this, "信息不完整", Toast.LENGTH_SHORT).show();
			return;
		}
		if(!Utils.isPhoneNumber(tel)){
			Toast.makeText(this, "请输入正确的电话号码", Toast.LENGTH_SHORT).show();
			return;
		}
		if(!Utils.isNetworkAvailable(this)){
			Toast.makeText(this, "无网络", Toast.LENGTH_SHORT).show();
			return;
		}

		DeliveryAddress address = new DeliveryAddress(null, null, name, shengshiqu, street, detial, tel, zipCode);
		service.addDeliveryAddress(address);
	}
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.shengshiqu:
			Utils.hideInputMethod(this, v);
			addressPicker.setVisibility(View.VISIBLE);
			shadowView.setVisibility(View.VISIBLE);
			break;

		case R.id.toolbar_text_menu:
			saveNewAddress();
			break;
		}
	}

	private WuzhaiServiceCallbackAdapter callbackAdapter = new WuzhaiServiceCallbackAdapter(){
		@Override
		public void onCompleted(int result) {
			switch (result) {
			case WuzhaiService.ADD_DELIVERY_ADDRESS_SUCC:
				Toast.makeText(AddNewAddressActivity.this, "保存成功", Toast.LENGTH_SHORT).show();
				break;

			case WuzhaiService.ADD_DELIVERY_ADDRESS_FAL:
				Toast.makeText(AddNewAddressActivity.this, "保存失败，请重试", Toast.LENGTH_SHORT).show();
				break;

			case WuzhaiService.NETWORK_ERR:
				Toast.makeText(AddNewAddressActivity.this, "网络错误，请重试", Toast.LENGTH_SHORT).show();
				break;
			}
		}
	};
}
