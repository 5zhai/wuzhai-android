package com.wuzhai.app.main.market.widget;

import java.io.Serializable;

public class Goods implements Serializable{

	private int id;
	private int commodityCategoryId;
	private String commodityCategory;
	private String publisherName;
	private String publisherAvatar;
	private String title;
	private String city;
	private String desc;
	private String mainImage;
	private int price;
	private int stock;
	private String[] tags;
	private double longitude;
	private double latitude;
	private int viewCount;
	private int commentsCount;
	private int saleCount;
	private int orderCount;
	private String[] detialImageList;
	private boolean collected = false;
	private boolean followed = false;

	public Goods(){}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCommodityCategoryId() {
		return commodityCategoryId;
	}

	public void setCommodityCategoryId(int commodityCategoryId) {
		this.commodityCategoryId = commodityCategoryId;
	}

	public String getCommodityCategory() {
		return commodityCategory;
	}

	public void setCommodityCategory(String commodityCategory) {
		this.commodityCategory = commodityCategory;
	}

	public String getPublisherName() {
		return publisherName;
	}

	public void setPublisherName(String publisherName) {
		this.publisherName = publisherName;
	}

	public String getPublisherAvatar() {
		return publisherAvatar;
	}

	public void setPublisherAvatar(String publisherAvatar) {
		this.publisherAvatar = publisherAvatar;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getMainImage() {
		return mainImage;
	}

	public void setMainImage(String mainImage) {
		this.mainImage = mainImage;
	}

	public String[] getDetialImages() {
		return detialImageList;
	}

	public void setDetialImages(String images) {
		detialImageList = images.split(",");
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public String[] getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags.split(",");
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public int getViewCount() {
		return viewCount;
	}

	public void setViewCount(int viewCount) {
		this.viewCount = viewCount;
	}

	public int getCommentsCount() {
		return commentsCount;
	}

	public void setCommentsCount(int commentsCount) {
		this.commentsCount = commentsCount;
	}

	public int getSaleCount() {
		return saleCount;
	}

	public void setSaleCount(int boughtCount) {
		this.saleCount = boughtCount;
	}

	public int getOrderCount() {
		return orderCount;
	}

	public void setOrderCount(int orderCount) {
		this.orderCount = orderCount;
	}

	public boolean isCollected() {
		return collected;
	}

	public void setCollected(boolean collected) {
		this.collected = collected;
	}

	public boolean isFollowed() {
		return followed;
	}

	public void setFollowed(boolean followed) {
		this.followed = followed;
	}
}
