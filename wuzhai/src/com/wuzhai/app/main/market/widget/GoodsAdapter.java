package com.wuzhai.app.main.market.widget;

import java.util.ArrayList;

import com.squareup.picasso.Picasso;
import com.wuzhai.app.R;
import com.wuzhai.app.main.widget.HyRecyclerViewAdapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class GoodsAdapter extends HyRecyclerViewAdapter implements OnClickListener{

	private Context context;
	private ArrayList<Goods> goodsList;
	public GoodsAdapter(Context context,ArrayList<Goods> goodsList){
		this.context = context;
		this.goodsList = goodsList;
	}

	public GoodsAdapter(Context context){
		this.context = context;
	}

	@Override
	public int getItemCount() {
		// TODO Auto-generated method stub
		return goodsList.size();
	}

	@Override
	public void onBindViewHolder(ViewHolder arg0, int arg1) {
		Goods goods = goodsList.get(arg1);
		GoodsViewHolder viewHolder = (GoodsViewHolder)arg0;
		if(!TextUtils.isEmpty(goods.getMainImage())){
			Picasso.with(context).load(goods.getMainImage()).resize(120, 200).into(viewHolder.goodsPicture);
		}
		viewHolder.goodsPrice.setText(goods.getPrice()+"");
		viewHolder.goodsTitle.setText(goods.getTitle());
		viewHolder.goodsPlace.setText(goods.getCity());
		viewHolder.itemView.setTag(arg1);
	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup arg0, int arg1) {
		View view = LayoutInflater.from(context).inflate(R.layout.market_goods_item, arg0,false);
		view.setOnClickListener(this);
		return new GoodsViewHolder(view);
	}

	private static class GoodsViewHolder extends ViewHolder{

		ImageView goodsPicture;
		TextView goodsTitle;
		TextView goodsPrice;
		TextView goodsPlace;
		
		public GoodsViewHolder(View arg0) {
			super(arg0);
			goodsPicture = (ImageView)arg0.findViewById(R.id.goods_picture);
			goodsTitle = (TextView)arg0.findViewById(R.id.goods_title);
			goodsPrice = (TextView)arg0.findViewById(R.id.goods_price);
			goodsPlace = (TextView)arg0.findViewById(R.id.goods_place);
		}
		
	}

	@Override
	public void onClick(View v) {
		if(recyclerViewItemClickListener!=null)
		recyclerViewItemClickListener.onItemClick(v, (Integer)(v.getTag()));
	}

	public void setGoodsList(ArrayList<Goods> goodsList){
		this.goodsList = goodsList;
		notifyDataSetChanged();
	}
}
