package com.wuzhai.app.main.market.widget;

import java.io.Serializable;

import com.wuzhai.app.tools.Utils;

public class ComicConNews implements Serializable{

	private int id;
	private String picturePath;
	private String title;
	private String place;
	private String beginTime;
	private String endTime;
	private String desc;
	private double longitude;
	private double latitude;
	private String summary;
	private String category;

	public ComicConNews(int id,String picturePath, String title, String place,
			int beginTime,int endTime,String desc,double longitude,double latitude,
			String summary,String category) {
		this.id = id;
		this.picturePath = picturePath;
		this.title = title;
		this.beginTime = Utils.getDateFromMillisecond(beginTime);
		this.endTime = Utils.getDateFromMillisecond(endTime);
		this.desc = desc;
		this.place = place;
		this.latitude = latitude;
		this.longitude = longitude;
		this.summary = summary;
		this.category = category;
	}

	public int getId() {
		return id;
	}

	public String getPicturePath() {
		return picturePath;
	}

	public String getTitle() {
		return title;
	}

	public String getPlace() {
		return place;
	}

	public String getTime() {
		return beginTime+" — "+endTime;
	}

	public String getDesc() {
		return desc;
	}

	public double getLongitude() {
		return longitude;
	}

	public double getLatitude() {
		return latitude;
	}

	public String getSummary() {
		return summary;
	}

	public String getCategory() {
		return category;
	}
}
