package com.wuzhai.app.login.activity;

import java.util.HashMap;

import cn.sharesdk.douban.Douban;
import cn.sharesdk.sina.weibo.SinaWeibo;

import com.wuzhai.app.R;
import com.wuzhai.app.application.WuZhaiApplication;
import com.wuzhai.app.login.service.LoginService;
import com.wuzhai.app.login.service.LoginService.LoginBind;
import com.wuzhai.app.main.MainActivity;
import com.wuzhai.app.objects.User;
import com.wuzhai.app.register.activity.RegisterActivity;
import com.wuzhai.app.register.activity.UploadUserInfoActivity;
import com.wuzhai.app.tools.Utils;
import com.wuzhai.app.widget.TitleToolbarActivity;

import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class LoginActivity extends TitleToolbarActivity implements OnClickListener,LoginService.CallBack{

	private EditText usernameEditText;
	private EditText passwordEditText;
	private Button loginButton;
	private Button weibo_loginButton;
	private Button wechat_loginButton;
	private Button douban_loginButton;
//	private Button tieba_loginButton;
	private TextView forgetpasswordTextView;
	private TextView loginproblemTextView;
//	private TextView register_nowTextView;
	private LoginService loginservice;
	private LinearLayout progressView;
	private ImageButton enableSwitcher;
	
	private ServiceConnection connection;
	private String TAG = "LoginActivity";
	private boolean isPasswordEnable = false;
	private ImageView bigIcon;
	private LinearLayout toolbarBack;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTitle("登录");
		setTextMenuString("注册");
		setTextMenuClickListener(this);
		setContentView(R.layout.activity_login);
		toolbarBack = (LinearLayout)findViewById(R.id.toolbar_back);
		toolbarBack.setVisibility(View.GONE);
		usernameEditText = (EditText) findViewById(R.id.username_edit);
		passwordEditText = (EditText) findViewById(R.id.password_edit);
		loginButton = (Button) findViewById(R.id.login_btn);
		weibo_loginButton = (Button) findViewById(R.id.weibo_login_btn);
		wechat_loginButton = (Button) findViewById(R.id.wechat_login_btn);
		douban_loginButton = (Button) findViewById(R.id.douban_login_btn);
		forgetpasswordTextView = (TextView) findViewById(R.id.forgetpassword);
		progressView = (LinearLayout)findViewById(R.id.progressView);
		enableSwitcher = (ImageButton)findViewById(R.id.enable_switcher);
		bigIcon = (ImageView)findViewById(R.id.big_icon);

		usernameEditText.setOnFocusChangeListener(focusChangeListener);
		passwordEditText.setOnFocusChangeListener(focusChangeListener);
		loginButton.setOnClickListener(this);
		weibo_loginButton.setOnClickListener(this);
		douban_loginButton.setOnClickListener(this);
		forgetpasswordTextView.setOnClickListener(this);
		enableSwitcher.setOnClickListener(this);
	}

	@Override
	protected void onStart() {
		super.onStart();
		bindLoginService();
	}

	@Override
	protected void onStop() {
		super.onStop();
		unbindService(connection);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.login_btn: login();break;
		case R.id.weibo_login_btn: thirdPartyLogin(SinaWeibo.NAME);break;
		case R.id.wechat_login_btn: /*thirdPartyLogin(QQ.NAME);*/break;
		case R.id.douban_login_btn: thirdPartyLogin(Douban.NAME);break;
		case R.id.forgetpassword:findPassword();break;
		case R.id.toolbar_text_menu:goToRegister();break;
		case R.id.enable_switcher:switchPasswordEnable();break;
		}
	}
	
	private void login(){
		String username = usernameEditText.getText().toString();
		String password = passwordEditText.getText().toString();
		if("".equals(username) || "".equals(password)){
			Toast.makeText(this, getString(R.string.login_info_not_complete), Toast.LENGTH_SHORT).show();
			return;
		}
		if(!Utils.isNetworkAvailable(this)){
			Toast.makeText(this, getString(R.string.no_network), Toast.LENGTH_SHORT).show();
			return;
		}
		doSomethingBeforeLogin();
		loginservice.login(username, password);
//		startActivity(new Intent(this, MainActivity.class));
	}
//----------第三方登录相关----------------	
	private void thirdPartyLogin(String platformName){	
		loginservice.thirdPartyLogin(platformName);
	}

//--------------公共-----------------
	private void bindLoginService(){
		Intent intent = new Intent(this,LoginService.class);
		connection = new ServiceConnection() {
			@Override
			public void onServiceDisconnected(ComponentName name) {
				// TODO Auto-generated method stub
			}
			@Override
			public void onServiceConnected(ComponentName name, IBinder service) {
				loginservice = ((LoginBind)service).getService();
				loginservice.setCallBack(LoginActivity.this);
			}
		};

		bindService(intent, connection, BIND_AUTO_CREATE);
	}

	@Override
	public void onLoginCompleted(int result) {
		doSomethingBeforeLoginCompleted();
		switch (result) {
		case LoginService.LOGIN_SUCCESSFUL:
			startActivity(new Intent(this, MainActivity.class));
			break;
		case LoginService.ACCOUND_ERR:
			Toast.makeText(this, getString(R.string.username_password_err),
					Toast.LENGTH_LONG).show();
			break;
		case LoginService.NETWORK_ERR:
			Toast.makeText(this, getString(R.string.network_err),
					Toast.LENGTH_LONG).show();
			break;
		}
	}

	@Override
	public void onThirdPartHaveLoginBefore() {
		Toast.makeText(this, "登录成功", Toast.LENGTH_LONG).show();
		startActivity(new Intent(this, MainActivity.class));
//		  User user = ((WuZhaiApplication)getApplication()).getUser();
//		  String resultString = "UserName:"+user.getName()+"\n"+"PhoneNumber:"+user.getPhoneNumber()+"\n"+"Id:"+user.getId()+"\n"+"AvatarUrl:"+user.getAvatarUrl()+"\n"+"Sex:"+user.getSexString()+"\n";
//		  Toast.makeText(this, "第三方以前登录过，跳到主页\n"+resultString, Toast.LENGTH_LONG).show();
	}

	@Override
	public void onThirdPartHaveNotLoginBefore(String platform, String userid,
			HashMap<String, Object> ext_info) {
		 Intent intent = new Intent(this,UploadUserInfoActivity.class);
		 Bundle bundle = new Bundle();
		 bundle.putString("platform", platform);
		 bundle.putString("uid", userid);
		 bundle.putSerializable("ext_info", ext_info);
		 intent.putExtras(bundle);
		 startActivity(intent);
		 finish();
	}
	
	private void doSomethingBeforeLogin(){
		progressView.setVisibility(View.VISIBLE);
		usernameEditText.setCursorVisible(false);
		passwordEditText.setCursorVisible(false);
//		InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
//		imm.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);
	}
	private void doSomethingBeforeLoginCompleted(){
		usernameEditText.setCursorVisible(true);
		passwordEditText.setCursorVisible(true);
		progressView.setVisibility(View.GONE);
	}
	
	private void goToRegister(){
		startActivity(new Intent(this,RegisterActivity.class));
	}

	private void findPassword(){
		startActivity(new Intent(this,FindPasswordActivity.class));
	}

	private void switchPasswordEnable(){
		if(isPasswordEnable){
			passwordEditText.setTransformationMethod(PasswordTransformationMethod.getInstance());
			isPasswordEnable = false;
			enableSwitcher.setBackgroundResource(R.drawable.icon_ciphertext);
		}else {
			passwordEditText.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
			isPasswordEnable = true;
			enableSwitcher.setBackgroundResource(R.drawable.icon_plaintext);
		}
	}

	private OnFocusChangeListener focusChangeListener = new OnFocusChangeListener() {

		@Override
		public void onFocusChange(View v, boolean hasFocus) {
			if(v.getId() == R.id.username_edit && hasFocus){
				bigIcon.setImageResource(R.drawable.login_bg1);
			}else {
				bigIcon.setImageResource(R.drawable.login_bg2);
			}
		}
	};
}
