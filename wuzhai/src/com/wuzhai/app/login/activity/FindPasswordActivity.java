package com.wuzhai.app.login.activity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.wuzhai.app.R;
import com.wuzhai.app.application.WuZhaiApplication;
import com.wuzhai.app.main.WuzhaiService;
import com.wuzhai.app.main.WuzhaiServiceCallbackAdapter;
import com.wuzhai.app.tools.Utils;
import com.wuzhai.app.widget.TitleToolbarActivity;

public class FindPasswordActivity extends TitleToolbarActivity {

	private WuzhaiService service;
	private EditText phoneNumEditText;
	private EditText verCodeEditText;
	private Button getVerCodeBtn;
	private EditText passwordEditText;
	private EditText passwordConfirmEditText;
	private Button registerBtn;
	private Handler handler;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTitle("找回密码");
		service = ((WuZhaiApplication)getApplication()).getService();
		service.setCallBack(callbackAdapter);
		setContentView(R.layout.activity_register);
		initView();
		handler = new Handler(){
			@Override
			public void handleMessage(Message msg) {
				if(msg.what!=0){
					getVerCodeBtn.setText(""+msg.what);
				}else {
					getVerCodeBtn.setText("发送验证码");
				}
			}
		};
	}

	private void initView(){
		phoneNumEditText = (EditText)findViewById(R.id.phone_number);
		verCodeEditText = (EditText)findViewById(R.id.verification_code);
		passwordEditText = (EditText)findViewById(R.id.password);
		passwordConfirmEditText = (EditText)findViewById(R.id.password_confirm);
		getVerCodeBtn = (Button)findViewById(R.id.get_verification_code);
		registerBtn = (Button)findViewById(R.id.register);
		registerBtn.setText("修改密码");
		getVerCodeBtn.setOnClickListener(clickListener);
		registerBtn.setOnClickListener(clickListener);
	}

	private void getVerificationCode(){
		String phoneNum = phoneNumEditText.getText().toString();
		if(TextUtils.isEmpty(phoneNum)){
			Toast.makeText(this, "请输入电话号码", Toast.LENGTH_SHORT).show();
			return;
		}
		if(!Utils.isPhoneNumber(phoneNum)){
			Toast.makeText(this, "请输入正确的电话号码", Toast.LENGTH_SHORT).show();
			return;
		}
		if(!Utils.isNetworkAvailable(this)){
			Toast.makeText(this, "无网络连接", Toast.LENGTH_SHORT).show();
			return;
		}
		service.getFindPasswordCode(phoneNum);
	}

	private OnClickListener clickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.get_verification_code:
				getVerificationCode();
				break;

			case R.id.register:
				changePassword();
				break;
			}
		}
	};

	private void changePassword(){
		String phoneNum = phoneNumEditText.getText().toString();
		String verCode = verCodeEditText.getText().toString();
		String password = passwordEditText.getText().toString();
		String passwordConfirm = passwordConfirmEditText.getText().toString();

		if(TextUtils.isEmpty(phoneNum) || TextUtils.isEmpty(verCode) ||
				TextUtils.isEmpty(password) || TextUtils.isEmpty(passwordConfirm)){
			Toast.makeText(this, "信息不完整！", Toast.LENGTH_SHORT).show();
			return;
		}
		if(!password.equals(passwordConfirm)){
			Toast.makeText(this, "密码不一致，请重新输入", Toast.LENGTH_SHORT).show();
			return;
		}
		if(!Utils.isNetworkAvailable(this)){
			Toast.makeText(this, "无网络连接", Toast.LENGTH_SHORT).show();
			return;
		}

		service.changePassword(phoneNum, verCode, password, passwordConfirm);
	}

	private WuzhaiServiceCallbackAdapter callbackAdapter =  new WuzhaiServiceCallbackAdapter(){

		@Override
		public void onCompleted(int result) {
			switch (result) {
			case WuzhaiService.GET_FIND_PASSWORD_SUCC:
				startCountdown();
				Toast.makeText(FindPasswordActivity.this, "验证码发送成功，请注意查收", Toast.LENGTH_SHORT).show();
				break;
			case WuzhaiService.GET_FIND_PASSWORD_FAIL:
				Toast.makeText(FindPasswordActivity.this, "验证码发送失败，请重新发送", Toast.LENGTH_SHORT).show();
				break;
			case WuzhaiService.CHANGE_PASSWORD_SUCC:
				Toast.makeText(FindPasswordActivity.this, "密码修改成功", Toast.LENGTH_SHORT).show();
				break;
			case WuzhaiService.CHANGE_PASSWORD_FAIL:
				Toast.makeText(FindPasswordActivity.this, "密码修改失败", Toast.LENGTH_SHORT).show();
				break;
			}
		}
	};

	private void startCountdown(){
		new Thread(new Runnable() {
			@Override
			public void run() {
				for(int n=30;n>=0;n--){
					try {
						handler.sendEmptyMessage(n);
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		}).start();
	}
}
