package com.wuzhai.app.login.service;

import io.rong.imkit.RongIM;
import io.rong.imlib.RongIMClient;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ResponseCache;
import java.net.URL;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.PlatformActionListener;
import cn.sharesdk.framework.ShareSDK;

import com.wuzhai.app.R;
import com.wuzhai.app.application.WuZhaiApplication;
import com.wuzhai.app.main.MainActivity;
import com.wuzhai.app.objects.User;
import com.wuzhai.app.tools.Utils;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

public class LoginService extends Service implements PlatformActionListener{

	private CallBack callBack;
	private Handler loginThreadhandler;
	//-------loginThreadhandler-----
	private final int LOGIN = 0;
	private final int OAUTH_SUCCESSFUL = 1;
	//------mainThreadHandler-------
	public static final int LOGIN_SUCCESSFUL = 0;
	public static final int ACCOUND_ERR = 1;
	public static final int NETWORK_ERR = 2;
	public static final int HAVE_LOGIN_BEFORE = 6;
	public static final int HAVE_NOT_LOGIN_BEFORE = 7;

    private String userId = null;
    private String platform = null;
    private HashMap<String, Object> ext_info = null;
	private Handler handler;
	private LoginThread loginThread;

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return new LoginBind();
	}

	public class LoginBind extends Binder{
		public LoginService getService(){
			return LoginService.this;
		}
	}

	public interface CallBack{
		public void onLoginCompleted(int result);
		public void onThirdPartHaveLoginBefore();
		public void onThirdPartHaveNotLoginBefore(String platform,String userid,HashMap<String, Object> ext_info);
	}

	public void setCallBack(CallBack callBack){
		this.callBack = callBack;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		loginThread = new LoginThread();
		loginThread.start();
		handler = new Handler(){
			@Override
			public void handleMessage(Message msg) {
				switch (msg.what) {
				case HAVE_LOGIN_BEFORE:
					callBack.onThirdPartHaveLoginBefore();
					break;
				case HAVE_NOT_LOGIN_BEFORE:
//					HashMap<String, String> map = (HashMap<String, String>)msg.obj;
//					callBack.onThirdPartHaveNotLoginBefore(map.get("platform"),map.get("uid"));
					Log.d("yue.huang", "第三方没登录过");
					Toast.makeText(LoginService.this, "第三方没登录过", Toast.LENGTH_LONG).show();
					break;
				default:
					callBack.onLoginCompleted(msg.what);
					break;
				}
			}
		};

	}

	@Override
	public void onDestroy() {
		loginThreadhandler.getLooper().quit();
		super.onDestroy();
	}

	private class LoginThread extends Thread{

		@Override
		public void run() {
			Looper.prepare();
			loginThreadhandler = new Handler(){
				@Override
				public void handleMessage(Message msg) {
					switch(msg.what){
					case LOGIN:login((HashMap<String, String>)msg.obj);break;
					case OAUTH_SUCCESSFUL:oauthSuccessful((HashMap<String, String>)msg.obj);break;
					}
				}
			};
			Looper.loop();
		}
	}

    private byte[] readStream(InputStream in) throws IOException{
        ByteArrayOutputStream bytesOut = new ByteArrayOutputStream();
        byte[] bytes = new byte[512];
        int len = 0;
        while(-1!=(len=in.read(bytes))){
            bytesOut.write(bytes, 0, len);
        }
        in.close();
        return bytesOut.toByteArray();
    }

    private void userInfoParse(JSONObject jsonObject) throws JSONException{
		JSONObject userJsonObject = jsonObject.getJSONObject("data");
		User user = ((WuZhaiApplication) getApplication()).getUser();
		user.setId(userJsonObject.getInt("id"));
		user.setName(userJsonObject.getString("username"));
		user.setPhoneNumber(userJsonObject.getString("phone_number"));
		user.setAvatarUrl(userJsonObject.getString("avatar"));
		user.setAccessKey(userJsonObject.getString("access_key"));
		user.setImToken(userJsonObject.getString("im_token"));
		user.setBirthday(userJsonObject.getString("birthday"));
		user.setSexId(userJsonObject.optInt("sex", -1));
		user.setSignature(userJsonObject.getString("signature"));
		user.setFollowedsCount(userJsonObject.getInt("followeds_count"));
		user.setFollowersCount(userJsonObject.getInt("followers_count"));
		user.setLevel(userJsonObject.getInt("level"));
		user.setBalance(userJsonObject.getString("balance"));
		user.setNeedExperience(userJsonObject.getInt("need_experience"));
		user.setCurrentExperience(userJsonObject.getInt("current_experience"));
		JSONArray oauthUsersJSONArray = userJsonObject.getJSONArray("oauth_users");
		for (int i = 0; i < oauthUsersJSONArray.length(); i++) {
			JSONObject oauthUserJsonObject = oauthUsersJSONArray.getJSONObject(i);
			user.addOauthUser(oauthUserJsonObject.getInt("id"), oauthUserJsonObject.getString("uid"), oauthUserJsonObject.getString("platform"));
		}
		JSONObject liveRoom = userJsonObject.optJSONObject("live_room");
		if(liveRoom!=null){
			user.getLiveRoom().setId(liveRoom.getInt("id"));
			user.getLiveRoom().setTitle(liveRoom.getString("title"));
			user.getLiveRoom().setDesc(liveRoom.getString("desc"));
			user.getLiveRoom().setRoomKey(liveRoom.getString("room_key"));
			user.getLiveRoom().setState(liveRoom.getString("state"));
			user.getLiveRoom().setOnlineCount(liveRoom.getInt("online_count"));
			user.getLiveRoom().setBrief(liveRoom.getString("brief"));
			user.getLiveRoom().setStreamJson(liveRoom.getJSONObject("stream_json").toString());
		}
		Log.d("yue.huang", "id:"+user.getId());
		Log.d("yue.huang", "username:"+user.getName());
		Log.d("yue.huang", "phonenumber:"+user.getPhoneNumber());
		Log.d("yue.huang", "avatar:"+user.getAvatarUrl());
		Log.d("yue.huang", "accesskey:"+user.getAccessKey());
		Log.d("yue.huang", "token:"+user.getImToken());
		Log.d("yue.huang", "Birthday:"+user.getBirthday());
		Log.d("yue.huang", "sex:"+user.getSexId());
		Log.d("yue.huang", "Signature:"+user.getSignature());
		Log.d("yue.huang", "FollowedsCount:"+user.getFollowedsCount());
		Log.d("yue.huang", "FollowersCount:"+user.getFollowersCount());
		Log.d("yue.huang", "Level:"+user.getLevel());
		Log.d("yue.huang", "Balance:"+user.getBalance());
		Log.d("yue.huang", "NeedExperience:"+user.getBalance());
		Log.d("yue.huang", "CurrentExperience:"+user.getCurrentExperience());
		if(user.getOauthUsers().size()!=0){
			Log.d("yue.huang", "platform_id:"+user.getOauthUsers().get(0).getId());
			Log.d("yue.huang", "platform_uid:"+user.getOauthUsers().get(0).getUid());
			Log.d("yue.huang", "platform_platform:"+user.getOauthUsers().get(0).getPlatform());
		}
    }
	//--------------login------------------------
	public void login(String username,String password){
		Message msg = Message.obtain();
		msg.what = LOGIN;
		HashMap<String, String> parametersMap = new HashMap<String, String>();
		parametersMap.put("phone_number", username);
		parametersMap.put("password", password);
		msg.obj = parametersMap;
		loginThreadhandler.sendMessage(msg);
	} 

	private void login(HashMap<String, String> parametersMap){
		HttpURLConnection connection = null;
		try {
            connection = Utils.getHttpsConnectionPost("https://www.5yuzhai.com:443/api/v1/sign_in", parametersMap);
            if(connection.getResponseCode() == 200){
                InputStream in = connection.getInputStream();
                byte[] bytes = readStream(in);
                String res = new String(bytes);
                JSONObject resultJson = new JSONObject(res);
                boolean result = resultJson.optBoolean("success");
                if(result){
                	userInfoParse(resultJson);
                	handler.sendEmptyMessage(LOGIN_SUCCESSFUL);
                }else{
                	handler.sendEmptyMessage(ACCOUND_ERR);
				}
            }else if(connection.getResponseCode() == 400){
            	handler.sendEmptyMessage(ACCOUND_ERR);
			}
		} catch (Exception e) {
			e.printStackTrace();
			handler.sendEmptyMessage(NETWORK_ERR);
		}finally{
			if(null!=connection){
				connection.disconnect();
			}
		}
	}

    //------------------第三方登录---------------------
    public void thirdPartyLogin(String platformName){
    	ShareSDK.initSDK(this);
		Platform platform = ShareSDK.getPlatform(platformName);
		platform.setPlatformActionListener(this);
		//关闭SSO授权
		platform.SSOSetting(true);
		platform.showUser(null);
    }

	@Override
	public void onCancel(Platform arg0, int arg1) {
		// TODO Auto-generated method stub
	}
	@Override
	public void onComplete(Platform arg0, int arg1, HashMap<String, Object> arg2) {
//		userId = arg0.getDb().getUserId();
//		platform = arg0.getName();
//		ext_info = arg2;
//		loginThreadhandler.sendEmptyMessage(OAUTH_SUCCESSFUL);
		Log.d("yue.huang", "complete："+"platform:"+arg0.getName()+"----"+"uid:"+arg0.getDb().getUserId());
		Log.d("yue.huang", "complete："+"username:"+arg0.getDb().getUserName()+"----"+"avatar:"+arg0.getDb().getUserIcon());
		Message msg = Message.obtain();
		msg.what = OAUTH_SUCCESSFUL;
		HashMap<String, String> parametersMap = new HashMap<String, String>();
		parametersMap.put("platform", arg0.getName());
		parametersMap.put("uid", arg0.getDb().getUserId());
		parametersMap.put("username", arg0.getDb().getUserName());
		parametersMap.put("avatar", arg0.getDb().getUserIcon());
		msg.obj = parametersMap;
		loginThreadhandler.sendMessage(msg);
	}
	@Override
	public void onError(Platform arg0, int arg1, Throwable arg2) {
		arg0.removeAccount();
	}
	
	private void oauthSuccessful(HashMap<String, String> parametersMap){
		//上传userid和第三方平台的名字(userid是唯一的，平台名字不必要)到服务器，服务器判断用户是否注册，如果注册则返回登录成功并返回用户信息；
		//如果没注册则返回未注册，客户端跳到注册页面
		HttpURLConnection connection = null;
		try {
			connection = Utils.getHttpsConnectionPost("https://www.5yuzhai.com:443/api/v1/check_oauth", parametersMap);
            if(connection.getResponseCode() == 200){
                InputStream in = connection.getInputStream();
                byte[] bytes = readStream(in);
                String res = new String(bytes);
                JSONObject resultJson = new JSONObject(res);
                boolean result = resultJson.optBoolean("success");
                Log.d("yue.huang", "resultJson:"+resultJson);
                if(result){
                	userInfoParse(resultJson);
                	handler.sendEmptyMessage(HAVE_LOGIN_BEFORE);
				} else {
//					Message msg = Message.obtain();
//					msg.what = HAVE_NOT_LOGIN_BEFORE;
//					msg.obj = parametersMap;
//					handler.sendMessage(msg);
				}
            }
		} catch (Exception e) {
			e.printStackTrace();
			handler.sendEmptyMessage(NETWORK_ERR);
		}finally{
			if(null!=connection){
				connection.disconnect();
			}
		}
	}
}
