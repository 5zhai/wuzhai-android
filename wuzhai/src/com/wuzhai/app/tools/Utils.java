package com.wuzhai.app.tools;

import io.rong.imkit.utils.BitmapUtil;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.http.conn.util.InetAddressUtils;
import org.json.JSONException;
import org.json.JSONObject;

import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.Platform.ShareParams;
import cn.sharesdk.framework.PlatformActionListener;
import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.onekeyshare.OnekeyShare;
import cn.sharesdk.tencent.qzone.QZone;

import com.ucloud.common.util.Md5;
import com.wuzhai.app.R;
import com.wuzhai.app.objects.Tag;
import com.wuzhai.app.widget.TagInputView;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.renderscript.Allocation;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.support.v4.graphics.BitmapCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1) @SuppressLint("NewApi") public class Utils {

	// 此方法将来会直接传递网络获取的bitmap
	public static RoundedBitmapDrawable getRoundedUserPortrait(Context context,
			int id) {
		Bitmap bitmap = BitmapFactory
				.decodeResource(context.getResources(), id);
		RoundedBitmapDrawable roundedPortraitDrawable = RoundedBitmapDrawableFactory
				.create(context.getResources(), bitmap);
		roundedPortraitDrawable.setAntiAlias(true);
		roundedPortraitDrawable.setCornerRadius(bitmap.getWidth() / 2);
		return roundedPortraitDrawable;
	}

	public static RoundedBitmapDrawable getRoundedUserPortrait(Context context,Bitmap bitmap) {
		RoundedBitmapDrawable roundedPortraitDrawable = RoundedBitmapDrawableFactory
				.create(context.getResources(), bitmap);
		roundedPortraitDrawable.setAntiAlias(true);
		roundedPortraitDrawable.setCornerRadius(bitmap.getWidth() / 2);
		return roundedPortraitDrawable;
	}

	public static int dpToPx(int dp) {
		return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
	}

	public static boolean isPhoneNumber(String tel) {
		Pattern p = Pattern
				.compile("^((13\\d{9}$)|(15[0,1,2,3,5,6,7,8,9]\\d{8}$)|(18[0,2,5,6,7,8,9]\\d{8}$)|(147\\d{8})$)");
		Matcher m = p.matcher(tel);
		return m.matches();
	}

	public static boolean isNetworkAvailable(Context context) {
		ConnectivityManager connectivityManager = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);

		if (connectivityManager == null) {
			return false;
		} else {
			NetworkInfo[] networkInfo = connectivityManager.getAllNetworkInfo();

			if (networkInfo != null && networkInfo.length > 0) {
				for (int i = 0; i < networkInfo.length; i++) {
					if (networkInfo[i].getState() == NetworkInfo.State.CONNECTED) {
						return true;
					}
				}
			}
		}
		return false;
	}

	public static String getParametersJsonString(HashMap<String, String> parameters) throws JSONException {
		JSONObject jsonObject = new JSONObject();
		for (String key : parameters.keySet()) {
			jsonObject.put(key, parameters.get(key));
		}
		return jsonObject.toString();
	}

	public static HttpsURLConnection getHttpsConnectionPost(String utl,HashMap<String, String> parametersMap) throws Exception {
		URL url = new URL(utl);
//		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		HttpsURLConnection connection = getHttpsConnection(url);
		connection.setConnectTimeout(5000);
		connection.setReadTimeout(5000);
		connection.setRequestMethod("POST");
		connection.setDoInput(true);
		connection.setDoOutput(true);
		connection.setUseCaches(false);
		connection.setRequestProperty("connection", "keep-alive");
		connection.setRequestProperty("Charsert", "UTF-8");
		connection.setRequestProperty("Content-Type","application/json;charset=utf-8");
		DataOutputStream dataOut = new DataOutputStream(connection.getOutputStream());
		String parameter = getParametersJsonString(parametersMap);
		dataOut.write(parameter.getBytes("UTF-8"));
		dataOut.flush();
		dataOut.close();
		return connection;
	}

	public static Bitmap getBitmapFromURI(Context context,Uri uri,boolean isCompression) {
		ContentResolver cr = context.getContentResolver();
		Bitmap bmp = null;
		try {
			if(!isCompression){
				bmp = BitmapFactory.decodeStream(cr.openInputStream(uri));
			}else {
				BitmapFactory.Options newOpts = new BitmapFactory.Options();
				newOpts.inJustDecodeBounds = true;
				BitmapFactory.decodeStream(cr.openInputStream(uri), null, newOpts);

		        int w = newOpts.outWidth;
		        int h = newOpts.outHeight;
		        float hh = 240f;
		        float ww = 320f;
		        int be = 1;
		        if (w > h && w > ww) {
		            be = (int) (newOpts.outWidth / ww);
		        } else if (w < h && h > hh) {
		            be = (int) (newOpts.outHeight / hh);
		        }
		        if (be <= 0)
		            be = 1;
		        newOpts.inSampleSize = be;
		        newOpts.inJustDecodeBounds = false;
		        bmp = BitmapFactory.decodeStream(cr.openInputStream(uri), null, newOpts);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return bmp;
	}

	public static byte[] readStream(InputStream in) throws IOException{
        ByteArrayOutputStream bytesOut = new ByteArrayOutputStream();
        byte[] bytes = new byte[512];
        int len = 0;
        while(-1!=(len=in.read(bytes))){
            bytesOut.write(bytes, 0, len);
        }
        in.close();
        return bytesOut.toByteArray();
    }

	public static void hideInputMethod(Context context,View view){
		InputMethodManager imm = (InputMethodManager)context.getSystemService(context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
	}

	public static String getDateFromMillisecond(int millisecond){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd");
		String date = sdf.format(new Date(millisecond*1000L));
		return date;
	}

	public static String getDifferenceWithCurrentTime(int millisecond){
	    long different = System.currentTimeMillis() - millisecond*1000L;
	    long second = different/1000;

		if (second < 60) {
			return "刚刚";
		} else if (second < 3600) {
			return second / 60 + "分前";
		} else if (second < 86400) {
			return second / 60 / 60 + "小时前";
		} else {
			return second / 60 / 60 / 24 + "天前";
		}
	}

	public static HttpsURLConnection getHttpsConnection(URL url) throws Exception{
	    SSLContext context = SSLContext.getInstance("TLS");
	    context.init(null, new TrustManager[] { new TrustAllManager() }, null);
	    HttpsURLConnection.setDefaultSSLSocketFactory(context.getSocketFactory());
	    HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
			@Override
			public boolean verify(String hostname, SSLSession session) {
				return true;
			}
	    });
	    HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
	    return connection;
	}

	 private static class TrustAllManager implements X509TrustManager {
			@Override
			public void checkClientTrusted(
					java.security.cert.X509Certificate[] chain, String authType)
					throws java.security.cert.CertificateException {
			}
			@Override
			public void checkServerTrusted(
					java.security.cert.X509Certificate[] chain, String authType)
					throws java.security.cert.CertificateException {
			}
			@Override
			public java.security.cert.X509Certificate[] getAcceptedIssuers() {
				return null;
			}
	};

	public static Bitmap decodeUriAsBitmap(Context context,Uri uri) {
		Bitmap bitmap = null;
		try {
			bitmap = BitmapFactory.decodeStream(context.getContentResolver().openInputStream(uri));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return null;
		}
		return bitmap;
	}

	public static void showSingleChoiceDialog(final Context context,String title,final TextView textview,final String[] items,int checkedItem){
		AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
		dialogBuilder.setTitle(title);
		dialogBuilder.setSingleChoiceItems(items, checkedItem, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				textview.setText(items[which]);
				dialog.dismiss();
			}
		});
		dialogBuilder.show();
	}

	public static void showTagInputDialog(Context context,ArrayList<Tag> tagsList,final TextView textView,String title){
		final TagInputView view = new TagInputView(context, tagsList);
		AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
		dialogBuilder.setView(view);
		dialogBuilder.setTitle(title);
		dialogBuilder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
		dialogBuilder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				textView.setText(view.getInputString());
			}
		});
		dialogBuilder.show();
	}

	public static int getIntTypeFromString(HashMap<Integer, String> typeMap,
			String type) {
		for (int key : typeMap.keySet()) {
			if (typeMap.get(key).equals(type)) {
				return key;
			}
		}
		return -1;
	}


	@SuppressLint("NewApi") public static String uriToPath(final Context context, final Uri uri) {

	    final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

	    // DocumentProvider
	    if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
	        // ExternalStorageProvider
	        if (isExternalStorageDocument(uri)) {
	            final String docId = DocumentsContract.getDocumentId(uri);
	            final String[] split = docId.split(":");
	            final String type = split[0];

	            if ("primary".equalsIgnoreCase(type)) {
	                return Environment.getExternalStorageDirectory() + "/" + split[1];
	            }

	            // TODO handle non-primary volumes
	        }
	        // DownloadsProvider
	        else if (isDownloadsDocument(uri)) {

	            final String id = DocumentsContract.getDocumentId(uri);
	            final Uri contentUri = ContentUris.withAppendedId(
	                    Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

	            return getDataColumn(context, contentUri, null, null);
	        }
	        // MediaProvider
	        else if (isMediaDocument(uri)) {
	            final String docId = DocumentsContract.getDocumentId(uri);
	            final String[] split = docId.split(":");
	            final String type = split[0];

	            Uri contentUri = null;
	            if ("image".equals(type)) {
	                contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
	            } else if ("video".equals(type)) {
	                contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
	            } else if ("audio".equals(type)) {
	                contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
	            }

	            final String selection = "_id=?";
	            final String[] selectionArgs = new String[] {
	                    split[1]
	            };

	            return getDataColumn(context, contentUri, selection, selectionArgs);
	        }
	    }
	    // MediaStore (and general)
	    else if ("content".equalsIgnoreCase(uri.getScheme())) {
	        return getDataColumn(context, uri, null, null);
	    }
	    // File
	    else if ("file".equalsIgnoreCase(uri.getScheme())) {
	        return uri.getPath();
	    }

	    return null;
	}

	/**
	 * Get the value of the data column for this Uri. This is useful for
	 * MediaStore Uris, and other file-based ContentProviders.
	 *
	 * @param context The context.
	 * @param uri The Uri to query.
	 * @param selection (Optional) Filter used in the query.
	 * @param selectionArgs (Optional) Selection arguments used in the query.
	 * @return The value of the _data column, which is typically a file path.
	 */
	private static String getDataColumn(Context context, Uri uri, String selection,
	        String[] selectionArgs) {

	    Cursor cursor = null;
	    final String column = "_data";
	    final String[] projection = {
	            column
	    };

	    try {
	        cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
	                null);
	        if (cursor != null && cursor.moveToFirst()) {
	            final int column_index = cursor.getColumnIndexOrThrow(column);
	            return cursor.getString(column_index);
	        }
	    } finally {
	        if (cursor != null)
	            cursor.close();
	    }
	    return null;
	}


	/**
	 * @param uri The Uri to check.
	 * @return Whether the Uri authority is ExternalStorageProvider.
	 */
	private static boolean isExternalStorageDocument(Uri uri) {
	    return "com.android.externalstorage.documents".equals(uri.getAuthority());
	}

	/**
	 * @param uri The Uri to check.
	 * @return Whether the Uri authority is DownloadsProvider.
	 */
	private static boolean isDownloadsDocument(Uri uri) {
	    return "com.android.providers.downloads.documents".equals(uri.getAuthority());
	}

	/**
	 * @param uri The Uri to check.
	 * @return Whether the Uri authority is MediaProvider.
	 */
	private static boolean isMediaDocument(Uri uri) {
	    return "com.android.providers.media.documents".equals(uri.getAuthority());
	}

	public static Drawable getDrawableFromResources(Context context,int id,int width_dp,int height_dp){
		Drawable drawable = context.getResources().getDrawable(id);
		drawable.setBounds(0,0,dpToPx(width_dp),dpToPx(height_dp));
		return drawable;
	}

	public static int[] getScreenSize(Context context) {
		WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
		DisplayMetrics dm = new DisplayMetrics();
		wm.getDefaultDisplay().getMetrics(dm);
		return new int[]{dm.widthPixels,dm.heightPixels};
	}

	public static void onKeyShareOperation(Context context,String title,String titleUrl,String imageUrl,String content,String url,
			String comment,String site,String siteUrl){
		 ShareSDK.initSDK(context);
		 OnekeyShare oks = new OnekeyShare();
		 //关闭sso授权
		 oks.disableSSOWhenAuthorize();
		 // 分享时Notification的图标和文字  2.5.9以后的版本不调用此方法
		 //oks.setNotification(R.drawable.ic_launcher, getString(R.string.app_name));
		 // title标题，印象笔记、邮箱、信息、微信、人人网和QQ空间使用
		 oks.setTitle(title);
		 // titleUrl是标题的网络链接，仅在人人网和QQ空间使用
		 oks.setTitleUrl(titleUrl);
		 // text是分享文本，所有平台都需要这个字段
		 oks.setText(content);
		 // imagePath是图片的本地路径，Linked-In以外的平台都支持此参数
//		 oks.setImagePath("/storage/emulate/legacy/Pictures/8338b336gw1ehblcucgrfj20c80gm3zq.jpg");//确保SDcard下面存在此张图片
		 oks.setImageUrl(imageUrl);
		 // url仅在微信（包括好友和朋友圈）中使用
		 if(url!=null)
		 oks.setUrl(url);
		 // comment是我对这条分享的评论，仅在人人网和QQ空间使用
		 if(comment!=null)
		 oks.setComment(comment);
		 // site是分享此内容的网站名称，仅在QQ空间使用
//		 oks.setSite(getString(R.string.app_name));
		 if(site!=null)
		 oks.setSite(site);
		 // siteUrl是分享此内容的网站地址，仅在QQ空间使用
		 if(siteUrl!=null)
		 oks.setSiteUrl(siteUrl);

		 // 启动分享GUI
		 oks.show(context);
	}

	public static void onePlatformShare(Context context,String platformName,PlatformActionListener l,
			String title,String titleUrl,String content,String imageUrl,
			String imagePath,String site,String siteUrl){
		ShareSDK.initSDK(context);
		ShareParams sp = new ShareParams();
		sp.setTitle(title);
		if(titleUrl!=null)
		sp.setTitleUrl(titleUrl); // 标题的超链接
		sp.setText(content);
		if(imageUrl!=null)
		sp.setImageUrl(imageUrl);
		if(imagePath!=null)
		sp.setImagePath(imagePath);
		if(site!=null)
		sp.setSite("发布分享的网站名称");
		if(siteUrl!=null)
		sp.setSiteUrl("发布分享网站的地址");
		Platform platform = ShareSDK.getPlatform (platformName);
		platform. setPlatformActionListener (l); // 设置分享事件回调
		// 执行图文分享
		platform.share(sp);
	}

    public static String getLocalHostIp(){
        try
        {
            Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces();
            // 遍历所用的网络接口
            while (en.hasMoreElements())
            {
                NetworkInterface nif = en.nextElement();// 得到每一个网络接口绑定的所有ip
                Enumeration<InetAddress> inet = nif.getInetAddresses();
                // 遍历每一个接口绑定的所有ip
                while (inet.hasMoreElements())
                {
                    InetAddress ip = inet.nextElement();
                    if (!ip.isLoopbackAddress()
                            && InetAddressUtils.isIPv4Address(ip
                                    .getHostAddress()))
                    {
                        return ip.getHostAddress();
                    }
                }

            }
        }
        catch (SocketException e)
        {
            e.printStackTrace();
        }
        return "";
    }

	public static String getWxPaySignedString(String appid, String partnerid,String prepayid, String pkg, String noncestr, String timestamp,String key) {
		String strToSign1 = "appid=" + appid + "&noncestr=" + noncestr+ "&package=" + pkg + "&partnerid=" + partnerid + "&prepayid="+ prepayid + "&timestamp=" + timestamp;
		String strToSign2 = strToSign1 + "&key=" + key;
		Log.d("yue.huang", "stringToSign:" + strToSign2);
		String sign = Md5.getMD5ofStr(strToSign2);
		return sign;
	}

	/*对图片进行高斯模糊处理*/
    public static Bitmap blur(Context context,Bitmap bkg, float radius){
		if(android.os.Build.VERSION.SDK_INT>17){
			return blurAndroid(context, bkg, radius);
		}else {
			return blurJava(bkg, (int)radius);
		}
	}

	//android版本的高斯模糊，用户api 17以上的系统
	private static Bitmap blurAndroid(Context context,Bitmap bkg, float radius){
        Bitmap overlay = Bitmap.createBitmap(bkg.getWidth(), bkg.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(overlay);
        canvas.drawBitmap(bkg, 0, 0, null);
        RenderScript rs = RenderScript.create(context);
        Allocation overlayAlloc = Allocation.createFromBitmap(rs, overlay);
        ScriptIntrinsicBlur blur = ScriptIntrinsicBlur.create(rs, overlayAlloc.getElement());
        blur.setInput(overlayAlloc);
        blur.setRadius(radius);
        blur.forEach(overlayAlloc);
        overlayAlloc.copyTo(overlay);
        rs.destroy();
        return overlay;
	}
    //java版本的高斯模糊，用户api 17以下的系统
	private static Bitmap blurJava(Bitmap sentBitmap, int radius){
        Bitmap bitmap = sentBitmap.copy(sentBitmap.getConfig(), true);

        if (radius < 1) {
            return (null);
        }

        int w = bitmap.getWidth();
        int h = bitmap.getHeight();

        int[] pix = new int[w * h];
        bitmap.getPixels(pix, 0, w, 0, 0, w, h);

        int wm = w - 1;
        int hm = h - 1;
        int wh = w * h;
        int div = radius + radius + 1;

        int r[] = new int[wh];
        int g[] = new int[wh];
        int b[] = new int[wh];
        int rsum, gsum, bsum, x, y, i, p, yp, yi, yw;
        int vmin[] = new int[Math.max(w, h)];

        int divsum = (div + 1) >> 1;
        divsum *= divsum;
        int dv[] = new int[256 * divsum];
        for (i = 0; i < 256 * divsum; i++) {
            dv[i] = (i / divsum);
        }

        yw = yi = 0;

        int[][] stack = new int[div][3];
        int stackpointer;
        int stackstart;
        int[] sir;
        int rbs;
        int r1 = radius + 1;
        int routsum, goutsum, boutsum;
        int rinsum, ginsum, binsum;

        for (y = 0; y < h; y++) {
            rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
            for (i = -radius; i <= radius; i++) {
                p = pix[yi + Math.min(wm, Math.max(i, 0))];
                sir = stack[i + radius];
                sir[0] = (p & 0xff0000) >> 16;
                sir[1] = (p & 0x00ff00) >> 8;
                sir[2] = (p & 0x0000ff);
                rbs = r1 - Math.abs(i);
                rsum += sir[0] * rbs;
                gsum += sir[1] * rbs;
                bsum += sir[2] * rbs;
                if (i > 0) {
                    rinsum += sir[0];
                    ginsum += sir[1];
                    binsum += sir[2];
                } else {
                    routsum += sir[0];
                    goutsum += sir[1];
                    boutsum += sir[2];
                }
            }
            stackpointer = radius;

            for (x = 0; x < w; x++) {

                r[yi] = dv[rsum];
                g[yi] = dv[gsum];
                b[yi] = dv[bsum];

                rsum -= routsum;
                gsum -= goutsum;
                bsum -= boutsum;

                stackstart = stackpointer - radius + div;
                sir = stack[stackstart % div];

                routsum -= sir[0];
                goutsum -= sir[1];
                boutsum -= sir[2];

                if (y == 0) {
                    vmin[x] = Math.min(x + radius + 1, wm);
                }
                p = pix[yw + vmin[x]];

                sir[0] = (p & 0xff0000) >> 16;
                sir[1] = (p & 0x00ff00) >> 8;
                sir[2] = (p & 0x0000ff);

                rinsum += sir[0];
                ginsum += sir[1];
                binsum += sir[2];

                rsum += rinsum;
                gsum += ginsum;
                bsum += binsum;

                stackpointer = (stackpointer + 1) % div;
                sir = stack[(stackpointer) % div];

                routsum += sir[0];
                goutsum += sir[1];
                boutsum += sir[2];

                rinsum -= sir[0];
                ginsum -= sir[1];
                binsum -= sir[2];

                yi++;
            }
            yw += w;
        }
        for (x = 0; x < w; x++) {
            rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
            yp = -radius * w;
            for (i = -radius; i <= radius; i++) {
                yi = Math.max(0, yp) + x;

                sir = stack[i + radius];

                sir[0] = r[yi];
                sir[1] = g[yi];
                sir[2] = b[yi];

                rbs = r1 - Math.abs(i);

                rsum += r[yi] * rbs;
                gsum += g[yi] * rbs;
                bsum += b[yi] * rbs;

                if (i > 0) {
                    rinsum += sir[0];
                    ginsum += sir[1];
                    binsum += sir[2];
                } else {
                    routsum += sir[0];
                    goutsum += sir[1];
                    boutsum += sir[2];
                }

                if (i < hm) {
                    yp += w;
                }
            }
            yi = x;
            stackpointer = radius;
            for (y = 0; y < h; y++) {
                // Preserve alpha channel: ( 0xff000000 & pix[yi] )
                pix[yi] = ( 0xff000000 & pix[yi] ) | ( dv[rsum] << 16 ) | ( dv[gsum] << 8 ) | dv[bsum];

                rsum -= routsum;
                gsum -= goutsum;
                bsum -= boutsum;

                stackstart = stackpointer - radius + div;
                sir = stack[stackstart % div];

                routsum -= sir[0];
                goutsum -= sir[1];
                boutsum -= sir[2];

                if (x == 0) {
                    vmin[y] = Math.min(y + r1, hm) * w;
                }
                p = x + vmin[y];

                sir[0] = r[p];
                sir[1] = g[p];
                sir[2] = b[p];

                rinsum += sir[0];
                ginsum += sir[1];
                binsum += sir[2];

                rsum += rinsum;
                gsum += ginsum;
                bsum += binsum;

                stackpointer = (stackpointer + 1) % div;
                sir = stack[stackpointer];

                routsum += sir[0];
                goutsum += sir[1];
                boutsum += sir[2];

                rinsum -= sir[0];
                ginsum -= sir[1];
                binsum -= sir[2];

                yi += w;
            }
        }

        bitmap.setPixels(pix, 0, w, 0, 0, w, h);

        return bitmap;
	}
	/*获取按比例压缩的图片*/
	public static Bitmap getScaleCompressedBitmap(Resources res,int resId,int width){
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		Bitmap btm = BitmapFactory.decodeResource(res, resId,options);
		int height = options.outHeight * 200 / options.outWidth;
		options.outWidth = 200;
		options.outHeight = height;
		options.inJustDecodeBounds = false;
		return BitmapFactory.decodeResource(res, resId,options);
	}
}
