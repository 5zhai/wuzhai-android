package com.wuzhai.app.tools;

import com.wuzhai.app.R;
import com.zhy.view.ClipImageLayout;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class CilpImageActivity extends Activity implements OnClickListener {
	private ClipImageLayout clipImageLayout;
	private Button clipButton;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_clipimage);
		setTitle("图片裁剪");
		clipButton = (Button) findViewById(R.id.clip_button);
		clipButton.setOnClickListener(this);
		clipImageLayout = (ClipImageLayout) findViewById(R.id.clipImageLayout);
		Uri uri = getIntent().getParcelableExtra("imgUri");
		Bitmap bitmap = getBitmap(uri);
		if(null!=bitmap){
			clipImageLayout.setImage(new BitmapDrawable(bitmap));
		}else{
			Toast.makeText(this, getString(R.string.get_pic_err), Toast.LENGTH_SHORT).show();
		}
		
	}

	@Override
	public void onClick(View v) {

		Bitmap bitmap = clipImageLayout.clip();
		Bitmap targetBitmap = Bitmap.createScaledBitmap(bitmap, 150, 150, true);
		Intent intent = new Intent();
		intent.putExtra("targetBitmap", targetBitmap);
		setResult(RESULT_OK, intent);
		finish();
	}

	private Bitmap getBitmap(Uri uri) {
		ContentResolver cr = this.getContentResolver();
		Bitmap bmp = null;
		try {
			bmp = BitmapFactory.decodeStream(cr.openInputStream(uri));

		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return bmp;
	}

}
