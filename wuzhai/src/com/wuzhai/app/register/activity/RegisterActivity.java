package com.wuzhai.app.register.activity;

import java.util.Timer;
import java.util.TimerTask;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.wuzhai.app.R;
import com.wuzhai.app.register.service.RegisterService;
import com.wuzhai.app.tools.Utils;
import com.wuzhai.app.widget.TitleToolbarActivity;

public class RegisterActivity extends TitleToolbarActivity implements RegisterService.CallBack,OnClickListener{
	private RegisterService registerService;
	private ServiceConnection connection;
	private EditText phoneNumEditText;
	private EditText verCodeEditText;
	private Button getVerCodeBtn;
	private EditText passwordEditText;
	private EditText passwordConfirmEditText;
	private Button registerBtn;
	private Handler handler;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTitle("注册");
		setContentView(R.layout.activity_register);
		initView();
		handler = new Handler(){
			@Override
			public void handleMessage(Message msg) {
				if(msg.what!=0){
					getVerCodeBtn.setText(""+msg.what);
				}else {
					getVerCodeBtn.setText("发送验证码");
				}
			}
		};
	}

	@Override
	protected void onStart() {
		super.onStart();
		bindRegisterService();
	}

	@Override
	protected void onStop() {
		super.onStop();
		unbindService(connection);
	}

	private void bindRegisterService(){
		Intent intent = new Intent(this,RegisterService.class);
		connection = new ServiceConnection() {
			@Override
			public void onServiceDisconnected(ComponentName name) {
				// TODO Auto-generated method stub
			}
			@Override
			public void onServiceConnected(ComponentName name, IBinder service) {
				registerService = ((RegisterService.RegisterBind)service).getService();
				registerService.setCallBack(RegisterActivity.this);
			}
		};
		bindService(intent, connection, BIND_AUTO_CREATE);
	}

	private void initView(){
		phoneNumEditText = (EditText)findViewById(R.id.phone_number);
		verCodeEditText = (EditText)findViewById(R.id.verification_code);
		passwordEditText = (EditText)findViewById(R.id.password);
		passwordConfirmEditText = (EditText)findViewById(R.id.password_confirm);
		getVerCodeBtn = (Button)findViewById(R.id.get_verification_code);
		registerBtn = (Button)findViewById(R.id.register);
		getVerCodeBtn.setOnClickListener(this);
		registerBtn.setOnClickListener(this);
	}
	@Override
	public void onCompleted(int result) {
		switch (result) {
		case RegisterService.GET_VERIFIVATION_CODE_SUCCESSFUL:
			startCountdown();
			Toast.makeText(this, getString(R.string.sms_has_sent), Toast.LENGTH_SHORT).show();
			break;

		case RegisterService.GET_VERIFIVATION_CODE_PHONE_EXIST:
			Toast.makeText(this, getString(R.string.sms_sent_exist), Toast.LENGTH_SHORT).show();
			break;

		case RegisterService.GET_VERIFIVATION_CODE_FREQUENT:
			Toast.makeText(this, getString(R.string.sms_sent_frequent), Toast.LENGTH_SHORT).show();
			break;
		case RegisterService.VERIFICATION_CODE_ERR:
			Toast.makeText(this, getString(R.string.verification_code_err), Toast.LENGTH_SHORT).show();
			break;
		case RegisterService.NETWORK_ERR:
			Toast.makeText(this, getString(R.string.network_err), Toast.LENGTH_SHORT).show();
			break;
		case RegisterService.REGISTER_SUCCESSFUL:
			Toast.makeText(this, getString(R.string.register_successful), Toast.LENGTH_SHORT).show();
			break;
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.get_verification_code:
			getVerificationCode();
			break;

		case R.id.register:
			gotoRegister();
			break;
		}
	}

	private void getVerificationCode(){
		String phoneNum = phoneNumEditText.getText().toString();
		if(TextUtils.isEmpty(phoneNum)){
			Toast.makeText(this, "请输入电话号码", Toast.LENGTH_SHORT).show();
			return;
		}
		if(!Utils.isPhoneNumber(phoneNum)){
			Toast.makeText(this, "请输入正确的电话号码", Toast.LENGTH_SHORT).show();
			return;
		}
		if(!Utils.isNetworkAvailable(this)){
			Toast.makeText(this, "无网络连接", Toast.LENGTH_SHORT).show();
			return;
		}
		registerService.getVerificationCode(phoneNum);
	}

	private void gotoRegister(){
		String phoneNum = phoneNumEditText.getText().toString();
		String verCode = verCodeEditText.getText().toString();
		String password = passwordEditText.getText().toString();
		String passwordConfirm = passwordConfirmEditText.getText().toString();

		if(TextUtils.isEmpty(phoneNum) || TextUtils.isEmpty(verCode) ||
				TextUtils.isEmpty(password) || TextUtils.isEmpty(passwordConfirm)){
			Toast.makeText(this, "信息不完整！", Toast.LENGTH_SHORT).show();
			return;
		}
		if(!password.equals(passwordConfirm)){
			Toast.makeText(this, "密码不一致，请重新输入", Toast.LENGTH_SHORT).show();
			return;
		}
		if(!Utils.isNetworkAvailable(this)){
			Toast.makeText(this, "无网络连接", Toast.LENGTH_SHORT).show();
			return;
		}

		registerService.gotoRegister(phoneNum, verCode, password, passwordConfirm);
	}

	private void startCountdown(){
		new Thread(new Runnable() {
			@Override
			public void run() {
				for(int n=30;n>=0;n--){
					try {
						handler.sendEmptyMessage(n);
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		}).start();
	}
}
