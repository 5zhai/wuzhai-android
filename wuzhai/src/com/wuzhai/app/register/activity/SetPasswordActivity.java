package com.wuzhai.app.register.activity;

import com.wuzhai.app.R;
import com.wuzhai.app.register.service.RegisterService;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class SetPasswordActivity extends Activity implements OnClickListener{

	private EditText setPasswordEditText;
	private EditText confirmPasswordEditText;
	private TextView nextStepTextView;
	private RegisterService registerService;
	private LinearLayout progressView;
	private ServiceConnection connection;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_set_password);
		progressView = (LinearLayout)findViewById(R.id.progressView);
		setPasswordEditText = (EditText)findViewById(R.id.set_password_edit);
		confirmPasswordEditText = (EditText)findViewById(R.id.confirm_password_edit);
		nextStepTextView = (TextView)findViewById(R.id.nextStepToUpLoadPic);
		nextStepTextView.setOnClickListener(this);
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		bindRegisterService();
	}

	@Override
	protected void onStop() {
		super.onStop();
		unbindService(connection);
	}
	
	private void bindRegisterService(){
		Intent intent = new Intent(this,RegisterService.class);
		connection = new ServiceConnection() {
			
			@Override
			public void onServiceDisconnected(ComponentName name) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onServiceConnected(ComponentName name, IBinder service) {
				registerService = ((RegisterService.RegisterBind)service).getService();
			}
		};
		
		bindService(intent, connection, BIND_AUTO_CREATE);
	}
	
	@Override
	public void onClick(View v) {
		if(setPassword()){
			startActivity(new Intent(this,UploadUserInfoActivity.class));
		}
		
	}
	
	private boolean setPassword(){
		String password = setPasswordEditText.getText().toString();
		String confirmPassword = confirmPasswordEditText.getText().toString();
		if(checkPassword(password,confirmPassword)){
			registerService.setPassword(confirmPassword);
			return true;
		}
		return false;
	}
	
	private boolean checkPassword(String password,String confirmPassword){
		if("".equals(password)){
			Toast.makeText(this, getString(R.string.input_set_password), Toast.LENGTH_SHORT).show();
			return false;
		}
		if("".equals(confirmPassword)){
			Toast.makeText(this, getString(R.string.input_confirm_password), Toast.LENGTH_SHORT).show();
			return false;
		}
		if(!password.equals(confirmPassword)){
			Toast.makeText(this, getString(R.string.confirm_password_err), Toast.LENGTH_SHORT).show();
			confirmPasswordEditText.setText(null);
			return false;
		}
		if(password.length()<=6){
			Toast.makeText(this, getString(R.string.password_less_six), Toast.LENGTH_SHORT).show();
			return false;
		}
		return true;
	}

}
