package com.wuzhai.app.register.activity;

import java.io.ByteArrayOutputStream;
import com.wuzhai.app.R;
import com.wuzhai.app.register.service.RegisterService;
import com.wuzhai.app.tools.CilpImageActivity;

import android.app.Activity;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class UploadUserInfoActivity extends Activity implements OnClickListener,RegisterService.CallBack{

	private EditText usernameEditText;
	private EditText sexEditText;
	private TextView finishTextView;
	private RegisterService registerService;
	private LinearLayout progressView;
	private ServiceConnection connection;
	private ImageView selectPicImageView;
	private String url;
    private int SELECT_IMG = 1;
    private int CILP_IMG = 2;
    private Bitmap targetBitmap = null;
    
	private final int PHONE_HAS_BEEN_TAKEN = 5;
	private final int USERNAME_HAS_BEEN_TAKEN = 6;
	private final int UPLOAD_USER_INFO_ERR = 7;
	private final int UPLOAD_USER_INFO_SUCCESSFUL = 8;
	
	private Bundle thirdPartLoginInfo = null;
	private boolean isThirdPartLogin = false;
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_upload_userinfo);
		usernameEditText = (EditText)findViewById(R.id.username_edit);
		sexEditText = (EditText)findViewById(R.id.sex_edit);
		finishTextView = (TextView)findViewById(R.id.finish_register);
		progressView = (LinearLayout)findViewById(R.id.progressView);
		selectPicImageView = (ImageView)findViewById(R.id.select_pic);
		selectPicImageView.setOnClickListener(this);
		finishTextView.setOnClickListener(this);
		
		thirdPartLoginInfo = getIntent().getExtras();
//		if(null != bundle){
//			isThirdPartLogin = true;
////			startService(new Intent(this,RegisterService.class));
//		}
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		bindRegisterService();
	}

	@Override
	protected void onStop() {
		super.onStop();
		unbindService(connection);
	}
	
	@Override
	protected void onDestroy() {
		stopService(new Intent(this,RegisterService.class));
		super.onDestroy();
	}
	
	
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {

            if (requestCode == SELECT_IMG) {
                Uri uri = data.getData();
                Intent intent = new Intent(this, CilpImageActivity.class);
                intent.putExtra("imgUri", uri);
                startActivityForResult(intent, CILP_IMG);
            } else if (requestCode == CILP_IMG) {
            	targetBitmap = data.getParcelableExtra("targetBitmap");
                
            }

        }
    }
	
	
	private void bindRegisterService(){
		Intent intent = new Intent(this,RegisterService.class);
		connection = new ServiceConnection() {
			
			@Override
			public void onServiceDisconnected(ComponentName name) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onServiceConnected(ComponentName name, IBinder service) {
				registerService = ((RegisterService.RegisterBind)service).getService();
				registerService.setCallBack(UploadUserInfoActivity.this);
			}
		};
		
		bindService(intent, connection, BIND_AUTO_CREATE);
	}

	
	private void uploadUserInfo(){
		String username = usernameEditText.getText().toString();
		String sex = sexEditText.getText().toString();
		if("".equals(username) || "".equals(sex)){
			Toast.makeText(this, "信息不完全", Toast.LENGTH_SHORT).show();
			return;
		}
		if(targetBitmap == null){
			Toast.makeText(this, "选择图片", Toast.LENGTH_SHORT).show();
			return;
		}
		if(thirdPartLoginInfo==null){
			registerService.uploadUserInfo(username, sex, targetBitmap);
		}else{
			registerService.uploadUserInfo_thirdPart(username, sex, targetBitmap,thirdPartLoginInfo);
		}
		
	}
	
	@Override
	public void onCompleted(int result) {
		switch(result){
		case PHONE_HAS_BEEN_TAKEN:
			Toast.makeText(this, getString(R.string.phone_has_been_taken), Toast.LENGTH_SHORT).show();
			break;
		case USERNAME_HAS_BEEN_TAKEN:
			Toast.makeText(this, getString(R.string.username_has_been_taken), Toast.LENGTH_SHORT).show();
			break;
		case UPLOAD_USER_INFO_ERR:
			Toast.makeText(this, getString(R.string.upload_userinfo_err), Toast.LENGTH_SHORT).show();
			break;
		case UPLOAD_USER_INFO_SUCCESSFUL:
			//弄清楚是跳到登录还是主页，如果跳到主页则需要在RegisterService的uploadUserInfo方法上传用户信息成功后保存用户信息好user类对象中
			Toast.makeText(this, "上传用户信息成功，跳到下级", Toast.LENGTH_SHORT).show();
			break;
		}
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.select_pic:selectPic();break;
		case R.id.finish_register:uploadUserInfo();break;
		}
	}
	
	private void selectPic(){
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        // intent.putExtra("crop", "circle");
        intent.putExtra("return-data", true);
        startActivityForResult(intent, 1);
	}
}
