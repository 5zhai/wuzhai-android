package com.wuzhai.app.register.service;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import org.json.JSONException;
import org.json.JSONObject;

import com.qiniu.android.http.ResponseInfo;
import com.qiniu.android.storage.UpCompletionHandler;
import com.qiniu.android.storage.UploadManager;
import com.wuzhai.app.application.WuZhaiApplication;
import com.wuzhai.app.objects.User;
import com.wuzhai.app.tools.Utils;

import android.app.Service;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;

public class RegisterService extends Service {

	private CallBack callBack;
	private Handler mainHandler;
	private Handler threadHandler;
	//threadHandler的常量
	private final int GO_TO_REGISTER = 0;
	private final int GET_VERIFIVATION_CODE = 1;
	private final int UPLOAD_USER_PIC = 3;
	private final int UPLOAD_USER_INFO = 4;
	//mainHandler的常量
	public static final int GET_VERIFIVATION_CODE_SUCCESSFUL = 0;
	public static final int GET_VERIFIVATION_CODE_PHONE_EXIST = 1;
	public static final int NETWORK_ERR = 2;
	public static final int REGISTER_SUCCESSFUL = 3;
	public static final int REGISTER_ERR = 4;
	public static final int PHONE_HAS_BEEN_TAKEN = 5;
	public static final int USERNAME_HAS_BEEN_TAKEN = 6;
	public static final int UPLOAD_USER_INFO_ERR = 7;
	public static final int UPLOAD_USER_INFO_SUCCESSFUL = 8;
	public static final int GET_VERIFIVATION_CODE_FREQUENT = 9;
	public static final int VERIFICATION_CODE_ERR = 10;
	
	private String phoneNum;
	private String verificationCode;
	private String password;
	private String picUrl = null;
	private String username;
	private String sex;
	private Bitmap pic;
	private Bundle thirdPartLoginInfo;
	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return new RegisterBind();
	}
	
	public class RegisterBind extends Binder{
		public RegisterService getService(){
			return RegisterService.this;
		}
	}

	public interface CallBack{
		public void onCompleted(int result);
	}
	
	public void setCallBack(CallBack callBack){
		this.callBack = callBack;
	}

	@Override
	public void onCreate() {
		mainHandler = new Handler(){
			@Override
			public void handleMessage(Message msg) {
				callBack.onCompleted(msg.what);
			}
		};
		RegisterThread registerThread = new RegisterThread();
		registerThread.start();
	}

	@Override
	public void onDestroy() {
		threadHandler.getLooper().quit();
		super.onDestroy();
	}

	//---------------RegisterActivity相关功能---------------
	public void getVerificationCode(String phone_num){
		Message msg = Message.obtain();
		msg.what = GET_VERIFIVATION_CODE;
		HashMap<String, String> parametersMap = new HashMap<String, String>();
		parametersMap.put("phone_number", phone_num);
		msg.obj = parametersMap;
		threadHandler.sendMessage(msg);
	}

	public void gotoRegister(String phoneNumber,String code,String password,String passwordConfirmation){
		Message msg = Message.obtain();
		msg.what = GO_TO_REGISTER;
		HashMap<String, String> parametersMap = new HashMap<String, String>();
		parametersMap.put("phone_number", phoneNumber);
		parametersMap.put("code", code);
		parametersMap.put("password", password);
		parametersMap.put("password_confirmation", passwordConfirmation);
		msg.obj = parametersMap;
		threadHandler.sendMessage(msg);
	}
	private void getVerificationCode(HashMap<String, String> parametersMap){
		HttpURLConnection connection = null;
		try {
			connection = Utils.getHttpsConnectionPost("https://www.5yuzhai.com:443/api/v1/sign_up_code",parametersMap);
            if(connection.getResponseCode() == 200){
                InputStream in = connection.getInputStream();
                byte[] bytes = readStream(in);
                String res = new String(bytes);
                JSONObject resultJson = new JSONObject(res);
                boolean result = resultJson.getBoolean("success");
				if (result) {
					mainHandler.sendEmptyMessage(GET_VERIFIVATION_CODE_SUCCESSFUL);
				} else {
					if (resultJson.getString("msg").equals("手机号已存在")) {
						mainHandler.sendEmptyMessage(GET_VERIFIVATION_CODE_PHONE_EXIST);
					}
				}
			} else if (connection.getResponseCode() == 400) {
				mainHandler.sendEmptyMessage(GET_VERIFIVATION_CODE_FREQUENT);
			}
		} catch (Exception e) {
			e.printStackTrace();
			mainHandler.sendEmptyMessage(NETWORK_ERR);
		}finally{
			if(null!=connection){
				connection.disconnect();
			}
		}
	}

	private void submitToRegister(HashMap<String, String> parametersMap){
		HttpURLConnection connection = null;
		try {
			connection = Utils.getHttpsConnectionPost("https://www.5yuzhai.com:443/api/v1/sign_up", parametersMap);
            if(connection.getResponseCode() == 200){
                InputStream in = connection.getInputStream();
                byte[] bytes = readStream(in);
                String res = new String(bytes);
                JSONObject resultJson = new JSONObject(res);
                boolean result = resultJson.getBoolean("success");
				if (result) {
					mainHandler.sendEmptyMessage(REGISTER_SUCCESSFUL);
				} else {
					mainHandler.sendEmptyMessage(VERIFICATION_CODE_ERR);
				}
			} else if (connection.getResponseCode() == 400) {
				mainHandler.sendEmptyMessage(VERIFICATION_CODE_ERR);
			}
		} catch (Exception e) {
			e.printStackTrace();
			mainHandler.sendEmptyMessage(NETWORK_ERR);
		}finally{
			if(null!=connection){
				connection.disconnect();
			}
		}
	}

//------------------setPasswordActivity相关功能-------------
	public void setPassword(String password){
		this.password = password;
	}

	//---------------UploadUserInfoActivity相关功能---------
	public void uploadUserInfo(String username,String sex,Bitmap pic){
		this.username = username;
		this.sex = sex;
		this.pic = pic;
		threadHandler.sendEmptyMessage(UPLOAD_USER_PIC);
	}

	public void uploadUserInfo_thirdPart(String username,String sex,Bitmap pic,Bundle thirdPartLoginInfo){
		this.username = username;
		this.sex = sex;
		this.pic = pic;
		this.thirdPartLoginInfo = thirdPartLoginInfo;
		threadHandler.sendEmptyMessage(UPLOAD_USER_PIC);
	}

	private void uploadUserPic(){
		String uploadToken = getUploadToken();
		if(null!=uploadToken){
			UploadManager uploadManager = new UploadManager();
            ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
            pic.compress(Bitmap.CompressFormat.PNG, 100, byteOut);
			byte[] data = byteOut.toByteArray();
			final String key = getPicName();
			String token = getUploadToken();
			if(null != token){
				uploadManager.put(data, key, token, new UpCompletionHandler() {
					@Override
					public void complete(String arg0, ResponseInfo arg1,
							JSONObject arg2) {
						if(arg1.isOK()){
							picUrl = "http://7xkj2y.com1.z0.glb.clouddn.com/"+key;
							//此方法是回调在主线程，所以不能直接调用uploadUserInfoAfterGetPicUrl();
							threadHandler.sendEmptyMessage(UPLOAD_USER_INFO);
						}else {
							mainHandler.sendEmptyMessage(UPLOAD_USER_INFO_ERR);
						}
					}
				},null);
			}
		}
	}

	private void uploadUserInfo(){
		HttpURLConnection connection = null;
		URL url = null;
		String parameter = null;
		try {
			if(thirdPartLoginInfo == null){
				url = new URL("http://www.5yuzhai.com:8088/api/v1/sign_up");
				parameter = getUserInfoToJsonString(username,password,phoneNum,picUrl,sex);
			}else {
				url = new URL("http://www.5yuzhai.com:8088/api/v1/oauth_register");
				parameter = getThirdPartUserInfoToJsonString(username,picUrl,sex,thirdPartLoginInfo);
			}

			connection = (HttpURLConnection )url.openConnection();
			connection.setConnectTimeout(5000);
			connection.setReadTimeout(5000);
			connection.setRequestMethod("POST");
			connection.setDoInput(true);
			connection.setDoOutput(true);
			connection.setUseCaches(false);
            connection.setRequestProperty("connection", "keep-alive");
            connection.setRequestProperty("Charsert", "UTF-8");
            connection.setRequestProperty("Content-Type", "application/json;charset=utf-8");
            DataOutputStream dataOut = new DataOutputStream(connection.getOutputStream());
            
            dataOut.write(parameter.getBytes("UTF-8"));//貌似只有这种方式才能将汉字转为UTF-8并上传服务器验证格式正确
            dataOut.flush();
            dataOut.close();
            if(connection.getResponseCode() == 200){
                InputStream in = connection.getInputStream();
                byte[] bytes = readStream(in);
                String res = new String(bytes);
                JSONObject resultJson = new JSONObject(res);
                int result = resultJson.getInt("code");
                if(10006==result){
                	saveUserInfo();
                	mainHandler.sendEmptyMessage(UPLOAD_USER_INFO_SUCCESSFUL);
                }else if(10002==result){
                	//注册失败应该删除七牛上的图片，以后添加此功能优化
                	JSONObject errInfo = resultJson.getJSONObject("errors");
                	if(errInfo.has("username")){
                		mainHandler.sendEmptyMessage(USERNAME_HAS_BEEN_TAKEN);
                	}else if(errInfo.has("phone_number")){
                		mainHandler.sendEmptyMessage(PHONE_HAS_BEEN_TAKEN);
					}
                }
            }
		} catch (Exception e) {
			e.printStackTrace();
			mainHandler.sendEmptyMessage(NETWORK_ERR);
		}finally{
			if(null!=connection){
				connection.disconnect();
			}
		}
	}

	private String getUserInfoToJsonString(String username,String password,String phone_number,String picUrl,String sex) throws JSONException{
		JSONObject userinfo = new JSONObject();
		userinfo.put("phone_number", phone_number);
		userinfo.put("password", password);
		userinfo.put("password_confirmation", password);
		userinfo.put("username", username);
		userinfo.put("avatar", picUrl);
		userinfo.put("sex", sex);
		return userinfo.toString();
	}
	
	private String getThirdPartUserInfoToJsonString(String username,String picUrl,String sex,Bundle thirdPartLoginInfo) throws JSONException{
		String platform = thirdPartLoginInfo.getString("platform");
		String uid = thirdPartLoginInfo.getString("uid");
		@SuppressWarnings("unchecked")
		HashMap<String, Object> ext_info = (HashMap<String, Object>)thirdPartLoginInfo.getSerializable("ext_info");
		
		JSONObject userinfo = new JSONObject();
		userinfo.put("platform", platform);
		userinfo.put("uid", uid);
		userinfo.put("username", username);
		userinfo.put("ext_info", ext_info);
		userinfo.put("avatar", picUrl);
		userinfo.put("sex", sex);
		return userinfo.toString();
	}
	
	private String getUploadToken(){
		HttpURLConnection connection =null;
		String token = null;
		try {
			URL url = new URL("http://www.5yuzhai.com:8088/api/v1/qiniu/token");
			connection = (HttpURLConnection)url.openConnection();
			connection.setConnectTimeout(5000);
			connection.setRequestMethod("GET");
            if(connection.getResponseCode() == 200){
                InputStream inputStream = connection.getInputStream();
                byte[] bytes = readStream(inputStream);
                String jsonString = new String(bytes);
                JSONObject resultJson = new JSONObject(jsonString);
                token = resultJson.getString("token");
            }
		} catch (Exception e) {
			// TODO: handle exception
		}finally{
			if(null!=connection){
				connection.disconnect();
			}
		}
		return token;
	}
	
	private String getPicName(){
		  SimpleDateFormat sdFormatter = new SimpleDateFormat("yyyyMMddHHmmss");
		  return username+sdFormatter.format(new Date());
		  
	}
	//---------------公共功能---------------
	private class RegisterThread extends Thread{

		@Override
		public void run() {
			Looper.prepare();
			threadHandler = new Handler(){
				@Override
				public void handleMessage(Message msg) {
					switch(msg.what){
					case GET_VERIFIVATION_CODE:
						getVerificationCode((HashMap<String, String>)msg.obj);
						break;
					case GO_TO_REGISTER:
						submitToRegister((HashMap<String, String>)msg.obj);
						break;
					case UPLOAD_USER_PIC:
						uploadUserPic();
						break;
					case UPLOAD_USER_INFO:
						uploadUserInfo();
						break;
					}
				}
				
			};
			Looper.loop();
		}
		
	}
	
    private byte[] readStream(InputStream in) throws IOException{
        ByteArrayOutputStream bytesOut = new ByteArrayOutputStream();
        byte[] bytes = new byte[512];
        int len = 0;
        while(-1!=(len=in.read(bytes))){
            bytesOut.write(bytes, 0, len);
        }
        in.close();
        return bytesOut.toByteArray();
    }
	
	private void saveUserInfo(){
//    	User user = ((WuZhaiApplication)getApplication()).getUser();
//    	user.setId();
//    	user.setName(username);
//    	user.setPhoneNumber(phoneNum);
//    	user.setAvatarUrl(picUrl);
//    	user.setSex(sex);
	}

}
