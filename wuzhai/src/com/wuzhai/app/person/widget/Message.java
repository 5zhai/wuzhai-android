package com.wuzhai.app.person.widget;

public class Message {

	private String avatarPath = null;
	private String username = null;
	private String messageSurvey = null;
	private String messageTime;
	private String messageContent;
	public Message(String avatarPath,String username,String messageSurvey,String messageTime,String messageContent){
		this.avatarPath = avatarPath;
		this.username = username;
		this.messageSurvey = messageSurvey;
		this.messageTime = messageTime;
		this.messageContent = messageContent;
	}
	public String getAvatarPath() {
		return avatarPath;
	}
	public String getUsername() {
		return username;
	}
	public String getMessageSurvey() {
		return messageSurvey;
	}
	public String getMessageContent() {
		return messageContent;
	}
	public String getMessageTime() {
		return messageTime;
	}

}
