package com.wuzhai.app.person.widget;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.wuzhai.app.R;
import com.wuzhai.app.application.WuZhaiApplication;
import com.wuzhai.app.main.widget.HyRecyclerViewAdapter;
import com.wuzhai.app.objects.User;
import com.wuzhai.app.person.activity.PersonalCenterActivity;

public class FollowFansAdapter extends HyRecyclerViewAdapter {

	private Context context;
	private ArrayList<FollowerFans> followers;
	private boolean isFollower;
	public FollowFansAdapter(Context context,ArrayList<FollowerFans> followerList,boolean isFollower){
		this.context = context;
		this.followers = followerList;
		this.isFollower = isFollower;
	}
	@Override
	public int getItemCount() {
		// TODO Auto-generated method stub
		return followers.size();
	}

	@Override
	public void onBindViewHolder(ViewHolder arg0, int arg1) {
		FollowViewHolder viewHolder = (FollowViewHolder)arg0;
		Picasso.with(context).load(followers.get(arg1).getAvatarPath()).into(viewHolder.avatar);
		viewHolder.username.setText(followers.get(arg1).getUsername());
		if(!isFollower && followers.get(arg1).isFollowed()){
//			viewHolder.followbtn.setEnabled(false);
//			viewHolder.followbtn.setBackgroundColor(0xffB5B5B5);
//			viewHolder.followbtn.setText("已关注");
			viewHolder.followbtn.setText("取消关注");
		}else if (isFollower) {
			viewHolder.followbtn.setText("取消关注");
		}
		viewHolder.followbtn.setTag(arg1);
		viewHolder.itemView.setTag(arg1);
	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup arg0, int arg1) {
		View view = LayoutInflater.from(context).inflate(R.layout.follow_list_item, arg0,false);
		return new FollowViewHolder(view);
	}

	private class FollowViewHolder extends ViewHolder{

		ImageView avatar;
		TextView username;
		Button followbtn;

		public FollowViewHolder(View arg0) {
			super(arg0);
			avatar = (ImageView)arg0.findViewById(R.id.userAvatar);
			username = (TextView)arg0.findViewById(R.id.username);
			followbtn = (Button)arg0.findViewById(R.id.followbtn);
			followbtn.setOnClickListener(clickListener);
			arg0.setOnClickListener(clickListener);
		}
		
	}

	public void setList(ArrayList<FollowerFans> followers){
		this.followers = followers;
		notifyDataSetChanged();
	}

	private OnClickListener clickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.followbtn:
				recyclerViewItemClickListener.onItemClick(v, (Integer)v.getTag());
//				Toast.makeText(context, "button", Toast.LENGTH_SHORT).show();
				break;
			default :
				//因为只有一个回调，又不想将services引入adapter中，所以点击item的跳转就放到了此处
				Intent intent = new Intent(context, PersonalCenterActivity.class);
				intent.putExtra("userId", followers.get((Integer)v.getTag()).getId());
				intent.putExtra("userType", 1);
				context.startActivity(intent);
				break;
			}
		}
	};
}
