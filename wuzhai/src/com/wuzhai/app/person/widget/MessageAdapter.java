package com.wuzhai.app.person.widget;

import java.util.ArrayList;

import android.content.Context;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.wuzhai.app.R;
import com.wuzhai.app.main.widget.HyRecyclerViewAdapter;

public class MessageAdapter extends HyRecyclerViewAdapter {

	private Context context;
	private ArrayList<Message> messages;
	public MessageAdapter(Context context,ArrayList<Message> messages){
		this.context = context;
		this.messages = messages;
	}
	@Override
	public int getItemCount() {
		// TODO Auto-generated method stub
		return messages.size();
	}

	@Override
	public void onBindViewHolder(ViewHolder arg0, int arg1) {
		MessageViewHolder viewHolder = (MessageViewHolder)arg0;
		Picasso.with(context).load(Integer.parseInt(messages.get(arg1).getAvatarPath())).into(viewHolder.avatar);
		viewHolder.username.setText(messages.get(arg1).getUsername());
		viewHolder.messageSurvey.setText(messages.get(arg1).getMessageSurvey());
		viewHolder.messageTime.setText(messages.get(arg1).getMessageTime());
		//如果消息是新消息，没有打开过，则显示flag
		viewHolder.newFlag.setVisibility(View.VISIBLE);
	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup arg0, int arg1) {
		View view = LayoutInflater.from(context).inflate(R.layout.message_list_item, arg0,false);
		return new MessageViewHolder(view);
	}

	private static class MessageViewHolder extends ViewHolder{

		ImageView avatar;
		TextView username;
		TextView messageSurvey;
		TextView messageTime;
		TextView newFlag;

		public MessageViewHolder(View arg0) {
			super(arg0);
			avatar = (ImageView)arg0.findViewById(R.id.avatar);
			username = (TextView)arg0.findViewById(R.id.username);
			messageSurvey = (TextView)arg0.findViewById(R.id.message_survey);
			messageTime = (TextView)arg0.findViewById(R.id.message_time);
			newFlag = (TextView)arg0.findViewById(R.id.new_flag);
		}
		
	}
}
