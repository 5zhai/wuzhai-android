package com.wuzhai.app.person.fragments;

import java.util.ArrayList;

import com.squareup.picasso.Picasso;
import com.wuzhai.app.R;
import com.wuzhai.app.objects.Order;
import com.wuzhai.app.person.activity.OrderOperation;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class AllOrderFragment extends Fragment {

	// private String flag = null;
	private LinearLayout waitpaymentContainer;
	private LayoutInflater inflater;

	@Override
	@Nullable
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		this.inflater = inflater;
		View view = inflater.inflate(R.layout.fragment_order, null);
		return view;
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		waitpaymentContainer = (LinearLayout) view
				.findViewById(R.id.waitpayment_container);
//		initView();
	}

	public void initView(ArrayList<Order> orderList) {
		// 目前不知道是每件商品弄一个合计，还是所有的待支付弄一个合计，如果所有的待支付弄一个合计，现在没有相应的设计图；暂时只添加一个作为效果示例
//		for (int n = 0; n < 1; n++) {
//			View waitPaymentItem = inflater.inflate(R.layout.goods_item,
//					waitpaymentContainer);
//			// waitpaymentContainer.addView(waitPaymentItem);
//		}
		for(Order order : orderList){
			View waitPaymentItem = inflater.inflate(R.layout.goods_item,
					null);
			initItemView(waitPaymentItem, order);
			waitPaymentItem.setTag(order);
			waitPaymentItem.setOnClickListener(clickListener);
			waitpaymentContainer.addView(waitPaymentItem);
		}
	}

	private void initItemView(View item,Order order){
		ImageView goods_pic = (ImageView)item.findViewById(R.id.goods_pic);
		TextView goods_info = (TextView)item.findViewById(R.id.goods_info);
	    TextView goods_price = (TextView)item.findViewById(R.id.goods_price);
	    TextView goods_place = (TextView)item.findViewById(R.id.goods_place);
	    Picasso.with(getContext()).load(order.getCommodityImage()).into(goods_pic);
	    goods_info.setText(order.getCommodityTitle());
	    goods_price.setText(order.getPayment()+"");
//	    goods_place.setText(order.getGoods().getCity());
	}

	private OnClickListener clickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			Order order = (Order)v.getTag();
			Intent intent = new Intent(getContext(),OrderOperation.class);
			intent.putExtra("order_id", order.getId());
			intent.putExtra("order_title", ""+order.getCommodityTitle());
			startActivity(intent);
		}
	};
}
