package com.wuzhai.app.person.activity;

import java.util.ArrayList;

import io.rong.imkit.RongIM;
import android.R.integer;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Process;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.LayoutParams;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.viewpagerindicator.TabPageIndicator;
import com.wuzhai.app.R;
import com.wuzhai.app.application.WuZhaiApplication;
import com.wuzhai.app.chat.ConversationListActivity;
import com.wuzhai.app.main.WuzhaiService;
import com.wuzhai.app.main.WuzhaiServiceCallbackAdapter;
import com.wuzhai.app.main.widget.HyRecyclerViewAdapter;
import com.wuzhai.app.main.widget.OnRecyclerViewItemLongClickListener;
import com.wuzhai.app.main.widget.ViewPagerAdapter;
import com.wuzhai.app.objects.User;
import com.wuzhai.app.objects.UserCenterInfo;
import com.wuzhai.app.objects.UserCenterInfo.Media;
import com.wuzhai.app.tools.Utils;
import com.wuzhai.app.widget.TitleToolbarActivity;

public class PersonalCenterActivity extends TitleToolbarActivity {

	private WuzhaiService service;
	private Button editButton;
	private ImageView userAvatar;
	private ImageView userSex;
	private TextView userName;
	private TextView followCount;
	private TextView fansCount;
	private TextView currentLevel;
	private ProgressBar levelProgressbar;
	private TextView currentExperience;
	private TextView needExperience;
	private TextView nextLevel;
	private RelativeLayout collectionBtn;
	private TextView collectionCount;
	private RelativeLayout market;
	private TextView marketCount;
	private LinearLayout mediaContainer;
	private LinearLayout photoBtn;
	private TextView photoCount;
	private LinearLayout videoBtn;
	private TextView videoCount;
	private LinearLayout currentUserEditlayout;
	private LinearLayout otherUserEditlayout;
	private LinearLayout sendmsgBtn;
	private LinearLayout canclefollowBtn;
	private int screenWidth;
	private RecyclerView photoListView;
	private RecyclerView videoListView;
	private LinearLayout followLayout;
	private LinearLayout fansLayout;
	private UserCenterInfo mUserCenterInfo;
	private int userType = 0;//0--当前用户；1--其它用户
	private int userId = -1;

//	private TextView signature;
	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTitle("个人中心");
		service = ((WuZhaiApplication)getApplication()).getService();
		service.setCallBack(callbackAdapter);
		setImgMenuResource(R.drawable.icon_more);
		setContentView(R.layout.activity_personal_center);
		initView();
	}

	private void initView(){
		userType = getIntent().getIntExtra("userType", 0);
		userId = getIntent().getIntExtra("userId", -1);
		service.getUserCenterInfo(userId);
		userAvatar = (ImageView)findViewById(R.id.user_avatar);
		userSex = (ImageView)findViewById(R.id.user_sex);
		userName = (TextView)findViewById(R.id.user_name);
		followLayout = (LinearLayout)findViewById(R.id.follow_layout);
		followCount = (TextView)findViewById(R.id.follow_count);
		fansLayout = (LinearLayout)findViewById(R.id.fans_layout);
		fansCount = (TextView)findViewById(R.id.fans_count);
		currentLevel = (TextView)findViewById(R.id.current_level);
		levelProgressbar = (ProgressBar)findViewById(R.id.level_progressbar);
		currentExperience = (TextView)findViewById(R.id.current_experience);
		needExperience = (TextView)findViewById(R.id.need_experience);
		nextLevel = (TextView)findViewById(R.id.next_level);
		collectionBtn = (RelativeLayout)findViewById(R.id.collection);
		collectionCount = (TextView)findViewById(R.id.collection_count);
		market = (RelativeLayout)findViewById(R.id.market);
		marketCount = (TextView)findViewById(R.id.market_count);
//		mediaViewpager = (ViewPager)findViewById(R.id.media_viewpager);
		mediaContainer = (LinearLayout)findViewById(R.id.media_container);
		photoBtn = (LinearLayout)findViewById(R.id.photo);
		photoBtn.setOnClickListener(clickListener);
		photoCount = (TextView)findViewById(R.id.photo_count);
		videoBtn = (LinearLayout)findViewById(R.id.video);
		currentUserEditlayout = (LinearLayout)findViewById(R.id.currentuser_editlayout);
		otherUserEditlayout = (LinearLayout)findViewById(R.id.otheruser_editlayout);
		sendmsgBtn = (LinearLayout)findViewById(R.id.sendmsg_btn);
		canclefollowBtn = (LinearLayout)findViewById(R.id.canclefollow_btn);

		videoBtn.setOnClickListener(clickListener);
		videoCount = (TextView)findViewById(R.id.video_count);
		editButton = (Button)findViewById(R.id.edit_btn);
		editButton.setOnClickListener(clickListener);
		screenWidth = Utils.getScreenSize(this)[0];
		fansLayout.setOnClickListener(clickListener);
		followLayout.setOnClickListener(clickListener);
		canclefollowBtn.setOnClickListener(clickListener);
		sendmsgBtn.setOnClickListener(clickListener);

		if(userType==1){
			collectionBtn.setVisibility(View.GONE);
			market.setVisibility(View.GONE);
			currentUserEditlayout.setVisibility(View.GONE);
			otherUserEditlayout.setVisibility(View.VISIBLE);
		}
	}

	private OnClickListener clickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.edit_btn:
				startActivity(new Intent(PersonalCenterActivity.this, DataSettingActivity.class));
//				RongIM.getInstance().startPrivateChat(PersonalCenterActivity.this, "265", "title");
//				RongIM.getInstance().startConversationList(PersonalCenterActivity.this);
//				RongIM.getInstance().startConversation(PersonalCenterActivity.this, io.rong.imlib.model.Conversation.ConversationType.CHATROOM, "9527", "标题");
//				RongIM.getInstance().disconnect(true);
//				android.os.Process.killProcess(Process.myPid());
				break;
			case R.id.photo:
				videoBtn.setBackgroundColor(0xffffffff);
				photoBtn.setBackgroundColor(0xaaff7e9c);
				mediaContainer.removeAllViews();
				mediaContainer.addView(photoListView);
				break;
			case R.id.video:
				photoBtn.setBackgroundColor(0xffffffff);
				videoBtn.setBackgroundColor(0xaaff7e9c);
				mediaContainer.removeAllViews();
				mediaContainer.addView(videoListView);
				break;
			case R.id.fans_layout:
				Intent intentFans = new Intent(PersonalCenterActivity.this, FollowFansActivity.class);
				intentFans.putExtra("type", "fans");
				PersonalCenterActivity.this.startActivity(intentFans);
				break;
			case R.id.follow_layout:
				Intent intentFollow = new Intent(PersonalCenterActivity.this, FollowFansActivity.class);
				intentFollow.putExtra("type", "follow");
				PersonalCenterActivity.this.startActivity(intentFollow);
				break;
			case R.id.canclefollow_btn:
				service.cancleFollowUser(userId);
				break;
			case R.id.sendmsg_btn:
				RongIM.getInstance().startPrivateChat(PersonalCenterActivity.this, userId+"", "title");
				break;
			}
		}
	};

	private WuzhaiServiceCallbackAdapter callbackAdapter = new WuzhaiServiceCallbackAdapter(){

		@Override
		public void onGetUserCenterInfoDone(UserCenterInfo userCenterInfo) {
			if(userCenterInfo!=null){
				mUserCenterInfo = userCenterInfo;
				initData(userCenterInfo);
			}
		}

		@Override
		public void onCompleted(int result) {
			if(WuzhaiService.DEL_MEDIA_SUCC == result){
				Toast.makeText(PersonalCenterActivity.this, "删除成功", Toast.LENGTH_SHORT).show();
			}else if (WuzhaiService.DEL_MEDIA_FAL== result) {
				Toast.makeText(PersonalCenterActivity.this, "删除出错", Toast.LENGTH_SHORT).show();
			}else if (WuzhaiService.CANCLE_FOLLOW_SUCC == result) {
				Toast.makeText(PersonalCenterActivity.this, "取消关注成功", Toast.LENGTH_SHORT).show();
				canclefollowBtn.setBackgroundResource(R.drawable.roundedbig_button_gray);
				canclefollowBtn.setEnabled(false);
			}
		}
	};

	private void initData(UserCenterInfo userCenterInfo){
		Picasso.with(this).load(userCenterInfo.getUser().getAvatarUrl()).into(userAvatar);
		if(userCenterInfo.getUser().getSexString().equals("女")){
			userSex.setImageResource(R.drawable.icon_female);
		}else {
			userSex.setImageResource(R.drawable.icon_male);
		}
		User user = userCenterInfo.getUser();
		userName.setText(user.getName());
		followCount.setText(""+user.getFollowedsCount());
		fansCount.setText(""+user.getFollowersCount()+"");
		currentLevel.setText(user.getLevel()+"");
		levelProgressbar.setProgress((int)(user.getCurrentExperience()*0.1/user.getNeedExperience()*100));
		currentExperience.setText(user.getCurrentExperience()+"");
		needExperience.setText(user.getNeedExperience()+"");
		nextLevel.setText(user.getLevel()+1+"");
		collectionCount.setText(userCenterInfo.getCollectionsCount()+"");
		marketCount.setText(userCenterInfo.getCommoditiesCount()+"");
		photoCount.setText(userCenterInfo.getPhotosCount()+"");
		videoCount.setText(userCenterInfo.getVideosCount()+"");

		photoListView = initMediaListView(userCenterInfo,0,userType);
		mediaContainer.addView(photoListView);
//		viewList.add(photoListView);

		videoListView = initMediaListView(userCenterInfo,1,userType);
//		viewList.add(videoListView);

//		MediaViewPagerAdapter viewPagerAdapter = new MediaViewPagerAdapter(viewList);
//		mediaViewpager.setAdapter(viewPagerAdapter);
	}

	//type:0--图片；2--视频
	private RecyclerView initMediaListView(UserCenterInfo userCenterInfo,int type,int userType){
		RecyclerView recyclerView = new RecyclerView(this);
		int recyclerViewHeight = 0;
		if(type == 0){
			recyclerViewHeight = (userCenterInfo.getPhotoList().size()+1)/2*screenWidth/2;
		}else {
			recyclerViewHeight = (userCenterInfo.getVideoList().size()+1)/2*screenWidth/2;
		}
		recyclerView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, recyclerViewHeight));
		GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 2);
//		LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
		recyclerView.setLayoutManager(gridLayoutManager);
		MediaAdapter adapter = new MediaAdapter(this);
		if(type==0){
		    adapter.setMediaList(userCenterInfo.getPhotoList());
		}else {
			adapter.setMediaList(userCenterInfo.getVideoList());
		}
		if(userType == 0){
			adapter.setOnRecyclerViewItemLongClickListener(new RecyclerViewItemLongClickListener(type));
		}
		recyclerView.setAdapter(adapter);
		return recyclerView;
	}

	private class MediaAdapter extends HyRecyclerViewAdapter{

		private Context context;
		private ArrayList<? extends Media> mediaList;
		public MediaAdapter(Context context){
			this.context = context;
		}
		@Override
		public int getItemCount() {
			// TODO Auto-generated method stub
			return mediaList.size();
		}

		@Override
		public void onBindViewHolder(ViewHolder arg0, final int arg1) {
			MediaViewHolder viewHolder = (MediaViewHolder)arg0;
			if(!TextUtils.isEmpty(mediaList.get(arg1).getImage()))
			Picasso.with(context).load(mediaList.get(arg1).getImage()).resize(100, 100).centerCrop().into(viewHolder.getMediaItem());
			viewHolder.itemView.setOnLongClickListener(new OnLongClickListener() {
				@Override
				public boolean onLongClick(View v) {
					if(recyclerViewItemLongClickListener!=null){
						recyclerViewItemLongClickListener.onItemLongClick(v, arg1);
					}
					return false;
				}
			});
		}

		@Override
		public ViewHolder onCreateViewHolder(ViewGroup arg0, final int arg1) {
			ImageView view = new ImageView(context);
			LayoutParams layoutParams = new LayoutParams(LayoutParams.MATCH_PARENT, screenWidth/2);
			layoutParams.topMargin = 4;
			layoutParams.leftMargin=2;
			layoutParams.rightMargin=2;
			view.setLayoutParams(layoutParams);
			view.setScaleType(ScaleType.FIT_XY);
//			arg0.addView(view);
			return new MediaViewHolder(view);
		}

		private class MediaViewHolder extends ViewHolder{
			private ImageView mediaItem;
			public MediaViewHolder(View arg0) {
				super(arg0);
			    mediaItem = (ImageView)arg0;
			}

			public ImageView getMediaItem(){
				return mediaItem;
			}
		}
		public void setMediaList(ArrayList<? extends Media> medias){
			mediaList = medias;
		}
	}

	private class RecyclerViewItemLongClickListener implements OnRecyclerViewItemLongClickListener{
		private int type;
		public RecyclerViewItemLongClickListener(int type){
			this.type = type;
		}
		@Override
		public void onItemLongClick(View view, int position) {
			if(type == 0){
				Log.d("yue.huang", "position:"+position);
				showDialog("删除图片？",mUserCenterInfo.getPhotoList().get(position).getId(),type,position);
			}else {
				showDialog("删除视频？",mUserCenterInfo.getVideoList().get(position).getId(),type,position);
			}
		}
	}

	private void showDialog(String msg,final int id,final int type,final int position){
		AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
		dialogBuilder.setMessage(msg);
		dialogBuilder.setPositiveButton("取消", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
		dialogBuilder.setNegativeButton("确定", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				service.deleteMedia(type,id);
				if(type == 0){
					mUserCenterInfo.getPhotoList().remove(position);
					photoListView.getAdapter().notifyDataSetChanged();
					photoCount.setText(mUserCenterInfo.getPhotoList().size()+"");
					int recyclerViewHeight = (mUserCenterInfo.getPhotoList().size()+1)/2*screenWidth/2;
					photoListView.getLayoutParams().height = recyclerViewHeight;
				}else {
					mUserCenterInfo.getVideoList().remove(position);
					videoListView.getAdapter().notifyDataSetChanged();
					videoCount.setText(mUserCenterInfo.getVideoList().size()+"");
					int recyclerViewHeight = (mUserCenterInfo.getVideoList().size()+1)/2*screenWidth/2;
					videoListView.getLayoutParams().height = recyclerViewHeight;
				}
			}
		});
		dialogBuilder.create().show();
	}
}
