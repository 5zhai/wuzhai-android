package com.wuzhai.app.person.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

import com.wuzhai.app.R;
import com.wuzhai.app.tools.Utils;
import com.wuzhai.app.widget.TitleToolbarActivity;

public class GiftGiveActivity extends TitleToolbarActivity {

	private LinearLayout myGiftContainer;
	private LinearLayout recommendGiftContainer;
	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTitle("礼物中心");
		setContentView(R.layout.activity_gift_give);
		initView();
	}
	
	private void initView(){
		myGiftContainer = (LinearLayout)findViewById(R.id.my_gift_container);
		LinearLayout horizontalLinearLayout = null;

		for(int n = 0;n<5;n++){
			if(n%2==0){
				horizontalLinearLayout = new LinearLayout(this);
				horizontalLinearLayout.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.WRAP_CONTENT));
				horizontalLinearLayout.setOrientation(LinearLayout.HORIZONTAL);
				horizontalLinearLayout.setPadding(8, 8, 8, 8);
				myGiftContainer.addView(horizontalLinearLayout);
			}
			LinearLayout view = (LinearLayout)LayoutInflater.from(this).inflate(R.layout.gift_item_layout, null);
			LayoutParams params = new LayoutParams(0,LayoutParams.WRAP_CONTENT,1);
			view.setLayoutParams(params);
			horizontalLinearLayout.addView(view);
		}
		if(5%2!=0){
			View view = new View(this);
			view.setLayoutParams(new LayoutParams(0,LayoutParams.WRAP_CONTENT,1));
			horizontalLinearLayout.addView(view);
		}
		
		//初始化推荐礼物
		recommendGiftContainer = (LinearLayout)findViewById(R.id.recommend_gift_container);
		LinearLayout horizontalLinearLayout1 = null;

		for(int n = 0;n<10;n++){
			if(n%2==0){
				horizontalLinearLayout1 = new LinearLayout(this);
				horizontalLinearLayout1.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.WRAP_CONTENT));
				horizontalLinearLayout1.setOrientation(LinearLayout.HORIZONTAL);
				horizontalLinearLayout1.setPadding(8, 8, 8, 8);
				recommendGiftContainer.addView(horizontalLinearLayout1);
			}
			LinearLayout view = (LinearLayout)LayoutInflater.from(this).inflate(R.layout.gift_item_layout, null);
			LayoutParams params = new LayoutParams(0,LayoutParams.WRAP_CONTENT,1);
			view.setLayoutParams(params);
			horizontalLinearLayout1.addView(view);
		}
		if(10%2!=0){
			View view = new View(this);
			view.setLayoutParams(new LayoutParams(0,LayoutParams.WRAP_CONTENT,1));
			horizontalLinearLayout1.addView(view);
		}
	}
}
