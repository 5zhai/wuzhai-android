package com.wuzhai.app.person.activity;

import java.util.ArrayList;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.wuzhai.app.R;
import com.wuzhai.app.application.WuZhaiApplication;
import com.wuzhai.app.main.WuzhaiService;
import com.wuzhai.app.main.WuzhaiServiceCallbackAdapter;
import com.wuzhai.app.main.widget.OnRecyclerViewItemClickListener;
import com.wuzhai.app.objects.Follower;
import com.wuzhai.app.person.widget.FollowFansAdapter;
import com.wuzhai.app.person.widget.FollowerFans;
import com.wuzhai.app.widget.TitleToolbarActivity;

public class FollowFansActivity extends TitleToolbarActivity implements OnRecyclerViewItemClickListener{

	private WuzhaiService service;
	private RecyclerView rvFeed_follow;
	private FollowFansAdapter adapter;
	private ArrayList<FollowerFans> followerList;
	private boolean isFollow;
	//根据传来的type从服务器中获取相应的数据，传给adapter显示
	private String type;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTitle("关注");
		setContentView(R.layout.acitvity_follow);
		service = ((WuZhaiApplication)getApplication()).getService();
		initView();
	}

	@Override
	protected void onResume() {
		super.onResume();
		service.setCallBack(callbackAdapter);
		if (type.equals("follow")) {
			setTitle("关注");
			isFollow = true;
			service.getFollowerFansList(0);
		}else if(type.equals("fans")){
			setTitle("粉丝");
			isFollow = false;
			service.getFollowerFansList(1);
		}
	}
	private void initView(){
		rvFeed_follow = (RecyclerView)findViewById(R.id.rvFeed_follow);
		type = getIntent().getStringExtra("type");
		adapter = new FollowFansAdapter(FollowFansActivity.this, null,isFollow);
		adapter.setOnRecyclerViewItemClickListener(this);
	}

	private WuzhaiServiceCallbackAdapter callbackAdapter = new WuzhaiServiceCallbackAdapter(){
		@Override
		public void onGetFollowersCompleted(ArrayList<FollowerFans> followersList) {
			if(followersList!=null){
				followerList = followersList;
				adapter.setList(followersList);
				rvFeed_follow.setAdapter(adapter);
				rvFeed_follow.setLayoutManager(new LinearLayoutManager(FollowFansActivity.this));
			}
		}

		@Override
		public void onCompleted(int result) {
			switch (result) {
			case WuzhaiService.FOLLOW_USER_SUCC:
				Toast.makeText(FollowFansActivity.this, "关注成功", Toast.LENGTH_SHORT).show();
				break;
			case WuzhaiService.CANCLE_FOLLOW_SUCC:
				Toast.makeText(FollowFansActivity.this, "取消成功", Toast.LENGTH_SHORT).show();
				break;
			default:
				break;
			}
		}
	};
	@Override
	public void onItemClick(View view, int position) {
		Button btn = (Button)view;
		if(btn.getText().toString().equals("取消关注")){
			service.cancleFollowUser(followerList.get(position).getId());
			if(isFollow){
				followerList.remove(position);
				adapter.setList(followerList);
			}else {
				btn.setText("加关注");
			}

		}else {
			service.followUser(followerList.get(position).getId());
			btn.setText("取消关注");
		}
	}
}
