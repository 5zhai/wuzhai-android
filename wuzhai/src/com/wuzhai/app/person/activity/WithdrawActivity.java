package com.wuzhai.app.person.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.wuzhai.app.R;
import com.wuzhai.app.widget.TitleToolbarActivity;

public class WithdrawActivity extends TitleToolbarActivity{

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_withdraw);
	}
}
