package com.wuzhai.app.person.activity;

import android.media.Image;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.wuzhai.app.R;
import com.wuzhai.app.R.id;
import com.wuzhai.app.application.WuZhaiApplication;
import com.wuzhai.app.main.WuzhaiService;
import com.wuzhai.app.tools.Utils;
import com.wuzhai.app.widget.TitleToolbarActivity;

public class RechargeActivity extends TitleToolbarActivity {

	private WuzhaiService service;
	private LinearLayout wechatPayment;
	private ImageView paysFlag_wechat;
	private LinearLayout alipayPayment;
	private ImageView paysFlag_alipay;
	private EditText rechargeMoney;
	private Button rechargeNew;
	
	private boolean isWxSelected = false;
	private boolean isAliSelected = false;
	
	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTitle("充值");
		setContentView(R.layout.activity_recharge);
		initView();
	}

	private void initView(){
		service = ((WuZhaiApplication)getApplication()).getService();
		wechatPayment = (LinearLayout)findViewById(R.id.wechat_payment);
		paysFlag_wechat = (ImageView)findViewById(R.id.pays_flag_wechat);
		alipayPayment = (LinearLayout)findViewById(R.id.alipay_payment);
		paysFlag_alipay = (ImageView)findViewById(R.id.pays_flag_alipay);
		rechargeMoney = (EditText)findViewById(R.id.recharge_money);
		rechargeNew = (Button)findViewById(R.id.recharge_new);
		
		wechatPayment.setOnClickListener(clickListener);
		alipayPayment.setOnClickListener(clickListener);
		rechargeNew.setOnClickListener(clickListener);
	}
	
	private OnClickListener clickListener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.wechat_payment:
				paysFlag_wechat.setImageResource(R.drawable.icon_pays_select);
				isWxSelected = true;
				paysFlag_alipay.setImageResource(R.drawable.icon_pays_normal);
				isAliSelected = false;
				break;
			case R.id.alipay_payment:
				paysFlag_wechat.setImageResource(R.drawable.icon_pays_normal);
				isWxSelected = false;
				paysFlag_alipay.setImageResource(R.drawable.icon_pays_select);
				isAliSelected = true;
				break;
			case R.id.recharge_new:
				String amount = rechargeMoney.getText().toString();
				if(!isAliSelected && !isWxSelected){Toast.makeText(RechargeActivity.this, "请选择支付方式", Toast.LENGTH_SHORT).show(); return;}
				if(TextUtils.isEmpty(amount)){Toast.makeText(RechargeActivity.this, "请输入金额", Toast.LENGTH_SHORT).show();return;}
				if(isWxSelected){
					getToWXRecharge(amount,Utils.getLocalHostIp());
				}
				break;
			default:
				break;
			}
		}
	};

	private void getToWXRecharge(String amount,String clientIP){
		if(TextUtils.isEmpty(clientIP)){
			Log.d("yue.huang", "获取ip出错");
			return;
		}
		service.gotoWxRecharge(amount, clientIP);
	}
}
