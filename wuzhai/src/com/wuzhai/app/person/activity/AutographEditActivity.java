package com.wuzhai.app.person.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;

import com.wuzhai.app.R;
import com.wuzhai.app.widget.TitleToolbarActivity;

public class AutographEditActivity extends TitleToolbarActivity {

	private EditText editText;
	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTitle("编辑签名");
		setTextMenuString("确定");
		setTextMenuClickListener(clickListener);
		setContentView(R.layout.activity_autograph);
		initView();
	}

	private void initView(){
		editText = (EditText)findViewById(R.id.editText);
		editText.setText(getIntent().getStringExtra("autograph"));
	}

	private OnClickListener clickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			Intent intent = new Intent();
			intent.putExtra("autograph", editText.getText().toString());
			setResult(RESULT_OK, intent);
			AutographEditActivity.this.finish();
		}
	};
}
