package com.wuzhai.app.person.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.webkit.WebView;

import com.wuzhai.app.R;
import com.wuzhai.app.application.WuZhaiApplication;
import com.wuzhai.app.main.WuzhaiService;
import com.wuzhai.app.main.WuzhaiServiceCallbackAdapter;
import com.wuzhai.app.widget.TitleToolbarActivity;

public class AboutWuZhaiActivity extends TitleToolbarActivity {

	private WuzhaiService service;
	private WebView webView;
	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTitle("关于吾宅");
		setContentView(R.layout.activity_about_wuzhai);
		service = ((WuZhaiApplication)getApplication()).getService();
		service.setCallBack(callback);
		service.getAboutUs();
		webView = (WebView)findViewById(R.id.webview);
	}

	private WuzhaiServiceCallbackAdapter callback = new WuzhaiServiceCallbackAdapter(){
		@Override
		public void onGetAboutUsDone(String result) {
			webView.loadDataWithBaseURL(null,result, "text/html", "UTF-8",null);
		}
	};
}
