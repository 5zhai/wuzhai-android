package com.wuzhai.app.person.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;

import com.wuzhai.app.R;
import com.wuzhai.app.tools.Utils;
import com.wuzhai.app.widget.PredicateLayout;
import com.wuzhai.app.widget.TitleToolbarActivity;

public class DailyAttendanceActivity extends TitleToolbarActivity {
	//考虑到每日签到可能就固定几个图标，所以不使用RecyclerView；使用动态向LinearLayout中添加
//	RecyclerView recyclerView;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTitle("每日签到");
		setTextMenuString("我的礼物");
		initView();
	}

	private void initView() {
//		recyclerView = (RecyclerView) findViewById(R.id.rvFeed_recycler);
		LinearLayout giftContainer = new LinearLayout(this);
		giftContainer.setOrientation(LinearLayout.VERTICAL);
		giftContainer.setBackgroundColor(0xffffffff);
		LinearLayout horizontalLinearLayout1 = null;

		for(int n = 0;n<11;n++){
			if(n%4==0){
				horizontalLinearLayout1 = new LinearLayout(this);
				horizontalLinearLayout1.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.WRAP_CONTENT));
				horizontalLinearLayout1.setOrientation(LinearLayout.HORIZONTAL);
				horizontalLinearLayout1.setPadding(8, 8, 8, 8);
				giftContainer.addView(horizontalLinearLayout1);
			}
			RelativeLayout view = (RelativeLayout)LayoutInflater.from(this).inflate(R.layout.daily_attendance_item, null);
			LayoutParams params = new LayoutParams(0,LayoutParams.WRAP_CONTENT,1);
			view.setLayoutParams(params);
			horizontalLinearLayout1.addView(view);
		}
		if(11%4!=0){
			for(int n = 0;n<(4-11%4);n++){
				View view = new View(this);
				view.setLayoutParams(new LayoutParams(0,1,1));
				horizontalLinearLayout1.addView(view);
			}
		}
		setContentView(giftContainer,new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
	}
}
