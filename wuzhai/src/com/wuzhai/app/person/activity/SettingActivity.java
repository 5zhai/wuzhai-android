package com.wuzhai.app.person.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RelativeLayout;

import com.wuzhai.app.R;
import com.wuzhai.app.widget.TitleToolbarActivity;

public class SettingActivity extends TitleToolbarActivity {
	private RelativeLayout dataSet;
	private RelativeLayout aboutUs;
	private RelativeLayout feedback;
	private RelativeLayout accountBinding;
	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTitle("设置");
		setContentView(R.layout.activity_setting);
		initView();
	}
	private void initView(){
		dataSet = (RelativeLayout)findViewById(R.id.data_set);
		dataSet.setOnClickListener(clickListener);
		aboutUs = (RelativeLayout)findViewById(R.id.about_wuzhai);
		aboutUs.setOnClickListener(clickListener);
		feedback = (RelativeLayout)findViewById(R.id.feedback);
		feedback.setOnClickListener(clickListener);
		accountBinding = (RelativeLayout)findViewById(R.id.account_binding);
		accountBinding.setOnClickListener(clickListener);
	}

	private OnClickListener clickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.data_set:
				startActivity(new Intent(SettingActivity.this, DataSettingActivity.class));
				break;

			case R.id.account_binding:
				startActivity(new Intent(SettingActivity.this, AccountBindingActivity.class));
				break;
			case R.id.about_wuzhai:
				startActivity(new Intent(SettingActivity.this, AboutWuZhaiActivity.class));
				break;
			case R.id.feedback:
				startActivity(new Intent(SettingActivity.this, SuggestionFeedbackActivity.class));
				break;
			}
		}
	};
}
