package com.wuzhai.app.person.activity;

import java.util.ArrayList;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;

import com.viewpagerindicator.TabPageIndicator;
import com.wuzhai.app.R;
import com.wuzhai.app.application.WuZhaiApplication;
import com.wuzhai.app.main.WuzhaiService;
import com.wuzhai.app.main.WuzhaiServiceCallbackAdapter;
import com.wuzhai.app.main.video.widget.DanceListFragment;
import com.wuzhai.app.main.widget.ViewPagerAdapter;
import com.wuzhai.app.objects.Order;
import com.wuzhai.app.person.fragments.AllOrderFragment;
import com.wuzhai.app.widget.TitleToolbarActivity;

public class MyOrderActivity extends TitleToolbarActivity {

	private WuzhaiService service;
	private ViewPager viewPager;
	private TabPageIndicator indicator;
	private ArrayList<Fragment> pageList = new ArrayList<Fragment>();
	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		service = ((WuZhaiApplication)getApplication()).getService();
		service.setCallBack(callbackAdapter);
		setTitle("我买到的");
		setContentView(R.layout.activity_my_order);
		initView();
	}

	private void initView(){
		viewPager = (ViewPager)findViewById(R.id.viewpager);
		indicator = (TabPageIndicator)findViewById(R.id.indicator);
		initPageList();
		ViewPagerAdapter vpAdapter= new ViewPagerAdapter(getSupportFragmentManager(), pageList);
		vpAdapter.setPageTitle(new String[]{"全部订单","待付款","待发货","待收货"});
		viewPager.setAdapter(vpAdapter);
		viewPager.addOnPageChangeListener(pageChangeListener);
		indicator.setViewPager(viewPager);
	}

	private void initPageList(){
		//因为订单页的四个page布局都一样，只是内容不一样，所以在activity中使用了三个
		//AllOrderFragment对象，用flag区分向服务器请求的数据分类
		AllOrderFragment allOrderFragment = new AllOrderFragment();
//		allOrderFragment.setFlag("all");
		AllOrderFragment waitPaymentFragment = new AllOrderFragment();
//		waitPaymentFragment.setFlag("payment");
		AllOrderFragment waitDeliveryFragment = new AllOrderFragment();
//		waitDeliveryFragment.setFlag("new");
		AllOrderFragment waitReceiptFragment = new AllOrderFragment();
//		waitReceiptFragment.setFlag("new");

		pageList.add(allOrderFragment);
		pageList.add(waitPaymentFragment);
		pageList.add(waitDeliveryFragment);
		pageList.add(waitReceiptFragment);
	}

	private OnPageChangeListener pageChangeListener = new OnPageChangeListener() {

		@Override
		public void onPageSelected(int arg0) {
			Log.d("yue.huang", "onPageSelected:"+arg0);
			switch (arg0) {
			case 0:
				Log.d("yue.huang", "getAllOrder");
				service.getOrders("");
				break;
			case 1:
				Log.d("yue.huang", "dai zhi fu");
				service.getOrders(Order.PEND_BUYER_PAY);
				break;
			default:
				break;
			}
		}

		@Override
		public void onPageScrolled(int arg0, float arg1, int arg2) {
			// TODO Auto-generated method stub
			Log.d("yue.huang", "onPageScrolled:"+arg0);
		}

		@Override
		public void onPageScrollStateChanged(int arg0) {
			// TODO Auto-generated method stub
			Log.d("yue.huang", "onPageScrollStateChanged:"+arg0);
		}
	};

	private WuzhaiServiceCallbackAdapter callbackAdapter  = new WuzhaiServiceCallbackAdapter(){

		@Override
		public void onGetOrderListCompleted(ArrayList<Order> order_list) {
			Log.d("yue.huang", "order_list:"+order_list.size());
			if(order_list!=null && order_list.size() != 0){
				switch (viewPager.getCurrentItem()) {
				case 0:
					((AllOrderFragment)pageList.get(0)).initView(order_list);
					break;
				case 1:
					((AllOrderFragment)pageList.get(1)).initView(order_list);
					break;
				default:
					break;
				}
			}
		}
	};
}
