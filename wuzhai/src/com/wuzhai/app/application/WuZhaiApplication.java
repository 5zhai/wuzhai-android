package com.wuzhai.app.application;

import io.rong.imkit.RongIM;
import io.rong.imlib.RongIMClient;
import io.rong.notification.PushNotificationMessage;

import com.wuzhai.app.main.WuzhaiService;
import com.wuzhai.app.main.WuzhaiService.WuzhaiBind;
import com.wuzhai.app.objects.User;

import android.app.ActivityManager;
import android.app.Application;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.WindowManager;

public class WuZhaiApplication extends Application {

	private int screenWidth;
	private int screenHeight;
	private User user;
	private WuzhaiService wuzhaiService;
	private ServiceConnection connection;
	private String currentCity;
	//微信支付相关
	private String wxAppId;
	private String wxKey;
	private String wxMchId;

	@Override
	public void onCreate() {
		super.onCreate();
		if(getApplicationInfo().packageName.equals(getCurProcessName(getApplicationContext()))){
			bindLoginService();
		}
		getScreenSize();
		user = new User();
		user.setAccessKey("a3a1da32e4065f189715da13f270421c");
		user.setImToken("yhsJ0Pxo18M2CnWDomLTp6qEChM9NshECOAjqSruhNbmDL+fynUnBjkNFHvxAKLH2wGbnNvA5Yi53ieQ6zXaa62ioRj5gsAvtB77JmgiL7UDa8lEqEcy4iYpbPZyECQJQwn61+z7v1E=");

        if (getApplicationInfo().packageName.equals(getCurProcessName(getApplicationContext())) ||
                "io.rong.push".equals(getCurProcessName(getApplicationContext()))) {
            RongIM.init(this);
        }
	}

	private void bindLoginService(){
		Intent intent = new Intent(this,WuzhaiService.class);
		connection = new ServiceConnection() {
			@Override
			public void onServiceDisconnected(ComponentName name) {
				// TODO Auto-generated method stub
			}
			@Override
			public void onServiceConnected(ComponentName name, IBinder service) {
				wuzhaiService = ((WuzhaiBind)service).getService();
			}
		};
		bindService(intent, connection, BIND_AUTO_CREATE);
	}

	private void getScreenSize() {
		WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
		DisplayMetrics dm = new DisplayMetrics();
		wm.getDefaultDisplay().getMetrics(dm);
		screenWidth = dm.widthPixels;
		screenHeight = dm.heightPixels;
	}

	public User getUser() {
		return user;
	}

	public int getScreenWidth() {
		return screenWidth;
	}

	public int getScreenHeight() {
		return screenHeight;
	}

	public WuzhaiService getService(){
		return wuzhaiService;
	}

    public static String getCurProcessName(Context context) {

        int pid = android.os.Process.myPid();

        ActivityManager activityManager = (ActivityManager) context
                .getSystemService(Context.ACTIVITY_SERVICE);

        for (ActivityManager.RunningAppProcessInfo appProcess : activityManager
                .getRunningAppProcesses()) {

            if (appProcess.pid == pid) {
                return appProcess.processName;
            }
        }
        return null;
    }

	public String getCurrentCity() {
		return currentCity;
	}

	public void setCurrentCity(String currentCity) {
		this.currentCity = currentCity;
	}

	public String getWxAppId() {
		return wxAppId;
	}

	public void setWxAppId(String wxAppId) {
		this.wxAppId = wxAppId;
	}

	public String getWxKey() {
		return wxKey;
	}

	public void setWxKey(String wxKey) {
		this.wxKey = wxKey;
	}

	public String getWxMchId() {
		return wxMchId;
	}

	public void setWxMchId(String wxMchId) {
		this.wxMchId = wxMchId;
	}
}
